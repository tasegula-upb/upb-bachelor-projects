"""
	This module represents a cluster's computational node.

	Computer Systems Architecture Course
	Assignment 1 - Cluster Activity Simulation
	March 2014
"""

from Queue import Queue
from threading import *
from my_utils import *
from time import sleep
from random import randint
from operator import add, sub, mul


class Node:
	"""
		Class that represents a cluster node with computation and storage
		functionalities.
	"""

	barrier = None 	# One barrier used by every node

	def __init__(self, node_id, matrix_size):
		"""
			Constructor.

			@type node_id: Integer
			@param node_id: an integer less than 'matrix_size' uniquely
				identifying the node
			@type matrix_size: Integer
			@param matrix_size: the size of the matrix A
		"""
		self.node_id = node_id
		self.matrix_size = matrix_size
		self.datastore = None
		self.nodes = None
		# TODO other code
		self.x = Queue(1)		# The result\

		self.queue = None 		# Used to transfer lines node-to-node
		self.reqs = None 		# Used to send requests

		# Assures number of threads working in the datastore at one time
		self.putSem = Semaphore(0)

		self.ds_threads = [] 	# The datastore threads
		self.me = None 			# The principal thread

		if (self.node_id == 0):
			Node.barrier = ReusableBarrierSem(self.matrix_size)


	def __str__(self):
		"""
			Pretty prints this node.

			@rtype: String
			@return: a string containing this node's id
		"""
		return "Node %d" % self.node_id


	def getX(self):
		"""
			Puts the result in the result Queue
		"""
		A = self.datastore.get_A(self, self.node_id)
		b = self.datastore.get_b(self)

		self.x.put(b / A)


	def put_line(self, row, pivot):
		"""
			Puts the given line in the datastore,
				replacing the old one

			@type row: List
			@param row: the list to be put in the datastore
			@type pivot: Integer
			@param pivot: from which element of the given list 
				to start replacing
		"""
		for i in xrange(pivot, self.matrix_size, 1):
			self.datastore.put_A(self, i, row[i])

		self.datastore.put_b(self, row[self.matrix_size])


	def get_line(self, pivot):
		"""
			Puts the given line in the datastore,
				replacing the old one

			@type pivot: Integer
			@param pivot: from which column to start getting 
				elements from the datastore
		"""
		row = [0] * (self.matrix_size + 1)
		for i in xrange(pivot, self.matrix_size, 1):
			row[i] = self.datastore.get_A(self, i)

		row[self.matrix_size] = self.datastore.get_b(self)

		self.queue.put(row)


	def swap(self, my_row, pivot):
		j = pivot + 1
		while (my_row[self.node_id] == 0 and j < self.matrix_size):
			self.nodes[j].reqs.put(Request(0, self.nodes[j], pivot))
			new_row = self.nodes[j].queue.get()
			self.nodes[j].queue.task_done()
			
			if (new_row[self.node_id] != 0):
				self.reqs.put(Request(1, self, pivot, new_row))
				self.putSem.acquire()

				self.nodes[j].reqs.put(Request(1, self.nodes[j], pivot, my_row))
				self.nodes[j].putSem.acquire()
				break
				
			my_row = new_row
			j += 1


	def nodeConn(self):
		"""
			Function that computes the Gauss Elimination Algorithm
		"""
		if (self.node_id == 0):
			self.reqs.put(Request(0, self, 0))
			row = self.queue.get()
			self.queue.task_done()
			if (row[0] == 0):
				self.swap(row, 0);

		for pivot in xrange(self.matrix_size):
			Node.barrier.wait()

			if (pivot != self.node_id):
				self.reqs.put(Request(0, self, pivot))
				my_row = self.queue.get()
				self.queue.task_done()

				if (my_row[pivot] != 0):	
					self.nodes[pivot].reqs.put(Request(0, self.nodes[pivot], pivot))
					row = self.nodes[pivot].queue.get()
					self.nodes[pivot].queue.task_done()

					factor = my_row[pivot] / row[pivot]
					if (factor != 0):
						my_row = map(sub, my_row, [x * factor for x in row])
						self.reqs.put(Request(1, self, pivot, my_row))
						self.putSem.acquire()

				Node.barrier.wait() #necesar

				if (self.node_id == pivot + 1):
					self.swap(my_row, pivot)
			else:
				Node.barrier.wait() #necesar

		self.reqs.put(Request(2, self, self.node_id))


	def dataConn(self):
		"""
			Datastore thread function
		"""
		while (True):
			req = self.reqs.get()
			self.reqs.task_done()

			if (req.type == 3):
				break

			if (req.type == 0):
				req.node.get_line(req.pivot)
			elif (req.type == 1):
				self.put_line(req.item, req.pivot)
				self.putSem.release()
			elif (req.type == 2):
				for i in xrange(len(self.ds_threads) - 1):
					self.reqs.put(Request(3, self, i))
				self.getX()
				break


	def set_datastore(self, datastore):
		"""
			Gives the node a reference to its datastore. Guaranteed to be called
			before the first call to 'get_x'.

			@type datastore: Datastore
			@param datastore: the datastore associated with this node
		"""
		self.datastore = datastore
		

	def set_nodes(self, nodes):
		"""
			Informs the current node of the other nodes in the cluster. 
			Guaranteed to be called before the first call to 'get_x'.

			@type nodes: List of Node
			@param nodes: a list containing all the nodes in the cluster
		"""
		self.nodes = nodes
		# TODO other code
		data_max = self.datastore.get_max_pending_requests()

		if (data_max == 0):
			data_max = self.matrix_size * 2

		self.queue = Queue(data_max)
		self.reqs = Queue(data_max)

		for i in xrange(data_max):
			thread = Thread(target = self.dataConn, args = (), name = "[{}]-{}".format(self, i))
			self.datastore.register_thread(self, thread)
			self.ds_threads.append(thread)
			thread.start();

		self.me = Thread(target = self.nodeConn, args = (), name = "[{}]-Master".format(self))
		self.datastore.register_thread(self, self.me)
		self.me.start()


	def get_x(self):
		"""
			Computes the x value corresponding to this node. This method is
			invoked by the tester. This method must block until the result is
			available.

			@rtype: (Float, Integer)
			@return: the x value and the index of this variable in the solution
				vector
		"""
		self.me.join()

		res = self.x.get()
		self.x.task_done()
		return (res, self.node_id)


	def shutdown(self):
		"""
			Instructs the node to shutdown (terminate all threads). This method
			is invoked by the tester. This method must block until all the
			threads started by this node terminate.
		"""
		for i in xrange(len(self.ds_threads)):
			self.ds_threads[i].join()
