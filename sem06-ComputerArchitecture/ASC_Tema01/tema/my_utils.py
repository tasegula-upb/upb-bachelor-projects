import sys
import threading
from threading import *


class ReusableBarrierCond():
	""" Bariera reentranta, implementata folosind o variabila conditie """

	def __init__(self, num_threads):
		self.num_threads = num_threads
		self.count_threads = self.num_threads
		self.cond = Condition(Lock())

	def wait(self):
		self.cond.acquire()       # intra in regiunea critica
		self.count_threads -= 1;  
		if self.count_threads == 0:
			self.cond.notify_all() # trezeste toate thread-urile, acestea vor putea reintra  in regiunea critica dupa release
			self.count_threads=self.num_threads  
		else:          
			self.cond.wait();    # iese din regiunea critica, se blocheaza, cand se deblocheaza face acquire pe lock
		self.cond.release();     # iesim din regiunea critica

class ReusableBarrierSem():
	""" Bariera reentranta, implementata folosind semafoare """
	
	def __init__(self, num_threads):
		self.num_threads = num_threads
		self.count_threads1 = self.num_threads
		self.count_threads2 = self.num_threads
		
		self.counter_lock = Lock()       # protejam decrementarea numarului de threaduri
		self.threads_sem1 = Semaphore(0) # contorizam numarul de threaduri pentru prima etapa
		self.threads_sem2 = Semaphore(0) # contorizam numarul de threaduri pentru a doua etapa

	def wait(self):
		self.phase1()
		self.phase2()

	def phase1(self):
		with self.counter_lock:
			self.count_threads1 -= 1
			if self.count_threads1 == 0:
				for i in range(self.num_threads):
					self.threads_sem1.release()
			self.count_threads2 = self.num_threads
		 
		self.threads_sem1.acquire()
		 
	def phase2(self):
		with self.counter_lock:
			self.count_threads2 -= 1
			if self.count_threads2 == 0:
				for i in range(self.num_threads):
					self.threads_sem2.release()
			self.count_threads1 = self.num_threads
		 
		self.threads_sem2.acquire()

class Request():
	""" Class that represent a request data type."""

	def __init__(self, op_type, node, pivot, item = []):
		"""
			Constructor.

			@type op_type: Integer
			@param op_type: 0 for get_data, 
							1 for put_data,
							2 for get_x
			@type node: Node
			@param node: the node upon which the request is made
			@type item: List
			@param item: the list to be put in the Node datastore;
				it will only be called by the Node who owns the datastore
			@type pivot: Integer
			@param pivot: the position from which to start working with 
				the datastore
		"""
		self.type = op_type
		self.node = node
		self.item = item
		self.pivot = pivot


	def __str__(self):
		""" 
			Prints the Request type 

			@rtype: String			
			@return: a string of the following format: 
			[operation] [node] @ [pivot] <item>

		"""

		if self.type == 0:
			t = "get"
			i = ""
		elif self.type == 1:
			t = "put"
			i = ": {}".format(self.item)
		elif self.type == 2:
			t = "done"
			i = ""
		else:
			return "break"

		ret = "{} th-{} @ {}{}".format(t, self.node.node_id, self.pivot, i)
		return ret