"""
	This module represents a cluster's computational node.

	Computer Systems Architecture Course
	Assignment 1 - Cluster Activity Simulation
	March 2014
"""

from Queue import Queue
from threading import *
from barrier import *
from time import sleep
from random import randint
from operator import add, sub, mul


class Node:
	"""
		Class that represents a cluster node with computation and storage
		functionalities.
	"""

	barrier = None
	MAX = 1024

	def __init__(self, node_id, matrix_size):
		"""
			Constructor.

			@type node_id: Integer
			@param node_id: an integer less than 'matrix_size' uniquely
				identifying the node
			@type matrix_size: Integer
			@param matrix_size: the size of the matrix A
		"""
		self.node_id = node_id
		self.matrix_size = matrix_size
		self.datastore = None
		self.nodes = None
		# TODO other code
		self.A = None;
		self.b = None;
		self.x = None;

		self.queue = None;
		self.sem = None;

		self.me = Thread(target = self.resolve, args = ())

		if (self.node_id == 0):
			Node.barrier = ReusableBarrierSem(self.matrix_size)


	def __str__(self):
		"""
			Pretty prints this node.

			@rtype: String
			@return: a string containing this node's id
		"""
		return "Node %d" % self.node_id


	def getX(self):
		self.sem.acquire();
		self.A = self.datastore.get_A(self, self.node_id)
		self.sem.release()

		self.sem.acquire();
		self.b = self.datastore.get_b(self)
		self.sem.release()

		self.x = self.b / self.A;


	def put_data(self, row, pivot):
		thread = Thread(target = self.put_line,	args = (row, pivot))
		self.datastore.register_thread(self, thread)

		thread.start()
		thread.join()


	def put_line(self, row, pivot):
		for i in xrange(pivot, self.matrix_size, 1):
			self.sem.acquire();
			self.datastore.put_A(self, i, row[i])
			self.sem.release();

		self.sem.acquire();
		self.datastore.put_b(self, row[self.matrix_size])		
		self.sem.release();


	def get_line(self, n, pivot):
		row = [0] * (self.matrix_size + 1);
		for i in xrange(pivot, self.matrix_size, 1):
			self.sem.acquire();
			row[i] = self.datastore.get_A(self, i)
			self.sem.release();

		self.sem.acquire();
		row[self.matrix_size] = self.datastore.get_b(self)
		self.sem.release();

		self.queue.put(row)


	def get_data(self, node, pivot):
		thread = Thread(target = node.get_line,	args = (self.node_id, pivot))
		node.datastore.register_thread(node, thread)

		thread.start()
		thread.join()

		row = node.queue.get()
		node.queue.task_done()

		return row


	def resolve(self):
		row = None

		for pivot in xrange(self.matrix_size):
			Node.barrier.wait()

			if (pivot != self.node_id):				
				row = self.get_data(self.nodes[pivot], pivot)

				if (row[pivot] != 0):
					my_row = self.get_data(self, pivot)

					factor = my_row[pivot] / row[pivot]
					if (factor != 0):
						my_row = map(sub, my_row, [x * factor for x in row])
						self.put_data(my_row, pivot)

				Node.barrier.wait()

				if (self.node_id == pivot + 1):
					j = pivot + 1
					while (my_row[self.node_id] == 0 and j < self.matrix_size):
						new_row = self.nodes[j].get_data(self.nodes[j], pivot)
						
						if (new_row[self.node_id] != 0):
							self.put_data(new_row, pivot)
							self.nodes[j].put_data(my_row, pivot)
							break
							
						my_row = new_row
						j += 1;
			else:
				Node.barrier.wait()


		thread = Thread(target = self.getX, args = ());
		self.datastore.register_thread(self, thread);
		thread.start();
		thread.join()


	def set_datastore(self, datastore):
		"""
			Gives the node a reference to its datastore. Guaranteed to be called
			before the first call to 'get_x'.

			@type datastore: Datastore
			@param datastore: the datastore associated with this node
		"""
		self.datastore = datastore

		# TODO other code
		self.datastore.register_thread(self, self.me)
		data_max = self.datastore.get_max_pending_requests();

		if (data_max == 0):
			self.sem = Semaphore(Node.MAX);
			self.queue = Queue();
		else:
			self.sem = Semaphore(data_max);
			self.queue = Queue(data_max);


	def set_nodes(self, nodes):
		"""
			Informs the current node of the other nodes in the cluster. 
			Guaranteed to be called before the first call to 'get_x'.

			@type nodes: List of Node
			@param nodes: a list containing all the nodes in the cluster
		"""
		self.nodes = nodes
		# TODO other code
		#print "[{0}] Starting\n".format(self.node_id)
		self.me.start()


	def get_x(self):
		"""
			Computes the x value corresponding to this node. This method is
			invoked by the tester. This method must block until the result is
			available.

			@rtype: (Float, Integer)
			@return: the x value and the index of this variable in the solution
				vector
		"""
		# TODO other code

		self.me.join()
		return (self.x, self.node_id)


	def shutdown(self):
		"""
			Instructs the node to shutdown (terminate all threads). This method
			is invoked by the tester. This method must block until all the
			threads started by this node terminate.
		"""
		pass
		# TODO other code
