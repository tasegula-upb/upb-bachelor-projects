/*
 * Computer System Architecture - Lab 6
 * SPU code 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <spu_intrinsics.h>
#include <spu_mfcio.h>
#include <math.h>
#include <libmisc.h>

#include "../common.h"
#include "btc.h"

#define waitag(t) mfc_write_tag_mask(1<<t); mfc_read_tag_status_all();

//	Print an array of vector float variables with the given length.
void print_vector_float(vector float *v, int length);
void print_vector_uint(vector unsigned int *v, int length);
void print_vector_short(FILE *f, vector signed short *v, int length);
void print8(FILE* f, short int *v);
void print_8line(FILE* file, short int *crop, int width);

int spu_id;

int main(unsigned long long speid,
		 unsigned long long argp,
		 unsigned long long envp)
{
	int i, j, k, l;		// for using with loops
	pointers_t p __attribute__ ((aligned(16)));

	uint32_t tag_id;		// tags

	tag_id = mfc_tag_reserve();
	if (tag_id == MFC_TAG_INVALID){
		printf("SPU: ERROR can't allocate tag ID\n"); return -1;
	}

	/* transferul initial, cu structura de pointeri */
	mfc_get((void*)&p, (uint32_t)argp, (int)envp, tag_id, 0, 0);
	waitag(tag_id);
	spu_id = p.index;

	int transfer_dim = sizeof(short int) * p.width;	// size of a transfer
	int block_dim = BLOCK_SIZE * BLOCK_SIZE;		// size of an entire block

	// computing helpers
	short int crop[BLOCK_SIZE * p.width] __attribute__ ((aligned(16)));
	short int pixel_block[block_dim] __attribute__ ((aligned(16)));

	vector signed short *v_crop;	// contains a block (8 lines)
	vector signed short v_sum;		// sum of all lines

	// used at compression
	int sum = 0;
	float avg = 0.0f, 
		  dev = 0.0f, 
		  q = 0.0f;

	// to transfer
	block block __attribute__ ((aligned(16)));
	short int decomp[block_dim] __attribute__ ((aligned(16)));

	int block_jump = p.width / BLOCK_SIZE;
	int block_address = spu_id * block_jump;
	int a, b;

	// // BLOCK_SIZE line
	for (i = spu_id * BLOCK_SIZE; i < p.height; i += (BLOCK_SIZE * p.spus)) {

		for (k = i; k < i + BLOCK_SIZE; k++) {
			// is under 16K
			mfc_getb((void*)(&crop[(k - i) * p.width]), 
					 (uint32_t)(p.pixels + (k * p.width)), 
					 transfer_dim, 
					 tag_id, 0, 0);
			waitag(tag_id);
		}

		// processing each block
		for (j = 0; j < p.width; j += BLOCK_SIZE) {
			// getting the block
			for (k = 0; k < BLOCK_SIZE; k++) {
				memcpy(&pixel_block[k * BLOCK_SIZE], 
					   &crop[(k * p.width) + j],
					   BLOCK_SIZE * sizeof(short int));
			}

			// find average
			v_crop = (vector signed short*)(pixel_block);
			sum = 0;
			avg = 0.0f;
			for (k = 0; k < BLOCK_SIZE; k++) {
				v_sum = v_sum + v_crop[k];
			}
			for (k = 0; k < BLOCK_SIZE; k++) {
				sum += v_sum[k];
				avg += v_sum[k];
				v_sum[k] = 0;
			}
			avg /= block_dim;

			// find standard deviation - non-vectorial operation
			// doing it vectorial doesn't make sense given the structure
			for (k = 0; k < BLOCK_SIZE; k++) {
				for (l = 0; l < BLOCK_SIZE; l++) {
					dev += ((v_crop[k][l] - avg) * (v_crop[k][l] - avg));
				}
			}
			dev /= block_dim;
			dev = sqrt(dev);

			q = 0;
			for (k = 0; k < block_dim; k++) {
				block.bitplane[k] = (pixel_block[k] > avg ? 1 : 0);
				q += block.bitplane[k];
			}

			// compute a and b
			if (q == 0) {
				a = b = (unsigned char)avg;
			}
			else {
				a = (unsigned char)(avg - dev * sqrt(q / (block_dim - q)));
				b = (unsigned char)(avg + dev * sqrt((block_dim - q) / q));
			}

			// avoid error
			block.a = (a >= 0)  ? (unsigned char)a : (unsigned char)0;
			block.b = (b < 255) ? (unsigned char)b : (unsigned char)255;

			for (k = 0; k < block_dim; k++) {
				decomp[k] = (block.bitplane[k] == 0 ? a : b);
			}

			mfc_putb((void*)(&block), 
					 (uint32_t)(p.blocks + block_address), 
					 sizeof(block),
					 tag_id, 0, 0);
			waitag(tag_id);
			block_address++; // we took a BLOCK_SIZE line

			for (k = i; k < i + BLOCK_SIZE; k++) {
				mfc_putb((void*)(&decomp[(k - i) * BLOCK_SIZE]), 
						 (uint32_t)(p.decomp + (k * p.width) + j), 
						 BLOCK_SIZE * sizeof(short int), 
						 tag_id, 0, 0);
				waitag(tag_id);
			}
			fflush(NULL);
		}

		block_address += (p.spus - 1) * block_jump;
	}

return 0;
}

void print_vector_float(vector float *v, int length) 
{
		int i;
		for (i = 0; i < length; i += 1)
			printf("%.2lf %.2lf %.2lf %.2lf \n", v[i][0], v[i][1], v[i][2], v[i][3]);
		printf("\n");
}

void print_vector_uint(vector unsigned int *v, int length) 
{
		int i;
		for (i = 0; i < length; i += 1)
			printf("%u %u %u %u \n", v[i][0], v[i][1], v[i][2], v[i][3]);
		printf("\n");
}

void print_vector_short(FILE *f, vector signed short *v, int length) 
{
	int i, j;
		for (i = 0; i < length; i += 1) {
			for (j = 0; j < 8; j++) {
				fprintf(f, "%d ", v[i][j]);
			}
			fprintf(f, "\n");
		}
		fprintf(f, "\n");
}

void print8(FILE* f, short int *v)
{
	int i;
	fprintf(f, "[%d] ", spu_id);
	for (i = 0; i < BLOCK_SIZE; i++) {
		fprintf(f, "%3d ", v[i]);
	}
	fprintf(f, "\n");
}

void print_8line(FILE* file, short int *crop, int width)
{
	int k, l;
	for (k = 0; k < BLOCK_SIZE; k++) {
		for (l = 0; l < width; l++) {
			fprintf(file, "%3d ", crop[(k * width) + l]);
		}
		fprintf(file, "\n");
	}
}
