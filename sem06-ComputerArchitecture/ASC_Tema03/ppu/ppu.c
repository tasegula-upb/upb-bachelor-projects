#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <errno.h>
#include <libspe2.h>
#include <pthread.h>
#include <sys/time.h>
#include <libmisc.h>

#include "../common.h"
#include "btc.h"

extern spe_program_handle_t spu;

#define MAX_SPU_THREADS 16

int num_blocks, num_pixels;
int width, height;

void print_image(FILE* file, img image);
void print_block(block* blocks);

void *ppu_pthread_function(void *thread_arg) 
{
	spe_context_ptr_t ctx;
	pointers_t *arg = (pointers_t *)thread_arg;

	/* Create SPE context */
	if ((ctx = spe_context_create (0, NULL)) == NULL) {
		perror ("Failed creating context");
		exit (1);
	}

	/* Load SPE program into context */
	if (spe_program_load (ctx, &spu)) {
		perror ("Failed loading program");
		exit (1);
	}

	/* Run SPE context */
	unsigned int entry = SPE_DEFAULT_ENTRY;
	if (spe_context_run(ctx, &entry, 0, arg, (void*)sizeof(pointers_t), NULL) < 0) { 
		perror ("Failed running context");
		exit (1);
	}

	/* Destroy context */
	if (spe_context_destroy (ctx) != 0) {
		perror("Failed destroying context");
		exit (1);
	}

	pthread_exit(NULL);
}

int main(int argc, char* argv[])
{
	if (argc != 6) {
		fprintf(stderr, "Usage: ./tema3 mod num_spus in.pgm out.btc out.pgm\n");
		exit(1);
	}

	int i;
	int spu_threads = 0;

	// Determine the number of SPE threads to create.
	spu_threads = atoi(argv[2]);
	if (spu_threads > MAX_SPU_THREADS) {
		printf("Too many SPUs ... setting as %d\n", MAX_SPU_THREADS);
		spu_threads = MAX_SPU_THREADS;
	}

	struct img image_original, image_decomp;
	struct c_img compressed;

	pthread_t threads[spu_threads];
	pointers_t pointers[spu_threads] __attribute__ ((aligned(16)));

	struct timeval t_start, t_end,
				   t_comps, t_compe;

	double scale_time = 0,
		   total_time = 0;

	gettimeofday(&t_start, NULL);
	read_pgm(argv[3], &image_original);

	//print_image(image_original);
	width = image_original.width;
	height = image_original.height;
	num_pixels = width * height;
	num_blocks = num_pixels / (BLOCK_SIZE * BLOCK_SIZE);

	// initialize the compressed image
	compressed.width = width;
	compressed.height = height;
	compressed.blocks = _alloc(num_blocks * sizeof(block));

	// initialize the decompressed image
	image_decomp.width = width;
	image_decomp.height = height;
	image_decomp.pixels = _alloc(num_pixels * sizeof(short int));

	for (i = 0; i < spu_threads; i++) {
		pointers[i].index = i;
		pointers[i].spus = spu_threads;

		pointers[i].width = width;
		pointers[i].height = height;
		pointers[i].pixels = image_original.pixels;
		pointers[i].decomp = image_decomp.pixels;
		pointers[i].blocks = compressed.blocks;		// aligned blocks

		// Create thread for each SPE context
		if (pthread_create (&threads[i], NULL, &ppu_pthread_function, &pointers[i])) {
			perror ("Failed creating thread");
			exit (1);
		}
	}

	gettimeofday(&t_comps, NULL);

	// Wait for SPU-thread to complete execution.
	for (i = 0; i < spu_threads; i++) {
		if (pthread_join (threads[i], NULL)) {
			perror("Failed pthread_join");
			exit (1);
		}
	}

	gettimeofday(&t_compe, NULL);

	// create the btc file and the new image
	write_btc(argv[4], &compressed);
	write_pgm(argv[5], &image_decomp);

	// free memory
	free_pgm(&image_original);
	free_pgm(&image_decomp);
	free_btc(&compressed);

	gettimeofday(&t_end, NULL);

	total_time += GET_TIME_DELTA(t_start, t_end);
	scale_time += GET_TIME_DELTA(t_comps, t_compe);

	printf("Encoding / Decoding time: %lf\n", scale_time);
	printf("Total time: %lf\n", total_time);

return 0;
}

void print_block(block* blocks) {
	int i, j, k;
	for (i = 0; i < num_blocks; i++) {
		printf("[%d] a(%d) \t b(%d)\n", i, blocks[i].a, blocks[i].b);
		printf("--------------\n");

		for (j = 0; j < 8; j++) {
			for (k = 0; k < 8; k++) {
				printf("%d ", blocks[i].bitplane[j * 8 + k]);
			}
			printf("\n");
		}
		printf("\n\n");
	}
}

void print_image(FILE* file, img image) {
	int i, j;

	for (i = 0; i < image.height; i++) {

		if (i % BLOCK_SIZE == 0)
			fprintf(file, "\n");

		for (j = 0; j < image.width; j++) {

			if (j % BLOCK_SIZE == 0)
				fprintf(file, "\t");

			fprintf(file, "%3d ", image.pixels[i * image.width + j]);
		}

		fprintf(file, "\n");
	}
}
