#! /bin/bash

spus=(1 2 4 8)
input=(in1 in2 in3)

output=(out1 out2 out3)
my_output=(out_1 out_2 out_3)

rm compare_*
touch compare_btc
touch compare_pgm

for i in 0 1 2; do
	for N in "${spus[@]}"; do
		echo "-------------------- Compare: ${input[$i]} - ${N} SPUs --------------------"
		./serial/compare btc ./btc_output/${output[$i]}.btc ./results/${my_output[$i]}_${N}.btc >> compare_btc
		echo
		./serial/compare pgm ./btc_output/${output[$i]}.pgm ./results/${my_output[$i]}_${N}.pgm >> compare_pgm
		echo "-------------------- Done     ${input[$i]} - ${N} SPUs ---------------------"
		done
	done