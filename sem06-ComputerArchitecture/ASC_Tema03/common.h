/*
 *	Aligned versions of the structures in btc.h
 */

#define BUF_SIZE 		256
#define BLOCK_SIZE 		8
#define BITS_IN_BYTE	8

typedef struct a_img {
	// regular image
	int width, height;

	short int* pixels;
} __attribute__ ((aligned(16))) a_img;


typedef struct a_block {
	// data for a block from the compressed image
	unsigned char a, b;
	unsigned char bitplane[BLOCK_SIZE * BLOCK_SIZE];
	// one byte for each bit in the bitplane
	// quite memory inefficient, but let's keep it simple
} __attribute__ ((aligned(16))) a_block;

typedef struct a_cimg {
	// compressed image
	int width, height;

	struct a_block* blocks;
} __attribute__ ((aligned(16))) a_cimg;


typedef struct a_bits {
	unsigned bit0 : 1;
	unsigned bit1 : 1;
	unsigned bit2 : 1;
	unsigned bit3 : 1;
	unsigned bit4 : 1;
	unsigned bit5 : 1;
	unsigned bit6 : 1;
	unsigned bit7 : 1;
} __attribute__ ((aligned(16))) a_bits;


// pointers structure
typedef struct a_pointers_t {
	int index;			// index of the SPU
	int spus;			// number of SPUs

	int width, height;	// image details
	short int* pixels __attribute__ ((aligned(16)));	// original image
	short int* decomp __attribute__ ((aligned(16)));	// original image

	a_block* blocks;		// compressed image

} __attribute__ ((aligned(16))) a_pointers_t;

