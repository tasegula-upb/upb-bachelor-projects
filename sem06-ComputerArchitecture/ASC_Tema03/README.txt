Gula Tanase, 334CA
Arhitectura Sistemelor de Calcul - Tema 03 (Cell)

Structura de pointeri aleasa este urmatoarea:
typedef struct pointers_t {
	int index;			// indexul SPU-ului
	int spus;			// numarul total de SPU-uri

	int width, height;	// detaliile despre imagini
	
	// valoarea pixelilor din imaginea initiala
	short int* pixels __attribute__ ((aligned(16)));
	
	// vectorul pixelilor din imaginea dezarhivata
	// pointeaza efectiv catre adresa din imaginea dezarhivata
	short int* decomp __attribute__ ((aligned(16)));

	// vectorul de block-uri din imaginea arhivata
	// pointeaza efectiv catre adresa din imaginea arhivata
	// structura blocks este aliniata la 16.
	block* blocks;
} __attribute__ ((aligned(16))) pointers_t;

Pentru a alinia vectorii (block*, short int*) am folosit 
malloc_align(dimensiune, 4). Astfel, am modificat pgm.c, btc.c si utils.c
pentru a folosi malloc_align, respectiv free_align.

PPU:
 >	creeaza N thread-uri, N fiind 1 2 4 sau 8;
 >	trimite SPU-urilor o structura de tipul pointers_t;
 >	asteapta ca SPU-urile sa termine, dupa care creeaza imaginea 
	arhivata, respectiv cea dezarhivata.
		
SPU:
 >	fiecare SPU ia, la un moment dat, din memoria comuna, BLOCK_SIZE linii, 
	aceasta fiind de fapt o linie de viitoare blocuri. 
 >	avand o linie, proceseaza toate blocurile din ea, astfel:
	 >	calculeaza media, apoi deviatia standard
	 >	construieste un element de tip block
	 >	adauga acel element in vectorul de block-uri
	 >	calculeaza noua valoarea a imaginii dezarhivate, conform blocului
	 >	adauga noile valori din imaginea dezarhivata

Explicatie / exemplu:
 >	pentru o imagine de 800 x 1600, ruland cu 4 SPU-uri:
	 >	SPU-1 ia prima data liniile 0 - 8;
		 >	proceseaza blocul dat de liniile respective si coloanele:
			- 0-8, apoi 9-15, 16-23, etc pana la 1592-1599;
	 >	SPU-2: 9 - 15; SPU3: 16 - 23; SPU-4: 24 - 31;
	 >	SPU-1 va lua liniile 32 - 39;
		 
Analiza a performantei:
 >	pentru obtinerea rezultatelor, am creat un script care ruleaza programul
	folosind fiecare input, respectiv fiecare numar de thread-uri.

REZULTATE:
|-----------------------------------------------|
| #Test	| 	           #SPU				| Dim	|
|		|	1	|	2	|	4	|	8	| (MB)	|
|-------|-------|-------|-------|-------|-------|
|  in1	| 3.19	| 2.36	| 1.65	| 1.65	| 7.6	|
|  in2	| 6.07	| 5.14	| 3.56	| 3.18	| 15.1	|
|  in3	| 8.28	| 6.96	| 4.90	| 4.48	| 21.0	|
|-----------------------------------------------|

 >	se poate observa scalabilitatea rularii cu mai multe SPU-uri
 >	de asemenea, se observa cum diferenta dintre rularea cu 1 SPU si 2 SPU-uri
	aproape se dubleaza fata de rulatre dintre 2 SPU-uri si 4 SPU-uri
	 >	se observa cum arhitectura prezinta suport pentru multithreading
 >	pe aceeasi idee, se poate observa cum performanta nu se dubleaza intre 
	4 SPU-uri si 8; astfel, se observa cum odata cu cresterea nuamrului de 
	thread-uri, scade diferenta de performanta primita, ajungand chiar la 0
	sau fiind una negativa (datorita overhead-ului dat de trimiterea mesajelor)
 >	se pastreaza scalabilitatea fata de dimensiunea fisierului de intrare
	 
 >	probabil am ales o metoda gresita de a procesa imaginea, 
	deoarece timpii de rulare sunt foarte mari
