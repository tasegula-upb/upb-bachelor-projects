#! /bin/bash

spus=(1 2 4 8)
input=(in1 in2 in3)

output=(out1 out2 out3)
my_output=(out_1 out_2 out_3)

serial_prefix="../results/serial_"

echo "Compile serial code ..."

cd ./serial
make clean
make

echo
echo "Run serial code ..."

for i in 0 1 2; do
	echo "--------------------      Serial ${input[$i]}      --------------------"
	./btc ../btc_input/${input[$i]}.pgm $serial_prefix$i.btc $serial_prefix$i.pgm > ../results/$serial_prefix$i.time
	echo "--------------------      Done   ${input[$i]}      --------------------"
	done

cd ..