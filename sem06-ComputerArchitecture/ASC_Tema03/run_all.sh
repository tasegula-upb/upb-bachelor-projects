#!/bin/bash

rm -R ./results
mkdir results

spus=(1 2 4 8)
input=(in1 in2 in3)

output=(out1 out2 out3)
my_output=(out_1 out_2 out_3)

echo "Run serial code ..."
qsub -cwd -q ibm-cell-qs22.q -pe openmpi*12 12 serial.sh

echo
echo "Run paralel code ..."
for i in 0 1 2; do
	for N in "${spus[@]}"; do
		echo "--------------------   Test: ${input[$i]} - ${N} SPUs  --------------------"
		qsub -cwd -q ibm-cell-qs22.q -pe openmpi*12 12 script.sh ${N} ${input[$i]} ${my_output[$i]}
		echo "--------------------   Done ${input[$i]} - ${N} SPUs  ---------------------"
		done
	done

echo
echo "Compare ..."
qsub -cwd -q ibm-cell-qs22.q -pe openmpi*12 12 compare.sh
