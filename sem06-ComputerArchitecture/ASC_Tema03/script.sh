#! /bin/bash

echo "--------------------   Test: $2 - $1 SPUs  --------------------"
./ppu/tema3 0 $1 ./btc_input/$2.pgm ./results/$3_$1.btc ./results/$3_$1.pgm > ./results/$3_$1.time
echo "--------------------   Done: $2 - $1 SPUs  --------------------"
