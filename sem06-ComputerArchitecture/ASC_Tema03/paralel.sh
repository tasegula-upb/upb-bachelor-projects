#! /bin/bash

spus=(1 2 4 8)
input=(in1 in2 in3)

output=(out1 out2 out3)
my_output=(out_1 out_2 out_3)

for i in 0 1 2; do
	for N in "${spus[@]}"; do
		echo "--------------------   Test: ${input[$i]} - ${N} SPUs  --------------------"
		./ppu/tema3 0 ${N} ./btc_input/${input[$i]}.pgm ./results/${my_output[$i]}_${N}.btc ./results/${my_output[$i]}_${N}.pgm > ./results/${my_output[$i]}_${N}.time
		echo "--------------------   Done ${input[$i]} - ${N} SPUs  ---------------------"
		done
	done