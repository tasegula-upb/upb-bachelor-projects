/*
 *	Gula Tanase, 334CA
 *	Arhitectura Sistemelor de Calcul - Tema 02
 *
 *	Functii utile pentru implementarea temei
 */

#include "utils.h"

/*
 *	Returns an upper matrix with random values
 */
double* get_random_upper_matrix(int N) 
{
	double *matrix = calloc(N * N, sizeof(double));
	int i = 0, j = 0;
	double value = 0.0f;

	for (i = 0; i < N; i++) {
		for (j = i; j < N; j++) {

			value = rand() / (1.0 * N * ULONG_MAX);
			matrix[i * N + j] = value;
		}
	}

	return matrix;
}


/*
 *	Returns a lower matrix with random values
 */
double* get_random_lower_matrix(int N) 
{
	double *matrix = calloc(N * N, sizeof(double));
	int i = 0, j = 0;
	double value = 0.0f;

	for (i = 0; i < N; i++) {
		for (j = 0; j < i + 1; j++) {

			value = rand() / (1.0 * N * ULONG_MAX);
			matrix[i * N + j] = value;
		}
	}

	return matrix;
}


/*
 *	Returns an array with random values
 */
double* get_random_array(int N) 
{
	double *array = calloc(N, sizeof(double));
	int i = 0;
	double value = 0.0f;

	for (i = 0; i < N; i++) {
		value = rand() / (1.0 * N * ULONG_MAX);
		array[i] = value;
	}

	return array;
}


/*
 *	Prints the given matrix
 */
void print_matrix(double *matrix, int N, FILE *file) {
	int i = 0, j = 0;

	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			fprintf(file, "%2.14lg ", matrix[i * N + j]);
		}
		fprintf(file, "\n");
	}
}

/*
 *	Prints the given array
 */
void print_array(double *array, int N, FILE *file) {
	int i = 0;

	for (i = 0; i < N; i++) {
		fprintf(file, "%2.14lg ", array[i]);
	}
	fprintf(file, "\n");
}
