#
# -----------------------------------------------------------------------------
#
#	Gula Tanase, 334CA
#	Arhitectura Sistemelor de Calcul - Tema 02
#
#	Fisier gnuplot de creare a unui plot care afiseaza functia cblas.h
# pe toate arhitecturile
# -----------------------------------------------------------------------------

set output "dtrmv_cblas.png"
set terminal png

set xlabel "Size (N)"
set ylabel "Time (miliseconds)"

set xrange[0:40000]
set yrange[0:2000]

set grid

plot "plot_nehalem-O0.data" using 1:2 title "Nehalem CBLAS -O0" with lines, \
	 "plot_nehalem-O3.data" using 1:2 title "Nehalem CBLAS -O3" with lines, \
	 \
	 "plot_opteron-O0.data" using 1:2 title "Opteron CBLAS -O0" with lines, \
	 "plot_opteron-O3.data" using 1:2 title "Opteron CBLAS -O3" with lines, \
	 \
	 "plot_quad-O0.data" using 1:2 title "Quad CBLAS -O0" with lines, \
	 "plot_quad-O3.data" using 1:2 title "Quad CBLAS -O3" with lines;

