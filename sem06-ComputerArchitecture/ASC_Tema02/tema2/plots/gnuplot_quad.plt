#
# -----------------------------------------------------------------------------
#
#	Gula Tanase, 334CA
#	Arhitectura Sistemelor de Calcul - Tema 02
#
#	Fisier gnuplot de creare a unui plot care afiseaza toate functiile,
# pe arhitectura QUAD, cu sau fara optimizare
# -----------------------------------------------------------------------------

set output "quad.png"
set terminal png

set xlabel "Size (N)"
set ylabel "Time (miliseconds)"

set xrange[0:40000]
set yrange[0:2000]

set grid

plot "plot_quad-O0.data" using 1:2 title "Quad CBLAS -O0" with lines, \
	 "plot_quad-O0.data" using 1:3 title "Quad BAD -O0" with lines, \
	 "plot_quad-O0.data" using 1:4 title "Quad OPTIMIZED -O0" with lines, \
	 \
	 "plot_quad-O3.data" using 1:2 title "Quad CBLAS -O3" with lines, \
	 "plot_quad-O3.data" using 1:3 title "Quad BAD -O3" with lines, \
	 "plot_quad-O3.data" using 1:4 title "Quad OPTIMIZED -O3" with lines;

