#
# -----------------------------------------------------------------------------
#
#	Gula Tanase, 334CA
#	Arhitectura Sistemelor de Calcul - Tema 02
#
#	Fisier gnuplot de creare a unui plot care afiseaza toate functiile,
# pe arhitectura OPTERON, cu sau fara optimizare
# -----------------------------------------------------------------------------

set output "opteron.png"
set terminal png

set xlabel "Size (N)"
set ylabel "Time (miliseconds)"

set xrange[0:40000]
set yrange[0:2000]

set grid

plot "plot_opteron-O0.data" using 1:2 title "Opteron CBLAS -O0" with lines, \
	 "plot_opteron-O0.data" using 1:3 title "Opteron BAD -O0" with lines, \
	 "plot_opteron-O0.data" using 1:4 title "Opteron OPTIMIZED -O0" with lines, \
	 \
	 "plot_opteron-O3.data" using 1:2 title "Opteron CBLAS -O3" with lines, \
	 "plot_opteron-O3.data" using 1:3 title "Opteron BAD -O3" with lines, \
	 "plot_opteron-O3.data" using 1:4 title "Opteron OPTIMIZED -O3" with lines;

