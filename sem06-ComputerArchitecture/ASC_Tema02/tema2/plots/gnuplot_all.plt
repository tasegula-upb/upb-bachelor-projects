#
# -----------------------------------------------------------------------------
#
#	Gula Tanase, 334CA
#	Arhitectura Sistemelor de Calcul - Tema 02
#
#	Fisier gnuplot de creare a unui plot care afiseaza toate functiile,
# pe toate arhitecturile, cu sau fara optimizare
# -----------------------------------------------------------------------------

set output "allInOne.png"
set terminal png

set xlabel "Size (N)"
set ylabel "Time (miliseconds)"

set xrange[0:40000]
set yrange[0:2000]

plot "plot_nehalem-O0.data" using 1:2 title "Nehalem CBLAS -O0" with lines, \
	 "plot_nehalem-O0.data" using 1:3 title "Nehalem BAD -O0" with lines, \
	 "plot_nehalem-O0.data" using 1:4 title "Nehalem OPTIMIZED -O0" with lines, \
	 \
	 "plot_nehalem-O3.data" using 1:2 title "Nehalem CBLAS -O3" with lines, \
	 "plot_nehalem-O3.data" using 1:3 title "Nehalem BAD -O3" with lines, \
	 "plot_nehalem-O3.data" using 1:4 title "Nehalem OPTIMIZED -O3" with lines, \
	 \
	 "plot_opteron-O0.data" using 1:2 title "Opteron CBLAS -O0" with lines, \
	 "plot_opteron-O0.data" using 1:3 title "Opteron BAD -O0" with lines, \
	 "plot_opteron-O0.data" using 1:4 title "Opteron OPTIMIZED -O0" with lines, \
	 \
	 "plot_opteron-O3.data" using 1:2 title "Opteron CBLAS -O3" with lines, \
	 "plot_opteron-O3.data" using 1:3 title "Opteron BAD -O3" with lines, \
	 "plot_opteron-O3.data" using 1:4 title "Opteron OPTIMIZED -O3" with lines, \
	 \
	 "plot_quad-O0.data" using 1:2 title "Quad CBLAS -O0" with lines, \
	 "plot_quad-O0.data" using 1:3 title "Quad BAD -O0" with lines, \
	 "plot_quad-O0.data" using 1:4 title "Quad OPTIMIZED -O0" with lines, \
	 \
	 "plot_quad-O3.data" using 1:2 title "Quad CBLAS -O3" with lines, \
	 "plot_quad-O3.data" using 1:3 title "Quad BAD -O3" with lines, \
	 "plot_quad-O3.data" using 1:4 title "Quad OPTIMIZED -O3" with lines;

