#
# -----------------------------------------------------------------------------
#
#	Gula Tanase, 334CA
#	Arhitectura Sistemelor de Calcul - Tema 02
#
#	Fisier gnuplot de creare a unui plot care afiseaza functia neoptimizata
# pe toate arhitecturile
# -----------------------------------------------------------------------------

set output "dtrmv_bad.png"
set terminal png

set xlabel "Size (N)"
set ylabel "Time (miliseconds)"

set xrange[0:40000]
set yrange[0:2000]

set grid

plot "plot_nehalem-O0.data" using 1:3 title "Nehalem BAD -O0" with lines, \
	 "plot_nehalem-O3.data" using 1:3 title "Nehalem BAD -O3" with lines, \
	 \
	 "plot_opteron-O0.data" using 1:3 title "Opteron BAD -O0" with lines, \
	 "plot_opteron-O3.data" using 1:3 title "Opteron BAD -O3" with lines, \
	 \
	 "plot_quad-O0.data" using 1:3 title "Quad BAD -O0" with lines, \
	 "plot_quad-O3.data" using 1:3 title "Quad BAD -O3" with lines;

