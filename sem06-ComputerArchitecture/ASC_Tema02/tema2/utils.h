/*
 *	Gula Tanase, 334CA
 *	Arhitectura Sistemelor de Calcul - Tema 02
 *
 *	Functii utile pentru implementarea temei
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

/*
 *	Generates random array of N or N*N size;
 */
double* get_random_upper_matrix(int N);
double* get_random_lower_matrix(int N);
double* get_random_array(int N);

/*
 *	Prints out the given array of N or N*N size into the given file;
 */
void print_matrix(double *matrix, int N, FILE *file);
void print_array(double *array, int N, FILE *file);

#endif