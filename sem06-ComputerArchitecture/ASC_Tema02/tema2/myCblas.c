/*
 *	Gula Tanase, 334CA
 *	Arhitectura Sistemelor de Calcul - Tema 02
 *
 *	Implementarea propriilor functii dtrmv
 */

#include "myCblas.h"
#include "utils.h"
#include <string.h>

/*
 *	THIS IS THE TRIVIAL VERSION: only execute the multiplication;
 *	@N	size of the matrix
 *	@A	square triunghiular matrix
 *	@X	array
 *
 *	Executes: x := A * x;
 */
void dtrmv_bad(
				 const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
				 const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_DIAG Diag,
				 const int N, const double *A, const int lda,
				 double *X, const int incX)
{
	int i = 0, j = 0;

	double *oldX = calloc(N, sizeof(double));
	memcpy(oldX, X, N * sizeof(double));

	for (i = 0; i < N; i++) {
		double sum = 0.0f;
		for (j = 0; j < N; j++) {
			sum += A[i * N + j] * oldX[j];
		}
		*(X + i) = sum;
	}

	free(oldX);
}


/*
 *	THIS IS THE OPTIMIZED VERSION
 *	@N	size of the matrix
 *	@A	square triunghiular matrix
 *	@X	array
 *
 *	Executes: x := A * x;
 */
void dtrmv_opt(
				 const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
				 const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_DIAG Diag,
				 const int N, const double *A, const int lda,
				 double *X, const int incX)
{
	if (Uplo == CblasUpper) {
		if (TransA == CblasNoTrans) {
			upper(A, X, N);
		}
		else {
			/* upper transpose => lower matrix calculation */
			upper_trans(A, X, N);
		}
	}
	else {
		if (TransA == CblasNoTrans) {
			lower(A, X, N);
		}
		else {
			/* lower transpose => upper matrix calculation */
			lower_trans(A, X, N);
		}
	}
}


/*
 *	Returns the array result of (A * x), with
 *	@A	upper triungular matrix
 */
void upper(const double *A, double *X, const int N) 
{
	unsigned int i = 0;
	unsigned int j = 0;

	double *oldX = calloc(N, sizeof(double));
	memcpy(oldX, X, N * sizeof(double));

	double *X_pos = &X[0];
	const double *A_pos = &A[0];

	for (i = 0; i < N; i++) {
		register double sum = 0;
		register double *oldX_pos = &oldX[i];

		A_pos += i;

		for (j = i; j < N; j++) {
			sum += (*(A_pos++)) * (*(oldX_pos++));
		}

		*X_pos = sum;
		X_pos++;
	}

	free(oldX);
}


/*
 *	Returns the array result of ((A ** T) * x), with
 *	@A	upper triungular matrix
 */
void upper_trans(const double *A, double *X, const int N) 
{
	unsigned int i = 0;
	unsigned int j = 0;

	double *oldX = calloc(N, sizeof(double));
	memcpy(oldX, X, N * sizeof(double));

	double *X_pos = &X[0];
	const double *A_pos = &A[0];
	
	for (j = 0; j < N; j++) {
		register double sum = 0;
		register double *oldX_pos = &oldX[j];

		A_pos += j;

		for (i = 0; i < j; i++) {
			sum += (*(A_pos++)) * (*(oldX_pos++));
		}

		*(X + i) = sum;
	}

	free(oldX);
}

/*
 *	Returns the array result of (A * x), with
 *	@A	lower triungular matrix
 */
void lower(const double *A, double *X, const int N) 
{
	unsigned int i = 0;
	unsigned int j = 0;

	double *oldX = calloc(N, sizeof(double));
	memcpy(oldX, X, N * sizeof(double));

	double *X_pos = &X[0];
	const double *A_pos = &A[0];
	
	for (i = 0; i < N; i++) {
		register double sum = 0;
		register double *oldX_pos = &oldX[i];

		A_pos += i;

		for (j = 0; j <= i; j++) {
			sum += (*(A_pos++)) * (*(oldX_pos++));
		}

		*(X + i) = sum;
	}

	free(oldX);
}


/*
 *	Returns the array result of ((A ** T) * x), with
 *	@A	lower triungular matrix
 */
void lower_trans(const double *A, double *X, const int N) 
{
	unsigned int i = 0;
	unsigned int j = 0;
	
	double *oldX = calloc(N, sizeof(double));
	memcpy(oldX, X, N * sizeof(double));

	double *X_pos = &X[0];
	const double *A_pos = &A[0];
	
	for (j = 0; j < N; j++) {
		register double sum = 0;
		register double *oldX_pos = &oldX[j];

		A_pos += j;

		for (i = j; i < N; i++) {
			sum += (*(A_pos++)) * (*(oldX_pos++));
		}

		*(X + i) = sum;
	}

	free(oldX);
}