/*
 *	Gula Tanase, 334CA
 *	Arhitectura Sistemelor de Calcul - Tema 02
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "utils.h"
#include "myCblas.h"
#include "cblas.h"


int main(int argc, char *argv[]) 
{
	if (argc != 2) {
		printf("Usage: %s test_size\n", argv[0]);
		return -1;	/* exit */
	}

	int i = 0;		/* used for iterating through arrays */
	unsigned short bad = 0, opt = 0;	/* correctness */
	float epsilon = 0.00000000001;

	int N;			/* size of the vector; */
	double *A;		/* Matrix to be multiplied */

	/* Vector to be multiplied */
	double *X_gen;	/* used for testing purposes*/
	double *cblaX;	/* using the cblas implementation */
	double *X_bad;	/* using the bad implementation */
	double *X_opt;	/* using the optimized implementation */

	enum CBLAS_ORDER 	 order 	= CblasRowMajor;
	enum CBLAS_UPLO 	 uplo 	= CblasUpper;
	enum CBLAS_TRANSPOSE trans 	= CblasNoTrans;
	enum CBLAS_DIAG 	 diag 	= CblasNonUnit;

	struct timeval start, end;
	double elapsed_bad, elapsed_opt, elapsed_cbs;	/* elapsed time during execution */
	double total = 0.0f;							/* aproximtive total time of main */

	fprintf(stderr, "\nSTARTING ");


	/* Alloc memory */
	gettimeofday(&start, NULL);

	N = atoi(argv[1]);
	A = calloc(N * N, sizeof(double));
	X_gen = calloc(N, sizeof(double));
	cblaX = calloc(N, sizeof(double));
	X_bad = calloc(N, sizeof(double));
	X_opt = calloc(N, sizeof(double));

	/* Make Random Matrix and Vector */
	A = get_random_upper_matrix(N);
	X_gen = get_random_array(N);

	/* Copy the arrays */
	memcpy(cblaX, X_gen, N * sizeof(double));
	memcpy(X_bad, X_gen, N * sizeof(double));
	memcpy(X_opt, X_gen, N * sizeof(double));

	fprintf(stderr, "TEST %d\n", N);

	gettimeofday(&end, NULL);

	/* also, initiate total time */
	total = ((end.tv_sec - start.tv_sec) * 1000000.0f + end.tv_usec - start.tv_usec)/1000.0f;
	fprintf(stderr, "Generate all arrays:  %12lf\n", total);


	/* Bad implementation */
	gettimeofday(&start, NULL);

	dtrmv_bad(order, uplo, trans, diag, N, A, N, X_bad, 1);

	gettimeofday(&end, NULL);

	elapsed_bad = ((end.tv_sec - start.tv_sec) * 1000000.0f + end.tv_usec - start.tv_usec)/1000.0f;
	total += elapsed_bad;


	/* Optimized implementation */
	gettimeofday(&start, NULL);

	dtrmv_opt(order, uplo, trans, diag, N, A, N, X_opt, 1);

	gettimeofday(&end, NULL);

	elapsed_opt = ((end.tv_sec - start.tv_sec) * 1000000.0f + end.tv_usec - start.tv_usec)/1000.0f;
	total += elapsed_opt;


	/* Cblas implementation */
	gettimeofday(&start, NULL);

	cblas_dtrmv(order, uplo, trans, diag, N, A, N, cblaX, 1);

	gettimeofday(&end, NULL);

	elapsed_cbs = ((end.tv_sec - start.tv_sec) * 1000000.0f + end.tv_usec - start.tv_usec)/1000.0f;
	total += elapsed_cbs;


	/* Testing for correctness */
	for (i = 0; i < N; i++) {
		if (X_bad[i] - epsilon > cblaX[i] &&
			X_bad[i] + epsilon < cblaX[i]) {
			bad++;
			fprintf(stderr, "[%d] X_gen = %2.14lg \t cblaX = %2.14lg \t X_bad = %2.14lg\n", 
					i, X_gen[i], cblaX[i], X_bad[i]);
		}
		if (X_opt[i] - epsilon > cblaX[i] &&
			X_opt[i] + epsilon < cblaX[i]) {
			opt++;
			fprintf(stderr, "[%d] X_gen = %2.14lg \t cblaX = %2.14lg \t X_opt = %2.14lg\n", 
					i, X_gen[i], cblaX[i], X_opt[i]);
		}
	}

	fprintf(stderr, "CBLAS implementation: %15lf\n", elapsed_cbs);
	fprintf(stderr, "BAD implementation:   %15lf \t %15lf %% \n", elapsed_bad, 100.0 * (elapsed_cbs / elapsed_bad));
	fprintf(stderr, "OPT implementation:   %15lf \t %15lf %% \n", elapsed_opt, 100.0 * (elapsed_cbs / elapsed_opt));

	fprintf(stderr, "BAD: corect \t %f %% \n", 100.0 * (N-bad) / N);
	fprintf(stderr, "OPT: corect \t %f %% \n", 100.0 * (N-opt) / N);
	fprintf(stderr, "\nTOT: %12lf\n---------------------------------\n\n", total);


	/* printing into the plotting file */
	printf("%d %15lf %15lf %15lf\n", N, elapsed_cbs, elapsed_bad, elapsed_opt);


/* free memory */
free(cblaX);
free(X_bad);
free(X_opt);
free(A);

return 0;
}
