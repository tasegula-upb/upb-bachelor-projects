#!/bin/bash
#
# -----------------------------------------------------------------------------
#
#	Gula Tanase, 334CA
#	Arhitectura Sistemelor de Calcul - Tema 02
#
#	Script de executie a programului in coada, pe arhitectura OPTERON.
# -----------------------------------------------------------------------------

PREFIX="libraries/atlas-3.10.1-gcc-4.4.6-opteron"	# The module to load

LIB="3.10.1-opteron-gcc-4.4.6"	# The library to be loaded
OUT=""							# The output file
LOG="log_opteron"				# The stderr / extra output file

# Testing size
test_size=(2500 5000 7500 \
			10000 12500 15000 17500 \
			20000 22500 25000 27500 \
			30000 32500 35000 37500 40000)

# Actual running
if test $COMPILER="gcc"; then

	OUT="plot_opteron-O0.data"

	echo "# OPTIMIZATION -O0" >> $LOG
	echo "# OPTIMIZATION -O0" >> $OUT
	echo "# N          Cblas           Bad             Optimized" >> $OUT
	echo "  0          0               0               0        " >> $OUT

	make clean
	make VERSION=$LIB OPTFLAGS="-O0"

	for N in "${test_size[@]}"; do
		make run SIZE="${N}" >> $OUT 2>> $LOG
	done

	OUT="plot_opteron-O3.data"

	echo "# OPTIMIZATION -O3" >> $LOG
	echo "# OPTIMIZATION -O3" >> $OUT
	echo "# N          Cblas           Bad             Optimized" >> $OUT
	echo "  0          0               0               0        " >> $OUT

	make clean
	make VERSION=$LIB OPTFLAGS="-O3"

	for N in "${test_size[@]}"; do
		make run SIZE="${N}" >> $OUT 2>> $LOG
	done
fi

exit 0

