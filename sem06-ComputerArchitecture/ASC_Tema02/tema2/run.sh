#!/bin/bash
#
# -----------------------------------------------------------------------------
#
#	Gula Tanase, 334CA
#	Arhitectura Sistemelor de Calcul - Tema 02
#
#	Script de submitere a programului pe fiecare arhitectura, folosind 
# compilatoare diferite.
# -----------------------------------------------------------------------------

PREFIX="libraries/atlas-3.10.1-gcc-4.4.6-"

NEHALEM="nehalem"
OPTERON="opteron"
QUAD="quad"

DIR_N="x_$NEHALEM"
DIR_O="y_$OPTERON"
DIR_Q="z_$QUAD"

LANG=C

# -----------------------------------------------------------------------------
# 	Testing NEHALEM
# -----------------------------------------------------------------------------
rm -r -f $DIR_N
mkdir $DIR_N
chmod +x $DIR_N

cp *.c *.h *.sh Makefile $DIR_N
cd $DIR_N

mprun.sh --job-name "test_$NEHALEM" \
		 --queue ibm-nehalem.q \
		 --modules "$PREFIX$NEHALEM" \
		 --script "script_nehalem.sh" \
		 --show-qsub \
		 --show-script \
		 --batch-job

cd ..

# -----------------------------------------------------------------------------
# 	Testing OPTERON
# -----------------------------------------------------------------------------
rm -r -f $DIR_O
mkdir $DIR_O
chmod +x $DIR_O

cp *.c *.h *.sh Makefile $DIR_O
cd $DIR_O

mprun.sh --job-name "test_$OPTERON" \
		 --queue ibm-opteron.q \
		 --modules "$PREFIX$OPTERON" \
		 --script "script_opteron.sh" \
		 --show-qsub \
		 --show-script \
		 --batch-job

cd ..

# -----------------------------------------------------------------------------
# 	Testing QUAD
# -----------------------------------------------------------------------------
rm -r -f $DIR_Q
mkdir $DIR_Q
chmod +x $DIR_Q

cp *.c *.h *.sh Makefile $DIR_Q
cd $DIR_Q

mprun.sh --job-name "test_$QUAD" \
		 --queue ibm-quad.q \
		 --modules "$PREFIX$QUAD" \
		 --script "script_quad.sh" \
		 --show-qsub \
		 --show-script \
		 --batch-job

cd ..

exit 0

