/*
 *	Gula Tanase, 334CA
 *	Arhitectura Sistemelor de Calcul - Tema 02
 *
 *	Implementarea propriilor functii dtrmv
 */

#ifndef MYCBLAS_H_
#define MYCBLAS_H_

#ifndef CBLAS_ENUM_DEFINED_H
	#define CBLAS_ENUM_DEFINED_H
	enum CBLAS_ORDER 	 {CblasRowMajor=101, CblasColMajor=102 };
	enum CBLAS_TRANSPOSE {CblasNoTrans=111, CblasTrans=112, CblasConjTrans=113,
						  AtlasConj=114};
	enum CBLAS_UPLO 	 {CblasUpper=121, CblasLower=122};
	enum CBLAS_DIAG 	 {CblasNonUnit=131, CblasUnit=132};
	enum CBLAS_SIDE 	 {CblasLeft=141, CblasRight=142};
#endif

#include <stdio.h>
#include <stdlib.h>

/*
 *	THIS IS THE TRIVIAL VERSION: only execute the multiplication;
 *	@N	size of the matrix
 *	@A	square triunghiular matrix
 *	@X	array
 *
 *	Executes: x := A * x;
 */
void dtrmv_bad(
				 const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
				 const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_DIAG Diag,
				 const int N, const double *A, const int lda,
				 double *X, const int incX);

/*
 *	THIS IS THE OPTIMIZED VERSION
 *	@N	size of the matrix
 *	@A	square triunghiular matrix
 *	@X	array
 *
 *	Executes: x := A * x;
 */
void dtrmv_opt(
				 const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
				 const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_DIAG Diag,
				 const int N, const double *A, const int lda,
				 double *X, const int incX);


/*
 *	Returns the array result of (A * x), with
 *	@A	upper triungular matrix
 */
void upper(const double *A, double *X, const int N);

/*
 *	Returns the array result of ((A ** T) * x), with
 *	@A	upper triungular matrix
 */
void upper_trans(const double *A, double *X, const int N);


/*
 *	Returns the array result of (A * x), with
 *	@A	lower triungular matrix
 */
void lower(const double *A, double *X, const int N);

/*
 *	Returns the array result of ((A ** T) * x), with
 *	@A	lower triungular matrix
 */
void lower_trans(const double *A, double *X, const int N);

#endif