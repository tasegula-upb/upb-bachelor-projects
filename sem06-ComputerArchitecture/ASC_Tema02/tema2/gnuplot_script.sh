#!/bin/bash
#
# -----------------------------------------------------------------------------
#
#	Gula Tanase, 334CA
#	Arhitectura Sistemelor de Calcul - Tema 02
#
#	Script de generare a plot-urilor
# -----------------------------------------------------------------------------

cp ./x_nehalem/plot_nehalem-O0.data ./plots/plot_nehalem-O0.data
cp ./x_nehalem/plot_nehalem-O3.data ./plots/plot_nehalem-O3.data
cp ./y_opteron/plot_opteron-O0.data ./plots/plot_opteron-O0.data
cp ./y_opteron/plot_opteron-O3.data ./plots/plot_opteron-O3.data
cp ./z_quad/plot_quad-O0.data ./plots/plot_quad-O0.data
cp ./z_quad/plot_quad-O3.data ./plots/plot_quad-O3.data

cd ./plots
gnuplot *.plt

cd ..
exit 0s
