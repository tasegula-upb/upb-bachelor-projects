/*
 * Copyright 1993-2006 NVIDIA Corporation.  All rights reserved.
 *
 * NOTICE TO USER:
 *
 * This source code is subject to NVIDIA ownership rights under U.S. and
 * international Copyright laws.
 *
 * This software and the information contained herein is PROPRIETARY and
 * CONFIDENTIAL to NVIDIA and is being provided under the terms and
 * conditions of a Non-Disclosure Agreement.  Any reproduction or
 * disclosure to any third party without the express written consent of
 * NVIDIA is prohibited.
 *
 * NVIDIA MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THIS SOURCE
 * CODE FOR ANY PURPOSE.  IT IS PROVIDED "AS IS" WITHOUT EXPRESS OR
 * IMPLIED WARRANTY OF ANY KIND.  NVIDIA DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOURCE CODE, INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE.
 * IN NO EVENT SHALL NVIDIA BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
 * OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE
 * OR PERFORMANCE OF THIS SOURCE CODE.
 *
 * U.S. Government End Users.  This source code is a "commercial item" as
 * that term is defined at 48 C.F.R. 2.101 (OCT 1995), consisting  of
 * "commercial computer software" and "commercial computer software
 * documentation" as such terms are used in 48 C.F.R. 12.212 (SEPT 1995)
 * and is provided to the U.S. Government only as a commercial end item.
 * Consistent with 48 C.F.R.12.212 and 48 C.F.R. 227.7202-1 through
 * 227.7202-4 (JUNE 1995), all U.S. Government End Users acquire the
 * source code with only those rights set forth herein.
 */

// includes, system
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <helper_cuda.h>
#include <helper_timer.h>
#include <helper_functions.h>
#include <helper_math.h>

// includes, project
#include "2Dconvolution.h"

////////////////////////////////////////////////////////////////////////////////
// declarations, forward

extern "C"
void computeGold(float*, const float*, const float*, unsigned int, unsigned int);

Matrix AllocateDeviceMatrix(int width, int height);
Matrix AllocateMatrix(int width, int height);
void FreeDeviceMatrix(Matrix* M);
void FreeMatrix(Matrix* M);

void ConvolutionOnDevice(const Matrix M, const Matrix N, Matrix P);
void ConvolutionOnDeviceShared(const Matrix M, const Matrix N, Matrix P);

FILE *results;

////////////////////////////////////////////////////////////////////////////////
// Înmulțirea fără memorie partajată
////////////////////////////////////////////////////////////////////////////////
__global__ void ConvolutionKernel(Matrix M, Matrix N, Matrix P)
{
	double sum = 0;
	unsigned int m, n;

	// element row and column
	unsigned int row = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int col = blockIdx.y * blockDim.y + threadIdx.y;

	if (row >= N.height || col >= N.width) {
		return;
	}

	// starting and ending points for convultion calculation
	unsigned int mbegin = (row < 2)              ? (2 - row)            : 0;
	unsigned int mend   = (row > (N.height - 3)) ? (N.height - row + 2) : KERNEL_SIZE;
	unsigned int nbegin = (col < 2)              ? (2 - col)            : 0;
	unsigned int nend   = (col > (N.width - 3))  ? (N.width - col + 2)  : KERNEL_SIZE;

	for (m = mbegin; m < mend; m++) {
		for (n = nbegin; n < nend; n++) {

			sum += M.elements[m * KERNEL_SIZE + n] * 
				   N.elements[(row + m - 2) * N.width + (col + n - 2)];
		}
	}

	P.elements[row * P.width + col] = (float) sum;
}


////////////////////////////////////////////////////////////////////////////////
// Înmulțirea cu memorie partajată
////////////////////////////////////////////////////////////////////////////////
__global__ void ConvolutionKernelShared(Matrix M, Matrix N, Matrix P)
{
	double sum = 0;
	unsigned int m, n;
//	unsigned int aux = 0;

	// element row and column
	unsigned int Nrow = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int Ncol = blockIdx.y * blockDim.y + threadIdx.y;

	unsigned int row = threadIdx.x;
	unsigned int col = threadIdx.y;

	if (Nrow >= N.height || Ncol >= N.width) {
		return;
	}

	__shared__ float Ns[BLOCK_SIZE + 4][BLOCK_SIZE + 4];
	__shared__ float Ms[KERNEL_SIZE][KERNEL_SIZE];

	// starting and ending points for convultion calculation
	unsigned int mbegin = (Nrow < 2)              ? (2 - Nrow)            : 0;
	unsigned int mend   = (Nrow > (N.height - 3)) ? (N.height - Nrow + 2) : KERNEL_SIZE;
	unsigned int nbegin = (Ncol < 2)              ? (2 - Ncol)            : 0;
	unsigned int nend   = (Ncol > (N.width - 3))  ? (N.width - Ncol + 2)  : KERNEL_SIZE;

	if (threadIdx.x == 0 && threadIdx.y == 0) {
		// copy the M matrix into shared memory
		for (m = 0; m < KERNEL_SIZE; ++m) {
			int aux = m * M.width;
			for (n = 0; n  <KERNEL_SIZE; ++n) {
				Ms[m][n] = M.elements[aux + n];
			}
		}
	}

	if ((threadIdx.x == 0 || threadIdx.x == BLOCK_SIZE - 1) &&
		(threadIdx.y== 0 || threadIdx.y == BLOCK_SIZE - 1)) {
			for (m = mbegin; m < mend; ++m) {
				for (n = nbegin; n < nend; ++n) {
					Ns[row + m][col + n] = N.elements[N.pitch * (Nrow + m - 2) + (Ncol + n - 2)];
				}
			}
	}
	else if (threadIdx.x == 0) {
		for (m = mbegin; m < 2; ++m) {
			Ns[row + m][col + 2] = N.elements[N.pitch * (Nrow + m - 2) + Ncol];
		}
	}
	else if (threadIdx.x == BLOCK_SIZE - 1) {
		for (m = 3; m < mend; ++m) {
			Ns[row + m][col + 2] = N.elements[N.pitch * (Nrow + m - 2) + Ncol];
		}
	}

	else if (threadIdx.y == 0) {
		for (n = nbegin; n < 2; ++n) {
			Ns[row + 2][col + n] = N.elements[N.pitch * Nrow + (Ncol + n - 2)];
		}
	}
	else if (threadIdx.y == BLOCK_SIZE - 1) {
		for (n = 3; n < nend; ++n) {
			Ns[row + 2][col + n] = N.elements[N.pitch * Nrow + (Ncol + n - 2)];
		}
	}

	Ns[row + 2][col + 2] = N.elements[N.pitch * Nrow + Ncol];
	__syncthreads();

	for (m = mbegin; m < mend; ++m) {
		for (n = nbegin; n < nend; ++n) {
			sum += Ms[m][n] * Ns[row + m][col + n];
		}
	}

	P.elements[Nrow * P.width + Ncol] = (float) sum;
}

////////////////////////////////////////////////////////////////////////////////
// Returnează 1 dacă matricele sunt ~ egale
////////////////////////////////////////////////////////////////////////////////
int CompareMatrices(Matrix A, Matrix B)
{
	int i;
	int res = 1;

	if(A.width != B.width || A.height != B.height || A.pitch != B.pitch) {
		printf("different sizes\n");
		return 0;
	}

	int size = A.width * A.height;

	for(i = 0; i < size; i++)
		if(fabs(A.elements[i] - B.elements[i]) > MAX_ERR) {
			printf("A[%d] = %1.3f \t B[%d] = %1.3f\n", 
					i, A.elements[i], i, B.elements[i]);
			return 0;
		}

	return res;
}
void GenerateRandomMatrix(Matrix m)
{
	int i;
	int size = m.width * m.height;

	srand(time(NULL));

	for(i = 0; i < size; i++)
		m.elements[i] = rand() / (float)RAND_MAX;
}

////////////////////////////////////////////////////////////////////////////////
// main
////////////////////////////////////////////////////////////////////////////////
int main(int argc, char** argv) 
{
	int width = 0, height = 0;
	FILE *f, *out;
	StopWatchInterface *kernelTime = NULL;

	if(argc < 2)
	{
		printf("Argumente prea puține, trimiteți id-ul testului care trebuie rulat\n");
		return 0;
	}

	// Create timer for computeGold
	sdkCreateTimer(&kernelTime);
	sdkResetTimer(&kernelTime);

	char name[100];
	sprintf(name, "./tests/test_%s.txt", argv[1]);

	f       = fopen(name, "r");
	out     = fopen("out.txt", "a");
	results = fopen("results.txt", "a");

	fscanf(f, "%d%d", &width, &height);

	Matrix M;//kernel de pe host
	Matrix N;//matrice inițială de pe host
	Matrix P;//rezultat fără memorie partajată calculat pe GPU
	Matrix PS;//rezultatul cu memorie partajată calculat pe GPU

	M = AllocateMatrix(KERNEL_SIZE, KERNEL_SIZE);
	N = AllocateMatrix(width, height);        
	P = AllocateMatrix(width, height);
	PS = AllocateMatrix(width, height);

	GenerateRandomMatrix(M);
	GenerateRandomMatrix(N);

	fprintf(results, "%6d ", P.width * P.height);

	// M * N pe device
	ConvolutionOnDevice(M, N, P);

	// M * N pe device cu memorie partajată
	ConvolutionOnDeviceShared(M, N, PS);

	// calculează rezultatul pe CPU pentru comparație
	Matrix reference = AllocateMatrix(P.width, P.height);
	sdkStartTimer(&kernelTime);
	computeGold(reference.elements, M.elements, N.elements, N.height, N.width);
	sdkStopTimer(&kernelTime);
	fprintf(results, "%3.3f\n", sdkGetTimerValue(&kernelTime));

	// verifică dacă rezultatul obținut pe device este cel așteptat
	int res = CompareMatrices(reference, P);
	printf("Test global %s\n", (1 == res) ? "PASSED" : "FAILED");
	fprintf(out, "Test global %s %s\n", argv[1], (1 == res) ? "PASSED" : "FAILED");

	// verifică dacă rezultatul obținut pe device cu memorie partajată este cel așteptat
	// int ress = CompareMatrices(reference, PS);
	int ress = CompareMatrices(reference, PS);
	printf("Test shared %s\n", (1 == ress) ? "PASSED" : "FAILED");
	fprintf(out, "Test shared %s %s\n", argv[1], (1 == ress) ? "PASSED" : "FAILED");

	// Free matrices
	FreeMatrix(&M);
	FreeMatrix(&N);
	FreeMatrix(&P);
	FreeMatrix(&PS);

	fclose(f);
	fclose(out);
	return 0;
}


////////////////////////////////////////////////////////////////////////////////
//! Run a simple test for CUDA
////////////////////////////////////////////////////////////////////////////////
void ConvolutionOnDevice(const Matrix M, const Matrix N, Matrix P)
{
	Matrix Md, Nd, Pd; //matricele corespunzătoare de pe device
	int Msize = M.width * M.height * sizeof(float);
	int Nsize = N.width * N.height * sizeof(float);
	int Psize = P.width * P.height * sizeof(float);

	//pentru măsurarea timpului de execuție în kernel
	StopWatchInterface *kernelTime = NULL;
	sdkCreateTimer(&kernelTime);
	sdkResetTimer(&kernelTime);

	// alocați matricele de pe device
	Md = AllocateDeviceMatrix(M.width, M.height);
	Nd = AllocateDeviceMatrix(N.width, N.height);
	Pd = AllocateDeviceMatrix(P.width, P.height);

	// copiați datele de pe host (M, N) pe device (Md, Nd)
	cudaMemcpy(Md.elements, M.elements, Msize, cudaMemcpyHostToDevice);
	cudaMemcpy(Nd.elements, N.elements, Nsize, cudaMemcpyHostToDevice);

	// setați configurația de rulare a kernelului
	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid((N.height + BLOCK_SIZE - 1) / BLOCK_SIZE, (N.width + BLOCK_SIZE - 1) / BLOCK_SIZE);

	sdkStartTimer(&kernelTime);

	// lansați în execuție kernelul
	ConvolutionKernel<<<dimGrid, dimBlock>>>(Md, Nd, Pd);

	cudaThreadSynchronize();
	sdkStopTimer(&kernelTime);
	printf ("Timp execuție kernel: %f ms\n", sdkGetTimerValue(&kernelTime));
	fprintf(results, "%3.3f ", sdkGetTimerValue(&kernelTime));

	// copiaţi rezultatul pe host
	cudaMemcpy(P.elements, Pd.elements, Psize, cudaMemcpyDeviceToHost);

	// eliberați memoria matricelor de pe device
	FreeDeviceMatrix(&Md);
	FreeDeviceMatrix(&Nd);
	FreeDeviceMatrix(&Pd);
}


void ConvolutionOnDeviceShared(const Matrix M, const Matrix N, Matrix P)
{
	Matrix Md, Nd, Pd; //matricele corespunzătoare de pe device
	int Msize = M.width * M.height * sizeof(float);
	int Nsize = N.width * N.height * sizeof(float);
	int Psize = P.width * P.height * sizeof(float);

	//pentru măsurarea timpului de execuție în kernel
	StopWatchInterface *kernelTime = NULL;
	sdkCreateTimer(&kernelTime);
	sdkResetTimer(&kernelTime);

	// alocați matricele de pe device
	Md = AllocateDeviceMatrix(M.width, M.height);
	Nd = AllocateDeviceMatrix(N.width, N.height);
	Pd = AllocateDeviceMatrix(P.width, P.height);

	// copiați datele de pe host (M, N) pe device (MD, Nd)
	cudaMemcpy(Md.elements, M.elements, Msize, cudaMemcpyHostToDevice);
	cudaMemcpy(Nd.elements, N.elements, Nsize, cudaMemcpyHostToDevice);

	// setați configurația de rulare a kernelului
	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid((N.height + BLOCK_SIZE - 1) / BLOCK_SIZE, 
				 (N.width + BLOCK_SIZE - 1) / BLOCK_SIZE);

	sdkStartTimer(&kernelTime);

	// lansați în execuție kernelul
	ConvolutionKernelShared<<<dimGrid, dimBlock>>>(Md, Nd, Pd);

	cudaThreadSynchronize();
	sdkStopTimer(&kernelTime);
	printf ("Timp execuție kernel cu memorie partajată: %f ms\n", sdkGetTimerValue(&kernelTime));
	fprintf(results, "%3.3f ", sdkGetTimerValue(&kernelTime));

	// copiaţi rezultatul pe host
	cudaMemcpy(P.elements, Pd.elements, Psize, cudaMemcpyDeviceToHost);

	// eliberați memoria matricelor de pe device
	FreeDeviceMatrix(&Md);
	FreeDeviceMatrix(&Nd);
	FreeDeviceMatrix(&Pd);
}


// Alocă o matrice de dimensiune height * width pe device
Matrix AllocateDeviceMatrix(int width, int height)
{
	Matrix m;
	int size = width * height * sizeof(float);

	// alocați matricea și setați width, pitch și height
	m.width = m.pitch = width;
	m.height = height;

	cudaMalloc((void**)&m.elements, size);
	cudaMemset(m.elements, 0, size);

	return m;
}

// Alocă matrice pe host de dimensiune height*width
Matrix AllocateMatrix(int width, int height)
{
	Matrix M;
	M.width = M.pitch = width;
	M.height = height;
	int size = M.width * M.height;    
	M.elements = (float*) malloc(size*sizeof(float));
	return M;
}

// Eliberează o matrice de pe device
void FreeDeviceMatrix(Matrix* M)
{
	cudaFree(M->elements);
	M->elements = NULL;
}

// Eliberează o matrice de pe host
void FreeMatrix(Matrix* M)
{
	free(M->elements);
	M->elements = NULL;
}

