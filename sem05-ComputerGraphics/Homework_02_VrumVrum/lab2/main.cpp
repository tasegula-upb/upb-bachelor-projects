//	IT WAS A PLEASURE :D
#pragma once
#include "Framework/DrawingWindow.h"
#include "Framework/Visual2D.h"
#include "Framework/Transform2D.h"
#include "Framework/Transform3D.h"
#include "Framework/Line2D.h"
#include "Framework/Rectangle2D.h"
#include "Framework/Circle2D.h"
#include "Framework/Polygon2D.h"
#include <iostream>
#include <windows.h>
#include <vector>
#include <time.h>

#include "GameStuff/x_defines.h"
#include "GameStuff/x_variables.h"
#include "GameStuff/x_functions.h"

using namespace std;

int i;			// using in for(...);
int aux;

//functia care permite adaugarea de obiecte
void DrawingWindow::init()
{
	// Get current time
	time(&initial);

	v2ds = new Visual2D(0, 0, BOARD_WIDTH, BOARD_HEIGHT, 0, 0, DrawingWindow::width - 16, DrawingWindow::height * 0.1);
	v2ds->tipTran(false);
	addVisual2D(v2ds);

	v2dp = new Visual2D(0, 0, BOARD_WIDTH, BOARD_HEIGHT, 0, DrawingWindow::height * 0.1, DrawingWindow::width - 16, DrawingWindow::height);
	v2dp->tipTran(false);
	addVisual2D(v2dp);
	
	// Sky Color
	glClearColor(0.80, 0.93, 1.00, 1);
	// Movement
	move_up = move_down = move_right = move_left = false;
	
	// Creating the car
	speed = 0;
	speedLevel = 1;
	angle = PI / 2;
	car = new CarShip(Point3D(cx, cy, cz));
	car->add_to_Visual2D(v2dp);

	// Creating the speed, life, score, time, distance and level counter
	nc = create_colors();
	lifes = MAX_LIFES;
	show_life();
	show_speed();
	show_score();
	show_level();
	time_left = MAX_TIME;
	show_time();
	show_distance();


	// The 3-lane ROAD is here
	path = new Road(Point3D(0, 0, 0));
	path->add_to_Visual2D(v2dp);

	// The perspective view
	centerEye = Point3D(BOARD_WIDTH * 0.5, BOARD_HEIGHT * 1.1, 1000);
	carEye = Point3D(BOARD_WIDTH * 0.5, BOARD_HEIGHT * 0.5, 300);

	for (i = 0; i < 2; i++) {
		type[i] = lane[i] = loc[i] = 0;
	}

	tx = ty = tz = 0;
	pause = false;
	gameover = false;

	// The info text, which will tell if GAME OVER or WIN!
	over =		new Text("GAME OVER! THE CAR IS A RACK!", Point2D(BOARD_WIDTH * 0.3, BOARD_HEIGHT * 0.5), RED, BITMAP_HELVETICA_18);
	win =		new Text("YOU REACHED THE FINISH LINE!", Point2D(BOARD_WIDTH * 0.3, BOARD_HEIGHT * 0.5), RED, BITMAP_HELVETICA_18);
	timeout =	new Text("TIMEOUT!   YOU'RE TIME IS UP!", Point2D(BOARD_WIDTH * 0.3, BOARD_HEIGHT * 0.5), RED, BITMAP_HELVETICA_18);
	
	//	The FINISH LINE
	finishLine = new Line3D(Point3D(0, 0, -520000), BOARD_WIDTH, BOARD_HEIGHT * 0.4, RED, true);
	DrawingWindow::addObject3D_to_Visual2D(finishLine->obj, v2dp);

	//	The factors for the speed of generating obstacles
	f1 = 100;	f2 = 200;	f3 = 300;
	
	//	RANDOM GENERATOR STARTED
	srand(initial);
}

//functia care permite animatia
void DrawingWindow::onIdle()
{
	if (!pause || !gameover) {

		if (move_right) {
			if (!car->move_to_right(tx, speed, angle)) {
				move_left = false;
			}
		}
		if (move_left) {
			if (!car->move_to_left(tx, speed, angle)) {
				move_right = false;
			}
		}
		if (!move_left && !move_right) {
			car->rotate_back(angle);
		}

		if (move_up) {
			car->get_speed(speed, speedLevel);
		}

		if (move_down) {
			car->push_break(speed);
		}
		if (!move_up && !move_down) {
			car->reduce_speed(speed);
		}
		
		/* ---------------------------------------------------------------------------
		**		Apply center perspective for the road and for all the obstacles
		**	tx = modifies as we move the car to the right or left
		**	ty = remains 0, as we don't want our car or any obstacle to levitate
		**	tz = increments as we move (the car moves)
		** ------------------------------------------------------------------------ */
		Transform3D::loadIdentityProjectionMatrix();
		Transform3D::perspectiveProjectionMatrix(centerEye.x + tx / 10.0, centerEye.y, centerEye.z);

		//	3-lane Road : apply perspective projection 
		path->transform(tx, ty, tz);

		//	apply perspective projection for the existing obstacles
		tz += 50 * speed / 100;
		translate_obstacles(tx, ty, tz);

		//	apply perspective projection over the life counter
		animate_life(lifes);

		/* ----------------------------------------------------------------------------
		**				APLLY PERSPECTIVE PROJECTION OVER THE CAR
		** ------------------------------------------------------------------------- */
		Transform3D::loadIdentityProjectionMatrix();
		Transform3D::perspectiveProjectionMatrix(carEye.x, carEye.y, carEye.z);
		car->transform(angle);

		/* ----------------------------------------------------------------------------
		**							CREATE NEW OBSTACLES
		** ------------------------------------------------------------------------- */
		if (speed > 0) {
			timer++;
			if (added < MAX_OBST + speedLevel) {
				if (timer > rand() % f1 && timer > rand() % f2 && timer > rand() % f3) {
					// type[i] = 0 => DO NOT GENERATE OBSTACLE on lane[i]
					type[0] = rand() % 12;
					type[1] = rand() % 12;
					// lane[i] = 0 => DO NOT GENERATE OBSTACLE of type[i]
					lane[0] = rand() % 4;
					lane[1] = rand() % 4;
					generate_enemies(type, lane);
					timer = 0;				// reset the timer;
				}
			}
			//	if number of enemies is 0, then put one :D
			if (added == 0) {
				type[0] = rand() % 12;
				type[1] = rand() % 12;
				// lane[i] = 0 => DO NOT GENERATE OBSTACLE of type[i]
				lane[0] = rand() % 4;
				lane[1] = rand() % 4;
				generate_enemies(type, lane);
				timer = 0;				// reset the timer;
			}
		}

		//	show time
		time(&current);
		aux = (int)difftime(current, initial);
		animate_time(aux);

		//	animate distance
		animate_distance(tx, ty, tz);
	}

	if (!gameover) {

		//	animate score and level over the secondary context
		animate_score(score);
		animate_level(speedLevel);
		if (score > 2000 * speedLevel) {
			if (speedLevel < 5) {
				speedLevel++;
			}
		}

		//	Animate speed
		animate_speed(speed);

		//	Check if the player reached the finish line;
		if (tz > 500000 && tz < 520000) {
			tz += 20 * speed / 100;
			Transform3D::loadIdentityProjectionMatrix();
			Transform3D::perspectiveProjectionMatrix(centerEye.x + tx / 10.0, centerEye.y, centerEye.z);
			Transform3D::loadIdentityModelMatrix();
			Transform3D::translateMatrix(tx, ty, tz);
			Transform3D::applyTransform(finishLine->obj);
			pause = true;
		}
		if (tz > 520000) {
			addText_to_Visual2D(win, v2ds);
			speed = 0;
			gameover = true;
			pause = false;
		}

		//	check if is GAME OVER!!!
		if (lifes == 0 || aux == MAX_TIME) {
			if (lifes == 0) {
				addText_to_Visual2D(over, v2ds);
			}
			else {
				addText_to_Visual2D(timeout, v2ds);
			}
			pause = true;
			gameover = true;
		}
	}

	if (gameover && !pause) {
		score += aux * speedLevel;			// extra score
		ty += 50;
		car->end(centerEye, -ty);
		if (ty == 10000) {
			pause = true;
		}
	}
}

//functia care se apeleaza la redimensionarea ferestrei
void DrawingWindow::onReshape(int width, int height)
{
	v2dp->poarta(0, height * 0.1, width, height);
	v2ds->poarta(0, 0, width, height * 0.1);
}

//functia care defineste ce se intampla cand se apasa pe tastatura
void DrawingWindow::onKey(unsigned char key)
{
	switch (key) {
	case 27: exit(0);
	case KEY_UP:
		move_up = true;
		break;
	case KEY_DOWN:
		move_down = true;
		break;
	case KEY_LEFT:
		move_left = true;
		break;
	case KEY_RIGHT:
		move_right = true;
		break;
	}
}

void DrawingWindow::onKeyUp(unsigned char key)
{
	switch (key) {
	case KEY_UP:
		move_up = false;
		break;
	case KEY_DOWN:
		move_down = false;
		break;
	case KEY_LEFT:
		move_left = false;
		break;
	case KEY_RIGHT:
		move_right = false;
		break;
	}
}

//functia care defineste ce se intampla cand se da click pe mouse
void DrawingWindow::onMouse(int button, int state, int x, int y)
{
	
}


int main(int argc, char** argv)
{
	//creare fereastra
	DrawingWindow dw(argc, argv, BOARD_WIDTH, BOARD_HEIGHT, 0, 0, "Vrum-Vrum Power Rainbows! by Tase Gula");
	//se apeleaza functia init() - in care s-au adaugat obiecte
	dw.init();
	//se intra in bucla principala de desenare - care face posibila desenarea, animatia si procesarea evenimentelor
	dw.run();
	return 0;
}