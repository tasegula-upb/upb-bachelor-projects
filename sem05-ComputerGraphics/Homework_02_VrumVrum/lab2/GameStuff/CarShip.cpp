#pragma once
#include "../Framework/DrawingWindow.h"
#include "../Framework/Visual2D.h"
#include "../Framework/Transform2D.h"
#include "../Framework/Transform3D.h"
#include "../Framework/Object3D.h"

#include "Car/CarBody.cpp"
#include "Car/Wheel.cpp"
#include "x_defines.h"

class CarShip {
public:

	CarBody* wire;
	CarBody* full;
	vector<Wheel*> wheels;

	Point3D center;

	CarShip(Point3D p) {
		center.x = p.x;
		center.y = p.y;
		center.z = p.z;

		wire = new CarBody(p, WHITE, false, CAR_WIDTH, CAR_HEIGHT, CAR_LENGTH);
		full = new CarBody(p, CAR_COL, true, CAR_WIDTH, CAR_HEIGHT, CAR_LENGTH);

		wheels.push_back(new Wheel(Point3D(p.x - CAR_WIDTH * 0.55, p.y, p.z - CAR_LENGTH * 0.375), 5, 10));
		wheels.push_back(new Wheel(Point3D(p.x + CAR_WIDTH * 0.35, p.y, p.z - CAR_LENGTH * 0.375), 5, 10));
		wheels.push_back(new Wheel(Point3D(p.x + CAR_WIDTH * 0.35, p.y, p.z + CAR_LENGTH * 0.375), 5, 10));
		wheels.push_back(new Wheel(Point3D(p.x - CAR_WIDTH * 0.55, p.y, p.z + CAR_LENGTH * 0.375), 5, 10));
		
	}

	void add_to_Visual2D(Visual2D* v2d) {
		DrawingWindow::addObject3D_to_Visual2D(wire, v2d);
		DrawingWindow::addObject3D_to_Visual2D(full, v2d);

		for (int i = 0; i < wheels.size(); i++) {
			wheels[i]->add_to_Visual2D(v2d);
		}
	}

	void transform(float angle) {
		Transform3D::loadIdentityModelMatrix();
		Transform3D::translateMatrix(-center.x, -center.y, -center.z);
		Transform3D::rotateMatrixOy(angle);
		Transform3D::translateMatrix(center.x, center.y, center.z);
		Transform3D::applyTransform(wire);
		Transform3D::applyTransform(full);

		for (int i = 0; i < wheels.size(); i++) {
			wheels[i]->transform(center.x, center.y, center.z, angle);
		}
		
	}

	void get_speed(float &speed, int speedLevel) {
		if (speed < MAX_SPEED - 300 + 50 * speedLevel) {
			speed += 1;
		}
	}

	void push_break(float &speed) {
		if (speed > 0) {
			speed -= 5;
		}
		else {
			speed = 0;
		}
	}

	bool move_to_right(float &tx, float &speed, float &angle) {
		if (center.x + CAR_WIDTH < BOARD_WIDTH + tx - 10) {
			tx -= 10 * (speed / 100);
			if (angle < 0.25 * PI) {
				angle += (PI / 180) * (speed / 100);
			}
			return true;
		}
		return false;
	}

	bool move_to_left(float &tx, float &speed, float &angle) {
		if (center.x - CAR_WIDTH > tx + 10) {
			tx += 10 * (speed / 100);
			if (angle > -0.25 * PI) {
				angle -= (PI / 180) * (speed / 100);
			}
			return true;
		}
		return false;
	}

	void rotate_back(float &angle) {
		if (angle > 0.05 || angle < -0.05) {
			if (angle > 0)
				angle -= PI / 90;
			else
				angle += PI / 90;
		}
		else {
			angle = 0;
		}
	}

	void reduce_speed(float &speed) {
		if (speed > 0) {
			speed -= 1;
		}
		if (speed < 0) {
			speed = 0;
		}
	}

	void end(Point3D carEye, float t) {
		Transform3D::loadIdentityProjectionMatrix();
		Transform3D::perspectiveProjectionMatrix(carEye.x, carEye.y, carEye.z);
		Transform3D::loadIdentityModelMatrix();
		Transform3D::translateMatrix(0, 0, t);
		Transform3D::applyTransform(wire);
		Transform3D::applyTransform(full);
		for (int i = 0; i < wheels.size(); i++) {
			Transform3D::applyTransform(wheels[i]->wire);
			Transform3D::applyTransform(wheels[i]->full);
		}
	}
};