#pragma once
#include "../Framework/DrawingWindow.h"
#include "../Framework/Visual2D.h"
#include "../Framework/Transform2D.h"
#include "../Framework/Transform3D.h"
#include "../Framework/Object3D.h"
#include <math.h>
#include <vector>
#include "Obstacle.h"
#include "x_defines.h"

#define PI 3.141592653589793238462643383279502884197
#define R2 1.41421356237

class Heart :public Obstacle {

public:
	/* ITEMS FROM CLASS OBSTACLE
	Object3D* wire;
	Object3D* full;
	Point3D center;
	bool colored;
	int type;
	//*/
	float radius;

	Heart(Point3D _center, float _radius, Color c, bool _type) {

		center.x = _center.x;
		center.y = _center.y;
		center.z = _center.z;

		radius = _radius;
		colored = _type;
		type = 6;

		//construim lista de puncte tinand cont de centru si raza
		vector<Point3D*> vertices;
		vector<Face*> faces;
		vector<int> contour;

		float phi, theta;
		int i = 0;
		float d = 18.0;

		for (phi = 0; phi < 2 * PI; phi += PI / d) {
			// AICI INCEPE
			contour.clear();
			for (theta = 0; theta <= PI; theta += PI / d) {
				Point3D *p = new Point3D();
				p->x = center.x + radius * sin(phi) * sin(theta);
				p->y = center.y + radius * sin(phi) * cos(theta);
				p->z = center.z + radius * sin(theta);
				vertices.push_back(p);
				//				transf_vertices.push_back(p);
				contour.push_back(i);
				i++;
			}
			faces.push_back(new Face(contour));
			// AICI SE TERMINA
		}

		// necesare pentru coliziune - incadram intr-un patrat
		vertices.push_back(new Point3D(center.x - radius * R2, center.y - radius, center.z + radius * R2));
		vertices.push_back(new Point3D(center.x + radius * R2, center.y - radius, center.z - radius * R2));

		wire = new Object3D(vertices, faces, WHITE, false);
		if (_type) {
			full = new Object3D(vertices, faces, Color(1 - c.r, 1 - c.g, 1 - c.b), true);
		}
		else {
			full = new Object3D();
		}
	}

};