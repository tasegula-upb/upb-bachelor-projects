#pragma once
#include "../dependente/freeglut.h"
#include "../Framework/Visual2D.h"
#include "../Framework/DrawingWindow.h"
#include "../Framework/Transform2D.h"
#include "../Framework/Transform3D.h"
#include "../Framework/Line2D.h"
#include "../Framework/Rectangle2D.h"
#include "../Framework/Circle2D.h"
#include "../Framework/Polygon2D.h"

#include <iostream>
#include <windows.h>
#include <time.h>

#include "Cuboid.cpp"
#include "Sphere.cpp"
#include "Pyramid.cpp"
#include "Hexacone.cpp"
#include "Cone.cpp"
#include "CarShip.cpp"
#include "Obstacle.h"
#include "Road.cpp"
#include "Weird.cpp"
#include "Heart.cpp"
#include "CarObstacle.cpp"

#include "Car/CarBody.cpp"
#include "Car/Wheel.cpp"

/* ----------------------------------------------------------------------------
**							The Visual2D contexts
** ------------------------------------------------------------------------- */
Visual2D *v2dp, *v2ds;

/* ----------------------------------------------------------------------------
**								THE ROAD
** ------------------------------------------------------------------------- */
Road* path;


/* ----------------------------------------------------------------------------
**								THE CAR
** ------------------------------------------------------------------------- */
CarShip* car;
float angle;
float speed;
int speedLevel;
int counter;

/* ----------------------------------------------------------------------------
**							FIXED OBSTACLES
**	vector<type*>	vector of the specific objects
**	type* name		object of the specific type, 
**					used to add into the vector of the pecific type
**	vector<Color>	vector of colors, used to generate random colors
**	type[2]			type of the obstacle	
**	lane[2]			on which lane should the obstacle be generated
**					and there must be only two occupied at a time;
**	loc[2]			at which width should the obstacle be generated
**	timer			an int that increments at each call of the onIdle()
**					if the speed of the car is not 0 (the car is moving)
**	added			at a moment, on the road should not be more than 
**					a specific number of obstacles (MAX_OBST)
**	nc				what color is chosen to be given to the obstacle
**	f1, f2, f3		numbers used to generate obstacles faster and faster etc.
** ------------------------------------------------------------------------- */
vector<Obstacle*>	obstacles;			// don't remember using it, but it's 3AM and I'm a little tired to check now
vector <Sphere*>	spheres;
vector <Pyramid*>	pyramids;
vector <Cone*>		cones;
vector <Hexacone*>	hexacones;
vector <Weird*>		weirds;
vector <Heart*>		hearts;

vector <CarObstacle*> cars;
CarObstacle* carObst;

Sphere* sphere;
Pyramid* pyramid;
Hexacone* hexacone;
Cone* cone;
Weird* weird;
Heart* heart;

vector <Color> colors;

int type[2], lane[2], loc[2];
int timer;
int added;
int nc;

int f1, f2, f3;

/* ----------------------------------------------------------------------------
**								MOVEMENT
** ------------------------------------------------------------------------- */
bool move_up, move_down, move_right, move_left;
float tx, ty, tz;

Point3D centerEye;
Point3D carEye;

/* ----------------------------------------------------------------------------
**								ENVIROMENT
** ------------------------------------------------------------------------- */
// Speed Counter
vector<Rectangle2D*> speedCounterR;

// Life COunter
vector<Wheel*> lifeCounter;
int lifes;

// Score Cunter
vector<Rectangle2D*> scoreCounter;
vector<Text*> scoreDigits;
int score;

//	Level Counter
Text* levelCounter;

// Time Counter
Text* ttime;
time_t initial, current;
float time_left;

//	Distance Counter
Rectangle2D* map;
Circle2D* self;
Line2D *l1, *l2, *l3, *l4;
Line2D *start;
Rectangle2D *finish;

/* ----------------------------------------------------------------------------
**							START, GAME OVER & PAUSE
** ------------------------------------------------------------------------- */
bool pause, gameover;
Text* over;
Text* win;
Text* timeout;

Line3D* startLine;		// not used
Line3D* finishLine;