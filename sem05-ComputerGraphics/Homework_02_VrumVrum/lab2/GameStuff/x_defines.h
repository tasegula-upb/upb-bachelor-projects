#pragma once
#include "../Framework/Object3D.h"
#include <iostream>
#include <windows.h>


#define PI 3.141592653589793238462643383279502884197
#define inf 1000000
#define MAX(a, b)	((a < b) ? b : a)
#define MIN(a, b)	((a > b) ? b : a)

/* ----------------------------------------------------------------------------
**						THE GAME BOARD DIMENSIONS
**				and other general stuff relative to the game
** ------------------------------------------------------------------------- */
#define BOARD_WIDTH 825		// actualy 809!
#define BOARD_HEIGHT 500

/* ----------------------------------------------------------------------------
**							GENERAL COLORS
** ------------------------------------------------------------------------- */
#define BLACK	Color(0, 0, 0)
#define WHITE	Color(1, 1, 1)
#define RED		Color(1, 0, 0)
#define GREEN	Color(0, 1, 0)
#define BLUE	Color(0, 0, 1)
#define CYAN	Color(0, 1, 1)
#define MAGENTA	Color(1, 0, 1)
#define YELLOW	Color(1, 1, 0)

#define ASPHALT	Color(0.157, 0.165, 0.169)
#define GRASS	Color(0.004, 0.651, 0.067)

#define CAR_COL Color(0.1, 0.7, 0.3)

/* ----------------------------------------------------------------------------
**								CAR STUFF
** ------------------------------------------------------------------------- */
#define CAR_WIDTH (BOARD_WIDTH * 0.06)		// 49.5 = 50
#define CAR_HEIGHT (BOARD_HEIGHT * 0.05)	// 25
#define CAR_LENGTH (BOARD_HEIGHT * 0.2)		// 100
const float cx = BOARD_WIDTH * 0.5;	
const float cy = 150;		
const float cz = 100;

#define MAX_SPEED 500

/* ----------------------------------------------------------------------------
**							OBSTACLES POINTS
** ------------------------------------------------------------------------- */
const int location[4] = { 0, BOARD_WIDTH * 0.15, BOARD_WIDTH * 0.50, BOARD_WIDTH * 0.85 };
#define KILL_ZONE1 -300
#define KILL_ZONE2 (KILL_ZONE1 + CAR_LENGTH)
#define BASIC_Z -10000
#define MAX_OBST 10

#define SPHERE_R	125
#define HEART_R		125
#define WEIRD_R		125
#define CONE_R		75
#define PYRAMID_R	75

#define	HEXACONE_R	75

/* ----------------------------------------------------------------------------
**								COUNTERS
** ------------------------------------------------------------------------- */
#define MAX_TIME 100
#define MAX_LIFES	5
#define MAX_DIGITS	5

#define SCORE_HEX		100
#define SCORE_SPHERE	80
#define SCORE_WEIRD		75
#define SCORE_HEART		50
#define SCORE_PYR		35
#define SCORE_CONE		25
#define SCORE_CAR		100