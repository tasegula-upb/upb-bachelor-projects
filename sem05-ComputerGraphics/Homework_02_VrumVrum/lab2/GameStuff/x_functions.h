#pragma once
#include <cstdio>

#include "x_defines.h"
#include "x_variables.h"

// IDEA FOR TESTING COLLISION
// SOME PART IMPLEMENTED IN COLLISION WITH PYRAMID
bool collision(CarShip* car, Point3D* obst1, Point3D* obst2) {
	Point3D* c1 = car->wire->transf_vertices[0];
	Point3D* c2 = car->wire->transf_vertices[1];
	Point3D* c3 = car->wire->transf_vertices[2];
	Point3D* c4 = car->wire->transf_vertices[3];

	if (c1->x > obst1->x && c1->y > obst1->y && c1->x < obst2->x && c1->y < obst2->y ||
		c2->x > obst1->x && c2->y > obst1->y && c2->x < obst2->x && c2->y < obst2->y ||
		c3->x > obst1->x && c3->y > obst1->y && c3->x < obst2->x && c3->y < obst2->y ||
		c4->x > obst1->x && c4->y > obst1->y && c4->x < obst2->x && c4->y < obst2->y) 
	{
		return true;
	}

	return false;
}

void translate_obstacles(float tx, float ty, float tz) {
	// aux:		folosit pt translatarea obtacolelor mobile
	// kill:	folosit pt a verifica daca are loc coliziune
	int aux, kill, i;		// i:	contor

	kill = speed > 250 ? (100 * (speedLevel - 1)) : 0;

	Point3D* c1 = car->wire->transf_vertices[0];
	Point3D* c2 = car->wire->transf_vertices[1];
	Point3D *obst1, *obst2;

	float carx1 = car->center.x - tx - CAR_WIDTH * 0.5;
	float carx2 = car->center.x - tx + CAR_WIDTH * 0.5;
	float carx = car->center.x - tx;
	float obstx, obstr;				// obstacles x-coordinate and radius

	

	/* ----------------------------------------------------------------------------
	**							STATIC OBSTACLES
	** ------------------------------------------------------------------------- */

	// SPHERES
	for (i = 0; i < spheres.size(); i++) {
		spheres[i]->translate(tx, ty, tz);
		obstx = spheres[i]->center.x;
		obstr = spheres[i]->radius;
		if (tz + spheres[i]->center.z > KILL_ZONE1 - 100 * (speedLevel - 1) && tz + spheres[i]->center.z < KILL_ZONE2) {
			if ((carx1 > obstx - obstr && carx1 < obstx + obstr) ||
				((carx2 > obstx - obstr && carx2 < obstx + obstr))) {
				spheres[i]->remove_from_Visual2D(v2dp);
				spheres.erase(spheres.begin() + i);
				added--;
				lifes--;
				score -= SCORE_SPHERE * 3;
				continue;
			}
		}
		if (tz + spheres[i]->center.z > 500) {
			spheres[i]->remove_from_Visual2D(v2dp);
			spheres.erase(spheres.begin() + i);
			added--;
			score += SCORE_SPHERE;
			continue;
		}
	}
	// PYRAMIDS
	for (i = 0; i < pyramids.size(); i++) {
		pyramids[i]->translate(tx, ty, tz);
		obst1 = pyramids[i]->wire->transf_vertices[0];
		obst2 = pyramids[i]->wire->transf_vertices[2];
		if (c1->x > obst1->x && c1->y > obst1->y && c1->x < obst2->x && c1->y < obst2->y) {
			pyramids[i]->remove_from_Visual2D(v2dp);
			pyramids.erase(pyramids.begin() + i);
			added--;
			lifes--;
			score -= SCORE_PYR * 3;
			continue;
		}
		if (tz + pyramids[i]->center.z > 500) {
			pyramids[i]->remove_from_Visual2D(v2dp);
			pyramids.erase(pyramids.begin() + i);
			added--;
			score += SCORE_PYR;
			continue;
		}
	}
	
	// WEIRDS
	for (i = 0; i < weirds.size(); i++) {
		weirds[i]->translate(tx, ty, tz);
		obstx = weirds[i]->center.x;
		obstr = weirds[i]->radius;
		if (tz + weirds[i]->center.z > KILL_ZONE1 - kill && tz + weirds[i]->center.z < KILL_ZONE2) {
			if ((carx1 > obstx - obstr && carx1 < obstx + obstr) ||
				((carx2 > obstx - obstr && carx2 < obstx + obstr))) {
				weirds[i]->remove_from_Visual2D(v2dp);
				weirds.erase(weirds.begin() + i);
				added--;
				lifes--;
				score -= SCORE_WEIRD * 3;
				continue;
			}
		}
		if (tz + weirds[i]->center.z > 500) {
			weirds[i]->remove_from_Visual2D(v2dp);
			weirds.erase(weirds.begin() + i);
			added--;
			score += SCORE_WEIRD;
			continue;
		}
	}

	// HEARTS
	for (i = 0; i < hearts.size(); i++) {
		hearts[i]->translate(tx, ty, tz);
		obstx = hearts[i]->center.x;
		obstr = hearts[i]->radius;
		if (tz + hearts[i]->center.z > KILL_ZONE1 - kill && tz + hearts[i]->center.z < KILL_ZONE2) {
			if ((carx1 > obstx - obstr && carx1 < obstx + obstr) ||
				((carx2 > obstx - obstr && carx2 < obstx + obstr))) {
				hearts[i]->remove_from_Visual2D(v2dp);
				hearts.erase(hearts.begin() + i);
				added--;
				lifes--;
				score -= SCORE_HEART * 3;
				continue;
			}
		}
		if (tz + hearts[i]->center.z > 500) {
			hearts[i]->remove_from_Visual2D(v2dp);
			hearts.erase(hearts.begin() + i);
			added--;
			score += SCORE_HEART;
			continue;
		}
	}

	/* ----------------------------------------------------------------------------
	**							ROTATING OBSTACES
	** ------------------------------------------------------------------------- */

	// CONES
	for (i = 0; i < cones.size(); i++) {
		cones[i]->translate(tx, ty, tz);
		obstx = cones[i]->center.x;
		obstr = cones[i]->radius;
		if (tz + cones[i]->center.z > KILL_ZONE1 - kill && tz + cones[i]->center.z < KILL_ZONE2) {
			if ((carx1 > obstx - obstr && carx1 < obstx + obstr) ||
				((carx2 > obstx - obstr && carx2 < obstx + obstr))) {
				cones[i]->remove_from_Visual2D(v2dp);
				cones.erase(cones.begin() + i);
				added--;
				lifes--;
				score -= SCORE_CONE * 3;
				continue;
			}
		}
		if (tz + cones[i]->center.z > 500) {
			cones[i]->remove_from_Visual2D(v2dp);
			cones.erase(cones.begin() + i);
			added--;
			score += SCORE_CONE;
			continue;
		}
	}

	/* ----------------------------------------------------------------------------
	**								BONUS
	** ------------------------------------------------------------------------- */

	// HEXACONES
	for (i = 0; i < hexacones.size(); i++) {
		hexacones[i]->translate(tx, ty, tz);
		obstx = hexacones[i]->center.x;
		obstr = hexacones[i]->radius;
		if (tz + hexacones[i]->center.z > KILL_ZONE1 - kill && tz + hexacones[i]->center.z < KILL_ZONE2) {
			if ((carx1 > obstx - obstr && carx1 < obstx + obstr) ||
				((carx2 > obstx - obstr && carx2 < obstx + obstr))) {
				hexacones[i]->remove_from_Visual2D(v2dp);
				hexacones.erase(hexacones.begin() + i);
				added--;
//				lifes--;							// maybe it should also take life
				score += SCORE_HEX * 10;			// BONUS
				continue;
			}
		}
		if (tz + hexacones[i]->center.z > 500) {
			hexacones[i]->remove_from_Visual2D(v2dp);
			hexacones.erase(hexacones.begin() + i);
			added--;
			score -= SCORE_HEX * 0.5;
			continue;
		}
	}

	/* ----------------------------------------------------------------------------
	**								MOBILE OBSTACLES
	** ------------------------------------------------------------------------- */

	// OTHER CARS
	for (i = 0; i < cars.size(); i++) {
//		cars[i]->translate(tx, ty, tz);
		aux = tz - cars[i]->dist;
		cars[i]->translate(tx, ty, aux);				// viteza mai mica decat masina
		obstx = cars[i]->center.x;
		obstr = cars[i]->radius;
		
		if (aux + cars[i]->center.z > KILL_ZONE1 - kill && aux + cars[i]->center.z < KILL_ZONE2) {
			if ((carx1 > obstx - obstr && carx1 < obstx + obstr) ||
				((carx2 > obstx - obstr && carx2 < obstx + obstr))) {
				cars[i]->remove_from_Visual2D(v2dp);
				cars.erase(cars.begin() + i);
				added--;
				lifes--; 
				score -= SCORE_CAR * 3;
				continue;
			}
		}
		if (aux + cars[i]->center.z > 500) {
			cars[i]->remove_from_Visual2D(v2dp);
			cars.erase(cars.begin() + i);
			added--;
			score -= SCORE_CAR * 0.5;
			continue;
		}
	}
}

void generate_enemies(int type[2], int lane[2]) {
	int i;
	int n = 2;
	int aux;

	if (lane[0] == lane[1]) {
		n = 1;
	}

	for (i = 0; i < n; i++) {
		if (lane[i] == 0) {
			continue;
		}
		aux = rand() % nc;
		switch (type[i]) {
		case 1:
		case 7:
			sphere = new Sphere(Point3D(location[lane[i]], 100, BASIC_Z - tz), SPHERE_R, colors[aux], true);
			spheres.push_back(sphere);
//			obstacles.push_back(sphere);
			aux = spheres.size() - 1;
			spheres[aux]->add_to_Visual2D(v2dp);
			added++;
			break;
		case 2:
		case 8:
			pyramid = new Pyramid(Point3D(location[lane[i]], 0, BASIC_Z - tz), PYRAMID_R, 300, colors[aux], true);
			pyramids.push_back(pyramid);
//			obstacles.push_back(pyramid);
			aux = pyramids.size() - 1;
			pyramids[aux]->add_to_Visual2D(v2dp);
			added++;
			break;
		case 3:
		case 9:
			cone = new Cone(Point3D(location[lane[i]], 0, BASIC_Z - tz), CONE_R, 300, colors[aux], true);
			cones.push_back(cone);
//			obstacles.push_back(cone);
			aux = cones.size() - 1;
			cones[aux]->add_to_Visual2D(v2dp);
			added++;
			break;
		case 4:
		case 10:
			weird = new Weird(Point3D(location[lane[i]], 100, BASIC_Z - tz), WEIRD_R, colors[aux], true);
			weirds.push_back(weird);
//			obstacles.push_back(weird);
			aux = weirds.size() - 1;
			weirds[aux]->add_to_Visual2D(v2dp);
			added++;
			break;
		case 5:
		case 6:
			heart = new Heart(Point3D(location[lane[i]], 100, BASIC_Z - tz), HEART_R, colors[aux], true);
			hearts.push_back(heart);
//			obstacles.push_back(heart);
			aux = hearts.size() - 1;
			hearts[aux]->add_to_Visual2D(v2dp);
			added++;
			break;
		case 11:
			carObst = new CarObstacle(Point3D(location[lane[i]], 100, BASIC_Z - tz), colors[aux], true);
			cars.push_back(carObst);
//			obstacles.push_back(carObst);
			aux = cars.size() - 1;
			cars[aux]->add_to_Visual2D(v2dp);
			added++;
			break;
		default:
			hexacone = new Hexacone(Point3D(location[lane[i]], 0, BASIC_Z - tz), HEXACONE_R, 250);
			hexacones.push_back(hexacone);
//			obstacles.push_back(hexacone);
			aux = hexacones.size() - 1;
			hexacones[aux]->add_to_Visual2D(v2dp);
			added++;
			break;
		}
	}
	path->redraw(v2dp);
}

int create_colors() {
	//purple
	colors.push_back(Color(0.50, 0.00, 0.50));	// purple
	colors.push_back(Color(0.50, 0.00, 1.00));	// violet
	colors.push_back(Color(0.47, 0.32, 0.66));
	
	//blue
	colors.push_back(BLUE);
	colors.push_back(Color(0.13, 0.46, 0.99));
	colors.push_back(Color(0.46, 0.62, 0.80));
	colors.push_back(Color(0.00, 0.49, 0.65));
	colors.push_back(CYAN);

	// green
	colors.push_back(Color(0.44, 0.74, 0.47));
	colors.push_back(Color(0.50, 0.50, 0.00));	// olive
	colors.push_back(Color(0.00, 0.62, 0.42));
	colors.push_back(Color(0.01, 0.75, 0.23));
	colors.push_back(GREEN);
	colors.push_back(Color(0.67, 1.00, 0.18));
	//yellow
	colors.push_back(YELLOW);
	colors.push_back(Color(1.00, 0.84, 0.00));	// gold
	// orange
	colors.push_back(Color(1.00, 0.75, 0.00));
	colors.push_back(Color(1.00, 0.60, 0.40));
	colors.push_back(Color(1.00, 0.50, 0.00));	// orange
	//pink
	colors.push_back(Color(0.97, 0.51, 0.47));
	colors.push_back(Color(1.00, 0.75, 0.80));	// pink
	colors.push_back(MAGENTA);
	colors.push_back(Color(0.99, 0.07, 0.37));	// ruby
	//red
	colors.push_back(RED);
	colors.push_back(Color(0.77, 0.01, 0.20));
	

	/*
	vector<Text*> test;
	int i = 0;

	for (i = 0; i < colors.size(); i++) {
	test.push_back(new Text("lllllllll", Point2D(50, 10 + 10 * (i)), colors[i], BITMAP_HELVETICA_10));
	DrawingWindow::addText_to_Visual2D(test[i], v2dp);
	}
	//*/
	return colors.size();
}

void show_speed() {
	for (int i = 0; i < 25; i++) {
		speedCounterR.push_back(new Rectangle2D(Point2D(BOARD_WIDTH * 0.95, 150 + 8.5 * (i)), 25, 7, BLACK, true));
		DrawingWindow::addObject2D_to_Visual2D(speedCounterR[i], v2dp);
	}
}

void animate_speed(float speed) {
	int factor;

	factor = (int)speed / (7.5 + 2.5 * speedLevel) + speedLevel - 1;
//	factor = MAX(factor - speedLevel, 0);
	for (int i = factor; i < 25; i++) {
		speedCounterR[i]->change_color(BLACK);
	}

	factor = (int)speed / (7.5 + 2.5 * speedLevel) + speedLevel - 1;
	factor = MIN(factor, 25);
	for (int i = 0; i < factor; i++) {
		speedCounterR[i]->change_color(colors[i]);
	}


}

void show_life() {
	for (int i = 0; i < MAX_LIFES; i++) {
		lifeCounter.push_back(new Wheel(Point3D(-350 + (50 * i), 375, -1000), 25, 25));
		lifeCounter[i]->add_to_Visual2D(v2dp);
	}
}

void animate_life(int n) {
	static float angle = 0;
	angle += PI / 18;

	if (n < 0) {
		return;
	}

	for (int i = n; i < MAX_LIFES; i++) {
		lifeCounter[i]->wire->change_color(Color(0.20, 0.20, 0.20));
		lifeCounter[i]->full->change_color(Color(0.55, 0.57, 0.67));
		Transform3D::loadIdentityModelMatrix();
		Transform3D::applyTransform(lifeCounter[i]->wire);
		Transform3D::applyTransform(lifeCounter[i]->full);
	}
	for (int i = 0; i < n; i++) {
		Transform3D::loadIdentityModelMatrix();
		Transform3D::translateMatrix(-lifeCounter[i]->center.x, -lifeCounter[i]->center.y, -lifeCounter[i]->center.z);
		Transform3D::rotateMatrixOx(angle);
		Transform3D::translateMatrix(lifeCounter[i]->center.x, lifeCounter[i]->center.y, lifeCounter[i]->center.z);
		Transform3D::applyTransform(lifeCounter[i]->wire);
		Transform3D::applyTransform(lifeCounter[i]->full);
	}
}

void show_score() {
	scoreCounter.push_back(new Rectangle2D(Point2D(BOARD_WIDTH * 0.80, 50), 20, 300, BLACK, false));
	scoreDigits.push_back(new Text("", Point2D(BOARD_WIDTH * 0.805, 100), BLACK, BITMAP_TIMES_ROMAN_24));
	DrawingWindow::addObject2D_to_Visual2D(scoreCounter[0], v2ds);
	DrawingWindow::addText_to_Visual2D(scoreDigits[0], v2ds);

	for (int i = 1; i < MAX_DIGITS + 1; i++) {
		scoreCounter.push_back(new Rectangle2D(Point2D(BOARD_WIDTH * 0.80 + 25 * i, 50), 20, 300, BLACK, false));
		scoreDigits.push_back(new Text("0", Point2D(BOARD_WIDTH * 0.805 + 25 * i, 100), BLACK, BITMAP_TIMES_ROMAN_24));
		DrawingWindow::addObject2D_to_Visual2D(scoreCounter[i], v2ds);
		DrawingWindow::addText_to_Visual2D(scoreDigits[i], v2ds);
	}
}

void animate_score(int score) {
	int i, j;
	int d[MAX_DIGITS + 1];

	if (score > 0) {
		scoreDigits[0]->change_text("+");
	}
	if (score < 0) {
		scoreDigits[0]->change_text("-");
		score *= -1;
	}

	j = MAX_DIGITS - 1;
	for (i = 1; i < MAX_DIGITS + 1; i++) {
		d[i + j] = (int)(score / pow(10, i - 1)) % 10;
		j -= 2;
	}

	char *intStr = new char();
	string str = "";

	for (i = 1; i < MAX_DIGITS + 1; i++) {
		itoa(d[i], intStr, 10);
		str = string(intStr);
		scoreDigits[i]->change_text(str);
	}
}

void show_level() {
	levelCounter = new Text("LEVEL: 01", Point2D(10, 100), BLACK, BITMAP_HELVETICA_18);
	DrawingWindow::addText_to_Visual2D(levelCounter, v2ds);
}

void animate_level(int level) {

	char *intStr = new char();
	string str = "";
	itoa(level, intStr, 10);

	if (level < 10) {
		str += "LEVEL: 0";
	}
	else {
		str += "LEVEL: ";
	}
	str += string(intStr);

	levelCounter->change_text(str);
}

void show_time() {
	ttime = new Text("00 : 00" , Point2D(BOARD_WIDTH * 0.45, 100), BLACK, BITMAP_HELVETICA_18);
	DrawingWindow::addText_to_Visual2D(ttime, v2ds);
}

void animate_time(int sec) {
	int m, s, aux;
	
	time_left = MAX_TIME - (int)sec;
	m = (int)time_left / 60;
	s = (int)time_left % 60;

	string str = "";

	if (m < 10) {
		str += "0";
	}
	str += to_string(m);
	str += " : ";
	if (s < 10) {
		str += "0";
	}
	str += to_string(s);
		
	ttime->change_text(str);
}

void show_distance() {
	map = new Rectangle2D(Point2D(20, 100), 30, 300, ASPHALT, true);
	self = new Circle2D(Point2D(35, 105), 5, CAR_COL, true);
	l1 = new Line2D(Point2D(20, 100), Point2D(20, 400), WHITE);
	l2 = new Line2D(Point2D(30, 100), Point2D(30, 400), WHITE);
	l3 = new Line2D(Point2D(40, 100), Point2D(40, 400), WHITE);
	l4 = new Line2D(Point2D(50, 100), Point2D(50, 400), WHITE);
	start = new Line2D(Point2D(20, 100), Point2D(50, 100), WHITE);
	finish = new Rectangle2D(Point2D(20, 400), 30, 5, RED, true);

	DrawingWindow::addObject2D_to_Visual2D(self, v2dp);
	DrawingWindow::addObject2D_to_Visual2D(l1, v2dp);
	DrawingWindow::addObject2D_to_Visual2D(l2, v2dp);
	DrawingWindow::addObject2D_to_Visual2D(l3, v2dp);
	DrawingWindow::addObject2D_to_Visual2D(l4, v2dp);
	DrawingWindow::addObject2D_to_Visual2D(map, v2dp);
	DrawingWindow::addObject2D_to_Visual2D(start, v2dp);
	DrawingWindow::addObject2D_to_Visual2D(finish, v2dp);
}

void animate_distance(float tx, float ty, float tz) {
	float sx, sz;
	sz = tz * 0.000577;
	sx = tx * 0.036;

	Transform2D::loadIdentityMatrix();
	Transform2D::translateMatrix(-sx, sz);
	Transform2D::applyTransform(self);
}