#pragma once
#include "../Framework/DrawingWindow.h"
#include "../Framework/Visual2D.h"
#include "../Framework/Transform2D.h"
#include "../Framework/Transform3D.h"
#include "../Framework/Object3D.h"
#include <vector>

#define PI 3.141592653589793238462643383279502884197

class Wheel {
public:
	Object3D* wire;
	Object3D* full;

	Point3D center;

	float radius;
	float length;
	float angle;


	Wheel(Point3D p, float rad, float l) {
		int i, k;
		float fcos, fsin;
		float angle;

		center.x = p.x;
		center.y = p.y;
		center.z = p.z;

		radius = rad;
		length = l;

		// additional vectors
		vector <Point3D*> vertices;
		vector <Face*> faces;
		vector <int> contour;

		/* --------------------------------------------------------------------
		**					CREATE VERTICES one side at a time
		** ----------------------------------------------------------------- */
		angle = 0;
		for (k = 0; k < 2; k++) {
			for (i = 0; i < 12; i++) {
				fcos = cos(angle);
				fsin = sin(angle);
				vertices.push_back(new Point3D(p.x + l * k, p.y + rad * fcos, p.z + rad * fsin));
				angle += PI / 6;
			}
		}

		/* --------------------------------------------------------------------
		**							CREATE FACES
		**						THE SIDES OF THE WHEEL
		** ----------------------------------------------------------------- */
		for (k = 0; k < 2; k++) {
			contour.clear();
			for (i = 12 * k; i < 12 + 12 * k; i++) {
				contour.push_back(0 + i);
			}
			faces.push_back(new Face(contour));
		}
		/* -------------- THE PART OF THE WHEEL THAT'S THE TIRE ------------ */
		for (i = 0; i < 11; i++) {
			contour.clear();
			contour.push_back(0 + i);
			contour.push_back(1 + i);
			contour.push_back(13 + i);
			contour.push_back(12 + i);
			faces.push_back(new Face(contour));
		}
		contour.clear();
		contour.push_back(11);
		contour.push_back(0);
		contour.push_back(12);
		contour.push_back(23);
		faces.push_back(new Face(contour));

		/* --- INITIALIZE THE OBJECTS --- */
		
		wire = new Object3D(vertices, faces, Color(0.55, 0.57, 0.67), false);
		full = new Object3D(vertices, faces, Color(0.20, 0.20, 0.20), true);
		angle = 0;
	}
	
	void remove_from_Visual2D(Visual2D* v2d) {
		DrawingWindow::removeObject3D_from_Visual2D(wire, v2d);
		DrawingWindow::removeObject3D_from_Visual2D(full, v2d);
	}

	void add_to_Visual2D(Visual2D* v2d) {
		DrawingWindow::addObject3D_to_Visual2D(wire, v2d);
		DrawingWindow::addObject3D_to_Visual2D(full, v2d);
	}

	void transform(float tx, float ty, float tz, float _angle) {
		Transform3D::loadIdentityModelMatrix();

		Transform3D::translateMatrix(-tx, -ty, -tz);
		Transform3D::rotateMatrixOy(_angle);
		Transform3D::translateMatrix(tx, ty, tz);

		Transform3D::applyTransform(wire);
		Transform3D::applyTransform(full);

//s		rotate();
	}

private:
	void rotate() {
		angle += PI / 18;

		Transform3D::loadIdentityModelMatrix();

		Transform3D::translateMatrix(-center.x, -center.y, -center.z);
		Transform3D::rotateMatrixOx(angle);
		Transform3D::translateMatrix(center.x, center.y, center.z);

		Transform3D::applyTransform(wire);
		Transform3D::applyTransform(full);
	}

};