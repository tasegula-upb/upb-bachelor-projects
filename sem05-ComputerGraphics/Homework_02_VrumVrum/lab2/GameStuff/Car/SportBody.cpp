//*
#pragma once
#include "../../Framework/DrawingWindow.h"
#include "../../Framework/Visual2D.h"
#include "../../Framework/Transform2D.h"
#include "../../Framework/Transform3D.h"
#include "../../Framework/Object3D.h"
#include <vector>

#include "../x_defines.h"

class CarBody :public Object3D {
public:
	Object3D* full;

	Point3D center;
	float width, height, length;

	CarBody(Point3D p) {
		color.r = CAR_COL.r;
		color.g = CAR_COL.g;
		color.b = CAR_COL.b;
		fill = false;

		center.x = p.x;
		center.y = p.y;
		center.z = p.z;
		width = CAR_WIDTH;
		height = CAR_HEIGHT;
		length = CAR_LENGTH;

		/* --------------------------------------------------------------------
		** modificatori pentru a construi masina;
		**	mx =	0.5 * width oricare ar fi nivelul
		**	my =	creste odata cu nivelul
		**	mz =	se mofifica in interiorul fiecarui nivel, 
		** ----------------------------------------------------------------- */
		int mx, my, mz;
		mx = width * 0.5;

		// varfurile de nivel 0 a.k.a varfurile de jos
		my = height * 0.00;
		mz = length * 0.50;
				vertices.push_back(new Point3D(p.x - mx, p.y + my, p.z - mz));	// 00
		transf_vertices.push_back(new Point3D(p.x - mx, p.y + my, p.z - mz));
				vertices.push_back(new Point3D(p.x + mx, p.y + my, p.z - mz));	// 01
		transf_vertices.push_back(new Point3D(p.x + mx, p.y + my, p.z - mz));
				vertices.push_back(new Point3D(p.x + mx, p.y + my, p.z + mz));	// 02
		transf_vertices.push_back(new Point3D(p.x + mx, p.y + my, p.z + mz));
				vertices.push_back(new Point3D(p.x - mx, p.y + my, p.z + mz));	// 03
		transf_vertices.push_back(new Point3D(p.x - mx, p.y + my, p.z + mz));

		// varfurile de nivel 1
		my = height * 2/7.0;
		mz = length * 0.50;
				vertices.push_back(new Point3D(p.x - mx, p.y + my, p.z - mz));	// 04
		transf_vertices.push_back(new Point3D(p.x - mx, p.y + my, p.z - mz));
				vertices.push_back(new Point3D(p.x + mx, p.y + my, p.z - mz));	// 05
		transf_vertices.push_back(new Point3D(p.x + mx, p.y + my, p.z - mz));
				vertices.push_back(new Point3D(p.x + mx, p.y + my, p.z + mz));	// 06
		transf_vertices.push_back(new Point3D(p.x + mx, p.y + my, p.z + mz));
				vertices.push_back(new Point3D(p.x - mx, p.y + my, p.z + mz));	// 07
		transf_vertices.push_back(new Point3D(p.x - mx, p.y + my, p.z + mz));

		// varfurile de nivel 2
		my = height * 4/7.0;
		mz = length * 0.30;
				vertices.push_back(new Point3D(p.x - mx, p.y + my, p.z - mz));	// 08
		transf_vertices.push_back(new Point3D(p.x - mx, p.y + my, p.z - mz));
				vertices.push_back(new Point3D(p.x + mx, p.y + my, p.z - mz));	// 09
		transf_vertices.push_back(new Point3D(p.x + mx, p.y + my, p.z - mz));
		mz = length * 0.50;
				vertices.push_back(new Point3D(p.x + mx, p.y + my, p.z + mz));	// 10
		transf_vertices.push_back(new Point3D(p.x + mx, p.y + my, p.z + mz));
				vertices.push_back(new Point3D(p.x - mx, p.y + my, p.z + mz));	// 11
		transf_vertices.push_back(new Point3D(p.x - mx, p.y + my, p.z + mz));

		// varfurile de nivel 3
		my = height * 4 / 7.0;
		mz = length * 0.20;
				vertices.push_back(new Point3D(p.x - mx, p.y + my, p.z - mz));	// 12
		transf_vertices.push_back(new Point3D(p.x - mx, p.y + my, p.z - mz));
				vertices.push_back(new Point3D(p.x + mx, p.y + my, p.z - mz));	// 13
		transf_vertices.push_back(new Point3D(p.x + mx, p.y + my, p.z - mz));
		mz = length * 0.40;
				vertices.push_back(new Point3D(p.x + mx, p.y + my, p.z + mz));	// 14
		transf_vertices.push_back(new Point3D(p.x + mx, p.y + my, p.z + mz));
				vertices.push_back(new Point3D(p.x - mx, p.y + my, p.z + mz));	// 15
		transf_vertices.push_back(new Point3D(p.x - mx, p.y + my, p.z + mz));

		// varfurile de nivel 4 a.k.a varfurile de sus
		my = height * 1.00;
			vertices.push_back(new Point3D(p.x - mx, p.y + my, p.z));			// 16
		transf_vertices.push_back(new Point3D(p.x - mx, p.y + my, p.z));
			vertices.push_back(new Point3D(p.x + mx, p.y + my, p.z));			// 17
		transf_vertices.push_back(new Point3D(p.x + mx, p.y + my, p.z));
		mz = length * 0.30;
				vertices.push_back(new Point3D(p.x + mx, p.y + my, p.z + mz));	// 18
		transf_vertices.push_back(new Point3D(p.x + mx, p.y + my, p.z + mz));
				vertices.push_back(new Point3D(p.x - mx, p.y + my, p.z + mz));	// 19
		transf_vertices.push_back(new Point3D(p.x - mx, p.y + my, p.z + mz));

		// cele 4 nivele + 4*4 fete
		vector <int> contour;
		int i = 0;
		// creez nivelele
		for (i = 0; i < 13; i += 4) {
			contour.clear();
			contour.push_back(0 + i);
			contour.push_back(1 + i);
			contour.push_back(2 + i);
			contour.push_back(3 + i);
			faces.push_back(new Face(contour));
		}

		// make the laterals of each level
		i = 0;

		while(i != -1) {
			// front 
			contour.clear();
			contour.push_back(0 + i);
			contour.push_back(1 + i);
			contour.push_back(5 + i);
			contour.push_back(4 + i);
			faces.push_back(new Face(contour));
			// right
			contour.clear();
			contour.push_back(1 + i);
			contour.push_back(2 + i);
			contour.push_back(6 + i);
			contour.push_back(5 + i);
			faces.push_back(new Face(contour));
			// back 
			contour.clear();
			contour.push_back(2 + i);
			contour.push_back(3 + i);
			contour.push_back(7 + i);
			contour.push_back(6 + i);
			faces.push_back(new Face(contour));
			// left 
			contour.clear();
			contour.push_back(3 + i);
			contour.push_back(0 + i);
			contour.push_back(4 + i);
			contour.push_back(7 + i);
			faces.push_back(new Face(contour));
			
			i += 4;
			if (i == 16) {
				i = -1;
			}
		}

		full = new Object3D(vertices, faces, car_int, true);
	}
	void add_to_Visual2D(Visual2D* v2d) {
		DrawingWindow::addObject3D_to_Visual2D(this, v2d);
		DrawingWindow::addObject3D_to_Visual2D(full, v2d);
	}

	void transform(float angle) {
		Transform3D::loadIdentityModelMatrix();
		Transform3D::translateMatrix(-center.x, -center.y, -center.z);
		Transform3D::rotateMatrixOy(angle);
		Transform3D::translateMatrix(center.x, center.y, center.z);
		Transform3D::applyTransform(this);
		Transform3D::applyTransform(full);
	}
};
//*/