#pragma once
#include "../Framework/DrawingWindow.h"
#include "../Framework/Visual2D.h"
#include "../Framework/Transform2D.h"
#include "../Framework/Transform3D.h"
#include "../Framework/Object3D.h"

#include "Car/CarBody.cpp"
#include "Car/Wheel.cpp"
#include "x_defines.h"

class CarObstacle {
public:

	CarBody* wire;
	CarBody* full;
	vector<Wheel*> wheels;

	Point3D center;

	bool colored;
	int type;
	float radius;

	float dist;

	CarObstacle(Point3D p, Color _color, bool _type) {
		center.x = p.x;
		center.y = p.y;
		center.z = p.z;

		colored = _type;
		type = 11;

		radius = CAR_LENGTH * 0.5;

		dist = 0;

		wire = new CarBody(p, WHITE, false, CAR_WIDTH * 2, CAR_HEIGHT * 2, CAR_LENGTH * 2);
		full = new CarBody(p, _color, true, CAR_WIDTH * 2, CAR_HEIGHT * 2, CAR_LENGTH * 2);

		wheels.push_back(new Wheel(Point3D(p.x - CAR_WIDTH * 0.55, p.y, p.z - CAR_LENGTH * 0.375), 5, 10));
		wheels.push_back(new Wheel(Point3D(p.x + CAR_WIDTH * 0.35, p.y, p.z - CAR_LENGTH * 0.375), 5, 10));
		wheels.push_back(new Wheel(Point3D(p.x + CAR_WIDTH * 0.35, p.y, p.z + CAR_LENGTH * 0.375), 5, 10));
		wheels.push_back(new Wheel(Point3D(p.x - CAR_WIDTH * 0.55, p.y, p.z + CAR_LENGTH * 0.375), 5, 10));

	}

	void add_to_Visual2D(Visual2D* v2d) {
		DrawingWindow::addObject3D_to_Visual2D(wire, v2d);
		DrawingWindow::addObject3D_to_Visual2D(full, v2d);

		for (int i = 0; i < wheels.size(); i++) {
			wheels[i]->add_to_Visual2D(v2d);
		}
	}

	void remove_from_Visual2D(Visual2D* v2d) {
		DrawingWindow::removeObject3D_from_Visual2D(wire, v2d);
		DrawingWindow::removeObject3D_from_Visual2D(full, v2d);

		for (int i = 0; i < wheels.size(); i++) {
			wheels[i]->remove_from_Visual2D(v2d);
		}
	}

	void translate(float tx, float ty, float tz) {
		dist += 25;
		Transform3D::loadIdentityModelMatrix();
		Transform3D::translateMatrix(tx, ty, tz);
		Transform3D::applyTransform(wire);
		Transform3D::applyTransform(full);

		for (int i = 0; i < wheels.size(); i++) {
			wheels[i]->transform(center.x, center.y, center.z, 0);
		}
	}

};