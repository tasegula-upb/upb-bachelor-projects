#pragma once
#include "../Framework/DrawingWindow.h"
#include "../Framework/Visual2D.h"
#include "../Framework/Transform2D.h"
#include "../Framework/Transform3D.h"
#include "../Framework/Object3D.h"
#include <vector>

#define PI 3.141592653589793238462643383279502884197

class Obstacle :public Object3D {
public:
	Object3D* wire;
	Object3D* full;

	Point3D center;
	bool colored;
	int type;		// tipul obstacolului;
	float angle;	// unghiul de rotatie;
	float dist, ant;

	Obstacle() {
		angle = 0;
		colored = true;
		type = 0;
	}

	void add_to_Visual2D(Visual2D* v2d) {
		DrawingWindow::addObject3D_to_Visual2D(wire, v2d);
		DrawingWindow::addObject3D_to_Visual2D(full, v2d);
	}

	void translate(float tx, float ty, float tz) {
		Transform3D::loadIdentityModelMatrix();
		Transform3D::translateMatrix(tx, ty, tz);
		Transform3D::applyTransform(wire);
		Transform3D::applyTransform(full);
	}

	void remove_from_Visual2D(Visual2D* v2d) {
		DrawingWindow::removeObject3D_from_Visual2D(wire, v2d);
		DrawingWindow::removeObject3D_from_Visual2D(full, v2d);
	}
};