#pragma once
#include "../Framework/DrawingWindow.h"
#include "../Framework/Visual2D.h"
#include "../Framework/Transform2D.h"
#include "../Framework/Transform3D.h"
#include "../Framework/Object3D.h"
#include <vector>
#include "Obstacle.h"

#include "x_defines.h"

#define PI 3.141592653589793238462643383279502884197

class Cone :public Obstacle {
public:
/* ITEMS FROM CLASS OBSTACLE
	Object3D* wire;
	Object3D* full;
	Point3D center;
	bool colored;
	int type;
//*/
	float radius;
	float length;
	float angle;

	Cone(Point3D p, float rad, float l, Color c, bool _type) {

		center.x = p.x;
		center.y = p.y;
		center.z = p.z;
		radius = rad;
		length = l;
		colored = _type;
		type = 3;
		angle = 0;

		float fact = cos(PI / 4);

		// fetele + baza
		vector <Point3D*> vertices;
		vector <Face*> faces;
		vector <int> contour;

		//baza
		vertices.push_back(new Point3D(p.x + rad * 1.0, p.y, p.z + rad * 0.0));
		vertices.push_back(new Point3D(p.x + rad * fact, p.y, p.z + rad * fact));
		vertices.push_back(new Point3D(p.x + rad * 0.0, p.y, p.z + rad * 1.0));
		vertices.push_back(new Point3D(p.x - rad * fact, p.y, p.z + rad * fact));
		vertices.push_back(new Point3D(p.x - rad * 1.0, p.y, p.z - rad * 0.0));
		vertices.push_back(new Point3D(p.x - rad * fact, p.y, p.z - rad * fact));
		vertices.push_back(new Point3D(p.x + rad * 0.0, p.y, p.z - rad * 1.0));
		vertices.push_back(new Point3D(p.x + rad * fact, p.y, p.z - rad * fact));
		// varful
		vertices.push_back(new Point3D(p.x, p.y + l, p.z));

		//baza
		contour.clear();
		contour.push_back(0);
		contour.push_back(1);
		contour.push_back(2);
		contour.push_back(3);
		contour.push_back(4);
		contour.push_back(5);
		contour.push_back(6);
		contour.push_back(7);
		faces.push_back(new Face(contour));
		//fete
		contour.clear();
		contour.push_back(0);
		contour.push_back(8);
		contour.push_back(1);
		faces.push_back(new Face(contour));
		contour.clear();
		contour.push_back(1);
		contour.push_back(8);
		contour.push_back(2);
		faces.push_back(new Face(contour));
		contour.clear();
		contour.push_back(2);
		contour.push_back(8);
		contour.push_back(3);
		faces.push_back(new Face(contour));
		contour.clear();
		contour.push_back(3);
		contour.push_back(8);
		contour.push_back(4);
		faces.push_back(new Face(contour));
		contour.clear();
		contour.push_back(4);
		contour.push_back(8);
		contour.push_back(5);
		faces.push_back(new Face(contour));
		contour.clear();
		contour.push_back(5);
		contour.push_back(8);
		contour.push_back(6);
		faces.push_back(new Face(contour));
		contour.clear();
		contour.push_back(6);
		contour.push_back(8);
		contour.push_back(7);
		faces.push_back(new Face(contour));
		contour.clear();
		contour.push_back(7);
		contour.push_back(8);
		contour.push_back(0);
		faces.push_back(new Face(contour));

		wire = new Object3D(vertices, faces, WHITE, false);
		if (_type) {
			full = new Object3D(vertices, faces, Color(1 - c.r, 1 - c.g, 1 - c.b), true);
		}
		else {
			full = new Object3D();
		}
	}

	void translate(float tx, float ty, float tz) {
		angle += PI * 0.3;

		Transform3D::loadIdentityModelMatrix();
		Transform3D::translateMatrix(-center.x, -center.y, -center.z);
		Transform3D::rotateMatrixOy(angle);
		Transform3D::translateMatrix(center.x + tx, center.y + ty, center.z + tz);
//		Transform3D::translateMatrix(tx, ty, tz);
		Transform3D::applyTransform(wire);
		Transform3D::applyTransform(full);
	}

};