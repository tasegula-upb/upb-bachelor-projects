#pragma once
#include "../Framework/DrawingWindow.h"
#include "../Framework/Visual2D.h"
#include "../Framework/Transform2D.h"
#include "../Framework/Transform3D.h"
#include "../Framework/Object3D.h"
#include <vector>
#include "Obstacle.h"
#include "x_defines.h"

#define PI 3.141592653589793238462643383279502884197

class Pyramid :public Obstacle {
public:
/* ITEMS FROM CLASS OBSTACLE
	Object3D* wire;
	Object3D* full;
	Point3D center;
	bool colored;
	int type;
//*/
	float radius;
	float length;

	Pyramid(Point3D p, float r, float l, Color c, bool _type) {

		center.x = p.x;
		center.y = p.y;
		center.z = p.z;

		radius = r;
		length = l;
		colored = _type;
		type = 2;

		//vectori
		vector <int> contour;
		vector <Point3D*> vertices;
		vector <Face*> faces;

		//baza
		vertices.push_back(new Point3D(p.x - r, p.y, p.z + r));
		vertices.push_back(new Point3D(p.x + r, p.y, p.z + r));
		vertices.push_back(new Point3D(p.x + r, p.y, p.z - r));
		vertices.push_back(new Point3D(p.x - r, p.y, p.z - r));
		
		// varful
		vertices.push_back(new Point3D(p.x, p.y + l, p.z));

		//fete
		contour.clear();
		contour.push_back(0);
		contour.push_back(1);
		contour.push_back(4);
		faces.push_back(new Face(contour));
		contour.clear();
		contour.push_back(1);
		contour.push_back(2);
		contour.push_back(4);
		faces.push_back(new Face(contour));
		contour.clear();
		contour.push_back(2);
		contour.push_back(3);
		contour.push_back(4);
		faces.push_back(new Face(contour));
		contour.clear();
		contour.push_back(3);
		contour.push_back(0);
		contour.push_back(4);
		faces.push_back(new Face(contour));
		
		//baza
		contour.clear();
		contour.push_back(0);
		contour.push_back(1);
		contour.push_back(2);
		contour.push_back(3);
		faces.push_back(new Face(contour));
		//*/	
		
		wire = new Object3D(vertices, faces, WHITE, false);
		if (_type) {
			full = new Object3D(vertices, faces, Color(1 - c.r, 1 - c.g, 1 - c.b), true);
		}
		else {
			full = new Object3D();
		}
	}
};