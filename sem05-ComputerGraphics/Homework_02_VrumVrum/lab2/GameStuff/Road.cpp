#pragma once
#include "../Framework/DrawingWindow.h"
#include "../Framework/Visual2D.h"
#include "../Framework/Transform2D.h"
#include "../Framework/Transform3D.h"
#include "../Framework/Object3D.h"
#include "../Framework/Rectangle2d.h"

#include "x_defines.h"
#include <vector>

//*
class Line3D {

public:
	Object3D* obj;

	Line3D(Point3D p, float w, float h, Color c, bool type) {

		// the auxialiary vectors
		vector <int> contour;
		vector <Point3D*> vertices;
		vector <Face*> faces;

		//vertices
		vertices.push_back(new Point3D(p.x + 0, p.y, p.z));
		vertices.push_back(new Point3D(p.x + w, p.y, p.z));
		vertices.push_back(new Point3D(p.x + w, p.y, p.z + h));
		vertices.push_back(new Point3D(p.x + 0, p.y, p.z + h));

		// create the line (a thickness cuboid);
		contour.clear();
		contour.push_back(0);
		contour.push_back(1);
		contour.push_back(2);
		contour.push_back(3);
		faces.push_back(new Face(contour));

		obj = new Object3D(vertices, faces, c, type);
	}
};
//*/

class Road {
public:
	vector<Line3D*> lines;
	Object3D* asphalt;
	Object3D* grass1;
	Object3D* grass2;

	Road(Point3D p) {
		for (int i = 0; i < 4; i++) {
			lines.push_back(new Line3D(Point3D(0, 0, 0), 10, inf, WHITE, true));
		}

		//vectori
		vector <int> contour;
		vector <Point3D*> vertices;
		vector <Face*> faces;

		vertices.clear();
		vertices.push_back(new Point3D(0, 0, 0));
		vertices.push_back(new Point3D(BOARD_WIDTH, 0, 0));
		vertices.push_back(new Point3D(BOARD_WIDTH, 0, inf));
		vertices.push_back(new Point3D(0, 0, inf));
		contour.clear();
		contour.push_back(0);
		contour.push_back(1);
		contour.push_back(2);
		contour.push_back(3);
		faces.push_back(new Face(contour));
		asphalt = new Object3D(vertices, faces, ASPHALT, true);
		
		vertices.clear();
		vertices.push_back(new Point3D(-BOARD_WIDTH * 10, 0, 0));
		vertices.push_back(new Point3D(0, 0, 0));
		vertices.push_back(new Point3D(0, 0, inf));
		vertices.push_back(new Point3D(-BOARD_WIDTH * 10, 0, inf));
		contour.clear();
		contour.push_back(0);
		contour.push_back(1);
		contour.push_back(2);
		contour.push_back(3);
		faces.push_back(new Face(contour));
		grass1 = new Object3D(vertices, faces, GRASS, true);

		vertices.clear();
		vertices.push_back(new Point3D(BOARD_WIDTH, 0, 0));
		vertices.push_back(new Point3D(BOARD_WIDTH * 10, 0, 0));
		vertices.push_back(new Point3D(BOARD_WIDTH * 10, 0, inf));
		vertices.push_back(new Point3D(BOARD_WIDTH, 0, inf));
		contour.clear();
		contour.push_back(0);
		contour.push_back(1);
		contour.push_back(2);
		contour.push_back(3);
		faces.push_back(new Face(contour));
		grass2 = new Object3D(vertices, faces, GRASS, true);
	}

	void add_to_Visual2D(Visual2D* v2d) {
		for (int i = 0; i < lines.size() ; i++) {
			DrawingWindow::addObject3D_to_Visual2D(lines[i]->obj, v2d);
		}
		DrawingWindow::addObject3D_to_Visual2D(asphalt, v2d);
		DrawingWindow::addObject3D_to_Visual2D(grass1, v2d);
		DrawingWindow::addObject3D_to_Visual2D(grass2, v2d);
	}

	void transform(float tx, float ty, float tz) {
		for (int i = 0; i < lines.size(); i++) {
			Transform3D::loadIdentityModelMatrix();
			Transform3D::translateMatrix(((BOARD_WIDTH / 3) * i) + tx, 0, 0);
			Transform3D::applyTransform(lines[i]->obj);
		}
		Transform3D::loadIdentityModelMatrix();
		Transform3D::translateMatrix(tx, 0, 0);
		Transform3D::applyTransform(asphalt);
		Transform3D::applyTransform(grass1);
		Transform3D::applyTransform(grass2);
	}

	void redraw(Visual2D* v2d) {
		for (int i = 0; i < lines.size(); i++) {
			DrawingWindow::removeObject3D_from_Visual2D(lines[i]->obj, v2d);
		}
		DrawingWindow::removeObject3D_from_Visual2D(asphalt, v2d);
		DrawingWindow::removeObject3D_from_Visual2D(grass1, v2d);
		DrawingWindow::removeObject3D_from_Visual2D(grass2, v2d);

		add_to_Visual2D(v2d);
	}
};

