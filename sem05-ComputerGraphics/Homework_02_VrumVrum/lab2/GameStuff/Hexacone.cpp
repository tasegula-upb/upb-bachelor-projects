#pragma once
#include "../Framework/DrawingWindow.h"
#include "../Framework/Visual2D.h"
#include "../Framework/Transform2D.h"
#include "../Framework/Transform3D.h"
#include "../Framework/Object3D.h"
#include <vector>
#include "Obstacle.h"

#include "x_defines.h"

class Hexacone :public Obstacle {
public:
/* ITEMS FROM CLASS OBSTACLE
	Object3D* wire;
	Object3D* full;
	Point3D center;
	bool colored;
	int type;
//*/
	Object3D* obj;
	Object3D *f1, *f2, *f3, *f4, *f5, *f6;

	float radius;
	float height;
	float angle;

	Hexacone(Point3D p, float r, float h) {

		center.x = p.x;
		center.y = p.y;
		center.z = p.z;

		radius = r;
		height = h;
		type = 7;

		angle = PI / 6;		// 30 degrees

		//cele 6 fete + baza
		vector <Point3D*> vertices;
		vector <Face*> faces;
		vector <int> contour;

		//centrul
		vertices.push_back(new Point3D(p.x, p.y, p.z));

		//baza
		vertices.push_back(new Point3D(p.x + r * cos(angle), p.y, p.z + r * sin(angle)));
		angle += PI / 3;
		vertices.push_back(new Point3D(p.x + r * cos(angle), p.y, p.z + r * sin(angle)));
		angle += PI / 3;
		vertices.push_back(new Point3D(p.x + r * cos(angle), p.y, p.z + r * sin(angle)));
		angle += PI / 3;
		vertices.push_back(new Point3D(p.x + r * cos(angle), p.y, p.z + r * sin(angle)));
		angle += PI / 3;
		vertices.push_back(new Point3D(p.x + r * cos(angle), p.y, p.z + r * sin(angle)));
		angle += PI / 3;
		vertices.push_back(new Point3D(p.x + r * cos(angle), p.y, p.z + r * sin(angle)));

		// varful
		vertices.push_back(new Point3D(p.x, p.y + height, p.z));

		//baza
		contour.clear();
		contour.push_back(1);
		contour.push_back(2);
		contour.push_back(3);
		contour.push_back(4);
		contour.push_back(5);
		contour.push_back(6);
		contour.push_back(7);
		faces.push_back(new Face(contour));
		obj = new Object3D(vertices, faces, BLACK, true);

		//fete
		contour.clear();
		faces.clear();
//		contour.push_back(0);
		contour.push_back(1);
		contour.push_back(2);
		contour.push_back(7);
		faces.push_back(new Face(contour));
		f1 = new Object3D(vertices, faces, RED, true);

		contour.clear();
		faces.clear();
//		contour.push_back(0);
		contour.push_back(2);
		contour.push_back(3);
		contour.push_back(7);
		faces.push_back(new Face(contour));
		f2 = new Object3D(vertices, faces, YELLOW, true);

		contour.clear();
		faces.clear();
//		contour.push_back(0);
		contour.push_back(3);
		contour.push_back(4);
		contour.push_back(7);
		faces.push_back(new Face(contour));
		f3 = new Object3D(vertices, faces, GREEN, true);

		contour.clear();
		faces.clear();
//		contour.push_back(0);
		contour.push_back(4);
		contour.push_back(5);
		contour.push_back(7);
		faces.push_back(new Face(contour));
		f4 = new Object3D(vertices, faces, CYAN, true);

		contour.clear();
		faces.clear();
//		contour.push_back(0);
		contour.push_back(5);
		contour.push_back(6);
		contour.push_back(7);
		faces.push_back(new Face(contour));
		f5 = new Object3D(vertices, faces, BLUE, true);

		contour.clear();
		faces.clear();
//		contour.push_back(0);
		contour.push_back(6);
		contour.push_back(1);
		contour.push_back(7);
		faces.push_back(new Face(contour));
		f6 = new Object3D(vertices, faces, MAGENTA, true);

		// from the parent
		wire = new Object3D();
		full = new Object3D();

		// re-initialize the angle
		angle = 0;
	}

	void add_to_Visual2D(Visual2D *v2d) {
		DrawingWindow::addObject3D_to_Visual2D(f1, v2d);
		DrawingWindow::addObject3D_to_Visual2D(f2, v2d);
		DrawingWindow::addObject3D_to_Visual2D(f3, v2d);
		DrawingWindow::addObject3D_to_Visual2D(f4, v2d);
		DrawingWindow::addObject3D_to_Visual2D(f5, v2d);
		DrawingWindow::addObject3D_to_Visual2D(f6, v2d);
		DrawingWindow::addObject3D_to_Visual2D(obj, v2d);
	}

	void translate(float tx, float ty, float tz) {
		angle += PI * 0.10;

		Transform3D::loadIdentityModelMatrix();
		Transform3D::translateMatrix(-center.x, -center.y, -center.z);
		Transform3D::rotateMatrixOy(angle);
		Transform3D::translateMatrix(center.x + tx, center.y + ty, center.z + tz);
		Transform3D::applyTransform(f1);
		Transform3D::applyTransform(f2);
		Transform3D::applyTransform(f3);
		Transform3D::applyTransform(f4);
		Transform3D::applyTransform(f5);
		Transform3D::applyTransform(f6);
		Transform3D::applyTransform(obj);
	}

	void remove_from_Visual2D(Visual2D* v2d) {
		DrawingWindow::removeObject3D_from_Visual2D(obj, v2d);
		DrawingWindow::removeObject3D_from_Visual2D(f1, v2d);
		DrawingWindow::removeObject3D_from_Visual2D(f2, v2d);
		DrawingWindow::removeObject3D_from_Visual2D(f3, v2d);
		DrawingWindow::removeObject3D_from_Visual2D(f4, v2d);
		DrawingWindow::removeObject3D_from_Visual2D(f5, v2d);
		DrawingWindow::removeObject3D_from_Visual2D(f6, v2d);
	}
	
};