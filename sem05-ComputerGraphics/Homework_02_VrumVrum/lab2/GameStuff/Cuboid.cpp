#pragma once
#include "../Framework/DrawingWindow.h"
#include "../Framework/Visual2D.h"
#include "../Framework/Transform2D.h"
#include "../Framework/Transform3D.h"
#include "../Framework/Object3D.h"
#include <vector>

class Cuboid :public Object3D {
public: 
//	Object3D* full;
//	Object3D* wire;
	Point3D center;
	float width, height, length;

	Cuboid(Point3D p, float w, float h, float l, Color c, bool type) {
		color.r = c.r;
		color.g = c.g;
		color.b = c.b;
		fill = type;

		center.x = p.x;
		center.y = p.y;
		center.z = p.z;
		width = w;
		height = h;
		length = l;

		//varfurile de jos
		vertices.push_back(new Point3D(p.x - w / 2, p.y - h / 2, p.z - l / 2));
		transf_vertices.push_back(new Point3D(p.x - w / 2, p.y - h / 2, p.z - l / 2));
		vertices.push_back(new Point3D(p.x + w / 2, p.y - h / 2, p.z - l / 2));
		transf_vertices.push_back(new Point3D(p.x + w / 2, p.y - h / 2, p.z - l / 2));
		vertices.push_back(new Point3D(p.x + w / 2, p.y - h / 2, p.z + l / 2));
		transf_vertices.push_back(new Point3D(p.x + w / 2, p.y - h / 2, p.z + l / 2));
		vertices.push_back(new Point3D(p.x - w / 2, p.y - h / 2, p.z + l / 2));
		transf_vertices.push_back(new Point3D(p.x - w / 2, p.y - h / 2, p.z + l / 2));
		//varfurile de sus
		vertices.push_back(new Point3D(p.x - w / 2, p.y + h / 2, p.z - l / 2));
		transf_vertices.push_back(new Point3D(p.x - w / 2, p.y + h / 2, p.z - l / 2));
		vertices.push_back(new Point3D(p.x + w / 2, p.y + h / 2, p.z - l / 2));
		transf_vertices.push_back(new Point3D(p.x + w / 2, p.y + h / 2, p.z - l / 2));
		vertices.push_back(new Point3D(p.x + w / 2, p.y + h / 2, p.z + l / 2));
		transf_vertices.push_back(new Point3D(p.x + w / 2, p.y + h / 2, p.z + l / 2));
		vertices.push_back(new Point3D(p.x - w / 2, p.y + h / 2, p.z + l / 2));
		transf_vertices.push_back(new Point3D(p.x - w / 2, p.y + h / 2, p.z + l / 2));

		//cele 6 fete
		vector <int> contour;

		//fata jos
		contour.clear();
		contour.push_back(0);
		contour.push_back(1);
		contour.push_back(2);
		contour.push_back(3);
		faces.push_back(new Face(contour));
		//fata sus
		contour.clear();
		contour.push_back(4);
		contour.push_back(5);
		contour.push_back(6);
		contour.push_back(7);
		faces.push_back(new Face(contour));
		//fata fata
		contour.clear();
		contour.push_back(0);
		contour.push_back(1);
		contour.push_back(5);
		contour.push_back(4);
		faces.push_back(new Face(contour));
		//fata dreapta
		contour.clear();
		contour.push_back(1);
		contour.push_back(2);
		contour.push_back(6);
		contour.push_back(5);
		faces.push_back(new Face(contour));
		//fata spate
		contour.clear();
		contour.push_back(2);
		contour.push_back(3);
		contour.push_back(7);
		contour.push_back(6);
		faces.push_back(new Face(contour));
		//fata stanga
		contour.clear();
		contour.push_back(3);
		contour.push_back(0);
		contour.push_back(4);
		contour.push_back(7);
		faces.push_back(new Face(contour));
/*
		wire = new Object3D(vertices, faces, c, false);
		if (type) {
			full = new Object3D(vertices, faces, Color(1 - c.r, 1 - c.g, 1 - c.b), true);
		}
		else {
			full = new Object3D();
		}
//*/
	}
/*
	void add_to_Visual2D(Visual2D* v2d) {
		DrawingWindow::addObject3D_to_Visual2D(this, v2d);
		DrawingWindow::addObject3D_to_Visual2D(wire, v2d);
		DrawingWindow::addObject3D_to_Visual2D(full, v2d);
	}

	void translate(float tx, float ty, float tz) {
		Transform3D::loadIdentityModelMatrix();
		Transform3D::translateMatrix(tx, ty, tz);
		Transform3D::applyTransform(this);
		Transform3D::applyTransform(wire);
		Transform3D::applyTransform(full);
	}
//*/
};