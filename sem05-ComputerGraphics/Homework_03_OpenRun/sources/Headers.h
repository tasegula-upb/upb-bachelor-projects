#pragma once

/**	---------------------------------------------------------------------------
 **	FROM THE LAB 
 **	------------------------------------------------------------------------ */
//incarcator de meshe
#include "../lab_mesh_loader.hpp"
//geometrie: drawSolidCube, drawWireTeapot...
#include "../lab_geometry.hpp"
//incarcator de shadere
#include "../lab_shader_loader.hpp"
//interfata cu glut, ne ofera fereastra, input, context opengl
#include "../lab_glut.hpp"
//camera
#include "../lab_camera.hpp"

/**	---------------------------------------------------------------------------
 **		FROM WINDOWS
 **	------------------------------------------------------------------------ */
//time
#include <ctime>

/**	---------------------------------------------------------------------------
 **		FROM ME
 **	------------------------------------------------------------------------ */
#include "defines.h"
#include "variables.h"