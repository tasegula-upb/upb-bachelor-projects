#pragma once
#include "..\dependente\glew\glew.h"
#include "..\dependente\glm\glm.hpp"

struct vertex {
	glm::vec3 position;
	glm::vec3 color;
	glm::vec2 jiggle = glm::vec2(0.00, 0.00);
};