#pragma once
#include <cmath>

/** ---------------------------------------------------------------------------
**		MATH
** ------------------------------------------------------------------------ */
const double e = std::exp(1.0);
#define		PI	 3.14159265358

/** ---------------------------------------------------------------------------
**		COLORS
** ------------------------------------------------------------------------ */
#define WHITE		glm::vec3(1, 1, 1)
#define BLACK		glm::vec3(0, 0, 0)
#define trackColor	glm::vec3(0.00, 0.42, 0.235)


#define RADIUS	120
#define trackRatio	2.312
#define FPX 0
#define FPY 300
#define FPZ	200

/** ---------------------------------------------------------------------------
**		KEYS
** ------------------------------------------------------------------------ */
#define UP		0
#define DOWN	1
#define RIGHT	2
#define LEFT	3
#define CAM0	4
#define CAM1	5
#define CAM2	6
#define CAM3	7
#define CAM4	8
#define CAM5	9


/** ---------------------------------------------------------------------------
**		CHARACTERS
** ------------------------------------------------------------------------ */
#define MAX_CHAR	3		// max number of characters on track

#define ANDROID		0
#define IRONMAN		1
#define SUPERMAN	2
#define LEGO		3
#define THOR		4
#define HAMMER		5
#define GOKU		6
#define BULBASAUR	7
#define CHAMANDER	8
#define	SQUIRTLE	9