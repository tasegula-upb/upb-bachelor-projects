#pragma once
#include "..\dependente\glew\glew.h"
#include "..\dependente\glm\glm.hpp"
#include "..\dependente\glm\gtc\type_ptr.hpp"
#include "..\dependente\glm\gtc\matrix_transform.hpp"
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>

#include "defines.h"
#include "Vertex.h"

using namespace std;

class Track {
private:
	float radius;
	float width;
	float length;	// total length
	float size;		// the length of the straight part
	
	int nlane;
	
	glm::vec3 color;

	vector<vertex> vertices;
	vector<unsigned int> indices;

public:
	float startDistance[3];
	unsigned int vbo, ibo, vao, num_indices;

	Track(float _radius, glm::vec3 _color, int _nlane) {
		float x = 0;
		float y = 0;
		float z = 0;
		radius = _radius;
		color = _color;
		//	all the lanes should be half radius, while one lane should be a quarter of the track; 
		width = radius * 0.50 * 0.20;
		//	the straight part of the track has the size:
		size = radius * trackRatio;
		//	the length of the track
		length = size + radius * 2;
		//	how many lanes has the track
		nlane = _nlane;

		//	calculate the distance between the start points
		float aux = 2 * PI * width;
		for (int i = 0; i < 3; i++) {
			startDistance[i] = -size * 0.3 - (i * aux);
		}

		//	auxiliars used to generate the running track;
		vertex v;
		int k = 1;	//	the counter for vertices

		//	First Semicircle
		k = makeSemicircleTrack(x, y, z, 0, PI, PI / 180.0, k, 15);

		// Second Semicircle
		k++;
		k = makeSemicircleTrack(x, y, z - size, -PI, 0, PI/180.0, k, k + 15);

		//	Straight lines
		k = makeStraightTrack(x, y, z, -radius, k, +1);
		k = makeStraightTrack(x, y, z, +radius, k, -1);		// this is actually the first straight part

		//	Start line
		for (int i = 0; i < 3; i++) {
			k = makeStartLine(i, k);
		}

		//	Finish line
		k = makeFinishLine(k);

		cout << "Track vertices: " << k << endl;

		//creeaza obiectele OpenGL necesare desenarii
		unsigned int gl_vertex_array_object, gl_vertex_buffer_object, gl_index_buffer_object;

		//vertex array object -> un obiect ce reprezinta un container pentru starea de desenare
		glGenVertexArrays(1, &gl_vertex_array_object);
		glBindVertexArray(gl_vertex_array_object);

		//vertex buffer object -> un obiect in care tinem vertecsii
		glGenBuffers(1, &gl_vertex_buffer_object);
		glBindBuffer(GL_ARRAY_BUFFER, gl_vertex_buffer_object);
		glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(vertex), &vertices[0], GL_STATIC_DRAW);

		//index buffer object -> un obiect in care tinem indecsii
		glGenBuffers(1, &gl_index_buffer_object);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gl_index_buffer_object);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size()*sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

		//legatura intre atributele vertecsilor si pipeline, datele noastre sunt INTERLEAVED.
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), (void*)0);						//trimite pozitii pe pipe 0
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), (void*)(sizeof(float)* 3));		//trimite normale pe pipe 1
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(vertex), (void*)(2 * sizeof(float)* 3));	//trimite texcoords pe pipe 2

		vao = gl_vertex_array_object;
		vbo = gl_vertex_buffer_object;
		ibo = gl_index_buffer_object;
		num_indices = indices.size();
	}

	void BindVertexArray() {
		glBindVertexArray(vao);
		glDrawElements(GL_TRIANGLES, num_indices, GL_UNSIGNED_INT, 0);
	}

	float getSize() {
		return size;
	}

	float getWidth() {
		return width;
	}

	~Track() {
		glDeleteBuffers(1, &vbo);
		glDeleteBuffers(1, &ibo);
		glDeleteVertexArrays(1, &vao);
	};

private: 
	/**	-------------------------------------------------------------------------
	**	(x, y, z):			the center of the semicircles
	**	(from, to, step):	used in the for loop
	** k:					counter for "how many vertices" are there
	**						it is equal to the length of the vector vertices
	**	startAdd:			from which point should it add vertices to indices
	**	---------------------------------------------------------------------- */
	int makeSemicircleTrack(float x, float y, float z, float from, float to, float step, int k, int startAdd) {
		vertex v;

		v.position = glm::vec3(x, y, z);	//	center of the semicircle
		v.color = color;
		vertices.push_back(v);

		for (float angle = from; angle <= to; angle += step) {
			for (int i = 0; i < nlane; i++) {
				//	WHITE LINE
				v.color = WHITE;
				v.position = glm::vec3(x + cos(angle) * (radius - i * width - 0), y, z + sin(angle) * (radius - i * width - 0));
				vertices.push_back(v);
				v.position = glm::vec3(x + cos(angle) * (radius - i * width - 1), y, z + sin(angle) * (radius - i * width - 1));
				vertices.push_back(v);
				k += 2;

				//	LANE
				v.color = color;
				v.position = glm::vec3(x + cos(angle) * (radius - i * width - 1), y, z + sin(angle) * (radius - i * width - 1));
				vertices.push_back(v);
				v.position = glm::vec3(x + cos(angle) * (radius - (i + 1) * width - 0), y, z + sin(angle) * (radius - (i + 1) * width - 0));
				vertices.push_back(v);
				k += 2;
			}

			//	ONE MORE WHITE LINE (the extreme interior)
			v.color = WHITE;
			v.position = glm::vec3(x + cos(angle) * (radius - nlane * width - 0), y, z + sin(angle) * (radius - nlane * width - 0));
			vertices.push_back(v);

			v.position = glm::vec3(x + cos(angle) * (radius - nlane * width - 1), y, z + sin(angle) * (radius - nlane * width - 1));
			vertices.push_back(v);
			k += 2;

			//	ADD THE INDICES
			if (k > startAdd) {
				addIndices3(k);
			}
		}
		return k;
	};

	void addIndices3(int k) {
		//	WHILE LINE 1
		indices.push_back(k - 28);	indices.push_back(k - 14);	indices.push_back(k - 27);
		indices.push_back(k - 14);	indices.push_back(k - 13);	indices.push_back(k - 27);
		//	LANE 1
		indices.push_back(k - 26);	indices.push_back(k - 12);	indices.push_back(k - 25);
		indices.push_back(k - 12);	indices.push_back(k - 11);	indices.push_back(k - 25);
		//	WHILE LINE 2
		indices.push_back(k - 24);	indices.push_back(k - 10);	indices.push_back(k - 23);
		indices.push_back(k - 10);	indices.push_back(k - 9);	indices.push_back(k - 23);
		//	LANE 2
		indices.push_back(k - 22);	indices.push_back(k - 8);	indices.push_back(k - 21);
		indices.push_back(k - 8);	indices.push_back(k - 7);	indices.push_back(k - 21);
		//	WHILE LINE 3
		indices.push_back(k - 20);	indices.push_back(k - 6);	indices.push_back(k - 19);
		indices.push_back(k - 6);	indices.push_back(k - 5);	indices.push_back(k - 19);
		//	LANE 3
		indices.push_back(k - 18);	indices.push_back(k - 4);	indices.push_back(k - 17);
		indices.push_back(k - 4);	indices.push_back(k - 3);	indices.push_back(k - 17);
		//	WHILE LINE 4
		indices.push_back(k - 16);	indices.push_back(k - 2);	indices.push_back(k - 15);
		indices.push_back(k - 2);	indices.push_back(k - 1);	indices.push_back(k - 15);
	}

	void addIndicesSmart(int k) {
		int aux1, aux2;

		//	aux1 = number of (white line, lane) sets * 4 verteces + 2 (last white line);
		aux1 = (nlane * 4 + 2) * 1; cout << aux1 << endl;
		//	aux1 = (number of (white line, lane) sets * 4 verteces + 2 (last white line)) * 2 sets of verteces;
		aux2 = (nlane * 4 + 2) * 2; cout << aux2 << endl;

		for (int i = 0; i < nlane; i++) {
			//	WHITE LINES
			indices.push_back(k - aux2);	indices.push_back(k - aux1);		indices.push_back(k - aux2 - 1);
			indices.push_back(k - aux1);	indices.push_back(k - aux1 - 1);	indices.push_back(k - aux2 - 1);
			aux1 -= 2;
			aux2 -= 2;

			//	LANES
			indices.push_back(k - aux2);	indices.push_back(k - aux1);		indices.push_back(k - aux2 - 1);
			indices.push_back(k - aux1);	indices.push_back(k - aux1 - 1);	indices.push_back(k - aux2 - 1);
			aux1 -= 2;
			aux2 -= 2;
		}

		//	THE EXTREME INTERIOR WHITE LINE
		indices.push_back(k - aux2);	indices.push_back(k - aux1);		indices.push_back(k - aux2 - 1);
		indices.push_back(k - aux1);	indices.push_back(k - aux1 - 1);	indices.push_back(k - aux2 - 1);
	}

	void addIndices5(int k) {
		//	WHILE LINE 1
		indices.push_back(k - 44);	indices.push_back(k - 22);	indices.push_back(k - 43);
		indices.push_back(k - 22);	indices.push_back(k - 21);	indices.push_back(k - 43);
		//	LANE 1
		indices.push_back(k - 42);	indices.push_back(k - 20);	indices.push_back(k - 41);
		indices.push_back(k - 20);	indices.push_back(k - 19);	indices.push_back(k - 41);
		//	WHILE LINE 2
		indices.push_back(k - 40);	indices.push_back(k - 18);	indices.push_back(k - 39);
		indices.push_back(k - 18);	indices.push_back(k - 17);	indices.push_back(k - 39);
		//	LANE 2
		indices.push_back(k - 38);	indices.push_back(k - 16);	indices.push_back(k - 37);
		indices.push_back(k - 16);	indices.push_back(k - 15);	indices.push_back(k - 37);
		//	WHILE LINE 3
		indices.push_back(k - 36);	indices.push_back(k - 14);	indices.push_back(k - 35);
		indices.push_back(k - 14);	indices.push_back(k - 13);	indices.push_back(k - 35);
		//	LANE 3
		indices.push_back(k - 34);	indices.push_back(k - 12);	indices.push_back(k - 33);
		indices.push_back(k - 12);	indices.push_back(k - 11);	indices.push_back(k - 33);
		//	WHILE LINE 4
		indices.push_back(k - 32);	indices.push_back(k - 10);	indices.push_back(k - 31);
		indices.push_back(k - 10);	indices.push_back(k - 9);	indices.push_back(k - 31);
		//	LANE 4
		indices.push_back(k - 30);	indices.push_back(k - 8);	indices.push_back(k - 29);
		indices.push_back(k - 8);	indices.push_back(k - 7);	indices.push_back(k - 29);
		//	WHILE LINE 5
		indices.push_back(k - 28);	indices.push_back(k - 6);	indices.push_back(k - 27);
		indices.push_back(k - 6);	indices.push_back(k - 5);	indices.push_back(k - 27);
		//	LANE 5
		indices.push_back(k - 26);	indices.push_back(k - 4);	indices.push_back(k - 25);
		indices.push_back(k - 4);	indices.push_back(k - 3);	indices.push_back(k - 25);
		//	WHILE LINE 6
		indices.push_back(k - 24);	indices.push_back(k - 2);	indices.push_back(k - 23);
		indices.push_back(k - 2);	indices.push_back(k - 1);	indices.push_back(k - 23);
	}

	int makeStraightTrack(float x, float y, float z, float r, int k, int q) {
		vertex v;
//*		//	GENERATE THE WHITE LINES
		v.color = WHITE;
		for (int i = 0; i < 4; i++) {
			v.position = glm::vec3(x + r + q * i * width + 0, y, z);
			vertices.push_back(v);

			v.position = glm::vec3(x + r + q * i * width + q, y, z);
			vertices.push_back(v);

			v.position = glm::vec3(x + r + q * i * width + 0, y, z - size);
			vertices.push_back(v);

			v.position = glm::vec3(x + r + q * i * width + q, y, z - size);
			vertices.push_back(v);

			k += 4;

			indices.push_back(k - 4);	indices.push_back(k - 3);	indices.push_back(k - 2);
			indices.push_back(k - 3);	indices.push_back(k - 1);	indices.push_back(k - 2);
		}
//*/
//*		//	GENERATE THE LANES
			v.color = color;
		for (int i = 0; i < 3; i++) {
			v.position = glm::vec3(x + r + q * i * width + q, y, z);
			vertices.push_back(v);

			v.position = glm::vec3(x + r + q * (i + 1) * width, y, z);
			vertices.push_back(v);

			v.position = glm::vec3(x + r + q * i * width + q, y, z - size);
			vertices.push_back(v);

			v.position = glm::vec3(x + r + q * (i + 1) * width, y, z - size);
			vertices.push_back(v);

			k += 4;

			indices.push_back(k - 4);	indices.push_back(k - 3);	indices.push_back(k - 2);
			indices.push_back(k - 3);	indices.push_back(k - 1);	indices.push_back(k - 2);
		}
//*/
		return k;
	}
	
	int makeStartLine(int i, int k) {
		vertex v;
		v.color = glm::vec3(0.988, 0.909, 0.513);
		v.position = glm::vec3(radius - width * i - 2, 0.10, startDistance[i]);
		vertices.push_back(v);

		v.position = glm::vec3(radius - width * (i + 1) + 1, 0.10, startDistance[i]);
		vertices.push_back(v);

		v.position = glm::vec3(radius - width * i - 2, 0.10, startDistance[i] + 2);
		vertices.push_back(v);

		v.position = glm::vec3(radius - width * (i + 1) + 1, 0.10, startDistance[i] + 2);
		vertices.push_back(v);

		k += 4;

		indices.push_back(k - 4);	indices.push_back(k - 3);	indices.push_back(k - 2);
		indices.push_back(k - 3);	indices.push_back(k - 1);	indices.push_back(k - 2);

		return k;
	}

	int makeFinishLine(int k) {
		vertex v;
		v.color = WHITE;
		v.position = glm::vec3(radius - 1, 0.50, -size * 0.50);
		vertices.push_back(v);

		v.position = glm::vec3(radius - width * 3, 0.50, -size * 0.50);
		vertices.push_back(v);

		v.position = glm::vec3(radius - 1, 0.50, -size * 0.50 + 2);
		vertices.push_back(v);

		v.position = glm::vec3(radius - width * 3, 0.50, -size * 0.50 + 2);
		vertices.push_back(v);

		k += 4;

		indices.push_back(k - 4);	indices.push_back(k - 3);	indices.push_back(k - 2);
		indices.push_back(k - 3);	indices.push_back(k - 1);	indices.push_back(k - 2);

		return k;
	}

};