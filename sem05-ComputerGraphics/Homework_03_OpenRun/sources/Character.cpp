#pragma once
#include "..\dependente\glew\glew.h"
#include "..\dependente\glm\glm.hpp"
#include "..\dependente\glm\gtc\type_ptr.hpp"
#include "..\dependente\glm\gtc\matrix_transform.hpp"
#include "..\lab_camera.hpp"
#include <cmath>

#include "defines.h"

#define min(a,b)            (((a) < (b)) ? (a) : (b))
#define max(a,b)            (((a) > (b)) ? (a) : (b))

using namespace std;

class Character {
public:
	unsigned int vbo, ibo, vao, num_indices;
	glm::vec3 position;
	float angle;
	float speed;

	float camHeight;
	glm::vec3 camPos;
	glm::vec3 lookAt;
	lab::Camera cam;
	const int dist = 100;

	//	track data
	float radius;
	float length;	// the length of the straight line
	float width;	// the width of a lane

	Character() {
		position = glm::vec3(0, 0, 0);
		camHeight = 0;
		speed = 0;
		angle = 0;
	}

	Character(glm::vec3 _position, float _camHeight) {
		position = _position;
		camHeight = _camHeight;
		angle = 0.0f;
		camPos = glm::vec3(position.x, position.y + camHeight + 1, position.z);
		lookAt = glm::vec3(position.x + dist * sin(PI * angle / 180), position.y, position.z + dist * cos(PI * angle / 180));
		setCamera();
	}

	void setSpeed(float speed) {
		this->speed = speed;
	}

	void setPosition(glm::vec3 pos, float _angle) {
		position = pos;
		angle = _angle;
	}

	void setCamera() {
		cam.set(camPos, lookAt, glm::vec3(0, 1, 0));
	}

	// aux = +/- 1 which is actually equal to Oz
	void moveForward(float aux, float radius, float laneWidth) {
		float trackLength = radius * trackRatio;
		float r = radius - MAX_CHAR * laneWidth;			// the inner circle

		float x = position.x;
		float z = position.z;
		position.x += aux * sin(PI * angle / 180);
		position.z += aux * cos(PI * angle / 180);

		float cx = camPos.x;
		float cz = camPos.z;
		camPos.x += aux * sin(PI * angle / 180);
		camPos.z += aux * cos(PI * angle / 180);

		//	straight line
		if (position.z <= 0 && position.z >= -trackLength) {
			if (x > 0) {
				position.x = max(min(position.x, radius), r);
			}
			else {
				position.x = max(min(position.x, -r), -radius);
			}
			camPos.x = position.x;
		}
		else {
			//	first semicircle
			float euclid = 0.0f;
			if (position.z >= 0) {
				euclid = sqrt(position.x * position.x + position.z * position.z);
				if (euclid > radius || euclid < r) {
					position.x = x;
					position.z = z;
					camPos.x = cx;
					camPos.z = cz;
				}
			}
			else {
				//	second semicircle
				euclid = sqrt(position.x * position.x + pow(position.z + trackLength, 2));
				if (euclid > radius || euclid < r) {
					position.x = x;
					position.z = z;
					camPos.x = cx;
					camPos.z = cz;
				}
			}
		}
		lookAt.x = camPos.x + dist * sin(PI * angle / 180);
		lookAt.z = camPos.z + dist * cos(PI * angle / 180);
	}

	void rotate(float aux) {
		angle += aux;
		lookAt.x = camPos.x + dist * sin(PI * angle / 180);
		lookAt.z = camPos.z + dist * cos(PI * angle / 180);
	}

	void checkBoundries(float radius, float trackLength) {
		if (position.z <= 0 && position.z >= -trackLength) {
			if (position.x >= radius) {

			}
		}
	}

	void print() {
		printf("POSITION: (%f, %f, %f)\n", position.x, position.y, position.z);
		printf("UNGHI: %f\n", angle);
	}
};