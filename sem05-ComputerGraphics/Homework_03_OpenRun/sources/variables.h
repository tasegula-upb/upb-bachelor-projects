#pragma once
#include "Headers.h"
#include "Track.cpp"
#include "Character.cpp"
#include "Cube.h"
#include "Flag.cpp"

/**	---------------------------------------------------------------------------
 **		THE TRACK
 **	------------------------------------------------------------------------ */
Track* track;
glm::vec3 middle;

/**	---------------------------------------------------------------------------
 **		THE PLAYERS
 **	------------------------------------------------------------------------ */
string filename[3];
string path;

Character* player[3];
int character[3];					//	what character is on lane i;

float speed[3];
float angle[3];
float lane[3];						//	radius of lane[i]

float camHeight[10];				//	at what height is the camera set
glm::vec3 color[10];

glm::mat4 mrotate[3], mtranslate[3], mscale[3];

/**	---------------------------------------------------------------------------
 **		TECHNIC
 **	------------------------------------------------------------------------ */
bool keys[10];
Cube* cube;

Flag* flag;
glm::vec3 flagPos;
int flagSize;