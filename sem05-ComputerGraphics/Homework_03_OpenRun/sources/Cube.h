#pragma once
#include "..\dependente\glew\glew.h"
#include "..\dependente\glm\glm.hpp"
#include "..\dependente\glm\gtc\type_ptr.hpp"
#include "..\dependente\glm\gtc\matrix_transform.hpp"
#include "..\lab_camera.hpp"

#include "defines.h"
#include "Vertex.h"

class Cube {
public:
	float size;
	float trackLength;
	float radius;
	float angle;
	glm::vec3 position;

	float camHeight;
	glm::vec3 camPos;
	glm::vec3 lookAt;
	lab::Camera cam;
	const int dist = 100;
	const int pos = 10;

	unsigned int vbo, ibo, vao, num_indices;

	Cube(float _size) {
		size = _size;
		trackLength = 0.0f;
		radius = 0.0f;
		angle = 0.0f;
		position = glm::vec3(0, 0, 0);

		std::vector<vertex> vertices;
		std::vector<unsigned int> indices;

		vertex v[8];

		for (int i = 0; i < 8; i++) {
			v[i].color = glm::vec3(0.1, 0.3, 0.7);
		}

		v[0].color = glm::vec3(0.1, 0.3, 0.7);
		v[1].color = glm::vec3(0.3, 0.5, 0.7);
		v[2].color = glm::vec3(0.2, 0.4, 0.7);
		v[3].color = glm::vec3(0.7, 0.3, 0.7);

		v[4].color = glm::vec3(0.5, 0.3, 0.2);
		v[5].color = glm::vec3(0.1, 0.8, 0.8);
		v[6].color = glm::vec3(0.7, 0.6, 0.4);
		v[7].color = glm::vec3(0.2, 0.4, 0.1);

		v[0].position = glm::vec3(-size * 0.5, 0, +size * 0.5);
		v[1].position = glm::vec3(+size * 0.5, 0, +size * 0.5);
		v[2].position = glm::vec3(-size * 0.5, +size, +size * 0.5);
		v[3].position = glm::vec3(+size * 0.5, +size, +size * 0.5);

		v[4].position = glm::vec3(-size * 0.5, 0, -size * 0.5);
		v[5].position = glm::vec3(+size * 0.5, 0, -size * 0.5);
		v[6].position = glm::vec3(-size * 0.5, +size, -size * 0.5);
		v[7].position = glm::vec3(+size * 0.5, +size, -size * 0.5);

		for (int i = 0; i < 8; i++) {
			vertices.push_back(v[i]);
		}

		indices.push_back(0);	indices.push_back(1);	indices.push_back(2);
		indices.push_back(1);	indices.push_back(3);	indices.push_back(2);

		indices.push_back(2);	indices.push_back(3);	indices.push_back(7);
		indices.push_back(2);	indices.push_back(7);	indices.push_back(6);

		indices.push_back(1);	indices.push_back(7);	indices.push_back(3);
		indices.push_back(1);	indices.push_back(5);	indices.push_back(7);

		indices.push_back(6);	indices.push_back(7);	indices.push_back(4);
		indices.push_back(7);	indices.push_back(5);	indices.push_back(4);

		indices.push_back(0);	indices.push_back(4);	indices.push_back(1);
		indices.push_back(1);	indices.push_back(4);	indices.push_back(5);

		indices.push_back(2);	indices.push_back(6);	indices.push_back(4);
		indices.push_back(0);	indices.push_back(2);	indices.push_back(4);

		//creeaza obiectele OpenGL necesare desenarii
		unsigned int gl_vertex_array_object, gl_vertex_buffer_object, gl_index_buffer_object;

		//vertex array object -> un obiect ce reprezinta un container pentru starea de desenare
		glGenVertexArrays(1, &gl_vertex_array_object);
		glBindVertexArray(gl_vertex_array_object);

		//vertex buffer object -> un obiect in care tinem vertecsii
		glGenBuffers(1, &gl_vertex_buffer_object);
		glBindBuffer(GL_ARRAY_BUFFER, gl_vertex_buffer_object);
		glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(vertex), &vertices[0], GL_STATIC_DRAW);

		//index buffer object -> un obiect in care tinem indecsii
		glGenBuffers(1, &gl_index_buffer_object);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gl_index_buffer_object);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size()*sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

		//legatura intre atributele vertecsilor si pipeline, datele noastre sunt INTERLEAVED.
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), (void*)0);						//trimite pozitii pe pipe 0
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), (void*)(sizeof(float)* 3));		//trimite normale pe pipe 1
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(vertex), (void*)(2 * sizeof(float)* 3));	//trimite texcoords pe pipe 2

		vao = gl_vertex_array_object;
		vbo = gl_vertex_buffer_object;
		ibo = gl_index_buffer_object;
		num_indices = indices.size();

		camPos = glm::vec3(position.x, position.y + 7, position.z);
		lookAt = glm::vec3(0, 0, position.z);
		setCamera();
	}

	void BindVertexArray() {
		glBindVertexArray(vao);
		glDrawElements(GL_TRIANGLES, num_indices, GL_UNSIGNED_INT, 0);
	}

	void setTrackData(float _size, float _radius) {
		trackLength = _size;
		radius = _radius + size * 0.50;
	}

	void setCamera() {
		//		cam.set(camPos, lookAt, glm::vec3(0, 1, 0));
	}

	void followPlayer(glm::vec3 playerPos) {

		if (playerPos.z <= 0 && playerPos.z >= -trackLength) {
			position.z = playerPos.z;
			//			camPos.z = playerPos.z;
			if (playerPos.x >= 0) {
				position.x = +radius + pos;		// the cube is made at (size * 0.50)
			}
			else {
				position.x = -radius - pos;
			}
		}

		//	first semicircle
		if (playerPos.z > 0) {
			angle = atan(playerPos.z / playerPos.x);
			if (playerPos.x >= 0) {
				position.x = (radius + pos) * cos(angle);
				position.z = (radius + pos) * sin(angle);
			}
			else {
				position.x = (radius + pos) * cos(angle + PI);
				position.z = (radius + pos) * sin(angle + PI);
			}
		}

		//	second semicircle
		if (playerPos.z < -trackLength) {
			angle = atan((playerPos.z + trackLength) / playerPos.x);
			if (playerPos.x <= 0) {
				position.x = (radius + pos) * cos(angle + PI);
				position.z = -trackLength + (radius + pos) * sin(angle + PI);
			}
			else {
				position.x = (radius + pos) * cos(angle + 2 * PI);
				position.z = -trackLength + (radius + pos) * sin(angle + 2 * PI);
			}
		}

		camPos.x = position.x;
		camPos.z = position.z;

		if (position.z < 0 && position.z > -trackLength) {
			lookAt.z = position.z;
		}
		else {
			if (position.z >= 0) {
				lookAt.z = 0.0f;
			}
			else {
				lookAt.z = -trackLength;
			}
		}
	}
};