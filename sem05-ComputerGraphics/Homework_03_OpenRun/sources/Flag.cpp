#pragma once
#include "..\dependente\glew\glew.h"
#include "..\dependente\glm\glm.hpp"
#include "..\dependente\glm\gtc\type_ptr.hpp"
#include "..\dependente\glm\gtc\matrix_transform.hpp"
#include "..\lab_camera.hpp"

#include <vector>

#include "defines.h"
#include "Vertex.h"

class Flag {
public:
	float height, width;
	float angle;

	glm::vec3 color;
	glm::vec3 position;

	std::vector<vertex> vertices;
	std::vector<unsigned int> indices;

	unsigned int vao, vbo, ibo, num_indices;

	Flag(glm::vec3 _position, float _width, float _height, glm::vec3 _color) {
		position = _position;
		height = _height;
		width = _width;
		color = _color;
		angle = 0;

		int n = 20;
		float h = height / n;
		float w = width / n;
//		printf("FLAG\t%f : %f\n", w, h);

		glm::vec3 c[2];		
		c[0] = color;
		c[1] = WHITE;
		vertex v;	
		v.color = color;
		v.jiggle = glm::vec2(1.00, 1.00);

		int i, j, k;					
		
		for (i = 0; i < n - 1; i++) {
			for (j = 0; j < n; j++) {
				v.jiggle = glm::vec2(1.00, 1.00);
				v.position.x = position.x + i * w;
				v.position.z = position.z + j * h;
				v.color = c[(i + j) % 2];
				vertices.push_back(v);
			}
			if (i >= 1) {
				for (k = 0; k < n - 1; k++) {
					indices.push_back((i - 1) * n + k);		indices.push_back((i - 1) * n + k + 1);	indices.push_back(i * n + k);
					indices.push_back((i - 1) * n + k + 1);	indices.push_back(i * n + k + 1);		indices.push_back(i * n + k);
				}
			}
		}
		//	one side of the flag should be still : i = n-1
		for (j = 0; j < n; j++) {
			v.jiggle = glm::vec2(0.00, 0.00);
			v.position.x = position.x + i * w;
			v.position.z = position.z + j * h;
			v.color = c[(i + j) % 2];
			vertices.push_back(v);
		}
		for (k = 0; k < n - 1; k++) {		// add to the whole
			indices.push_back((i - 1) * n + k);		indices.push_back((i - 1) * n + k + 1);	indices.push_back(i * n + k);
			indices.push_back((i - 1) * n + k + 1);	indices.push_back(i * n + k + 1);		indices.push_back(i * n + k);
		}

		//creeaza obiectele OpenGL necesare desenarii
		unsigned int gl_vertex_array_object, gl_vertex_buffer_object, gl_index_buffer_object;

		//vertex array object -> un obiect ce reprezinta un container pentru starea de desenare
		glGenVertexArrays(1, &gl_vertex_array_object);
		glBindVertexArray(gl_vertex_array_object);

		//vertex buffer object -> un obiect in care tinem vertecsii
		glGenBuffers(1, &gl_vertex_buffer_object);
		glBindBuffer(GL_ARRAY_BUFFER, gl_vertex_buffer_object);
		glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(vertex), &vertices[0], GL_STATIC_DRAW);

		//index buffer object -> un obiect in care tinem indecsii
		glGenBuffers(1, &gl_index_buffer_object);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gl_index_buffer_object);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size()*sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

		//legatura intre atributele vertecsilor si pipeline, datele noastre sunt INTERLEAVED.
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), (void*)0);						//trimite pozitii pe pipe 0
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), (void*)(sizeof(float)* 3));		//trimite normale pe pipe 1
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(vertex), (void*)(2 * sizeof(float)* 3));	//trimite texcoords pe pipe 2

		vao = gl_vertex_array_object;
		vbo = gl_vertex_buffer_object;
		ibo = gl_index_buffer_object;
		num_indices = indices.size();
	}

	void BindVertexArray() {
		glBindVertexArray(vao);
		glDrawElements(GL_TRIANGLES, num_indices, GL_UNSIGNED_INT, 0);
	}

};