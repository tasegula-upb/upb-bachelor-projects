﻿/**	-----------------------------------------------------------------------------------------------
**	Elemente de Grafica pe Calculator - Tema 03
**		Open Run
**		Gula Tanase, 334CA
** -------------------------------------------------------------------------------------------- */
#pragma once
#include <fstream>
#include <stdlib.h>
#include <stdio.h>

#include "sources\Headers.h"
#include "sources\variables.h"
#include "sources\defines.h"
#include "sources\Track.cpp"

class Laborator : public lab::glut::WindowListener{

	//variabile
private:
	glm::mat4 model_matrix, projection_matrix;		//matrici 4x4 pt modelare vizualizare proiectie
	lab::Camera cam;
	lab::Camera camGlobal;
	lab::Camera camFollow;
	lab::Camera camera;
	unsigned int gl_program_shader;					//id-ul de opengl al obiectului de tip program shader
	unsigned int screen_width, screen_height;

	int reachedFinish[3];		//	+= 1 daca a trecut de finishLine (player-ul controlabil va avea setat de la inceput 1)
	int winner;					//	este egal cu numarul castigatorului;
	bool gameOver;				//	is the game over?
	bool isSet;					//	is the flag set?
	bool startRace;				//	e pauza sau nu

	glm::vec3 wave;				//	folosit la animatia steagului
	float waveFreq;				//	frecventa cu care cresc wave.z (de fapt, timpul trecut pentru animatie)

	//metode
public:

	//constructor .. e apelat cand e instantiata clasa
	Laborator(){

		//setari pentru desenare, clear color seteaza culoarea de clear pentru ecran (format R,G,B,A)
		glClearColor(0.5, 0.5, 0.5, 1);
		glClearDepth(1);			//clear depth si depth test (nu le studiem momentan, dar avem nevoie de ele!)
		glEnable(GL_DEPTH_TEST);	//sunt folosite pentru a determina obiectele cele mai apropiate de camera (la curs: algoritmul pictorului, algoritmul zbuffer)

		//incarca un shader din fisiere si gaseste locatiile matricilor relativ la programul creat
		gl_program_shader = lab::loadShader("shadere\\shader_vertex.glsl", "shadere\\shader_fragment.glsl");

		//	incarca mesh-ul pentru track
		track = new Track(RADIUS, trackColor, 3);

		float startDistance = 2 * PI * track->getWidth();
		float trackLength = RADIUS * trackRatio;

		//	unde punem caracterele
		for (int i = 0; i < MAX_CHAR; i++) {
			lane[i] = RADIUS - track->getWidth() * (i + 0.5);
		}
		middle = glm::vec3(0, track->getSize() * 0.5, -RADIUS * trackRatio * 0.5);

		//	incarca mesh-ele jucatorilor
		for (int i = 0; i < 3; i++) {
			player[i] = new Character(glm::vec3(lane[i], 0, track->startDistance[i]), camHeight[character[i]]);
			player[i]->speed = speed[i];
			lab::loadObj(filename[i], player[i]->vao, player[i]->vbo, player[i]->ibo, player[i]->num_indices, color[character[i]]);
			reachedFinish[i] = 0;
		}
		//	setari speciale pentru jucatorul controlabil
		player[0]->speed = 0.0f;

		cube = new Cube(4);
		cube->setTrackData(track->getSize(), RADIUS);

		flagSize = 300;
		flagPos = glm::vec3(-flagSize, 0, -flagSize * 0.5 - track->getSize() * 0.5);
		wave = glm::vec3(5.00, 5.00, 0.00);
		waveFreq = 0.01;

		//matrici de modelare si vizualizare
		model_matrix = glm::mat4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);

		cam.set(glm::vec3(FPX, FPY, FPZ), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
		keys[CAM5] = true;
		keys[CAM0] = keys[CAM1] = keys[CAM2] = keys[CAM3] = keys[CAM4] = false;
		camGlobal.set(glm::vec3(0, 500, middle.z), glm::vec3(0, 0, middle.z), glm::vec3(0, 0, 1));
		camFollow.set(glm::vec3(0, 500, middle.z), glm::vec3(0, 0, middle.z), glm::vec3(0, 0, 1));

		camera = cam;		// cam o sa fie camera pe care o tot schimb

		gameOver = false;	// flag-ul de Game Over
		isSet = false;		// flag-ul nu e setat (va fi la game over);
		winner = -1;		// nu exista jucatorul -1 (doar 0, 1, 2)
		startRace = false;	// flag-ul de play/pause

		//desenare wireframe
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glLineWidth(10);
		glPointSize(10);
	}

	//destructor .. e apelat cand e distrusa clasa
	~Laborator(){
		//distruge shader
		glDeleteProgram(gl_program_shader);

		//distruge mesh incarcat
		delete track;
	}

	//--------------------------------------------------------------------------------------------
	//functii de cadru ---------------------------------------------------------------------------

	//functie chemata inainte de a incepe cadrul de desenare, o folosim ca sa updatam situatia scenei ( modelam/simulam scena)
	void notifyBeginFrame(){ }

	//functia de afisare (lucram cu banda grafica)
	void notifyDisplayFrame(){

		//pe tot ecranul
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//	setare cadru (fostul Visual2D)
		glViewport(0, 0, screen_width, screen_height);

		//foloseste shaderul
		glUseProgram(gl_program_shader);

		//trimite variabile uniforme la shader
		glUniformMatrix4fv(glGetUniformLocation(gl_program_shader, "model_matrix"), 1, false, glm::value_ptr(model_matrix));
		glUniformMatrix4fv(glGetUniformLocation(gl_program_shader, "view_matrix"), 1, false, glm::value_ptr(cam.getViewMatrix()));
		glUniformMatrix4fv(glGetUniformLocation(gl_program_shader, "projection_matrix"), 1, false, glm::value_ptr(projection_matrix));
		glUniform3f(glGetUniformLocation(gl_program_shader, "wave"), wave.x, wave.y, wave.z);

		//	START GAME
		if (gameOver == false) {

			if (startRace == true) {
				// mutari pentru jucatori (doar daca s-a dat startul la cursa, respectiv nu e pauza)
				if (keys[UP]) {
					if (player[0]->speed < speed[0]) {
						player[0]->speed += 0.01;
					}
					player[0]->moveForward(+player[0]->speed, RADIUS, track->getWidth());
				}
				if (keys[DOWN]) {
					if (player[0]->speed < speed[0]) {
						player[0]->speed += 0.01;
					}
					player[0]->moveForward(-player[0]->speed, RADIUS, track->getWidth());
				}
				if (keys[RIGHT]) {
					player[0]->rotate(-0.10);
				}
				if (keys[LEFT]) {
					player[0]->rotate(+0.10);
				}
			}

			//	alege camera;
			if (keys[CAM0]) {
				cam.set(player[0]->camPos, player[0]->lookAt, glm::vec3(0, 1, 0));
			}
			if (keys[CAM1]) {
				cam.set(player[1]->camPos, player[1]->lookAt, glm::vec3(0, 1, 0));
			}
			if (keys[CAM2]) {
				cam.set(player[2]->camPos, player[2]->lookAt, glm::vec3(0, 1, 0));
			}
			if (keys[CAM4]) {
				cam.set(cube->camPos, cube->lookAt, glm::vec3(0, 1, 0));
			}

			//bind track
			track->BindVertexArray();

			// bind opponents
			float d = 0.0f;
			float aux = 0.0f;
			for (int i = 0; i < 3; i++) {
				if (i != 0 && startRace) {
					aux = RADIUS / lane[i];
					d = lane[i] * sqrt(2 - 2 * cos(PI * player[i]->speed * aux / 180));		// in loc de *i trebuie sa pun ceva care sa depinda de raze (lane-uri)
					if (player[i]->position.z > 0 || player[i]->position.z < -track->getSize()) {
						player[i]->rotate(-player[i]->speed * aux * 0.5);
					}
					player[i]->moveForward(d * 0.50, RADIUS, track->getWidth());
				}

				mrotate[i] = glm::rotate(model_matrix, player[i]->angle, glm::vec3(0, 1, 0));
				mtranslate[i] = glm::translate(model_matrix, player[i]->position);

				if (!keys[CAM0 + i]) {
					glUniformMatrix4fv(glGetUniformLocation(gl_program_shader, "model_matrix"), 1, false, glm::value_ptr(mtranslate[i] * mrotate[i]));
					glBindVertexArray(player[i]->vao);
					glDrawElements(GL_TRIANGLES, player[i]->num_indices, GL_UNSIGNED_INT, 0);
				}
			}

			//bing camera TPS care urmareste jucatorul
			cube->followPlayer(player[0]->position);
			float a = -cube->angle * 180.0 / PI;
			mrotate[0] = glm::rotate(model_matrix, a, glm::vec3(0, 1, 0));
			mtranslate[0] = glm::translate(model_matrix, cube->position);
			glUniformMatrix4fv(glGetUniformLocation(gl_program_shader, "model_matrix"), 1, false, glm::value_ptr(mtranslate[0] * mrotate[0]));

			cube->BindVertexArray();

			// testam conditia de finish
			for (int i = 0; i < 3; i++) {
				if (reachedFinish[i] == 0 && player[i]->position.x > 0 && player[i]->position.z > 0) {
					reachedFinish[i] = 1;
					continue;
				}
				if (reachedFinish[i] == 1 && player[i]->position.x < 0 && player[i]->position.z > 0) {
					reachedFinish[i] = 2;
					continue;
				}
				if (reachedFinish[i] == 2 && player[i]->position.x < 0 && player[i]->position.z < -RADIUS * trackRatio) {
					reachedFinish[i] = 3;
					continue;
				}
				if (reachedFinish[i] == 3 && player[i]->position.x > 0 && player[i]->position.z < -RADIUS * trackRatio) {
					reachedFinish[i] = 4;
					continue;
				}

				if (reachedFinish[i] == 4) {
					if (player[i]->position.x > 0 && player[i]->position.z > -RADIUS * trackRatio * 0.5) {
						winner = i;
						gameOver = true;
					}
				}
			}
		}
		else {
			if (winner != -1) {
				cam.set(glm::vec3(+200, 500, middle.z), glm::vec3(0, 0, middle.z), glm::vec3(0, 0, 1));

				if (isSet == false) {
					flag = new Flag(flagPos, flagSize, flagSize, color[character[winner]]);
					isSet = true;
				}

				wave.z += waveFreq;
				flag->angle -= 0.01;

					mrotate[0] = glm::rotate(model_matrix, flag->angle, glm::vec3(0, 0, 1));

				glUniform3f(glGetUniformLocation(gl_program_shader, "wave"), wave.x, wave.y, wave.z);

					glUniformMatrix4fv(glGetUniformLocation(gl_program_shader, "model_matrix"), 1, false, glm::value_ptr(mrotate[0]));
				glUniformMatrix4fv(glGetUniformLocation(gl_program_shader, "view_matrix"), 1, false, glm::value_ptr(cam.getViewMatrix()));
				flag->BindVertexArray();
			}
		}
	}

	//functie chemata dupa ce am terminat cadrul de desenare (poate fi folosita pt modelare/simulare)
	void notifyEndFrame(){}

	//functei care e chemata cand se schimba dimensiunea ferestrei initiale
	void notifyReshape(int width, int height, int previos_width, int previous_height){
		//reshape
		if (height == 0)
			height = 1;
		screen_width = width;
		screen_height = height;
		float aspect = width * 0.5f / height;
		projection_matrix = glm::perspective(75.0f, aspect, 0.1f, 10000.0f);
	}

	//--------------------------------------------------------------------------------------------
	//functii de input output --------------------------------------------------------------------

	//tasta apasata
	void notifyKeyPressed(unsigned char key_pressed, int mouse_x, int mouse_y){
		static bool wire = true;

		switch (key_pressed) {
		case 27:					// ESC inchide glut
			lab::glut::close();
			break;
		case 32:					//SPACE reincarca shaderul si recalculeaza locatiile (offseti/pointeri)
			glDeleteProgram(gl_program_shader);
			gl_program_shader = lab::loadShader("shadere\\shader_vertex.glsl", "shadere\\shader_fragment.glsl");
			break;

		case'i':
			wire = !wire;
			glPolygonMode(GL_FRONT_AND_BACK, (wire ? GL_LINE : GL_FILL));
			break;
		case 'w':	if (keys[CAM3] || keys[CAM5]) { cam.translateForward(PI);	camGlobal.translateForward(PI); }	break;
		case 'a':	if (keys[CAM3] || keys[CAM5]) { cam.translateRight(-PI);	camGlobal.translateRight(-PI); }	break;
		case 's':	if (keys[CAM3] || keys[CAM5]) { cam.translateForward(-PI);	camGlobal.translateForward(-PI); }	break;
		case 'd':	if (keys[CAM3] || keys[CAM5]) { cam.translateRight(PI);		camGlobal.translateRight(PI); }		break;
		case 'r': 	if (keys[CAM3] || keys[CAM5]) { cam.translateUpward(PI);	camGlobal.translateUpward(PI); }	break;
		case 'f': 	if (keys[CAM3] || keys[CAM5]) { cam.translateUpward(-PI);	camGlobal.translateUpward(-PI); }	break;

		case 'q': 	if (keys[CAM3] || keys[CAM5]) { cam.rotateFPSoY(+PI / 45);	camGlobal.rotateFPSoY(+PI / 45); }	break;
		case 'e': 	if (keys[CAM3] || keys[CAM5]) { cam.rotateFPSoY(-PI / 45);	camGlobal.rotateFPSoY(-PI / 45); }	break;
		case 'z': 	if (keys[CAM3] || keys[CAM5]) { cam.rotateFPSoZ(-PI / 45);	camGlobal.rotateFPSoZ(-PI / 45); }	break;
		case 'c': 	if (keys[CAM3] || keys[CAM5]) { cam.rotateFPSoZ(+PI / 45);	camGlobal.rotateFPSoZ(+PI / 45); }	break;
		case 't': 	if (keys[CAM3] || keys[CAM5]) { cam.rotateFPSoX(+PI / 45);	camGlobal.rotateFPSoX(+PI / 45); }	break;
		case 'g': 	if (keys[CAM3] || keys[CAM5]) { cam.rotateFPSoX(-PI / 45);	camGlobal.rotateFPSoX(-PI / 45); }	break;

		case 'o':
			cam.set(glm::vec3(FPX, FPY, FPZ), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
			camGlobal.set(glm::vec3(0, 500, middle.z), glm::vec3(0, 0, middle.z), glm::vec3(0, 0, 1));
			keys[CAM0] = keys[CAM1] = keys[CAM2] = keys[CAM3] = keys[CAM4] = keys[CAM5] = false;
			break;

		case '0':
			keys[CAM0] = true;
			keys[CAM1] = keys[CAM2] = keys[CAM3] = keys[CAM4] = keys[CAM5] = false;
			cam.set(player[0]->camPos, player[0]->lookAt, glm::vec3(0, 1, 0));
			break;

		case '1':
			keys[CAM1] = true;
			keys[CAM0] = keys[CAM2] = keys[CAM3] = keys[CAM4] = keys[CAM5] = false;
			cam.set(player[1]->camPos, player[1]->lookAt, glm::vec3(0, 1, 0));
			break;
		case '2':
			keys[CAM2] = true;
			keys[CAM0] = keys[CAM1] = keys[CAM3] = keys[CAM4] = keys[CAM5] = false;
			cam.set(player[2]->camPos, player[2]->lookAt, glm::vec3(0, 1, 0));
			break;
		case '3':
			keys[CAM3] = true;
			keys[CAM0] = keys[CAM1] = keys[CAM2] = keys[CAM4] = keys[CAM5] = false;
			cam = camGlobal;
			break;

		case '4':
			keys[CAM4] = true;
			keys[CAM0] = keys[CAM1] = keys[CAM2] = keys[CAM3] = keys[CAM5] = false;
			cam = camFollow;
			break;

		case '5':
			keys[CAM5] = true;
			keys[CAM0] = keys[CAM1] = keys[CAM2] = keys[CAM3] = keys[CAM4] = false;
			cam.set(glm::vec3(FPX, FPY, FPZ), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
			break;

		case 'p':	startRace = !startRace;
		}
	}
	//tasta ridicata
	void notifyKeyReleased(unsigned char key_released, int mouse_x, int mouse_y){	}
	//tasta speciala (up/down/F1/F2..) apasata
	void notifySpecialKeyPressed(int key_pressed, int mouse_x, int mouse_y){
		switch (key_pressed) {
		case GLUT_KEY_UP:		keys[UP] = true;	break;
		case GLUT_KEY_DOWN:		keys[DOWN] = true;	break;
		case GLUT_KEY_RIGHT:	keys[RIGHT] = true;	break;
		case GLUT_KEY_LEFT:		keys[LEFT] = true;	break;
		case GLUT_KEY_F1:		lab::glut::enterFullscreen();
		case GLUT_KEY_F2:		lab::glut::exitFullscreen();
		}
	}
	//tasta speciala ridicata
	void notifySpecialKeyReleased(int key_released, int mouse_x, int mouse_y){
		switch (key_released) {
		case GLUT_KEY_UP:		keys[UP] = false;		player[0]->speed = 0.0f;	break;
		case GLUT_KEY_DOWN:		keys[DOWN] = false;		player[0]->speed = 0.0f;	break;
		case GLUT_KEY_RIGHT:	keys[RIGHT] = false;	break;
		case GLUT_KEY_LEFT:		keys[LEFT] = false;		break;
		}
	}
	//drag cu mouse-ul
	void notifyMouseDrag(int mouse_x, int mouse_y){ }
	//am miscat mouseul (fara sa apas vreun buton)
	void notifyMouseMove(int mouse_x, int mouse_y){ }
	//am apasat pe un boton
	void notifyMouseClick(int button, int state, int mouse_x, int mouse_y){ }
	//scroll cu mouse-ul
	void notifyMouseScroll(int wheel, int direction, int mouse_x, int mouse_y){ std::cout << "Mouse scroll" << std::endl; }

};

bool choosePlayerChar(char *cp, int k) {
	filename[k] = path;
	switch (cp[k]) {
	case 'A':
	case 'a':
		filename[k] += "Android.obj";
		character[k] = ANDROID;
		break;
	case 'I':
	case 'i':
		filename[k] += "IronMan.obj";
		character[k] = IRONMAN;
		break;
	case 'S':
	case 's':
		filename[k] += "Superman.obj";
		character[k] = SUPERMAN;
		break;
	case 'L':
	case 'l':
		filename[k] += "LegoMan.obj";
		character[k] = LEGO;
		break;
	case 'T':
	case 't':
		filename[k] += "Thor.obj";
		character[k] = THOR;
		break;
	case 'H':
	case 'h':
		filename[k] += "ThorHammer.obj";
		character[k] = HAMMER;
		break;
	case 'G':
	case 'g':
		filename[k] += "Goku3.obj";
		character[k] = GOKU;
		break;
	case 'B':
	case 'b':
		filename[k] += "Bulbasaur.obj";
		character[k] = BULBASAUR;
		break;
	case 'C':
	case 'c':
		filename[k] += "Charmander.obj";
		character[k] = CHAMANDER;
		break;
	case 'Q':
	case 'q':
		filename[k] += "Squirtle.obj";
		character[k] = SQUIRTLE;
		break;
	default:
		return false;
	}

	return true;
}

void printMenu() {
	int p[3];			//	the choices
	char cp[3];
	bool k;				//	test if the choice is valid

	path = "myResources\\";		// path-ul catre fisierele .obj

	cout << "MENU" << endl;
	cout << "===============================================================================" << endl;
	cout << "Press a key to choose the characters" << endl;
	cout << "i\t Iron Man Mark42 \t(from the movies ... duuuh)" << endl;
	cout << "s\t Superman: \t\tfrom the last movie" << endl;
	cout << "l\t Lego Figurine" << endl;
	cout << endl;
	cout << "t\t Thor: \t\t\tfrom the movies" << endl;
	cout << "h\t Thor's Hammer \t\tafter \"The Dark World\" he is cofused" << endl;
	cout << endl;
	cout << "g\t Goku SSJ3" << endl;
	cout << endl;
	cout << "b\t Bulbasaur" << endl;
	cout << "c\t Charmander" << endl;
	cout << "q\t Squirtle" << endl;
	cout << "===============================================================================" << endl;

	k = false;
	while (k == false) {
		cout << "Choose your character: ";
		cin >> cp[0];
		//		cp[0] = 'i';
		k = choosePlayerChar(cp, 0);
		if (!k) {
			cout << "Invalid character" << endl;
		}
	}

	k = false;
	while (k == false) {
		cout << "Choose your first opponent: ";
		cin >> cp[1];
		//		cp[1] = 'g';
		k = choosePlayerChar(cp, 1);
		if (!k || cp[1] == cp[0]) {
			cout << "Invalid character" << endl;
			k = false;
		}
	}

	k = false;
	while (k == false) {
		cout << "Choose your second opponent: ";
		cin >> cp[2];
		//		cp[2] = 'b';
		k = choosePlayerChar(cp, 2);
		if (!k || cp[2] == cp[1] || cp[2] == cp[0]) {
			cout << "Invalid character" << endl;
			k = false;
		}
	}
	cout << endl << "OK! Let's START !!!" << endl;
	cout << "===============================================================================" << endl;
	cout << "===============================================================================" << endl;
	cout << endl;
}

void initialize() {
	//setare flag-uri taste pe false;
	for (int i = 0; i < 10; i++) {
		keys[i] = false;
	}

	//	the height at which the FPS camera is positioned
	camHeight[SUPERMAN] = 11;
	camHeight[ANDROID] = 7;
	camHeight[GOKU] = 8;
	camHeight[THOR] = 11;
	camHeight[HAMMER] = 0.5;
	camHeight[LEGO] = 8;
	camHeight[IRONMAN] = 10;
	camHeight[BULBASAUR] = 5;
	camHeight[CHAMANDER] = 5;
	camHeight[SQUIRTLE] = 5;

	//	the color for each character
	color[SUPERMAN] = glm::vec3(0.023, 0.349, 0.541);
	color[ANDROID] = glm::vec3(0.502, 0.741, 0.004);
	color[GOKU] = glm::vec3(0.926, 0.647, 0.019);
	color[THOR] = glm::vec3(0.231, 0.231, 0.231);
	color[HAMMER] = glm::vec3(0.702, 0.702, 0.702);
	color[LEGO] = glm::vec3(1.000, 1.000, 0.004);
	color[IRONMAN] = glm::vec3(0.870, 0.117, 0.180);
	color[BULBASAUR] = glm::vec3(0.521, 0.804, 0.698);
	color[CHAMANDER] = glm::vec3(0.956, 0.694, 0.525);
	color[SQUIRTLE] = glm::vec3(0.513, 0.792, 0.858);
}

int main() {
	printMenu();

	ifstream file;
	file.open("data.in");
	if (file.is_open()) {
		file >> speed[1];
		file >> speed[2];
		file >> speed[0];
	}
	speed[0] *= 1.05;		//	because the player can accelerate, his speed is raised by 5%
	cout << speed[0] << " : " << speed[1] << " : " << speed[2] << endl;
	file.close();
	initialize();

	//initializeaza GLUT (fereastra + input + context OpenGL)
	lab::glut::WindowInfo window(std::string("Open Run - by Tase Gula"), 800, 500, 400, 100, true);
	lab::glut::ContextInfo context(3, 1, false);
	lab::glut::FramebufferInfo framebuffer(true, true, true, true);
	lab::glut::init(window, context, framebuffer);

	//initializeaza GLEW (ne incarca functiile openGL, altfel ar trebui sa facem asta manual!)
	glewExperimental = true;
	glewInit();
	std::cout << "GLEW:initializare" << std::endl;

	//creem clasa noastra si o punem sa asculte evenimentele de la GLUT
	//DUPA GLEW!!! ca sa avem functiile de OpenGL incarcate inainte sa ii fie apelat constructorul (care creeaza obiecte OpenGL)
	Laborator mylab;
	lab::glut::setListener(&mylab);

	//taste
	std::cout << "Taste:" << std::endl << "\tESC ... iesire" << std::endl << "\tSPACE ... reincarca shadere" << std::endl << "\ti ... toggle wireframe" << std::endl << "\to ... reseteaza camera" << std::endl;
	std::cout << "\tw ... translatie camera in fata" << std::endl << "\ts ... translatie camera inapoi" << std::endl;
	std::cout << "\ta ... translatie camera in stanga" << std::endl << "\td ... translatie camera in dreapta" << std::endl;
	std::cout << "\tr ... translatie camera in sus" << std::endl << "\tf ... translatie camera jos" << std::endl;
	std::cout << "\tq ... rotatie camera FPS pe Oy, minus" << std::endl << "\te ... rotatie camera FPS pe Oy, plus" << std::endl;
	std::cout << "\tz ... rotatie camera FPS pe Oz, minus" << std::endl << "\tc ... rotatie camera FPS pe Oz, plus" << std::endl;

	//run
	lab::glut::run();

	return 0;
}