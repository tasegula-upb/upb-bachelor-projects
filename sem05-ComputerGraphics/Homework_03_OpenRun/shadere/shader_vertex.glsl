#version 330

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_color;
layout(location = 2) in vec2 jiggle;
layout(location = 3) in vec3 in_normal;
layout(location = 4) in vec2 in_texcoord;

uniform mat4 model_matrix, view_matrix, projection_matrix;
uniform vec3 wave;

out vec3 vertex_to_fragment_color;

void main(){
	
	vertex_to_fragment_color = in_color;

	vec3 v = in_position;

	if (wave.z != 0) {
		if (jiggle.x == 1) {
			v.y = sin(wave.x * v.x + wave.z) * cos(wave.x * v.z + wave.z) * wave.y;
		}
	}

	gl_Position = projection_matrix*view_matrix*model_matrix*vec4(v,1);

}
