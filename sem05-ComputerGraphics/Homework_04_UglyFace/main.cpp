﻿//-------------------------------------------------------------------------------------------------
// Descriere: fisier main
//
// Autor: student
// Data: today
//-------------------------------------------------------------------------------------------------

//incarcator de meshe
#include "lab_mesh_loader.hpp"
//geometrie: drawSolidCube, drawWireTeapot...
#include "lab_geometry.hpp"
//incarcator de shadere
#include "lab_shader_loader.hpp"
//interfata cu glut, ne ofera fereastra, input, context opengl
#include "lab_glut.hpp"
//texturi
#include "lab_texture_loader.hpp"

//alte functii / chestii
#include "functions.h"

//time
#include <ctime>


class Laborator : public lab::glut::WindowListener{

//variabile
private:
	glm::mat4 model_matrix, view_matrix, projection_matrix;			//matrici 4x4 pt modelare vizualizare proiectie
	unsigned int gl_program_shader_faces;							//id obiect shader pentru fata
	unsigned int gl_program_shader_notes;							//id obiect shader pentru note
	unsigned int gl_program_shader_curent;							//id obiect shader curent

	unsigned int vao[6], vbo[6], ibo[6], num[6];					//meshe
	unsigned int pvao, pvbo, pibo, pnum;							//meshe pentru particula
	unsigned int gtexture, ptexture;								//texturi

	// faces stuff (name says everything)
	glm::vec3 light_position;
	glm::vec3 eye_position;
	unsigned int material_shininess;
	float material_kd;
	float material_ks;
	
	// time and animation stuff (name says everything)
	float timer, K, start;
	int N[6];
	int state;

	// time and animation stuff for particle system
	float ptimer, pk, pstart;
	int ptime;
	int x;

//metode
public:
	
	//constructor .. e apelat cand e instantiata clasa
	Laborator(){
		srand(time(NULL));

		//setari pentru desenare, clear color seteaza culoarea de clear pentru ecran (format R,G,B,A)
		glClearColor(0.5,0.5,0.5,1);
		glClearDepth(1);			//clear depth si depth test (nu le studiem momentan, dar avem nevoie de ele!)
		glEnable(GL_DEPTH_TEST);	//sunt folosite pentru a determina obiectele cele mai apropiate de camera (la curs: algoritmul pictorului, algoritmul zbuffer)
		
		//incarca un shader din fisiere si gaseste locatiile matricilor relativ la programul creat
		gl_program_shader_notes = lab::loadShader("shadere/shader_notes_vertex.glsl", "shadere/shader_notes_geometry.glsl", "shadere/shader_notes_fragment.glsl");
		gl_program_shader_faces = lab::loadShader("shadere/shader_faces_vertex.glsl", "shadere/shader_faces_fragment.glsl");
		gl_program_shader_curent = gl_program_shader_faces;
		
		//incarca meshele
		lab::loadObj(vao[0], vbo[0], ibo[0], num[0], "resurse/girl_sleep.obj", vao[1], vbo[1], ibo[1], num[1],
						vao[2], vbo[2], ibo[2], num[2], "resurse/girl_surprise.obj", vao[3], vbo[3], ibo[3], num[3],
						vao[4], vbo[4], ibo[4], num[4],	"resurse/girl_annoyed.obj", vao[5], vbo[5], ibo[5], num[5]);

		//incarca particula
		createParticleSystem(glm::vec3(-25, 11, 0), 201, 20, pvao, pvbo, pibo, pnum);

		//incarca textura pentru fata
		gtexture = lab::loadTextureBMP("resurse/girl_texture.bmp");
		ptexture = lab::loadTextureBMP("resurse/music.bmp");

		//lumina & material
		eye_position = glm::vec3(0,7,3);
		light_position = glm::vec3(1,8,3);
		material_shininess = 100;
		material_kd = 0.7;
		material_ks = 0.3;

		//matrici de modelare si vizualizare
		model_matrix = glm::mat4(1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1);
		view_matrix = glm::lookAt(eye_position, glm::vec3(0,7,0), glm::vec3(0,1,0));
		
		//animation timer
		timer = 0;
		N[0] = 3000;	// sleeping
		N[1] = 3000;	// sleep -> surprise
		N[2] = 1000;	// surprise
		N[3] = 3000;	// surprise -> annoyed
		N[4] = 1000;	// annoyed
		N[5] = 3000;	// annoyed -> sleep

		pstart = start = GetTickCount();

		//starea initiala
		state = 0;

		//particle timer
		ptimer = 0;
		x = 0;
		ptime = N[1] + N[2] + N[3] + N[4] + N[5];

		//desenare wireframe
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	//destructor .. e apelat cand e distrusa clasa
	~Laborator(){
		//distruge shadere
		glDeleteProgram(gl_program_shader_faces);
		glDeleteProgram(gl_program_shader_notes);
		
		for (int i = 0; i < 4; i++) {
			glDeleteBuffers(1, &vbo[i]);
			glDeleteBuffers(1, &ibo[i]);
			glDeleteVertexArrays(1, &vbo[i]);
		}

	}


	//--------------------------------------------------------------------------------------------
	//functii de cadru ---------------------------------------------------------------------------

	//functie chemata inainte de a incepe cadrul de desenare, o folosim ca sa updatam situatia scenei ( modelam/simulam scena)
	void notifyBeginFrame(){}

	int n = 0;
	//functia de afisare (lucram cu banda grafica)
	void notifyDisplayFrame(){
		//update timer
		pk = K = GetTickCount();
		//continua animatia
		int q = K - start;
		if (K - start < N[state]) {
			timer = (K - start) / N[state];
		}
		else {
			start = K;
			timer = 0;
			state == 5 ? state = 0, x = 0 : state++;

			if (state == 1 && x == 0) {
				pstart = pk;
				ptimer = 0;
				x = 1;
			}
		}

		ptimer = (pk - pstart) / ptime * x;

		//bufferele din framebuffer sunt aduse la valorile initiale (setate de clear color si cleardepth)
		//adica se sterge ecranul si se pune culoare (si alte propietati) initiala
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


//*
		//foloseste shaderul
		glUseProgram(gl_program_shader_faces);
				
		//trimite variabile uniforme la shader
		glUniformMatrix4fv(glGetUniformLocation(gl_program_shader_faces, "model_matrix"), 1, false, glm::value_ptr(model_matrix));
		glUniformMatrix4fv(glGetUniformLocation(gl_program_shader_faces, "view_matrix"), 1, false, glm::value_ptr(view_matrix));
		glUniformMatrix4fv(glGetUniformLocation(gl_program_shader_faces, "projection_matrix"), 1, false, glm::value_ptr(projection_matrix));
		glUniform1f(glGetUniformLocation(gl_program_shader_faces, "timer"), timer);

		glUniform3f(glGetUniformLocation(gl_program_shader_faces, "light_position"), light_position.x, light_position.y, light_position.z);
		glUniform3f(glGetUniformLocation(gl_program_shader_faces, "eye_position"), eye_position.x, eye_position.y, eye_position.z);
		glUniform1i(glGetUniformLocation(gl_program_shader_faces, "material_shininess"), material_shininess);
		glUniform1f(glGetUniformLocation(gl_program_shader_faces, "material_kd"), material_kd);
		glUniform1f(glGetUniformLocation(gl_program_shader_faces, "material_ks"), material_ks);
		
		//bing textura
		glActiveTexture(GL_TEXTURE0 + 1);
		glBindTexture(GL_TEXTURE_2D, gtexture);
		glUniform1i(glGetUniformLocation(gl_program_shader_faces, "textura"), 1);

		//bind obiect
		glBindVertexArray(vao[state]);
		glDrawElements(GL_TRIANGLES, num[state], GL_UNSIGNED_INT, 0);
//*/

//*
		/** ---------------------------------------------------------------------------------------------
		 **			PARTICLE SHADER 
		 **	------------------------------------------------------------------------------------------ */

		//foloseste shaderul
		glUseProgram(gl_program_shader_notes);

		//trimite variabile uniforme la shader
		glUniformMatrix4fv(glGetUniformLocation(gl_program_shader_notes, "model_matrix"), 1, false, glm::value_ptr(model_matrix));
		glUniformMatrix4fv(glGetUniformLocation(gl_program_shader_notes, "view_matrix"), 1, false, glm::value_ptr(view_matrix));
		glUniformMatrix4fv(glGetUniformLocation(gl_program_shader_notes, "projection_matrix"), 1, false, glm::value_ptr(projection_matrix));
		glUniform3f(glGetUniformLocation(gl_program_shader_notes, "eye_position"), eye_position.x, eye_position.y, eye_position.z);
		glUniform1f(glGetUniformLocation(gl_program_shader_notes, "timer"), ptimer);

		//bing textura
		glActiveTexture(GL_TEXTURE0 + 1);
		glBindTexture(GL_TEXTURE_2D, ptexture);
		glUniform1i(glGetUniformLocation(gl_program_shader_notes, "textura"), 1);

		//bind obiect
		glBindVertexArray(pvao);
		glDrawElements(GL_POINTS, pnum, GL_UNSIGNED_INT, 0);
//		glDrawArrays(GL_POINTS, 0, pnum);
//*/
	}

	//functie chemata dupa ce am terminat cadrul de desenare (poate fi folosita pt modelare/simulare)
	void notifyEndFrame(){}

	//functei care e chemata cand se schimba dimensiunea ferestrei initiale
	void notifyReshape(int width, int height, int previos_width, int previous_height){
		//reshape
		if(height==0) height=1;
		glViewport(0,0,width,height);
		projection_matrix = glm::perspective(90.0f, (float)width/(float)height,0.1f, 10000.0f);
	}


	//--------------------------------------------------------------------------------------------
	//functii de input output --------------------------------------------------------------------
	
	//tasta apasata
	void notifyKeyPressed(unsigned char key_pressed, int mouse_x, int mouse_y){
		if(key_pressed == 27) lab::glut::close();	//ESC inchide glut si 
		if(key_pressed == 32) {
			//SPACE reincarca shaderul si recalculeaza locatiile (offseti/pointeri)
			glDeleteProgram(gl_program_shader_faces);
			glDeleteProgram(gl_program_shader_notes);
			gl_program_shader_faces = lab::loadShader("shadere/shader_faces_vertex.glsl", "shadere/shader_faces_fragment.glsl");
			gl_program_shader_notes = lab::loadShader("shadere/shader_notes_vertex.glsl", "shadere/shader_notes_geometry.glsl", "shadere/shader_notes_fragment.glsl");
		}
		if(key_pressed == 'w'){
			static bool wire =true;
			wire=!wire;
			glPolygonMode(GL_FRONT_AND_BACK, (wire?GL_LINE:GL_FILL));
		}

	}

	//tasta ridicata
	void notifyKeyReleased(unsigned char key_released, int mouse_x, int mouse_y){	}

	//tasta speciala (up/down/F1/F2..) apasata
	void notifySpecialKeyPressed(int key_pressed, int mouse_x, int mouse_y){
		if(key_pressed == GLUT_KEY_F1) lab::glut::enterFullscreen();
		if(key_pressed == GLUT_KEY_F2) lab::glut::exitFullscreen();
	}

	//tasta speciala ridicata
	void notifySpecialKeyReleased(int key_released, int mouse_x, int mouse_y){}
	//drag cu mouse-ul
	void notifyMouseDrag(int mouse_x, int mouse_y){ }
	//am miscat mouseul (fara sa apas vreun buton)
	void notifyMouseMove(int mouse_x, int mouse_y){ }
	//am apasat pe un boton
	void notifyMouseClick(int button, int state, int mouse_x, int mouse_y){ }
	//scroll cu mouse-ul
	void notifyMouseScroll(int wheel, int direction, int mouse_x, int mouse_y){ std::cout<<"Mouse scroll"<<std::endl;}

};

int main(){
	//initializeaza GLUT (fereastra + input + context OpenGL)
	lab::glut::WindowInfo window(std::string("Creepy Face"),800,600,100,100,true);
	lab::glut::ContextInfo context(3,3,false);
	lab::glut::FramebufferInfo framebuffer(true,true,true,true);
	lab::glut::init(window,context, framebuffer);

	//initializeaza GLEW (ne incarca functiile openGL, altfel ar trebui sa facem asta manual!)
	glewExperimental = true;
	glewInit();
	std::cout<<"GLEW:initializare"<<std::endl;

	//creem clasa noastra si o punem sa asculte evenimentele de la GLUT
	//DUPA GLEW!!! ca sa avem functiile de OpenGL incarcate inainte sa ii fie apelat constructorul (care creeaza obiecte OpenGL)
	Laborator mylab;
	lab::glut::setListener(&mylab);

	//taste
	std::cout<<"Taste:"<<std::endl<<"\tESC ... iesire"<<std::endl<<"\tSPACE ... reincarca shadere"<<std::endl<<"\tw ... toggle wireframe"<<std::endl;

	//run
	lab::glut::run();

	return 0;
}