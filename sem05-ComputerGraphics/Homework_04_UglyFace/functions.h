#pragma once
#include "dependente\glew\glew.h"
#include "dependente\glm\glm.hpp"
#include "dependente\glm\gtc\type_ptr.hpp"
#include "dependente\glm\gtc\matrix_transform.hpp"

#include "lab_mesh_loader.hpp"
#include <vector>
#include <stdlib.h>
#include <time.h> 
#include <ctime>

glm::vec3 randomColor() {

	float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);	//r -= floor(r);
	float g = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);	//g -= floor(g);
	float b = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);	//b -= floor(b);

//	printf("%f %f %f\n", r, g, b);
	return glm::vec3(r, g, b);
}

glm::vec3 randomEnd(int xmin, int xmax, int ymin, int ymax) {
	float x = 0.0f;
	float y = 0.0f;

	x = rand() % xmax;
	while (x < xmin) x = rand() % xmax;
	x -= static_cast <float> (rand()) / static_cast <float> (RAND_MAX);

	y = rand() % ymax;
	while (y < ymin) y = rand() % ymax;
	y -= static_cast <float> (rand()) / static_cast <float> (RAND_MAX);

	return glm::vec3(x, y, 0.0f);
}

void createParticleSystem(glm::vec3 center, int n, int rows, unsigned int &vao, unsigned int& vbo, unsigned int &ibo, unsigned int &num) {

	std::vector<lab::VertexFormat> vertices;
	std::vector<unsigned int> indices;

	int i = 0, k = 0;
	unsigned int aux = 0;

	float x = +0.20;
	float y = -0.40;

	glm::vec3 color;
	glm::vec3 toPos = glm::vec3(11, 7, 0);
	glm::vec3 start;
	
	start = center + glm::vec3(-n * 0.5 * x, -rows * 0.5 * y, 0.00);
	for (i = 0; i < rows; i++) {
		for (k = 0; k < n; k++) {
			if (rand() % 5 == 0) {
				color = randomColor();

				vertices.push_back(lab::VertexFormat(start + glm::vec3(x * k, y * i, 0.00), color, randomEnd(11,17,3,7) ));
				indices.push_back(aux);
				aux++;
			}
		}
	}

	start += glm::vec3(0.5 * x, 0.5 * y, 0.00);
	for (i = 0; i < rows - 1; i++) {
		for (k = 0; k < n - 1; k++) {
			if (rand() % 5 == 0) {
				color = randomColor();

				vertices.push_back(lab::VertexFormat(start + glm::vec3(x * k, y * i, 0.00), color, randomEnd(11,17,3,7)));
				indices.push_back(aux);
				aux++;
			}
		}
	}


	unsigned int gl_vertex_array_object, gl_vertex_buffer_object, gl_index_buffer_object;

	glGenVertexArrays(1, &gl_vertex_array_object);
	glBindVertexArray(gl_vertex_array_object);

	glGenBuffers(1, &gl_vertex_buffer_object);
	glBindBuffer(GL_ARRAY_BUFFER, gl_vertex_buffer_object);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(lab::VertexFormat), &vertices[0], GL_STATIC_DRAW);

	glGenBuffers(1, &gl_index_buffer_object);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gl_index_buffer_object);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(lab::VertexFormat), (void*)0);						//trimite pozitii pe pipe 0
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(lab::VertexFormat), (void*)(sizeof(float)* 3));		//trimite pozitii pe pipe 1
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(lab::VertexFormat), (void*)(sizeof(float)* (2 * 3 + 2)));	//trimite pozitii pe pipe 2


	vao = gl_vertex_array_object;
	vbo = gl_vertex_buffer_object;
	ibo = gl_index_buffer_object;
	num = indices.size();

}