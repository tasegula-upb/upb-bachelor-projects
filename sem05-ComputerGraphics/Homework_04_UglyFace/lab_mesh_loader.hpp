//-------------------------------------------------------------------------------------------------
// Descriere: todo
//
// Contine:
//		todo
//
//// Nota2:
//		sunteti incurajati sa va scrieti parsele proprii pentru alte formaturi. Format sugerat: ply,off
//
// Autor: Lucian Petrescu
// Data: 28 Sep 2013
//-------------------------------------------------------------------------------------------------

#pragma once
#include "dependente\glew\glew.h"
#include "dependente\glm\glm.hpp"
#include "dependente\glm\gtc\type_ptr.hpp"
#include "dependente\glm\gtc\matrix_transform.hpp"
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>

namespace lab{

	//care este formatul unui vertex?
	struct VertexFormat{
		glm::vec3 position;				//	pozitia unui vertex (x, y, z)
		glm::vec3 normal;				//	normala
		glm::vec2 texcoord;				//	textura		
		glm::vec3 nextPos;
		glm::vec3 nextNorm;
		
		VertexFormat(){
			position = glm::vec3(0, 0, 0);
			normal = glm::vec3(0, 0, 0);
			texcoord = glm::vec2(0, 0);
			nextPos = glm::vec3(0, 0, 0);
			nextNorm = glm::vec3(0, 0, 0);
		}
		VertexFormat(glm::vec3 _position){
			position = _position;
			normal = glm::vec3(0, 0, 0);
			texcoord = glm::vec2(0, 0);
			nextPos = glm::vec3(0, 0, 0);
			nextNorm = glm::vec3(0, 0, 0);
		}
		VertexFormat(glm::vec3 _position, glm::vec3 _normal){
			position = _position;
			normal = _normal;
			texcoord = glm::vec2(0, 0);
			nextPos = glm::vec3(0, 0, 0);
			nextNorm = glm::vec3(0, 0, 0);
		}
		VertexFormat(glm::vec3 _position, glm::vec2 _texcoord){
			position = _position;
			normal = glm::vec3(0, 0, 0);
			texcoord = _texcoord;
			nextPos = glm::vec3(0, 0, 0);
			nextNorm = glm::vec3(0, 0, 0);
		}
		VertexFormat(glm::vec3 _position, glm::vec3 _normal, glm::vec2 _texcoord){
			position = _position;
			normal = _normal;
			texcoord = _texcoord;
			nextPos = glm::vec3(0, 0, 0);
			nextNorm = glm::vec3(0, 0, 0);
		}
		VertexFormat(glm::vec3 _position, glm::vec3 _color, glm::vec3 _nextPos) {
			position = _position;
			normal = _color;
			texcoord = glm::vec2(0, 0);
			nextPos = _nextPos;
			nextNorm = glm::vec3(0, 0, 0);
		}
		VertexFormat operator=(const VertexFormat &rhs){
			position = rhs.position;
			normal = rhs.normal;
			texcoord = rhs.texcoord;
			nextPos = rhs.nextPos;
			nextNorm = rhs.nextNorm;
			return (*this);
		}
	};

	//definitie forward
	void _loadObjFile(const std::string &filename, std::vector<VertexFormat> &vertices, std::vector<unsigned int> &indices);
	void _unifyObjects(std::vector<VertexFormat> &v1, std::vector<unsigned int> &i1, std::vector<VertexFormat> &v2, std::vector<unsigned int> &i2,
						unsigned int &vao, unsigned int& vbo, unsigned int &ibo, unsigned int &num);
	void _unifyObject(std::vector<VertexFormat> &vertices, std::vector<unsigned int> &indices, 
						unsigned int &vao, unsigned int& vbo, unsigned int &ibo, unsigned int &num);

	//incarca un fisier de tip Obj (fara NURBS, fara materiale)
	//returneaza in argumentele trimise prin referinta id-ul OpenGL pentru vao(Vertex Array Object), pentru vbo(Vertex Buffer Object) si pentru ibo(Index Buffer Object)

	void loadObj(unsigned int &vao0, unsigned int &vbo0, unsigned int &ibo0, unsigned int &num0,
					const std::string &filename1, unsigned int &vao1, unsigned int& vbo1, unsigned int &ibo1, unsigned int &num1,
					unsigned int &vao2, unsigned int& vbo2, unsigned int &ibo2, unsigned int &num2,
					const std::string &filename2, unsigned int &vao3, unsigned int& vbo3, unsigned int &ibo3, unsigned int &num3,
					unsigned int &vao4, unsigned int& vbo4, unsigned int &ibo4, unsigned int &num4,
					const std::string &filename3, unsigned int &vao5, unsigned int& vbo5, unsigned int &ibo5, unsigned int &num5)
	{

		//incarca vertecsii si indecsii din fisier
		std::vector<VertexFormat> vertices1;
		std::vector<unsigned int> indices1;
		_loadObjFile(filename1, vertices1, indices1);
		std::cout << "Mesh Loader : am incarcat fisierul " << filename1 << std::endl;

		std::vector<VertexFormat> vertices2;
		std::vector<unsigned int> indices2;
		_loadObjFile(filename2, vertices2, indices2);
		std::cout << "Mesh Loader : am incarcat fisierul " << filename2 << std::endl;
		
		std::vector<VertexFormat> vertices3;
		std::vector<unsigned int> indices3;
		_loadObjFile(filename3, vertices3, indices3);
		std::cout << "Mesh Loader : am incarcat fisierul " << filename3 << std::endl;
		
		//creeaza obiectele OpenGL necesare desenarii

//*		
		_unifyObject(vertices1, indices1, vao0, vbo0, ibo0, num0);

		_unifyObjects(vertices1, indices1, vertices2, indices2, vao1, vbo1, ibo1, num1);

		_unifyObject(vertices2, indices2, vao2, vbo2, ibo2, num2);
		_unifyObjects(vertices2, indices2, vertices3, indices3, vao3, vbo3, ibo3, num3);

		_unifyObject(vertices3, indices3, vao4, vbo4, ibo4, num4);
		_unifyObjects(vertices3, indices3, vertices1, indices1, vao5, vbo5, ibo5, num5);
//*/	
	}

	void _unifyObjects(std::vector<VertexFormat> &vertices1, std::vector<unsigned int> &indices1, std::vector<VertexFormat> &vertices2, std::vector<unsigned int> &indices2,
						unsigned int &vao, unsigned int& vbo, unsigned int &ibo, unsigned int &num)
	{

		//creeaza obiectele OpenGL necesare desenarii (vbo, ibo mai jos pentru fiecare fisier in parte)
		unsigned int gl_vertex_array_object;

		//vertex array object -> un obiect ce reprezinta un container pentru starea de desenare
		glGenVertexArrays(1, &gl_vertex_array_object);
		glBindVertexArray(gl_vertex_array_object);

		/**	---------------------------------------------------------------------------------------
		 **		ADAUGA PRIMA PARTE A VERTECSILOR (cea a obiectului orginal)
		 ** ------------------------------------------------------------------------------------ */
		unsigned int gl_vertex_buffer_object, gl_index_buffer_object;

		//vertex buffer object -> un obiect in care tinem vertecsii
		glGenBuffers(1, &gl_vertex_buffer_object);
		glBindBuffer(GL_ARRAY_BUFFER, gl_vertex_buffer_object);
		glBufferData(GL_ARRAY_BUFFER, vertices1.size()*sizeof(VertexFormat), &vertices1[0], GL_STATIC_DRAW);

		//index buffer object -> un obiect in care tinem indecsii
		glGenBuffers(1, &gl_index_buffer_object);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gl_index_buffer_object);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices1.size() * sizeof(unsigned int), &indices1[0], GL_STATIC_DRAW);

		//legatura intre atributele vertecsilor si pipeline, datele noastre sunt INTERLEAVED.
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)0);						//trimite pozitii pe pipe 0
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(sizeof(float)* 3));		//trimite normale pe pipe 1
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(2 * sizeof(float)* 3));	//trimite texcoords pe pipe 2
		
		/**	---------------------------------------------------------------------------------------
		 **		ADAUGA A DOUA PARTE A VERTECSILOR (cea a obiectului spre care tranzitam)
		 **	------------------------------------------------------------------------------------ */
		unsigned int gl_vertex_buffer_object2, gl_index_buffer_object2;

		//vertex buffer object -> un obiect in care tinem vertecsii
		glGenBuffers(1, &gl_vertex_buffer_object2);
		glBindBuffer(GL_ARRAY_BUFFER, gl_vertex_buffer_object2);
		glBufferData(GL_ARRAY_BUFFER, vertices2.size() * sizeof(VertexFormat), &vertices2[0], GL_STATIC_DRAW);

		//index buffer object -> un obiect in care tinem indecsii
		glGenBuffers(1, &gl_index_buffer_object2);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gl_index_buffer_object2);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices2.size() * sizeof(unsigned int), &indices2[0], GL_STATIC_DRAW);

		//legatura intre atributele vertecsilor si pipeline, datele noastre sunt INTERLEAVED.
		glEnableVertexAttribArray(3);
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)0);						//trimite pozitii pe pipe 3
		glEnableVertexAttribArray(4);
		glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(sizeof(float)* 3));		//trimite normale pe pipe 4

		vao = gl_vertex_array_object;
		vbo = gl_vertex_buffer_object;
		ibo = gl_index_buffer_object;
		num = indices1.size();
	}
	
	void _unifyObject(std::vector<VertexFormat> &vertices, std::vector<unsigned int> &indices, unsigned int &vao, unsigned int& vbo, unsigned int &ibo, unsigned int &num)
	{

		//creeaza obiectele OpenGL necesare desenarii (vbo, ibo mai jos pentru fiecare fisier in parte)
		unsigned int gl_vertex_array_object;

		//vertex array object -> un obiect ce reprezinta un container pentru starea de desenare
		glGenVertexArrays(1, &gl_vertex_array_object);
		glBindVertexArray(gl_vertex_array_object);

		/**	---------------------------------------------------------------------------------------
		**		ADAUGA PRIMA PARTE A VERTECSILOR (cea a obiectului orginal)
		**			si copiaza primele doua atribute din nou
		** ------------------------------------------------------------------------------------ */
		unsigned int gl_vertex_buffer_object, gl_index_buffer_object;

		//vertex buffer object -> un obiect in care tinem vertecsii
		glGenBuffers(1, &gl_vertex_buffer_object);
		glBindBuffer(GL_ARRAY_BUFFER, gl_vertex_buffer_object);
		glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(VertexFormat), &vertices[0], GL_STATIC_DRAW);

		//index buffer object -> un obiect in care tinem indecsii
		glGenBuffers(1, &gl_index_buffer_object);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gl_index_buffer_object);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

		//legatura intre atributele vertecsilor si pipeline, datele noastre sunt INTERLEAVED.
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)0);						//trimite pozitii pe pipe 0
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(sizeof(float)* 3));		//trimite normale pe pipe 1
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(2 * sizeof(float)* 3));	//trimite texcoords pe pipe 2
		glEnableVertexAttribArray(3);
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)0);						//trimite pozitii pe pipe 3
		glEnableVertexAttribArray(4);
		glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(sizeof(float)* 3));		//trimite normale pe pipe 4

		vao = gl_vertex_array_object;
		vbo = gl_vertex_buffer_object;
		ibo = gl_index_buffer_object;
		num = indices.size();
	}

	//-------------------------------------------------------------------------------------------------
	//urmeaza cod de parsare...

	//helper funcs
	float _stringToFloat(const std::string &source){
		std::stringstream ss(source.c_str());
		float result;
		ss>>result;
		return result;
	}
	//transforms a string to an int
	unsigned int _stringToUint(const std::string &source){
		std::stringstream ss(source.c_str());
		unsigned int result;
		ss>>result;
		return result;
	}
	//transforms a string to an int
	int _stringToInt(const std::string &source){
		std::stringstream ss(source.c_str());
		int result;
		ss>>result;
		return result;
	}
	//writes the tokens of the source string into tokens
	void _stringTokenize(const std::string &source, std::vector<std::string> &tokens){
		tokens.clear();
		std::string aux=source;	
		for(unsigned int i=0;i<aux.size();i++) if(aux[i]=='\t' || aux[i]=='\n') aux[i]=' ';
		std::stringstream ss(aux,std::ios::in);
		while(ss.good()){
			std::string s;
			ss>>s;
			if(s.size()>0) tokens.push_back(s);
		}
	}
	//variant for faces
	void _faceTokenize(const std::string &source, std::vector<std::string> &tokens){
		std::string aux=source;	
		for(unsigned int i=0;i<aux.size();i++) if(aux[i]=='\\' || aux[i]=='/') aux[i]=' ';
		_stringTokenize(aux,tokens);		
	}

	
	//incarca doar geometrie dintr-un fisier obj (nu incarca high order surfaces, materiale, coordonate extra, linii)
	// Format: http://paulbourke.net/dataformats/obj/
	//nu calculeaza normale sau coordonate de textura sau tangente, performanta neoptimala dar usor de citit (relativ la alte parsere..)
	//considera geometria ca pe un singur obiect, deci nu tine cont de grupuri sau de smoothing
	//daca apar probleme la incarcare fisier(?) incarcati mesha originala in meshlab(free!), salvati si folositi varianta noua.
	void _loadObjFile(const std::string &filename, std::vector<VertexFormat> &vertices, std::vector<unsigned int> &indices){
		//citim din fisier
		std::ifstream file(filename.c_str(), std::ios::in | std::ios::binary);
		if(!file.good()){
			std::cout<<"Mesh Loader: Nu am gasit fisierul obj "<<filename<<" sau nu am drepturile sa il deschid!"<<std::endl;
			std::terminate();
		}

		std::string line;
		std::vector<std::string> tokens, facetokens;
		std::vector<glm::vec3> positions;	positions.reserve(1000);
		std::vector<glm::vec3> normals;		normals.reserve(1000);
		std::vector<glm::vec2> texcoords;	texcoords.reserve(1000);
		
		while(std::getline(file,line)){
			//tokenizeaza linie citita
			_stringTokenize(line,tokens);

			//daca nu am nimic merg mai departe
			if(tokens.size()==0) continue;

			//daca am un comentariu merg mai departe
			if(tokens.size()>0 && tokens[0].at(0)=='#') continue;

			//daca am un vertex
			if(tokens.size()>3 && tokens[0]=="v") positions.push_back(glm::vec3(_stringToFloat(tokens[1]), _stringToFloat(tokens[2]), _stringToFloat(tokens[3]) ));
			
			//daca am o normala
			if(tokens.size()>3 && tokens[0]=="vn") normals.push_back(glm::vec3(_stringToFloat(tokens[1]), _stringToFloat(tokens[2]), _stringToFloat(tokens[3]) ));

			//daca am un texcoord
			if(tokens.size()>2 && tokens[0]=="vt") texcoords.push_back(glm::vec2(_stringToFloat(tokens[1]), _stringToFloat(tokens[2]) ));

			//daca am o fata (f+ minim 3 indecsi)
			if(tokens.size()>=4 && tokens[0]=="f") {


				//foloseste primul vertex al fetei pentru a determina formatul fetei (v v/t v//n v/t/n) = (1 2 3 4)
				//in acest caz, doar v/t/n => 4
				unsigned int face_format = 0;

/*
				if(tokens[1].find("//")!=std::string::npos) face_format = 3;
				_faceTokenize(tokens[1],facetokens);
				if(facetokens.size()==3) face_format =4; // vertecsi/texcoords/normale
				else{
					if(facetokens.size()==2){
						if(face_format !=3) face_format=2;	//daca nu am vertecsi/normale am vertecsi/texcoords
					}else{
						face_format =1; //doar vertecsi
					}
				}
//*/

				//primul index din acest poligon
				unsigned int index_of_first_vertex_of_face = -1;
				
				for(unsigned int num_token =1; num_token<tokens.size();num_token++) {
					if(tokens[num_token].at(0)=='#') break;					//comment dupa fata
					_faceTokenize(tokens[num_token],facetokens);

					//pozitie normala si texcoord (altceva nu putem avea)
					int p_index = _stringToInt(facetokens[0]);
					if (p_index>0) p_index -= 1;								//obj has 1...n indices
					else p_index = positions.size() + p_index;				//index negativ

					int t_index = _stringToInt(facetokens[1]);
					if (t_index > 0) t_index -= 1;								//obj has 1...n indices
					else t_index = normals.size() + t_index;					//index negativ

					int n_index = _stringToInt(facetokens[2]);
					if (n_index > 0) n_index -= 1;								//obj has 1...n indices
					else n_index = normals.size() + n_index;					//index negativ

					vertices.push_back(VertexFormat(positions[p_index], normals[n_index], texcoords[t_index]));
/*					
					if(face_format==1){
						//doar pozitie
						int p_index = _stringToInt(facetokens[0]);
						if(p_index>0) p_index -=1;								//obj has 1...n indices
						else p_index = positions.size()+p_index;				//index negativ
					
						vertices.push_back(VertexFormat(positions[p_index]));
					}else if(face_format==2){
						// pozitie si texcoord
						int p_index = _stringToInt(facetokens[0]);
						if(p_index>0) p_index -=1;								//obj has 1...n indices
						else p_index = positions.size()+p_index;				//index negativ

						int t_index = _stringToInt(facetokens[1]);
						if(t_index>0) t_index -=1;								//obj has 1...n indices
						else t_index = texcoords.size()+t_index;				//index negativ

						vertices.push_back(VertexFormat(positions[p_index], texcoords[t_index]));
					}else if(face_format==3){
						//pozitie si normala
						int p_index = _stringToInt(facetokens[0]);
						if(p_index>0) p_index -=1;								//obj has 1...n indices
						else p_index = positions.size()+p_index;				//index negativ

						int n_index = _stringToInt(facetokens[1]);
						if(n_index>0) n_index -=1;								//obj has 1...n indices
						else n_index = normals.size()+n_index;					//index negativ

						vertices.push_back(VertexFormat(positions[p_index], normals[n_index]));
					}
					else {
						
					}
//*/

					//adauga si indecsii
					if (num_token<4) {			
						if (num_token == 1) index_of_first_vertex_of_face = vertices.size() - 1;
						//doar triunghiuri f 0 1 2 3 (4 indecsi, primul e ocupat de f)
						indices.push_back(vertices.size()-1);
					} else {
						//polygon => triunghi cu ultimul predecesor vertexului nou adaugat si 0 relativ la vertecsi poligon(independent clockwise)
						indices.push_back(index_of_first_vertex_of_face);
						indices.push_back(vertices.size() - 2);
						indices.push_back(vertices.size() - 1);
					}
				}//end for

			}//end face
			
		}//end while
	}

}