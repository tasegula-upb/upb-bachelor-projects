#version 330

layout(location = 0) in vec3 in_position;		
layout(location = 1) in vec3 in_normal;		
layout(location = 2) in vec2 in_texcoord;
layout(location = 3) in vec3 to_position;		
layout(location = 4) in vec3 to_normal;	

uniform mat4 model_matrix, view_matrix, projection_matrix;

uniform float timer;

out vec3 world_pos;
out vec3 world_normal;

out vec2 texcoord;

void main(){
	texcoord = in_texcoord;

	vec3 position	= in_position;
	vec3 normal		= in_normal;

	position	= in_position * (1 - timer) + to_position * timer;
	normal		= in_normal * (1 - timer) + to_normal * timer;
	
	world_pos		= (model_matrix * vec4(position, 1)).xyz;
	world_normal	= normalize(mat3(model_matrix) * normal); 

	gl_Position	= projection_matrix * view_matrix * model_matrix * vec4(position, 1);
}
