#version 330
layout(location = 0) out vec4 out_color;

uniform vec3 light_position;
uniform vec3 eye_position;
uniform int material_shininess;
uniform float material_kd;
uniform float material_ks;

uniform sampler2D textura;
uniform int has_alpha;

in vec3 world_pos;
in vec3 world_normal;

in vec2 texcoord;

void main(){
	vec3 tex = texture(textura, texcoord).xyz;

	vec3 l = normalize(light_position - world_pos);
	vec3 v = normalize(eye_position - world_pos);
	vec3 h = normalize(l + v);

	float ambientala = 0.1f;
	float difuza	= material_kd * max(dot(world_normal, l), 0);
	float spectrala = material_ks * pow( max(dot(world_normal, h), 0), material_shininess) * (difuza < 0 ? 0 : 1);
	
	float light = 0;
	light = ambientala + difuza + spectrala;

	out_color = vec4(difuza * tex + spectrala + ambientala, 1);

}