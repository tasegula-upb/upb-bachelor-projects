#version 330

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_color;
layout(location = 2) in vec3 to_position;

uniform float timer;

out vec3 glcolor;

void main() {
	glcolor = in_color;

	vec3 position = in_position * (1 - timer) + to_position * timer;
	gl_Position = vec4(position, 1);	
}
