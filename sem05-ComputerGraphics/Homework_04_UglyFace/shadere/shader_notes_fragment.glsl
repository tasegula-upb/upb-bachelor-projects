#version 330

uniform sampler2D textura;

in vec2 texcoord;
in vec3 color;
out vec4 out_color;

void main() {
	vec3 tex = texture(textura, texcoord).xyz;

	if (tex.r > 0.75 && tex.g > 0.75 && tex.b > 0.75) {
		discard;
	}
	else {
		out_color = vec4(color, 1);
	}
}
