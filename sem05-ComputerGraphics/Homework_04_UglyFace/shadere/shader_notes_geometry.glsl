#version 330
layout(points) in;
layout(triangle_strip) out;
layout(max_vertices = 100) out;

uniform mat4 model_matrix, view_matrix, projection_matrix;
uniform vec3 eye_position;

out vec2 texcoord;

in vec3 glcolor[];
out vec3 color;

void main() {
	vec4 position = projection_matrix * view_matrix * model_matrix * gl_in[0].gl_Position;
	
	float offset = 0.2f;
	color = glcolor[0];

	gl_Position = position + vec4(-offset * 0.5, -offset * 0.5, 0.0, 0.0);
	texcoord = vec2(0.0, 0.0);
	EmitVertex();

	gl_Position = position + vec4(-offset * 0.5, +offset * 0.5, 0.0, 0.0);
	texcoord = vec2(0.0, 1.0);
	EmitVertex();

	gl_Position = position + vec4(+offset * 0.5, -offset * 0.5, 0.0, 0.0);
	texcoord = vec2(1.0, 0.0);
	EmitVertex();

	gl_Position = position + vec4(+offset * 0.5, +offset * 0.5, 0.0, 0.0);
	texcoord = vec2(1.0, 1.0);
	EmitVertex();

	EndPrimitive();
}

/*
void main() {
//	vec4 position = sin(gl_in[0].gl_Position.y) * view_matrix * model_matrix * gl_in[0].gl_Position;	// effect for previous main()
//	vec4 position = cos(gl_in[0].gl_Position.y) * view_matrix * model_matrix * gl_in[0].gl_Position;	// effect for previous main()
//	vec4 position = view_matrix * model_matrix * gl_in[0].gl_Position * sin(gl_in[0].gl_Position.y);	// effect for previous main()
	vec3 position = (view_matrix * model_matrix * gl_in[0].gl_Position).xyz
	
	float offset = 0.2f;
	color = glcolor[0];

	vec3 position = (view_matrix * gl_in[0].gl_Position).xyz;
	vec3 toCamera = normalize(position);
	vec3 up = vec3(0.0, 1.0, 0.0);
	vec3 right = cross(toCamera, up) * offset;

	position -= (right * 0.5);
	gl_Position = projection_matrix * vec4(position, 1.0);
	texcoord = vec2(0.0, 0.0);
	EmitVertex();

	position.y += offset;
	gl_Position = projection_matrix * vec4(position, 1.0);
	texcoord = vec2(0.0, 1.0);
	EmitVertex();

	position.y -= offset;
	position += right;
	gl_Position = projection_matrix * vec4(position, 1.0);
	texcoord = vec2(1.0, 0.0);
	EmitVertex();

	position.y += offset;
	gl_Position = projection_matrix * vec4(position, 1.0);
	texcoord = vec2(1.0, 1.0);
	EmitVertex();

	EndPrimitive();
}
//*/
