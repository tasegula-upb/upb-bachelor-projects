#pragma once
#include "../lab2/Framework/DrawingWindow.h"
#include "../lab2/Framework/Visual2D.h"
#include "../lab2/Framework/Transform2D.h"
#include "../lab2/Framework/Object2D.h"
#include <iostream>
#include <windows.h>

#include "Spaceship.h"
#include "GameStuff.h"

#define PI 3.14159265358979323846

class Enemy1 : public Object2D {

public:
	// Circle2D *shield;
	Polygon2D *layer;
	Circle2D *circle1;
	Circle2D *circle2;
	float x, y, radius, angle;
	//Visual2D *v2d;

	// variabile necesare miscarii
	float tx, ty, xi, yi;
	int time, sign;

	Enemy1(Point2D center, float r, Color c, Visual2D *v2d) {
		color.r = c.r;
		color.g = c.g;
		color.b = c.b;
		x = center.x; 
		y = center.y; 
		radius = r;
		angle = 0;
		type = 7;

		//v2d = _v2d;

		float unit = radius / 2.5;
		
		layer = new Polygon2D(c, false);
		layer->addPoint(Point2D(x + unit * 0, y - unit * 7));
		layer->addPoint(Point2D(x + unit * 1, y - unit * 4));
		layer->addPoint(Point2D(x + unit * 2, y - unit * 3));
		layer->addPoint(Point2D(x + unit * 3.5, y - unit * 3.5));
		layer->addPoint(Point2D(x + unit * 3, y - unit * 2));
		layer->addPoint(Point2D(x + unit * 4, y - unit * 1));
		layer->addPoint(Point2D(x + unit * 7, y - unit * 0));
		layer->addPoint(Point2D(x + unit * 4, y + unit * 1));
		layer->addPoint(Point2D(x + unit * 3, y + unit * 2));
		layer->addPoint(Point2D(x + unit * 3.5, y + unit * 3.5));
		layer->addPoint(Point2D(x + unit * 2, y + unit * 3));
		layer->addPoint(Point2D(x + unit * 1, y + unit * 4));
		layer->addPoint(Point2D(x + unit * 0, y + unit * 7));
		layer->addPoint(Point2D(x - unit * 1, y + unit * 4));
		layer->addPoint(Point2D(x - unit * 2, y + unit * 3));
		layer->addPoint(Point2D(x - unit * 3.5, y + unit * 3.5));
		layer->addPoint(Point2D(x - unit * 3, y + unit * 2));
		layer->addPoint(Point2D(x - unit * 4, y + unit * 1));
		layer->addPoint(Point2D(x - unit * 7, y + unit * 0));
		layer->addPoint(Point2D(x - unit * 4, y - unit * 1));
		layer->addPoint(Point2D(x - unit * 3, y - unit * 2));
		layer->addPoint(Point2D(x - unit * 3.5, y - unit * 3.5));
		layer->addPoint(Point2D(x - unit * 2, y - unit * 3));
		layer->addPoint(Point2D(x - unit * 1, y - unit * 4));
		DrawingWindow::addObject2D_to_Visual2D(layer, v2d);

		circle1 = new Circle2D(Point2D(x, y), radius / 2, Color(0, 0, 0), true);
		DrawingWindow::addObject2D_to_Visual2D(circle1, v2d);

		circle2 = new Circle2D(Point2D(x, y), radius, c, true);
		DrawingWindow::addObject2D_to_Visual2D(circle2, v2d);

		// initializez variabilele pentru miscare
		tx = ty = xi = yi = 0;
		time = 0;
		sign = 1;
	}

	void move(int speed) {
		if (time == 0) {
			if (rand() % 3 == 0) {
				sign = -1;
			}

			xi = rand() % 10 * sign * speed;
			yi = rand() % 10 * sign * speed;
			tx += xi;
			ty += yi;
		}
		else {
			tx += xi;
			ty += yi;
		}

		if (x + tx > radius * 3 + 10 && y + ty > radius * 3 && x + tx < BOARD_WIDTH - radius  * 3 && y + ty < BOARD_HEIGHT - radius * 3) {
			angle += PI / 180 * xi * sign;
			Transform2D::loadIdentityMatrix();
			Transform2D::translateMatrix(-x, -y);
			Transform2D::rotateMatrix(angle);
			Transform2D::translateMatrix(x + tx, y + ty);
			Transform2D::applyTransform(layer);
			Transform2D::applyTransform(circle1);
			Transform2D::applyTransform(circle2);
			time++;
			sign = 1;
			if (time == 100) {
				time = 0;
			}
		}
		else {
			angle += PI / 2;
			time = 0;
			tx -= xi;
			ty -= yi;
		}
	}

	int collision(Polygon2D* weapon, float px, float py, float _radius, Spaceship* player) {
		float euclid = sqrt(pow(x + tx - px, 2) + pow(y + ty - py, 2));

		if (euclid < radius * 3 + _radius) {
			if (test_kill(weapon, player)) {
				return 2;
			}
		}

		if (euclid < radius * 3 + _radius * 4) {
			if (test_collision(weapon)) {
				return 1;
			}
		}

		return 0;
	}

	int test_collision(Polygon2D* weapon) {
		Point2D *p = new Point2D();

		for (int i = 0; i < weapon->transf_points.size(); i++) {
			p = weapon->transf_points[i];

			if (abs(p->x - x - tx) <= radius * 3 && abs(p->y - y - ty) <= radius * 3) {
				return 1;
			}
		}
		return 0;
	}

	int test_kill(Polygon2D* weapon, Spaceship* player) {
		Point2D *p = new Point2D();

		for (int i = 0; i < player->transf_points.size(); i++) {
			p = player->transf_points[i];

			if (abs(p->x - x - tx) <= radius * 3 && abs(p->y - y - ty) <= radius * 3) {
				return 1;
			}
		}
		return 0;
	}

	void remove_from(Visual2D* v2d) {
		DrawingWindow::removeObject2D_from_Visual2D(layer, v2d);
		DrawingWindow::removeObject2D_from_Visual2D(circle1, v2d);
		DrawingWindow::removeObject2D_from_Visual2D(circle2, v2d);
	}

	~Enemy1(){}
};