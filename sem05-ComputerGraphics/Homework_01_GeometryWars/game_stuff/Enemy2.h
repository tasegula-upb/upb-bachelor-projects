#pragma once
#include "../lab2/Framework/DrawingWindow.h"
#include "../lab2/Framework/Visual2D.h"
#include "../lab2/Framework/Transform2D.h"
#include "../lab2/Framework/Object2D.h"
#include <iostream>
#include <windows.h>

#define PI 3.14159265358979323846

class Enemy2 : public Object2D {

public:
	// Circle2D *shield;
	Circle2D *circle;
	Polygon2D *arm1;
	Polygon2D *arm2;
	Polygon2D *extra;
	float x, y, radius, angle;
	float a = 0;					// unghiul pt urmarire

	// variabile necesare miscarii
	float tx, ty;
	int time, sign;

	Enemy2(Point2D center, float r, Color c, Visual2D *v2d) {
		color.r = c.r;
		color.g = c.g;
		color.b = c.b;
		x = center.x;
		y = center.y;
		radius = r;
		angle = 0;
		type = 8;

		extra = new Polygon2D(Color(), true);
		extra->addPoint(Point2D(x, y));
		extra->addPoint(Point2D(x, y - radius));
		extra->addPoint(Point2D(x - radius * 0.5, y));
		extra->addPoint(Point2D(x + radius * 0.5, y));
		extra->addPoint(Point2D(x, y + radius));
		DrawingWindow::addObject2D_to_Visual2D(extra, v2d);

		circle = new Circle2D(Point2D(x, y), radius, c, true);
		DrawingWindow::addObject2D_to_Visual2D(circle, v2d);

		arm1 = new Polygon2D(c, false);
		arm1->addPoint(Point2D(x + radius * 1.00, y + radius * 0.35));
		arm1->addPoint(Point2D(x + radius * 2.50, y + radius * 0.30));
		arm1->addPoint(Point2D(x + radius * 2.75, y + radius * 0.85));
		arm1->addPoint(Point2D(x + radius * 2.75, y + radius * 0.00));
		arm1->addPoint(Point2D(x + radius * 1.00, y - radius * 0.35));
		DrawingWindow::addObject2D_to_Visual2D(arm1, v2d);

		arm2 = new Polygon2D(c, false);
		arm2->addPoint(Point2D(x - radius * 1.00, y - radius * 0.35));
		arm2->addPoint(Point2D(x - radius * 2.50, y - radius * 0.30));
		arm2->addPoint(Point2D(x - radius * 2.75, y - radius * 0.85));
		arm2->addPoint(Point2D(x - radius * 2.75, y - radius * 0.00));
		arm2->addPoint(Point2D(x - radius * 1.00, y + radius * 0.35));
		DrawingWindow::addObject2D_to_Visual2D(arm2, v2d);
		
		// initializez variabilele pentru miscare
		tx = ty = 0;
		time = 0;
		sign = 1;
	}

	void move(float px, float py, float speed) {

		a = atan(abs(py - y - ty) / abs(px - x - tx));

		if (px < x + tx && py > y + ty) {
			a = PI - a;
		}
		else if (px < x + tx && py < y + ty) {
			a = PI + a;
		}
		else if (px > x + tx && py < y + ty) {
			a -= 2 * a;
		}

		tx += cos(a) * speed;
		ty += sin(a) * speed;

			angle += PI / 24 * speed;
			Transform2D::loadIdentityMatrix();
			Transform2D::translateMatrix(-x, -y);
			Transform2D::rotateMatrix(angle);
			Transform2D::translateMatrix(x + tx, y + ty);
			Transform2D::applyTransform(arm1);
			Transform2D::applyTransform(arm2);
			Transform2D::applyTransform(extra);
			Transform2D::applyTransform(circle);
	}

	int collision(Polygon2D* weapon, float px, float py, float _radius, Spaceship* player) {
		float euclid = sqrt(pow(x + tx - px, 2) + pow(y + ty - py, 2));
		if (euclid < radius * 3 + _radius) {
			if (test_kill(weapon, player)) {
				return 2;
			}
		}

		if (euclid < radius * 3 + _radius * 4) {
			if (test_collision(weapon)) {
				return 1;
			}
		}

		return 0;
	}

	int test_collision(Polygon2D* weapon) {
		Point2D *p = new Point2D();

		for (int i = 0; i < weapon->transf_points.size(); i++) {
			p = weapon->transf_points[i];

			if (abs(p->x - x - tx) <= radius * 3 && abs(p->y - y - ty) <= radius * 3) {
				return 1;
			}
		}
		return 0;
	}

	int test_kill(Polygon2D* weapon, Spaceship* player) {
		Point2D *p = new Point2D();

		for (int i = 0; i < player->transf_points.size(); i++) {
			p = player->transf_points[i];

			if (abs(p->x - x - tx) <= radius * 3 && abs(p->y - y - ty) <= radius * 3) {
				return 1;
			}
		}
		return 0;
	}
	
	void remove_from(Visual2D* v2d) {
		DrawingWindow::removeObject2D_from_Visual2D(circle, v2d);
		DrawingWindow::removeObject2D_from_Visual2D(extra, v2d);
		DrawingWindow::removeObject2D_from_Visual2D(arm1, v2d);
		DrawingWindow::removeObject2D_from_Visual2D(arm2, v2d);
	}

	~Enemy2(){}
};