#pragma once
#include "../lab2/Framework/DrawingWindow.h"
#include "../lab2/Framework/Visual2D.h"
#include "../lab2/Framework/Transform2D.h"
#include "../lab2/Framework/Object2D.h"
#include <iostream>
#include <windows.h>

#define PI 3.14159265358979323846

class Enemy3 : public Object2D {

public:
	// Circle2D *shield;
	Circle2D *princ1;
	Circle2D *princ2;
	Circle2D *princ3;
	Circle2D *sec1;
	Circle2D *sec2;
	Rectangle2D *link;
	float x, y, radius, angle;
	float a = 0;					// unghiul pt urmarire

	// variabile necesare miscarii
	float tx, ty;

	Enemy3(Point2D center, float r, Color c, Visual2D *v2d) {
		color.r = c.r;
		color.g = c.g;
		color.b = c.b;
		x = center.x;
		y = center.y;
		radius = r;
		angle = 0;
		type = 9;

		// corpul principal -> cercul mare
		princ3 = new Circle2D(Point2D(x, y), radius * 0.33, c, true);
		DrawingWindow::addObject2D_to_Visual2D(princ3, v2d);
		princ2 = new Circle2D(Point2D(x, y), radius * 0.66, Color(0, 0, 0), true);
		DrawingWindow::addObject2D_to_Visual2D(princ2, v2d);
		princ1 = new Circle2D(Point2D(x, y), radius * 1.00, c, true);
		DrawingWindow::addObject2D_to_Visual2D(princ1, v2d);

		// cercul secundar -> cel mic
		sec1 = new Circle2D(Point2D(x + radius * 2.66, y), radius * 0.33, Color(0, 0, 0), true);
		DrawingWindow::addObject2D_to_Visual2D(sec1, v2d);
		sec2 = new Circle2D(Point2D(x + radius * 2.66, y), radius * 0.66, c, true);
		DrawingWindow::addObject2D_to_Visual2D(sec2, v2d);

		// legatura dintre cercuri
		link = new Rectangle2D(Point2D(x + radius, y - radius * 0.2), radius, 3, c, true);
		DrawingWindow::addObject2D_to_Visual2D(link, v2d);

		// initializez variabilele pentru miscare
		tx = ty = 0;
	}

	void move(float px, float py, float speed) {
		a = atan(abs(py - y - ty) / abs(px - x - tx));

		if (px < x + tx && py > y + ty) {
			a = PI - a;
		}
		else if (px < x + tx && py < y + ty) {
			a = PI + a;
		}
		else if (px > x + tx && py < y + ty) {
			a -= 2 * a;
		}

		tx += cos(a) * speed;
		ty += sin(a) * speed;

		angle += PI / 24 * speed;
		Transform2D::loadIdentityMatrix();
		Transform2D::translateMatrix(-x, -y);
		Transform2D::rotateMatrix(angle);
		Transform2D::translateMatrix(x + tx, y + ty);
		Transform2D::applyTransform(link);
		Transform2D::applyTransform(sec1);
		Transform2D::applyTransform(sec2);
		Transform2D::applyTransform(princ1);
		Transform2D::applyTransform(princ2);
		Transform2D::applyTransform(princ3);
	}

	int collision(Polygon2D* weapon, float px, float py, float _radius, Spaceship* player) {
		float euclid = sqrt(pow(x + tx - px, 2) + pow(y + ty - py, 2));
		if (euclid < radius * 3 + _radius) {
			if (test_kill(weapon, player)) {
				return 2;
			}
		}

		if (euclid < radius * 3 + _radius * 4) {
			if (test_collision(weapon)) {
				return 1;
			}
		}

		return 0;
	}

	int test_collision(Polygon2D* weapon) {
		Point2D *p = new Point2D();

		for (int i = 0; i < weapon->transf_points.size(); i++) {
			p = weapon->transf_points[i];

			if (abs(p->x - x - tx) <= radius * 3 && abs(p->y - y - ty) <= radius * 3) {
				return 1;
			}
		}
		return 0;
	}

	int test_kill(Polygon2D* weapon, Spaceship* player) {
		Point2D *p = new Point2D();

		for (int i = 0; i < player->transf_points.size(); i++) {
			p = player->transf_points[i];

			if (abs(p->x - x - tx) <= radius * 3 && abs(p->y - y - ty) <= radius * 3) {
				return 1;
			}
		}
		return 0;
	}

	void remove_from(Visual2D* v2d) {
		DrawingWindow::removeObject2D_from_Visual2D(princ1, v2d);
		DrawingWindow::removeObject2D_from_Visual2D(princ2, v2d);
		DrawingWindow::removeObject2D_from_Visual2D(princ3, v2d);
		DrawingWindow::removeObject2D_from_Visual2D(sec1, v2d);
		DrawingWindow::removeObject2D_from_Visual2D(sec2, v2d);
		DrawingWindow::removeObject2D_from_Visual2D(link, v2d);
	}

	~Enemy3(){}
};