#pragma once
#include "../lab2/Framework/DrawingWindow.h"
#include "../lab2/Framework/Visual2D.h"
#include "../lab2/Framework/Transform2D.h"
#include "../lab2/Framework/Object2D.h"
#include <iostream>
#include <windows.h>

class Star : public Object2D {
	float x, y;
	float radius;


public:
	Star(Point2D center, float r, Color c)	{
		fill = true;
		color.r = c.r;
		color.g = c.g;
		color.b = c.b;

		x = center.x;
		y = center.y;
		radius = r;

		// Creez desenul din interiorul navei
		change_points(x, y, radius);
	}

	int collision(Polygon2D* weapon, float px, float py, float _radius, Spaceship* player) {
		float euclid = sqrt(pow(x - px, 2) + pow(y - py, 2));
		if (euclid < radius * 7 + _radius) {
			if (test_kill(weapon, player)) {
				return 2;
			}
		}

		if (euclid < radius * 7 + _radius * 4) {
			if (test_collision(weapon)) {
				return 1;
			}
		}

		return 0;
	}

	int test_collision(Polygon2D* weapon) {
		Point2D *p = new Point2D();

		for (int i = 0; i < weapon->transf_points.size(); i++) {
			p = weapon->transf_points[i];

			if (abs(p->x - x) <= radius * 3 && abs(p->y - y) <= radius * 3) {
				return 1;
			}
		}
		return 0;
	}

	int test_kill(Polygon2D* weapon, Spaceship* player) {
		Point2D *p = new Point2D();

		for (int i = 0; i < player->transf_points.size(); i++) {
			p = player->transf_points[i];

			if (abs(p->x - x) <= radius * 7 && abs(p->y - y) <= radius * 7) {
				return 1;
			}
		}
		return 0;
	}

	void change(Point2D p, Color _color) {
		x = p.x;
		y = p.y;
		color = _color;

		change_points(x, y, radius);
	}

	void change_points(float x, float y, float r) {
		this->points.clear();
		this->transf_points.clear();

		points.push_back(new Point2D(x + r * -2, y + r * 2));			// 08 (-2, 2)
		transf_points.push_back(new Point2D(x + r * -2, y + r * 2));
		points.push_back(new Point2D(x + r * 0, y + r * 7));			// 01 (+0, +7)
		transf_points.push_back(new Point2D(x + r * 0, y + r * 7));
		points.push_back(new Point2D(x + r * 2, y + r * 2));			// 02 (2, 2)
		transf_points.push_back(new Point2D(x + r * 2, y + r * 2));
		points.push_back(new Point2D(x + r * -3, y + r * -0.5));		// 06 (-3, -0.5)
		transf_points.push_back(new Point2D(x + r * -3, y + r * -0.5));
		points.push_back(new Point2D(x + r * -7, y + r * 2));			// 07 (-7, 2)
		transf_points.push_back(new Point2D(x + r * -7, y + r * 2));
		points.push_back(new Point2D(x + r * 7, y + r * 2));			// 09 (7, 2)
		transf_points.push_back(new Point2D(x + r * 7, y + r * 2));
		points.push_back(new Point2D(x + r * 0, y + r * -2.5));			// 05 (0, -2.5)
		transf_points.push_back(new Point2D(x + r * 0, y + r * -2.5));
		points.push_back(new Point2D(x + r * -3, y + r * -0.5));		// 06 (-3, -0.5)
		transf_points.push_back(new Point2D(x + r * -3, y + r * -0.5));
		points.push_back(new Point2D(x + r * -5, y + r * -6));			// 13 (-5, -6)
		transf_points.push_back(new Point2D(x + r * -5, y + r * -6));
		points.push_back(new Point2D(x + r * 0, y + r * -2.5));			// 05 (0, -2.5)
		transf_points.push_back(new Point2D(x + r * 0, y + r * -2.5));
		points.push_back(new Point2D(x + r * 3, y + r * -0.5));			// 03 (3, -0.5)
		transf_points.push_back(new Point2D(x + r * 3, y + r * -0.5));
		points.push_back(new Point2D(x + r * 5, y + r * -6));			// 04 (5, -6)
		transf_points.push_back(new Point2D(x + r * 5, y + r * -6));
		points.push_back(new Point2D(x + r * 0, y + r * -2.5));			// 05 (0, -2.5)
		transf_points.push_back(new Point2D(x + r * 0, y + r * -2.5));
	}
};