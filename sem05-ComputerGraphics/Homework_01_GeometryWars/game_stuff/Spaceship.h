#pragma once
#include "../lab2/Framework/DrawingWindow.h"
#include "../lab2/Framework/Visual2D.h"
#include "../lab2/Framework/Transform2D.h"
#include "../lab2/Framework/Object2D.h"
#include <iostream>
#include <windows.h>

#define PI 3.14159265358979323846
#define BOARD_HEIGHT 720
#define BOARD_WIDTH 1280

class Spaceship : public Object2D {
private:
	int boardX, boardY;

public:
	float radius;
	float px, py;

	Spaceship(Point2D center, float r, Color c)	{
		fill = false;
		color.r = c.r;
		color.g = c.g;
		color.b = c.b;

		// retin datele navei
		px = center.x;
		py = center.y;
		radius = r;
		
		// Creez SCUTUL: cercul exterior navei 
		// exprimare proasta, deoarece si el face parte din nava :D
		float u;
		float du = 10;
		float u_rad;
		for (u = 0; u <= 360; u += du) {
			Point2D *p = new Point2D();
			u_rad = u * 3.14159 / 180;
			p->x = center.x + radius * cos(u_rad);
			p->y = center.y + radius * sin(u_rad);

			points.push_back(p);
			transf_points.push_back(new Point2D(p->x, p->y));
		}

		float unit = radius / 10;
		// Creez desenul din interiorul navei
		points.push_back(new Point2D(px - unit * 2, py + unit * 0));			// 01
		transf_points.push_back(new Point2D(px - unit * 2, py + unit * 0));
		points.push_back(new Point2D(px + unit * 2, py - unit * 7));			// 02
		transf_points.push_back(new Point2D(px + unit * 2, py - unit * 7));
		points.push_back(new Point2D(px + unit * 7.5, py - unit * 3));			// 03
		transf_points.push_back(new Point2D(px + unit * 7.5, py - unit * 3));
		points.push_back(new Point2D(px + unit * 1.5, py - unit * 6.5));		// 04
		transf_points.push_back(new Point2D(px + unit * 1.5, py - unit * 6.5));
		points.push_back(new Point2D(px + unit * 2, py - unit * 7));			// 05
		transf_points.push_back(new Point2D(px + unit * 2, py - unit * 7));
		points.push_back(new Point2D(px + unit * 0, py - unit * 8));			// 06
		transf_points.push_back(new Point2D(px + unit * 0, py - unit * 8));
		points.push_back(new Point2D(px - unit * 6, py + unit * 0));			// 07
		transf_points.push_back(new Point2D(px - unit * 6, py + unit * 0));
		points.push_back(new Point2D(px + unit * 0, py + unit * 8));			// 08
		transf_points.push_back(new Point2D(px + unit * 0, py + unit * 8));
		points.push_back(new Point2D(px + unit * 2, py + unit * 7));			// 09
		transf_points.push_back(new Point2D(px + unit * 2, py + unit * 7));
		points.push_back(new Point2D(px + unit * 7.5, py + unit * 3));			// 10
		transf_points.push_back(new Point2D(px + unit * 7.5, py + unit * 3));
		points.push_back(new Point2D(px + unit * 1.5, py + unit * 6.5));		// 11
		transf_points.push_back(new Point2D(px + unit * 1.5, py + unit * 6.5));
		points.push_back(new Point2D(px + unit * 2, py + unit * 7));			// 12
		transf_points.push_back(new Point2D(px + unit * 2, py + unit * 7));

		boardX = 10;
		boardY = 0;
	}

	bool moveForward(float tx, float ty, float angle, int wpn, Polygon2D *weapon) {

		if (px + tx > radius + 10 && py + ty > radius && px + tx < 1280 - radius - boardX && py + ty < 720 - radius - boardY) {
			Transform2D::loadIdentityMatrix();
			Transform2D::translateMatrix(-px, -py);
			Transform2D::rotateMatrix(angle);
			Transform2D::translateMatrix(px + tx, py + ty);
			Transform2D::applyTransform(this);
			if (wpn) {
				Transform2D::applyTransform(weapon);
			}
			return true;
		}
		else {
			return false;
		}
	}

	void rotateLeft(float tx, float ty, float angle, int wpn, Polygon2D *weapon) {
		Transform2D::loadIdentityMatrix();
		Transform2D::translateMatrix(-px, -py);
		Transform2D::rotateMatrix(angle);
		Transform2D::translateMatrix(px + tx, py + ty);
		Transform2D::applyTransform(this);
		
		if (wpn) {
			Transform2D::applyTransform(weapon);
		}
	}

	void rotateRight(float tx, float ty, float angle, int wpn, Polygon2D *weapon) {
		Transform2D::loadIdentityMatrix();
		Transform2D::translateMatrix(-px, -py);
		Transform2D::rotateMatrix(angle);
		Transform2D::translateMatrix(px + tx, py + ty);
		Transform2D::applyTransform(this);
		
		if (wpn) {
			Transform2D::applyTransform(weapon);
		}
	}
};