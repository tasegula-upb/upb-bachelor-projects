#pragma once
#include "../lab2/Framework/DrawingWindow.h"
#include "../lab2/Framework/Visual2D.h"
#include "../lab2/Framework/Transform2D.h"
#include "../lab2/Framework/Object2D.h"
#include <iostream>
#include <windows.h>

#define PI 3.14159265358979323846

class Pacman : public Object2D {

public:
	float radius;
	float px, py;

	Pacman(Point2D center, float r, Color c, bool mouth)	{
		fill = false;
		color.r = c.r;
		color.g = c.g;
		color.b = c.b;

		radius = r;

		// retin centrul navei
		px = center.x;
		py = center.y;
		
		float u;
		float du = 10;
		float u_rad;

		if (mouth == true) {
			// Creez arcul de cerc de 300 grade 	
			for (u = 30; u <= 330; u += du) {
				Point2D *p = new Point2D();
				u_rad = u * 3.14159 / 180;
				p->x = center.x + radius * cos(u_rad);
				p->y = center.y + radius * sin(u_rad);

				points.push_back(p);
				transf_points.push_back(new Point2D(p->x, p->y));
			}

			// Creez gura
			points.push_back(new Point2D(px, py));			// 01
			transf_points.push_back(new Point2D(px, py));
		}
		else {
			// Creez arcul de cerc de 330 grade 	
			for (u = 0; u <= 360; u += du) {
				Point2D *p = new Point2D();
				u_rad = u * 3.14159 / 180;
				p->x = center.x + radius * cos(u_rad);
				p->y = center.y + radius * sin(u_rad);

				points.push_back(p);
				transf_points.push_back(new Point2D(p->x, p->y));
			}

			// Creez gura
			points.push_back(new Point2D(px, py));			// 01
			transf_points.push_back(new Point2D(px, py));
		}
	}

	~Pacman(){}
};