#pragma once
#include "../lab2/Framework/DrawingWindow.h"
#include "../lab2/Framework/Visual2D.h"
#include "../lab2/Framework/Transform2D.h"
#include "../lab2/Framework/Object2D.h"
#include "../lab2/Framework/Line2D.h"
#include "../lab2/Framework/Rectangle2D.h"
#include "../lab2/Framework/Circle2D.h"
#include "../lab2/Framework/Polygon2D.h"
#include <iostream>
#include <windows.h>

#include "Enemy1.h"
#include "Enemy2.h"
#include "Enemy3.h"
#include "Enemy4.h"
#include "Enemy5.h"
#include "Pacman.h"
#include "Spaceship.h"
#include "Star.h"

#define radius 25

#define BOARD_HEIGHT 720
#define BOARD_WIDTH 1280

#define MAX_LIFES 7
#define START_LIFES 3

#define re1 10
#define re2 10
#define re3 10
#define re4 10
#define re5 10

/* ----------------------------------------------------------------------------
**								Technical Stuff
** ----------------------------------------------------------------------------
** (px, py)		- punctul de plecare al navei
** (tx, ty)		- translatarea navei la momentul de timp in joc
** angle		- unghiul navei fata de orizontala (sens trigonometric)
** time			- variabile de joc; denumirea lor denota rolul
** move_*		- flag-uri necesare pentru continuous keyboard input
** wpn			- 0 - fara arma, 1 - burghiu, 2:5 - bonusuri de arma
** score, life	- variabile de joc; denumirea lor denota rolul
** pause		- true = pauza, false = play
** on			- true = deja am afisat pauza, false = putem afisa pauza
** ------------------------------------------------------------------------- */
Visual2D *v2dp;			// contextul principal
Visual2D *infov2d;		// contextul de informatii
float px, py;
float tx, ty;
float angle;
int time;
bool move_up = false;
bool move_right = false;
bool move_left = false;
bool pause = false;	
bool on = false;
float speed[6];			// retine atat viteza playerului (0), cat si a inamicilor(1-5)
bool restart = false;
bool start = false;

/* ----------------------------------------------------------------------------
**							Graphical Technical Stuff
** ------------------------------------------------------------------------- */
vector<Rectangle2D*> life;	// elemente necesare pentru reprezentarea si contorizarea vietilor
Text* xlife;
Text* tlifes;
int lifes;

Text* tscore;				// elemente necesare pentru reprezentarea si contorizaea scorului
int score;

Text* tlevel;				// elemente necesare pentru reprezentarea si contorizaea levelului
int level;

Star* star;					// elemente necesare pentru reprezentarea bonus life-ului
int starcounter;

Text* text;					// afiseaza "PAUZA" sau "GAME OVER" dupa caz

int wpn;					// 0 = fara arma, 1 = arma standard

Color c_score = Color(0.80, 0.00, 1.00);
Color c_time = Color(0.80, 0.00, 1.00);
Color c_v2d = Color(0.80, 0.00, 1.00);


/* ----------------------------------------------------------------------------
**								Player stuff
** ------------------------------------------------------------------------- */
Spaceship *player;	
Polygon2D *weapon;		 
Color c_player = Color(0.02, 0.70, 0.02); 
Color c_weapon = Color(0.02, 0.70, 0.02); 


/* ----------------------------------------------------------------------------
**								Enemies Stuff
** ------------------------------------------------------------------------- */
Enemy1 *enemy1;
Enemy2 *enemy2;
Enemy3 *enemy3;
Enemy5 *enemy5;
vector<Enemy1*> enemies1;				// vector de Enemy1
vector<Enemy2*> enemies2;				// vector de Enemy2
vector<Enemy3*> enemies3;				// vector de Enemy3
vector<Enemy5*> enemies5;				// vector de Enemy5
Color c_en1 = Color(1.00, 0.50, 0.00);
Color c_en2 = Color(0.00, 0.25, 1.00);
Color c_en3 = Color(0.40, 0.10, 0.55);
Color c_en4 = Color(0.00, 0.00, 0.00);
Color c_en5 = Color(1.00, 0.90, 0.00);



/* ----------------------------------------------------------------------------
**								Functii
** ------------------------------------------------------------------------- */

Polygon2D* make_weapon(float px, float py, float _radius) {
	Polygon2D *weapon = new Polygon2D(c_weapon, true);

	// adaug primele doua puncte de pe o latura
	weapon->addPoint(Point2D(px + _radius * 1.00, py - _radius * 0.75));
	weapon->addPoint(Point2D(px + _radius * 2.00, py - _radius * 0.50));

	// incepand de la distanta orizontala 2 fata de centru, vom mari x cu 0.25
	// incepand de la distanta verticala 0.5 fata de centru, vom micsora y cu 0.25 / 4
	float xFactor = 2.00 + 0.25;
	float yFactor = 0.50 - 0.25 / 4;

	// adaug restul punctelor de pe aceeasi latura
	while (xFactor != 4.00) {
		weapon->addPoint(Point2D(px + radius * xFactor, py - radius * yFactor));

		xFactor += 0.25;
		yFactor -= 0.25 / 4;
	}

	// adaug varful
	weapon->addPoint(Point2D(px + radius * (3 + 1), py));

	// adaug punctele de pe cealalta latura
	while (xFactor != 4.00) {
		weapon->addPoint(Point2D(px + radius * xFactor, py + radius * yFactor));

		xFactor -= 0.25;
		yFactor += 0.25 / 4;
	}
	
	// adaug ultimele puncte de pe latura
	weapon->addPoint(Point2D(px + radius * 2.00, py + radius * 0.50));
	weapon->addPoint(Point2D(px + radius * 1.00, py + radius * 0.75));
	
	return weapon;
}

Text* add_score(Visual2D* v2d) {
	Text* score = new Text("", Point2D(0, 0), c_score, GLUT_BITMAP_HELVETICA_18);
	DrawingWindow::addText_to_Visual2D(score, v2d);

	return score;
}

Text* change_score(int i, Point2D p, Visual2D* v2d, Text* _score) {
	char *intStr = new char();
	itoa(i, intStr, 10);

	// scoatem vechiul scor
	DrawingWindow::removeText_from_Visual2D(_score, v2d);
	free(_score);
	// si il adaugam pe cel nou
	Text* new_score = new Text(string(intStr), p, c_score, GLUT_BITMAP_HELVETICA_18);
	DrawingWindow::addText_to_Visual2D(new_score, v2d);

	return new_score;
}

Text* add_tlife(Visual2D* v2d) {
	Text* lf = new Text("", Point2D(0, 0), c_score, BITMAP_8_BY_13);
	DrawingWindow::addText_to_Visual2D(lf, v2d);

	return lf;
}

Text* change_tlife(int i, Point2D p, Visual2D* v2d, Text* _life) {
	char *intStr = new char();
	itoa(i, intStr, 10);
	string str = string(intStr);
	if (i > 1){
		str += " more hits!";
	}
	else if (i == 1) {
		str += " more hit!";
	}
	else {
		str += ": too damaged";
	}

	DrawingWindow::removeText_from_Visual2D(_life, v2d);
	free(_life);
	Text* new_life = new Text(str, p, c_score, BITMAP_8_BY_13);
	DrawingWindow::addText_to_Visual2D(new_life, v2d);

	return new_life;
}

Text* add_tlevel(Visual2D* v2d) {
	Text* lvl = new Text("LEVEL: 01", Point2D(100, 100), c_score, BITMAP_HELVETICA_18);
	DrawingWindow::addText_to_Visual2D(lvl, v2d);

	return lvl;
}

Text* change_tlevel(int i, Point2D p, Visual2D* v2d, Text* _level) {
	char *intStr = new char();
	itoa(i, intStr, 10);
	string str;

	if (i < 10) {
		str += "LEVEL: 0";
	}
	else {
		str += "LEVEL: ";
	}
		str += string(intStr);

	DrawingWindow::removeText_from_Visual2D(_level, v2d);
	free(_level);
	Text* new_lvl = new Text(str, p, c_score, BITMAP_HELVETICA_18);
	DrawingWindow::addText_to_Visual2D(new_lvl, v2d);

	return new_lvl;
}

/* ----------------------------------------------------------------------------
** Functii pentru afisarea intro-ului
** ------------------------------------------------------------------------- */
// TEXT
Text* line1 = new Text("", Point2D(0, 0), Color(), BITMAP_9_BY_15);
Text* line2 = new Text("", Point2D(0, 0), Color(), BITMAP_9_BY_15);
Text* line3 = new Text("", Point2D(0, 0), Color(), BITMAP_9_BY_15);

void intro1(Visual2D* v2d) {
	line1->change("A long time ago in a galaxy far, far away ....", Point2D(BOARD_WIDTH * 0.35, BOARD_HEIGHT * 0.85), c_en2, BITMAP_9_BY_15);
	DrawingWindow::addText_to_Visual2D(line1, v2d);

	line2->change("GALAXY", Point2D(BOARD_WIDTH * 0.45, BOARD_HEIGHT * 0.85 - 50), c_en5, BITMAP_HELVETICA_18);
	DrawingWindow::addText_to_Visual2D(line2, v2d);

	line3->change("WARS", Point2D(BOARD_WIDTH * 0.45, BOARD_HEIGHT * 0.85 - 75), c_en5, BITMAP_TIMES_ROMAN_24);
	DrawingWindow::addText_to_Visual2D(line3, v2d);
}

void intro2(Visual2D* v2d) {
	line1->change("INSTRUCTIONS! ", Point2D(BOARD_WIDTH * 0.35, BOARD_HEIGHT * 0.85), c_en2, BITMAP_TIMES_ROMAN_24);
	line2->change("Press UP to move forward, and", Point2D(BOARD_WIDTH * 0.35, BOARD_HEIGHT * 0.85 - 50), c_en5, BITMAP_HELVETICA_18);
	line3->change("LEFT / RIGHT to rotate.", Point2D(BOARD_WIDTH * 0.35, BOARD_HEIGHT * 0.85 - 100), c_en5, BITMAP_HELVETICA_18);
}

void intro3(Visual2D* v2d) {
	line1->change("You also have a weapon!", Point2D(BOARD_WIDTH * 0.35, BOARD_HEIGHT * 0.85), c_en5, BITMAP_HELVETICA_18);
	line2->change("Hit space to active it", Point2D(BOARD_WIDTH * 0.35, BOARD_HEIGHT * 0.85 - 50), c_en5, BITMAP_HELVETICA_18);
	line3->change("", Point2D(BOARD_WIDTH * 0.35, BOARD_HEIGHT * 0.85 - 100), c_en5, BITMAP_HELVETICA_18);
}

void intro4(Visual2D* v2d) {
	line1->change("You have 3 working parts of your ship", Point2D(BOARD_WIDTH * 0.35, BOARD_HEIGHT * 0.85), c_en5, BITMAP_HELVETICA_18);
	line2->change("You can help R2D2 to repare others", Point2D(BOARD_WIDTH * 0.35, BOARD_HEIGHT * 0.85 - 50), c_en5, BITMAP_HELVETICA_18);
	line3->change("by going through the stars.", Point2D(BOARD_WIDTH * 0.35, BOARD_HEIGHT * 0.85 - 100), c_en5, BITMAP_HELVETICA_18);
}

void intro5(Visual2D* v2d) {
	line1->change("Press P to pause, ", Point2D(BOARD_WIDTH * 0.35, BOARD_HEIGHT * 0.85), c_en5, BITMAP_HELVETICA_18);
	line2->change("and LEVEL UP after each 100 points", Point2D(BOARD_WIDTH * 0.35, BOARD_HEIGHT * 0.85 - 50), c_en5, BITMAP_HELVETICA_18);
	line3->change("", Point2D(BOARD_WIDTH * 0.35, BOARD_HEIGHT * 0.85 - 100), c_en5, BITMAP_TIMES_ROMAN_24);
}

void intro6(Visual2D* v2d) {
	line1->change("", Point2D(BOARD_WIDTH * 0.35, BOARD_HEIGHT * 0.85), c_en2, BITMAP_HELVETICA_18);
	line2->change("... and ...", Point2D(BOARD_WIDTH * 0.35, BOARD_HEIGHT * 0.85 - 25), c_en5, BITMAP_HELVETICA_18);
	line3->change("May the FORCE be with you!", Point2D(BOARD_WIDTH * 0.4, BOARD_HEIGHT * 0.85 - 75), c_en5, BITMAP_TIMES_ROMAN_24);
}

bool intro(int time, Visual2D* v2d) {
	if (time < 50) {
		intro1(v2d);
	}
	else if (time < 100) {
		intro2(v2d);
	}
	else if (time < 150) {
		intro3(v2d);
	}
	else if (time < 200) {
		intro4(v2d);
	}
	else if (time < 250) {
		intro5(v2d);
	}
	else if (time < 300) {
		intro6(v2d);
	}
	else {
		return false;
	}
	return true;
}

void remove_text(Visual2D* v2d) {
	line1->change("", Point2D(0, 0), Color(), BITMAP_9_BY_15);
	line2->change("", Point2D(0, 0), Color(), BITMAP_9_BY_15);
	line3->change("", Point2D(0, 0), Color(), BITMAP_9_BY_15);
	DrawingWindow::removeText_from_Visual2D(line1, v2d);
	DrawingWindow::removeText_from_Visual2D(line2, v2d);
	DrawingWindow::removeText_from_Visual2D(line3, v2d);
}

/* ----------------------------------------------------------------------------
** Functie pentru animatia obiectului care va da bonus;
** ------------------------------------------------------------------------- */
void animate_star(int time, float x, float y) {
	int factor;
	if (time % 50 < 25) {
		factor = (time % 25) * 0.1;
		Transform2D::loadIdentityMatrix();
		Transform2D::translateMatrix(-x, -y);
		Transform2D::scaleMatrix(factor, factor);
		Transform2D::translateMatrix(x, y);
		Transform2D::applyTransform(star);
	}
	else {
		factor = (25 - (time % 25)) * 0.1;
		Transform2D::loadIdentityMatrix();
		Transform2D::translateMatrix(-x, -y);
		Transform2D::scaleMatrix(factor, factor);
		Transform2D::translateMatrix(x, y);
		Transform2D::applyTransform(star);
	}
}