#pragma once
#include "../lab2/Framework/DrawingWindow.h"
#include "../lab2/Framework/Visual2D.h"
#include "../lab2/Framework/Transform2D.h"
#include "../lab2/Framework/Object2D.h"
#include <iostream>
#include <windows.h>

#include "Pacman.h"

#define PI 3.14159265358979323846

class Enemy5 : public Object2D {

public:
	Pacman *opened;
	Pacman *closed;
	Circle2D *eye;
	Visual2D *v2d;
	float x, y, radius, angle;
	bool mouth, type;

	// variabile necesare miscarii
	float tx, ty, xi, yi;
	int time;

	Enemy5(Point2D center, float r, Color c, Visual2D *_v2d) {
		color.r = c.r;
		color.g = c.g;
		color.b = c.b;
		x = center.x;
		y = center.y;
		radius = r;
		angle = 0;
		v2d = _v2d;
		type = 11;

		opened = new Pacman(center, radius * 3, c, true);
		closed = new Pacman(center, radius * 3, c, false);
		DrawingWindow::addObject2D_to_Visual2D(opened, v2d);

		eye = new Circle2D(Point2D(x + radius * 0.75, y + radius * 1.50), radius * 0.3, Color(1.00, 0.00, 0.50), true);
		DrawingWindow::addObject2D_to_Visual2D(eye, v2d);

		// initializez variabilele pentru miscare
		tx = ty = xi = yi = 0;
		time = 0;
	}

	void move(float px, float py, float speed) {
		// NOm Nom nOM nom NOM noM nom 
		if (time % 20 == 0) {
			if (mouth) {
				DrawingWindow::removeObject2D_from_Visual2D(opened, v2d);
				DrawingWindow::addObject2D_to_Visual2D(closed, v2d);
				mouth = false;
			}
			else {
				DrawingWindow::removeObject2D_from_Visual2D(closed, v2d);
				DrawingWindow::addObject2D_to_Visual2D(opened, v2d);
				mouth = true;
			}
		}

		// se decide cum va merge inamicul pentru urmatoare 100 iteratii
		// type = true => inamicul va incerca sa mearga pe orizontala (inainte/inapoi)
		// type = false => inamicul va incerca sa mearga pe verticala (sus/jos)
		while (time == 0) {
			if (type) {
				yi = 0;
				// pacman trebuie sa mearga orizontal inainte pentru a prinde player-ul
				if (x + tx < px) {
					if (speed != 0) {
						angle = 0;
					}
					xi = 1;
					time = 1;			// iesim din loop-ul while
				}
				// pacman trebuie sa mearga orizontal inapoi pentru a prinde player-ul
				else if (x + tx > px) {
					if (speed != 0) {
						angle = PI;
					}
					xi = -1;
					time = 1;			// iesim din loop-ul while
				} 
				type = false;
			}
			else {
				xi = 0;
				// pacman trebuie sa mearga vertical in sus pentru a prinde player-ul
				if (y + ty < py) {
					if (speed != 0) {
						angle = PI / 2;
					}
					yi = 1;
					time = 1;			// iesim din loop-ul while
				}
				// pacman trebuie sa mearga vertical in jos pentru a prinde player-ul
				else if (y + ty > py) {
					if (speed != 0) {
						angle = PI * 1.5;
					}
					yi = -1;
					time = 1;			// iesim din loop-ul while
				}
				type = true;
			}
		}

		tx += xi * speed;					// viteza de miscare
		ty += yi * speed;

		// conditie pentru a nu iesi din game board
		if (x + tx > radius * 3 + 10 && y + ty > radius * 3 && x + tx < BOARD_WIDTH - radius * 3 && y + ty < BOARD_HEIGHT - radius * 3) {
			Transform2D::loadIdentityMatrix();
			Transform2D::translateMatrix(-x, -y);
			Transform2D::rotateMatrix(angle);
			Transform2D::translateMatrix(x + tx, y + ty);
			if (mouth) {
				Transform2D::applyTransform(opened);
			}
			else {
				Transform2D::applyTransform(closed);
			}

			// repozitionez ochiul atunci cand pacman merge orizontal inapoi 
			// altfel ar fi creepy :D.
			if (xi == -1) {
				Transform2D::translateMatrix(0, radius * 3);
			}

			Transform2D::applyTransform(eye);

			time++;
			if (time == 100) {
				time = 0;
			}
		}
		else {
			time = 0;
			tx -= xi;
			ty -= yi;
		}
	}

	int collision(Polygon2D* weapon, float px, float py, float _radius, Spaceship* player) {
		float euclid = sqrt(pow(x + tx - px, 2) + pow(y + ty - py, 2));
		if (euclid < radius * 3 + _radius) {
			if (test_kill(weapon, player)) {
				return 2;
			}
		}

		if (euclid < radius * 3 + _radius * 4) {
			if (test_collision(weapon)) {
				return 1;
			}
		}

		return 0;
	}

	int test_collision(Polygon2D* weapon) {
		Point2D *p = new Point2D();

		for (int i = 0; i < weapon->transf_points.size(); i++) {
			p = weapon->transf_points[i];

			if (abs(p->x - x - tx) <= radius * 3 && abs(p->y - y - ty) <= radius * 3) {
				return 1;
			}
		}
		return 0;
	}

	int test_kill(Polygon2D* weapon, Spaceship* player) {
		Point2D *p = new Point2D();

		for (int i = 0; i < player->transf_points.size(); i++) {
			p = player->transf_points[i];

			if (abs(p->x - x - tx) <= radius * 3 && abs(p->y - y - ty) <= radius * 3) {
				return 1;
			}
		}
		return 0;
	}

	void remove() {
		DrawingWindow::removeObject2D_from_Visual2D(opened, v2d);
		DrawingWindow::removeObject2D_from_Visual2D(closed, v2d);
		DrawingWindow::removeObject2D_from_Visual2D(eye, v2d);
		delete opened;
		delete closed;
		delete eye;
	}

	~Enemy5(){}
};