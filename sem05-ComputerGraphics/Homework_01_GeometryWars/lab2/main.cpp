#include "Framework/DrawingWindow.h"
#include "Framework/Visual2D.h"
#include "Framework/Transform2D.h"
#include "Framework/Line2D.h"
#include "Framework/Rectangle2D.h"
#include "Framework/Circle2D.h"
#include "Framework/Polygon2D.h"
#include <iostream>
#include <windows.h>

#include <vector>

#include "../game_stuff/GameStuff.h"
#include "../game_stuff/Spaceship.h"
#include "../game_stuff/Enemy1.h"
#include "../game_stuff/Enemy2.h"
#include "../game_stuff/Enemy3.h"
#include "../game_stuff/Enemy4.h"
#include "../game_stuff/Enemy5.h"
#include "../game_stuff/Pacman.h"
#include "../game_stuff/Star.h"

#define PI 3.14159265358979323846

using namespace std;


//functia care permite adaugarea de obiecte
void DrawingWindow::init()
{
	glClearColor(0, 0, 0, 1);

	infov2d = new Visual2D(0, 0, BOARD_WIDTH, BOARD_HEIGHT, 0, 0, DrawingWindow::width, DrawingWindow::height / 10);
	infov2d->tipTran(true);
	infov2d->cadruPoarta(c_v2d);
	addVisual2D(infov2d);

	v2dp = new Visual2D(0, 0, BOARD_WIDTH, BOARD_HEIGHT, 0, DrawingWindow::height / 10, DrawingWindow::width, DrawingWindow::height);
	v2dp->tipTran(true);
	v2dp->cadruPoarta(c_v2d);
	addVisual2D(v2dp);

	px = py = 0;
	tx = ty = 0;

	text = new Text("Hit SPACE or CLICK anywhere to start the game!", Point2D(BOARD_WIDTH * 0.35, BOARD_HEIGHT / 2), c_en1, BITMAP_9_BY_15);
	addText_to_Visual2D(text, infov2d);

	xlife = new Text("SHIP DAMAGE: ", Point2D(BOARD_WIDTH - 300, BOARD_HEIGHT - 450), c_v2d, BITMAP_9_BY_15);
	
	// reprezentam vizual vietile
	for (int i = 1; i <= 7; i++) {
		Rectangle2D* rectangle = new Rectangle2D(Point2D(BOARD_WIDTH - 20 * (i + 1), BOARD_HEIGHT /2 - 150), 10, 300, Color(1,0,0), false);
		life.push_back(rectangle);
	}

	wpn = 0;				// arma este "inchisa"
	score = 0;				// momentan nu avem puncte castigate;
	lifes = START_LIFES;	// incepem cu 3 vieti;
	speed[0] = -1;			// daca arma e activa, va deveni 5; arma inactiva = 7;

	// vitezele inamicilor
	speed[1] = 1;
	speed[2] = 2;
	speed[3] = 3;
	speed[5] = 2.5;

	// cream bonusul; il afisam mai tarziu
	star = new Star(Point2D(100, 100), 1, c_en5);
	// addObject2D_to_Visual2D(star, v2dp);
	starcounter = 0;
	level = 1;
}

int sx = 0, sy = 0;

//functia care permite animatia
void DrawingWindow::onIdle()
{
	if (speed[0] == -1) {
		start = false;

		if (restart == false) {
			if (!intro(time, v2dp)) {
				remove_text(v2dp);

				removeText_from_Visual2D(text, infov2d);
				text->change("Hit SPACE or CLICK anywhere to start the game!", Point2D(BOARD_WIDTH * 0.35, BOARD_HEIGHT / 2), c_en1, BITMAP_9_BY_15);
				addText_to_Visual2D(text, v2dp);

				start = true;
			}
		}
		else {
			text->change("Hit SPACE or CLICK anywhere to start the game!", Point2D(BOARD_WIDTH * 0.35, BOARD_HEIGHT / 2), c_en1, BITMAP_9_BY_15);
			addText_to_Visual2D(text, v2dp);
			start = true;
		}
		if (start) {
			if (px != 0 && py!= 0) {
				addObject2D_to_Visual2D(player, v2dp);		// se adauga jucatorul
				
				// se adauga tabela de scor
				tscore = add_score(infov2d);

				// se adauga anuntul de level
				tlevel = add_tlevel(infov2d);

				// se adauga tabela de vieti
				tlifes = add_tlife(infov2d);				// text care anunta cate lovituri mai putem lua
				addText_to_Visual2D(xlife, infov2d);		// text care anunta ca obiectele de langa reprezinta nava

				for (int i = 0; i < START_LIFES; i++) {					// cate vieti avem
					addObject2D_to_Visual2D(life[i], infov2d);
				}
				for (int i = START_LIFES; i < MAX_LIFES; i++) {			// cate vieti mai putem avea
					life[i]->filling(true);
					addObject2D_to_Visual2D(life[i], infov2d);
				}

				removeText_from_Visual2D(text, v2dp);
				text->change("", Point2D(BOARD_WIDTH / 2, BOARD_HEIGHT / 2), c_en1, BITMAP_TIMES_ROMAN_24);
				time = 0;
				speed[0] = 8;
			}
		}
	}

	int i, x, y;
	x = px + tx;
	y = py + ty;
	time++;

	if (speed[0]!= -1 && lifes != 0 && !pause) {
		for (i = 0; i < enemies1.size(); i++) {
			enemies1[i]->move(speed[1]);
		}
		for (i = 0; i < enemies2.size(); i++) {
			enemies2[i]->move(x, y, speed[2]);
		}
		for (i = 0; i < enemies3.size(); i++) {
			enemies3[i]->move(x, y, speed[3]);
		}
		for (i = 0; i < enemies5.size(); i++) {
			enemies5[i]->move(x, y, speed[5]);
		}

		if (time > rand() % 50 && time > rand() % 100 && time > rand() % 200) {
			i = rand() % 10;
			// se gaseste pozitia relativ opusa navei pentru a introduce noi inamici
			if (x >= 640 && y >= 360) {				// STG SUS -> DREAPTA JOS
				x = 100;
				y = 100;
			}
			else if (x >= 640 && y < 360) {			// STG JOS -> DREAPTA SUS
				x = 100;
				y = DrawingWindow::height - 100;
			}
			else if (y < 360) {						// DREAPTA JOS -> STG SUS
				x = DrawingWindow::width - 100;
				y = DrawingWindow::height - 100;
			}
			else {									// DREAPTA SUS -> STG JOS
				x = DrawingWindow::width - 100;
				y = 100;
			}

			// se introduc inamici;
			if (i == 1) {
				enemy1 = new Enemy1(Point2D(x, y), re1, c_en1, v2dp);
				enemies1.push_back(enemy1);
			}
			if (i == 2 || i == 7) {
				enemy2 = new Enemy2(Point2D(x, y), re2, c_en2, v2dp);
				enemies2.push_back(enemy2);
			}
			if (i == 3 || i == 8) {
				enemy3 = new Enemy3(Point2D(x, y), re3, c_en3, v2dp);
				enemies3.push_back(enemy3);
			}
			if (i == 0 || i == 9) {
				enemy5 = new Enemy5(Point2D(x, y), re5, c_en5, v2dp);
				enemies5.push_back(enemy5);
			}
			if (i < 7) {
				if (starcounter == 0) {
					sx = x + time * 2;
					sy = y + time * 3;
					star->change(Point2D(x + time * 2, y + time * 3), c_en5);
					addObject2D_to_Visual2D(star, v2dp);
					starcounter = 1;
				}
			}
			time = 0;
		}

		// MISCARE PLAYER
		if (move_up){

			tx += speed[0] * cos(angle);
			ty += speed[0] * sin(angle);

			if (!player->moveForward(tx, ty, angle, wpn, weapon)) {
				tx -= speed[0] * cos(angle);
				ty -= speed[0] * sin(angle);
			}
		}

		if (move_left){
			angle += PI / 180 * speed[0];
			player->rotateLeft(tx, ty, angle, wpn, weapon);
		}

		if (move_right){
			angle -= PI / 180 * speed[0];
			player->rotateRight(tx, ty, angle, wpn, weapon);
		}

		// COLIZIUNE
		int k = 0;
		for (i = 0; i < enemies1.size(); i++) {
			k = enemies1[i]->collision(weapon, px + tx, py + ty, radius, player);
			if (wpn) {
				if (k == 1) { // TODO: get points
					enemies1[i]->remove_from(v2dp);
					enemies1.erase(enemies1.begin() + i);
					score += 20;
				}
			}
			if (k == 2) { // for now; TODO: lose life
				enemies1[i]->remove_from(v2dp);
				enemies1.erase(enemies1.begin() + i);
				lifes--;
				life[lifes]->filling(true);
			}
		}
		for (i = 0; i < enemies2.size(); i++) {
			k = enemies2[i]->collision(weapon, px + tx, py + ty, radius, player);
			if (wpn) {
				if (k == 1) { // TODO: get points
					enemies2[i]->remove_from(v2dp);
					enemies2.erase(enemies2.begin() + i);
					score += 15;
				}
			}
			if (k == 2) { // for now; TODO: lose life
				enemies2[i]->remove_from(v2dp);
				enemies2.erase(enemies2.begin() + i);
				lifes--;
				life[lifes]->filling(true);
			}
		}
		for (i = 0; i < enemies3.size(); i++) {
			k = enemies3[i]->collision(weapon, px + tx, py + ty, radius, player);
			if (wpn) {
				if (k == 1) { // TODO: get points
					enemies3[i]->remove_from(v2dp);
					enemies3.erase(enemies3.begin() + i);
					score += 25;
				}
			}
			if (k == 2) { // for now; TODO: lose life
				enemies3[i]->remove_from(v2dp);
				enemies3.erase(enemies3.begin() + i);
				lifes--;
				life[lifes]->filling(true);
			}
		}
		for (i = 0; i < enemies5.size(); i++) {
			k = enemies5[i]->collision(weapon, px + tx, py + ty, radius, player);
			if (wpn) {
				if (k == 1) { // TODO: get points
					enemies5[i]->remove();
					enemies5.erase(enemies5.begin() + i);
					score += 10;
				}
			}
			if (k == 2) { // for now; TODO: lose life
				enemies5[i]->remove();
				enemies5.erase(enemies5.begin() + i);
				lifes--;
				life[lifes]->filling(true);
			}
		}
		// COLIZIUNE cu bonusul
		if (starcounter > 0){
			starcounter++;
			if (star->collision(weapon, px + tx, py + ty, radius, player)) {
				removeObject2D_from_Visual2D(star, v2dp);
				sx = sy = 0;
				life[lifes]->filling(false);
				lifes++;
				starcounter = 0;
			}
		}
		if (starcounter > 500) {
			sx = sy = 0;
			removeObject2D_from_Visual2D(star, v2dp);
			starcounter = 0;
		}

		tscore = change_score(score, Point2D(BOARD_WIDTH / 2, 250), infov2d, tscore);
		tlifes = change_tlife(lifes, Point2D(1140, 100), infov2d, tlifes);
		

		// animatie stea
		if (starcounter) {
			animate_star(starcounter, sx, sy);
		}

		// level up
		if (score > (100 * level)) {
			cout << score << endl;
			for (int i = 1; i <= 5; i++) {
				speed[i] *= (1 + 0.01 * level);
			}
			level++;
			tlevel = change_tlevel(level, Point2D(100, 100), infov2d, tlevel);
		}
	}

	// Anuntam pauza
	if (pause && !on) {
		text->change("PAUSE !", Point2D(BOARD_WIDTH / 2, BOARD_HEIGHT / 2), c_en1, BITMAP_TIMES_ROMAN_24);
		addText_to_Visual2D(text, v2dp);
		on = true;
	}
	if (!pause && on) {
		removeText_from_Visual2D(text, v2dp);
		on = false;
	}

	// Anuntam GAME OVER!!!
	if (lifes == 0) {
		text->change("GAME OVER !!!", Point2D(BOARD_WIDTH / 2, BOARD_HEIGHT / 2), c_en3, BITMAP_9_BY_15);
		addText_to_Visual2D(text, v2dp);
		on = false;
	}
}

//functia care se apeleaza la redimensionarea ferestrei
void DrawingWindow::onReshape(int width, int height)
{

}

//functia care defineste ce se intampla cand se apasa pe tastatura
void DrawingWindow::onKey(unsigned char key)
{
	switch (key) {
	case 27:
		exit(0);
		break;

	case KEY_UP:
		move_up = true;
		break;

	case KEY_LEFT:
		move_left = true;
		break;

	case KEY_RIGHT:
		move_right = true;
		break;

	case KEY_SPACE:
		if (speed[0] == -1) {
			time = 400;

			px = DrawingWindow::width / 2.0;
			py = DrawingWindow::height / 2.0;

			player = new Spaceship(Point2D(px, py), radius, c_player);
			weapon = make_weapon(px, py, radius);
			break;
		}
		if (!wpn) {
			addObject2D_to_Visual2D(weapon, v2dp);
			Transform2D::loadIdentityMatrix();
			Transform2D::translateMatrix(-px, -py);
			Transform2D::rotateMatrix(angle);
			Transform2D::translateMatrix(px + tx, py + ty);
			Transform2D::applyTransform(weapon);
			wpn = 1;
			speed[0] = 6;

			break;
		}
		else {
			removeObject2D_from_Visual2D(weapon, v2dp);
			wpn = 0;
			speed[0] = 8;
			break;
		}

	case 'p':
		pause = !pause;
		break;
	case 'P':
		pause = !pause;
		break;
	}
}

void DrawingWindow::onKeyUp(unsigned char key) 
{
	switch (key) {
	case KEY_UP:
		move_up = false;
		break;

	case KEY_LEFT:
		move_left = false;
		break;

	case KEY_RIGHT:
		move_right = false;
		break;
	}
}

//functia care defineste ce se intampla cand se da click pe mouse
void DrawingWindow::onMouse(int button, int state, int x, int y)
{
	if (state) {
		if (speed[0] == -1) {
			px = x;
			py = (BOARD_HEIGHT - y) * 13/12;

			player = new Spaceship(Point2D(px, py), radius, c_player);
			weapon = make_weapon(px, py, radius);

			time = 400;
		}
	}
}


int main(int argc, char** argv)
{
	//creare fereastra
	DrawingWindow dw(argc, argv, BOARD_WIDTH, BOARD_HEIGHT, 0, 0, "Geometry Wars by Tase Gula (there's a Pacman!)");
	//se apeleaza functia init() - in care s-au adaugat obiecte
	dw.init();
	//se intra in bucla principala de desenare - care face posibila desenarea, animatia si procesarea evenimentelor
	dw.run();
	return 0;

}