
#!/bin/sh

TIMEOUT=10
NO_TESTS=10
TEST_POINTS=4

TOTAL=0

# Check a single test for the problem recieved as paremeter
#
# first argument: problem (either p1 or p2)
# second argument: problem name (either fotbal or zar)
# third argument: test number
check_test()
{
    problem=$1
    problem_name=$2
    test_no=$3

    echo
    echo -n "Test: $test_no ...................... "

    # Get the input and ref files for the current test
    input_file="$problem""_tests/input/"$problem_name$test_no".in"
    ref_file="$problem""_tests/ref/"$problem_name$test_no".ref"

    # Copy the input and output to the current directory
    cp $input_file $problem_name".in"
    cp $ref_file $problem_name".ref"

    # Run the test, consider the timeout
    (time timeout $TIMEOUT make "run-"$problem) 2>time.err 1>/dev/null
    if test $? -eq 124; then
        echo "TIMEOUT"

        rm -f my_diff
        rm -f time.err
        rm -f $problem_name".in" $problem_name".ref" $problem_name".out"
        return
    fi

    # Check the result
    diff -bB -i $problem_name".out" $problem_name".ref" 2>&1 1> my_diff
    if test $? -eq 0; then
        echo "PASS"
        cat time.err | grep real
        TOTAL=$(expr $TOTAL + $TEST_POINTS)
    else
        echo "FAILED"
        echo "Diff result (truncated):"
        cat my_diff | tail -n 10
    fi

    # Clean up
    rm -f my_diff
    rm -f time.err
    rm -f $problem_name".in" $problem_name".ref" $problem_name".out"
}

# Compile the solution
make build
if [ $? -gt 0 ]; then
    echo "Build failed"
    exit 1
fi

# Determine programming language
LANG="c++"
JAVA_FILES=$(find | grep '.*\.java')
if [ -n "$JAVA_FILES" ]; then
    LANG="java"
fi

# Determine timeouts
TIMEOUT_P1=$(cat timeout.txt | grep -i $LANG | grep -i p1 | awk -F: '{print $NF}')
TIMEOUT_P2=$(cat timeout.txt | grep -i $LANG | grep -i p2 | awk -F: '{print $NF}')

# Check the solution for the first problem
echo -e "\n"
echo -e "====================================================================\n"
echo -e "Checking PROBLEM 1: fotbal\n"
TIMEOUT=$TIMEOUT_P1

for i in $(seq $NO_TESTS); do
    check_test p1 "fotbal" $i
done

RES_P1=$TOTAL
echo -e "\nPROBLEM 1, total = $RES_P1/$(expr $TEST_POINTS \* $NO_TESTS)\n"
echo -e "====================================================================\n"

TOTAL=0

# Check the solution for the second problem
echo -e "\n"
echo -e "====================================================================\n"
echo -e "Checking PROBLEM 2: zar\n"

TIMEOUT=$TIMEOUT_P2
for i in $(seq $NO_TESTS); do
    check_test p2 "zar" $i
done

RES_P2=$TOTAL
echo -e "\nPROBLEM 2, total = $RES_P2/$(expr $TEST_POINTS \* $NO_TESTS)\n"
echo -e "====================================================================\n"

# Clean up
make clean 2>&1 1>/dev/null

# And the restul is ...
TOTAL=$(expr $RES_P1 + $RES_P2)
echo "TOTAL: $TOTAL/$(expr $TEST_POINTS \* $NO_TESTS \* 2)"
