/* -----------------------------------------------------------
 *  Autor:        Gula Tanase, 324CA
 * 
 *		Proiectarea Algoritmilor. Tema 3. Problema 2.
 * ----------------------------------------------------------- */

public class P2_Dice {
	int stateID;
	int[] dFace;
	int up, down, front, right, back, left;
	
	public static final int[][] states = new int[24][6];

	static {
		// up, down, front, right, back, left
		states[0] = new int[]{1, 6, 2, 3, 5, 4};	
		states[1] = new int[]{1, 6, 3, 5, 4, 2};
		states[2] = new int[]{1, 6, 4, 2, 3, 5};
		states[3] = new int[]{1, 6, 5, 4, 2, 3};

		states[4] = new int[]{2, 5, 1, 4, 6, 3};	
		states[5] = new int[]{2, 5, 3, 1, 4, 6};	
		states[6] = new int[]{2, 5, 4, 6, 3, 1};	
		states[7] = new int[]{2, 5, 6, 3, 1, 4};	
	
		states[8] = new int[]{3, 4, 1, 2, 6, 5}; 
		states[9] = new int[]{3, 4, 2, 6, 5, 1};	
		states[10] = new int[]{3, 4, 5, 1, 2, 6};	
		states[11] = new int[]{3, 4, 6, 5, 1, 2};

		states[12] = new int[]{4, 3, 1, 5, 6, 2};	
		states[13] = new int[]{4, 3, 2, 1, 5, 6};	
		states[14] = new int[]{4, 3, 5, 6, 2, 1};	
		states[15] = new int[]{4, 3, 6, 2, 1, 5};

		states[16] = new int[]{5, 2, 1, 3, 6, 4};	
		states[17] = new int[]{5, 2, 3, 6, 4, 1};	
		states[18] = new int[]{5, 2, 4, 1, 3, 6};	
		states[19] = new int[]{5, 2, 6, 4, 1, 3};

		states[20] = new int[]{6, 1, 2, 4, 5, 3};	
		states[21] = new int[]{6, 1, 3, 2, 4, 5};	
		states[22] = new int[]{6, 1, 4, 5, 3, 2};	
		states[23] = new int[]{6, 1, 5, 3, 2, 4};		
	};
	
	public P2_Dice(int x, int dFace[]) {
		stateID = x;
		this.dFace = dFace.clone();
		
		up = states[x][0];
		down = states[x][1];
		front = states[x][2];
		right = states[x][3];
		back = states[x][4];
		left = states[x][5];
	}
	
	public void goUp() {
		int up2 = up;
		int down2 = down;
		int front2 = front;
		int back2 = back;
		
		up = front2;
		front = down2;
		down = back2;
		back = up2;
	}
	
	public void goDown() {
		int up2 = up;
		int down2 = down;
		int front2 = front;
		int back2 = back;
		
		up = back2;
		down = front2;
		front = up2;
		back = down2;
	}
	
	public void goRight() {
		int up2 = up;
		int down2 = down;
		int right2 = right;
		int left2 = left;
		
		up = left2;
		left = down2;
		down = right2;
		right = up2;
	}
	
	public void goLeft() {
		int up2 = up;
		int down2 = down;
		int right2 = right;
		int left2 = left;
		
		up = right2;
		right = down2;
		down = left2;
		left = up2;
	}
	
	public int update() {
		for(int i = 0; i < 24; i++) {
			if(states[i][0] == up && states[i][2] == front) {
				stateID = i;
				break;
			}
		}
		
		return stateID;
	}
}
