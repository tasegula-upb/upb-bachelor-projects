
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Scanner;




/* -----------------------------------------------------------
 *  Autor:        Gula Tanase, 324CA
 * 
 *		Proiectarea Algoritmilor. Tema 3. Problema 2.
 * ----------------------------------------------------------- */

public class P2_Main {
	int N;		// numarul de linii ale tablei de joc
	int M;		// numarul de coloane ale tablei de joc
	int sourceX, sourceY;	// linia si coloana sursei
	int destX, destY;		// linia si coloana destinatiei
	
	int[] initialDice;			// retinem fetele zarulu
	int[][][] minPath;		// va fi folosita pentru calcularea drumului minim
	
	LinkedList<Node> nQueue;		// coada pentru noduri
	Node[][][] Nodes;	// matrice pentru noduri
	
	public P2_Main(String filename) throws FileNotFoundException {
		readData(filename);
		
		initObjects();
	}
	
	public void readData(String filename) throws FileNotFoundException {
		Scanner scanner = new Scanner(new File(filename));
				
		N = scanner.nextInt();
		M = scanner.nextInt();

		initialDice = new int[6];
		
		for (int i = 0; i < 6; i++) {
			initialDice[i] = scanner.nextInt();
		}
	
		sourceX = scanner.nextInt();
		sourceY = scanner.nextInt();
		destX = scanner.nextInt();
		destY = scanner.nextInt();
		
	}

	public void initObjects() {
		nQueue = new LinkedList();
		
		minPath = new int[N][M][24];
		Nodes = new Node[N][M][24];

		for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                for (int k = 0; k < 24; k++) {
                    Nodes[i][j][k] = new Node(i, j, k);
					minPath[i][j][k] = Integer.MAX_VALUE;
                }
            }
        }
	}
	
	public void writeData(String filename, int min) throws IOException {
		File file = new File(filename);
		if (!file.exists()) {
			file.createNewFile();
		}
		
		FileWriter fw = new FileWriter(file);
		BufferedWriter bw = new BufferedWriter(fw);
		
		bw.write(""+min+"\n");
		
		bw.close();	
	}
	
	public void BellmanFord(Node source, Node dest) {
		int sx = source.row;
		int sy = source.col;
		int ss = source.state;
		minPath[sx][sy][0] = 0;
		
		P2_Dice dice = null;
		Node node = null;
		
		int newState = -1;
		
		if (sx - 1 >= 0) {
			dice = new P2_Dice(ss, initialDice);
			dice.goUp();
			
			newState = dice.update();
			node = Nodes[sx - 1][sy][newState];
			
			updateMatrices(dice, node, -1);	
        }
        if (sy + 1 < M) {
			dice = new P2_Dice(ss, initialDice);
			dice.goRight();
			
			newState = dice.update();
			node = Nodes[sx][sy + 1][newState];
			
			updateMatrices(dice, node, -1);
        }
        if (sx + 1 < N) {
			dice = new P2_Dice(ss, initialDice);
			dice.goDown();
			
			newState = dice.update();
			node = Nodes[sx + 1][sy][newState];

			updateMatrices(dice, node, -1);
        }
        if (sy - 1 >= 0) {
			dice = new P2_Dice(ss, initialDice);
			dice.goLeft();
			
			newState = dice.update();
			node = Nodes[sx][sy - 1][newState];

			updateMatrices(dice, node, -1);
        }
		
		while (!nQueue.isEmpty()) {
			node = null;
			node = nQueue.removeFirst();
			sx = node.row;
			sy = node.col;
			ss = node.state;

			int aux = minPath[sx][sy][ss];
			int newAux;
			
			if (sx - 1 >= 0) {
				dice = new P2_Dice(ss, initialDice);
				dice.goUp();
				
				newState = dice.update();
				node = Nodes[sx - 1][sy][newState];

				updateMatrices(dice, node, aux);	
			}
			if (sy + 1 < M) {
				dice = new P2_Dice(ss, initialDice);
				dice.goRight();
				
				newState = dice.update();
				node = Nodes[sx][sy + 1][newState];

				updateMatrices(dice, node, aux);	
			}
			if (sx + 1 < N) {
				dice = new P2_Dice(ss, initialDice);
				dice.goDown();
				
				newState = dice.update();
				node = Nodes[sx + 1][sy][newState];

				updateMatrices(dice, node, aux);	
			}
			if (sy - 1 >= 0) {
				dice = new P2_Dice(ss, initialDice);
				dice.goLeft();
				
				newState = dice.update();
				node = Nodes[sx][sy - 1][newState];

				updateMatrices(dice, node, aux);	
			}
			
			
		}
	}

	public void updateMatrices(P2_Dice dice, Node node, int type) {
		int aux = 0;
		int newAux = Integer.MAX_VALUE;

		if (type != -1) {
			aux = type;
			newAux = minPath[node.row][node.col][node.state];
		}
		
		if (newAux > aux + dice.dFace[dice.up - 1]) {
			minPath[node.row][node.col][node.state] = aux + dice.dFace[dice.up - 1];
			nQueue.addLast(node);
		}
	}
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		P2_Main dg = new P2_Main("zar.in");
		Node source = dg.Nodes[dg.sourceX][dg.sourceY][0];
		Node dest = dg.Nodes[dg.destX][dg.destY][0];
		
		dg.BellmanFord(source, dest);
		
		int min = Integer.MAX_VALUE;
		
		for (int i = 0; i < 24; i++) {
			if (dg.minPath[dg.destX][dg.destY][i] < min) {
				min = dg.minPath[dg.destX][dg.destY][i];
			}
		}
		min += dg.initialDice[0];
		dg.writeData("zar.out", min);
	}
	
	static class Node {
		int row, col;
		int state;

		public Node(int x, int y, int s) {
			row = x;
			col = y;
			state = s;
		}
	}
}
