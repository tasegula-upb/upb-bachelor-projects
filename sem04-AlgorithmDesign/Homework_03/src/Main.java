/* -----------------------------------------------------------
 *  Autor:        Gula Tanase, 324CA
 * 
 *		Proiectarea Algoritmilor. Tema 3. Problema 1.
 * ----------------------------------------------------------- */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class Main {

	int N;		// numarul de echipe din campionat
	int P;		// numarul de partide jucate
	int K;		// numarul de etape ale campionatului
	int Q;		// numarul de intrebari
	
	int gTotal;				// numarul total de partide: (N*(N-1))*K
	int[] questions;		// retinem intrebarile X_i
	int[][] gPlayed;		// retinem cate meciuri s-au jucat intre x si y
	
	/*
	 * Matrice cu N coloane, fiecare reprezentand o echipa (x)
	 * prima linie = punctele castigate de echipa x
	 * a doua linie = numarul de meciuri ramase de jucat de echipa x
	 */
	int[][] utils;
	
	ArrayList<Graph.Node> games;
	
	final static int NONE = -1;
	
	public Main(String filename) throws FileNotFoundException {
		readData(filename);
	}
	
	/* -------------------------------------------------------
	 * citeste datele din fisierul de input 
	 * si initializeaza variabilele cu valorile date
	 * ------------------------------------------------------- */
	private void readData(String filename) throws FileNotFoundException {
		Scanner scanner = new Scanner(new File(filename));
				
		N = scanner.nextInt();
		P = scanner.nextInt();
		K = scanner.nextInt();
		Q = scanner.nextInt();
		
		initObjects();
		readGames(scanner);
		readQuestions(scanner);
		
		for (int i = 0; i < N; i++) {
			for (int j = i + 1; j < N; j++) {
				games.add(new Graph.Node(i, j, 2 * gPlayed[i][j]));
			}
		}
	}
	
	private void initObjects() {
		gPlayed = new int[N][N];
		questions = new int[Q];
		utils = new int[2][N];
		
		gTotal = N * (N - 1) / 2;				// numar total de meciuri
		games = new ArrayList<Graph.Node>(gTotal);	// retine meciurile jucate
		
		
		for (int i = 0; i < N; i++) {
			utils[0][i] = 0;			// punctele initiale castigate sunt 0
			utils[1][i] = (N - 1) * K;	// orice echipa are K partide cu cele (N-1) echipe
			gPlayed[i][i] = 0;
			for (int j = i + 1; j < N; j++) {
				// intre i si j vor avea loc K meciuri
				gPlayed[i][j] = gPlayed[j][i] = K;
			}
		}
	}
	
	private void readGames(Scanner scanner) {
		String buffer = new String();
		int team1, team2;
		
		for (int i = 0; i < P; i++) {
			team1 = scanner.nextInt();
			team2 = scanner.nextInt();
			
			// fiecare dintre echipe are cu un meci mai putin de jucat
			utils[1][team1]--;
			utils[1][team2]--;
			
			buffer = scanner.nextLine();
			if (buffer.equals(" WIN")) {
				utils[0][team1] += 2;
			}
			else {
				utils[0][team1] += 1;
				utils[0][team2] += 1;
			}
			
			gPlayed[team1][team2] = --gPlayed[team2][team1];
		}
	}
	
	private void readQuestions(Scanner scanner) {
		for (int i = 0; i < Q; i++) {
			questions[i] = scanner.nextInt();
		}
	}
	
	/* 
	 * Functii pentru calcularea fluxului maxim;
	 * Acestea sunt luate din laboratorul 10, cu adaugarile de rigoare astfel incat sa functioneze
	 */
	public static int maxFlow(Graph g, int u, int v) {
		int maxFlow = 0;

		// Vom incerca in mod repetat sa determinam drumuri de crestere folosind
		// BFS si sa le saturam pana cand nu mai putem determina un astfel de drum in
		// graf.
		while (true) {
			// Determina drum de crestere.
			ArrayList<Integer> saturation_path = bfs(g, u, v);

			// In functie de saturation_path determinati daca fluxul trebuie
			// marit sau trebuie iesit din while
			if (saturation_path.size() == 0) {
				break;
			}
			maxFlow = saturate_path(g, saturation_path);
		}

		return maxFlow;
	}
	
	public static int saturate_path(Graph g, ArrayList<Integer> path) {
		// Niciodata nu ar trebui sa se intample asta pentru ca sursa si destinatia
		// sunt noduri distincte si cel putin unul dintre ele se afla in path.
		if (path.size() < 2) {
			return 0;
		}

		// Determinam fluxul maxim prin drum.
		int flow = g.capMatrix[path.get(0)][path.get(1)];
		int current;
		for (int i = 0; i < path.size() - 1; ++i) {
			int u = path.get(i), v = path.get(i + 1);
			// Determinati fluxul in functie de capacitata muchiei (u, v) */
			current = g.capMatrix[u][v];
			if (current < flow) {
				flow = current;
			}
		}

		// Si il saturam in graf.
		for (int i = 0; i < path.size() - 1; ++i) {
			int u = path.get(i), v = path.get(i + 1);
			// Modificati fluxul in functie de capacitatea muchiei (u, v)
			g.capMatrix[u][v] -= flow;
			g.capMatrix[v][u] = -flow;
		}

		// Raportam fluxul cu care am saturat graful.
		return flow;
	}

	public static ArrayList<Integer> bfs(Graph g, int u, int v) {
		// Ne vom folosi de vectorul de parinti pentru a spune daca un nod a fost
		// adaugat sau nu in coada.
		ArrayList<Integer> parent = new ArrayList<Integer>(g.getSize());

		for (int i = 0; i < g.getSize(); ++i) {
			parent.add(NONE);
		}

		LinkedList<Integer> q = new LinkedList<Integer>();
		q.add(u);

		while (parent.get(v) == NONE && q.size() > 0) {
			int node = q.get(0);
			q.remove(0);
			for (int i = 0; i < g.getSize(); ++i) {
				if (g.capMatrix[node][i] > 0 && parent.get(i) == NONE) {
					parent.set(i, node);
					q.add(i);
				}
			}
		}

		// Daca nu s-a atins destinatia, atunci nu mai exista drumuri de crestere.
		if (parent.get(v) == NONE) {
			return new ArrayList<Integer>(0);
		}

		// Reconstituim drumul de la destinatie spre sursa.
		ArrayList<Integer> returnValue = new ArrayList<Integer>();
		for (int node = v; ; node = parent.get(node)) {
			returnValue.add(0, node);
			if (node == u) {
				break;
			}
		}

		return returnValue;
	}
	
	/*
	 * Returneaza true daca teamNo poate castiga campionatul sau false in caz contrar. 
	 */
	public boolean canWin(int teamNo) {
		Graph g = new Graph(teamNo, 2 + N + gTotal, games, utils);
		
		int flow = maxFlow(g, 0, g.getSize() - 1);
		int aux = 0;
		
		// Verifica daca exista muchie de iesire cu valoarea negativa
		for (int i = 1 + gTotal; i < g.getSize() - 1; i++) {
			if (g.capMatrix[i][g.getSize() - 1] < 0) {
				return false;
			}
		}
		
		for (int i = 0; i < utils[1].length; i++) {
			if (i != teamNo) {
				aux += utils[1][i];
			}
		}
		
		return (flow < aux);
	}
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		Main fb = new Main("fotbal.in");
		
		File file = new File("fotbal.out");
		if (!file.exists()) {
			file.createNewFile();
		}
		
		FileWriter fw = new FileWriter(file);
		BufferedWriter bw = new BufferedWriter(fw);
		
		int aux;
		for (int i = 0; i < fb.questions.length; i++) {
			aux = fb.questions[i];
			if (fb.canWin(aux)) {
				bw.write("TRUE\n");
			}
			else {
				bw.write("FALSE\n");
			}
		}
		
		bw.close();
	}

}
