/* -----------------------------------------------------------
 *  Autor:        Gula Tanase, 324CA
 * 
 *		Proiectarea Algoritmilor. Tema 3.
 * ----------------------------------------------------------- */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;
import java.util.Vector;

public class Graph {

	/**
	 * Muchie, reprezentata prin cele doua noduri si capacitatea asociata.
	 */
	static class Edge {
		public int x, y, c;
		
		/**
		 * @param x - primul nod
		 * @param y - al doilea nod
		 * @param c - capacitatea asociata muchiei
		 */
		public Edge(int n1, int n2, int cap) {
			x = n1;
			y = n2;
			c = cap;
		}
	}
	
	static class Node {
		int e1, e2;		// echipele care joaca meciul
		int pct;		// numarul de puncte de castigat intre e1 si e2
		
		public Node(int e1, int e2, int pct) {
			this.e1 = e1;
			this.e2 = e2;
			this.pct = pct;
		}
	}
	
	public int[][] capMatrix;		// matricea capacitatilor
	
	private Vector<Vector<Integer>> neigh;
	private Vector<Edge> edges;
	private Vector<Integer> nodes;

	private int[] parents;
	private int size;
	
	/*
	 * Construieste graful asociat echipei teamNo
	 */
	public Graph(int teamNo, int size, ArrayList<Node> gPlayed, int[][] utils) {
		newEmptyGraph(size);
		
		int gSize = gPlayed.size();
		
		for (int i = 0; i < gSize; i++) {
			Node m = gPlayed.get(i);
			if (m.pct != 0 && teamNo != m.e1 && teamNo != m.e2) {
				// legam nodul initial la toate meciurile de jucat
				addEdge(0, i + 1, m.pct);
				// legam fiecare meci la echipele aferente
				addEdge(i + 1, 1 + gSize + m.e1, m.pct);
				addEdge(i + 1, 1 + gSize + m.e2, m.pct);
			}
		}
		
		int maxPoints = utils[0][teamNo] + 2 * utils[1][teamNo];
		int aux = -1;

		for (int i = 1 + gSize; i < size - 1; i++) {
			if (teamNo != aux++) {
				addEdge(i, size - 1, maxPoints - utils[0][aux]);
			}
		}
	}
	
	/*
	 * Functii ce construiesc graful initial,
	 * graf ce va avea "size" noduri si zero muchii
	 */
	private void newEmptyGraph(int size) {
		this.size = size;
		
		allocObjects();
		
		// initializam matricea capacitatilor si parintii
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				capMatrix[i][j] = 0;		// initCapacityMatrix();
				parents[i] = 0;				// clearParents();
			}
		}
	}
	
	private void allocObjects() {
		/* Cost matrix */
		capMatrix = new int[size][size];

		/* Neighbours */
		neigh = new Vector<Vector<Integer>>();
		for (int i = 0; i < size; i++) {
			neigh.add(new Vector<Integer>());
		}

		/* Parents */
		parents = new int[size];

		/* Edges */
		edges = new Vector<Graph.Edge>();

		/* Nodes */
		nodes = new Vector<Integer>();
		for (int i = 0; i < size; i++) {
			nodes.add(i);
		}
	}
	
	/* 
	 * Functii pentru prelucrarea grafului
	 */
	public void addEdge(int x, int y, int c) {
		capMatrix[x][y] = c;
		neigh.get(x).add(y);
		edges.add(new Edge(x, y, c));
	}
		
	public int getSize() {
		return size;
	}
}
