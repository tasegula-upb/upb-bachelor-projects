/*---------------------------------------------------------------------
 *  Autor:        Gula Tanase, 324CA
 *
 *	Proiectarea Algoritmilor. Tema 1. Problema 2.
 *---------------------------------------------------------------------*/

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class TaskManager {
	int N;
	int T;
	int[] c;
	int gain;
	int[][] maxCol;
	int[][] sol;
	int[][] M;
	
	TaskManager(String filename) {
		readData(filename);
		sol = new int[2][T];
		M = new int[N+1][T+1];
		maxCol = new int[N+1][T+1];
		getMData();
		getMaxData();
	}
	
	private void readData(String filename) {
		Scanner scanner = null;
		
		try {
			scanner = new Scanner(new File(filename));
			N = scanner.nextInt();
			T = scanner.nextInt();
			c = new int[N+1];
			for (int i = 1; i <= N; i++){
				c[i] = scanner.nextInt();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (scanner != null) scanner.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	private void writeData(String filename, int x) {
		try {
			File file = new File(filename);
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write("" + gain + "\n");
			x++;
			bw.write("" + x + "\n");
			for (int i = x-1; i >= 0; i--) {
				bw.write("" + sol[0][i] + " " + sol[1][i] + "\n");
			}
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/* -------------------------------------------------------
	 * construieste matricea M si o copiaza in maxCol
	 * ------------------------------------------------------- */
	private void getMData() {
		int max;	
		
		// cream matricea, pe coloane		
		// prima coloana este 0 (daca luam 0 intervale castigul maxim este 0)
		// in timp ce cream M,cream un "vector" de perechi
		// (reprezentat printr.o matrice de int.uri din cauza usurintei de
		// a calcula complexitatea fata de Vector<Pair>, unde nu as fi stiut 
		// ce complexitate reprezinta fiecare add, get, indexOf etc);
		
		for (int j = 2; j <= T; j++) {
			for (int i = j; i <= N ; i++) {
				max = M[i-1][j-1] + c[i];
				for (int k = j-2; k <= i-1 && k > 0; k++) {
					max = Math.max(max, M[k][j-1]);
				}
				M[i][j] = max;
				maxCol[i][j] = max;
			}
		}
	}
	
	/* -------------------------------------------------------
	 * construieste matricea maxCol
	 * ------------------------------------------------------- */
	private void getMaxData() {
		for (int j = 1; j <= T; j++) {
			for (int i = j; i <= N; i++) {
				if (maxCol[i][j] < maxCol[i-1][j]) {
					maxCol[i][j] = maxCol[i-1][j];
				}
			}
		}
	}
	
	/* -------------------------------------------------------
	 * gaseste castigul maxim si il returneaza
	 * ------------------------------------------------------- */
	int findMaxWin() {
		int max = Integer.MIN_VALUE;
		
		for (int i = T; i <= N; i++) {
			if (M[i][T] > max) {
				max = M[i][T];
			}
		}
		
		return max;
	}
	
	/* -------------------------------------------------------
	 * gaseste intervalele solutiei, 
	 * de la ultimul selectat la primul
	 * ------------------------------------------------------- */
	int findSol() {
		// index-ii pentru parcurgerea matricei
		int line = N;	
		int col = T;
		
		int i = 0;		// contor pentru numarul de intervale din solutia finala
		int k = 1;		// numar de elemente din interval;
		int help = 0;	// auxiliar pentru oprire.
		
		int NewEl = -1;
		int OldEl = -1;
		
		while(M[line][col] != maxCol[N][T]){
			line--;
		}
		
		while (line > 1 && col > 1) {
			if (M[line][col] == M[line-1][col-1] + c[line]
					&& M[line][col] > maxCol[line-1][col-1]) {
				
				NewEl = line;
				help = 1;
				// sunt vecini, deci in acelasi interval
				if (NewEl == OldEl - 1) {
					OldEl = NewEl;
					k++;
					line--;
					col--;
				}
				else {
					// cazul de baza
					if (OldEl == -1) {
						OldEl = NewEl;
						line--;
						col--;
					}
					// nu sunt vecini, deci un interval trebuie adaugat;
					else {
						sol[0][i] = OldEl - 1;
						sol[1][i] = OldEl + k - 1;
						k = 1;
						i++;
						OldEl = NewEl;
						line--;
						col--;
					}
				}
			}
			// nu e element pe care sa.l adaug la solutie
			else {
				if (col == line) {
					line--;
					col--;
					help = 0;
				}
				else {
					line--;
					help = 0;
				}
			}
		}
		if (help != 0) {
			sol[0][i] = OldEl - 1;
			sol[1][i] = OldEl + k - 1;
			}
		
		return i;
		
	}
	
	public static void main(String[] args) {
		TaskManager tm = new TaskManager("date.in");
		tm.gain = tm.findMaxWin();
		int x = tm.findSol();
		tm.writeData("date.out", x);
	}
}
