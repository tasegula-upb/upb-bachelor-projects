Student:	Gula Tanase
Grupa:		324CA

Problema 1

	Ideea rezolvarii este cea a unui binary search pe lungime pentru a gasi distanta maxima.
	Astfel, pornind cu intervalul [0, L], observam daca L/2 este o distanta minima acceptabila prin taierea a maxim M stalpi.
	Daca este, vom trece in intervalul [L/2, L], cu mijlocul 3L/4. Daca nu, vom trece in intervalul [0, L/2], cu mijlocul L/4.
	Metoda prin care verificam daca o distanta este "acceptata" (posibila prin taierea a maxim M stalpi) este "ruperea" stalpilor, in ordine, pana cand ori se ajunge la sfarsitul gardului, si mai avem stalpi de taiat sau nu, caz in care inseamna ca este "acceptata", ori ma opresc inainte de sfarsit pentru ca am taiat toti stalpii si distanta minima inca nu e dist.
	Continuam astfel pana cand avem un interval de forma [a, a+1], caz in care inseamna ca am gasit distanta maxima posibila, a;
	
	Complexitate:
		Fiind un binary search, complexitatea cautarii distantei maxime este O(log(n)).
		Complexitatea verificarii daca o distanta este sau nu "acceptata" este O(n).
		Astfel, complexitatea gasirii distantei cerute este O(n*log(n)).
	
	
Problema 2
	
	Ideea rezolvarii e descompunerea in mai multe subprobleme.
	Astfel, pentru a diferentia problemele, construiesc o matrice M, cu M[i][j] ca fiind castigul maxim posibil daca ultimul interval selectat este i, si intre intervalele 1-i sunt selectate exact j intervale.
	Nu avem de cazurile cand j > i (nu putem lua 5 intervale, din 4 posibile).
	Desigur, castigul maxim va fi maximul de pe coloana T a matricei M (T intervale luate).
	De asemenea se foloseste o matrice, maxCol, cu maxCol[i][j] ca fiind maximul dintre elementele 1-i, de pe coloana j, din matricea M.
	Un interval e adaugat la solutie daca, adaugand la solutia anterioara (cu j-1 intervale selectate din intervalele 1:(i-1) ), rezulta solutia optima din j intervale luate din intervalele 1:i (M[i-1][j-1] + castig[i] = M[i][j]). 
	De asemenea, se doreste ca elementul adaugat la solutie sa fie mai mare decat maxCol[i-1][j-1].
	Astfel, construim solutia finala.
	
	Complexitate:
		Construirea matricilor M si maxCol se realizeaza in O(NT) ~ O(N^2).
		Aflarea castigului maxim se realizeaza in O(N-T) ~ O(N)
		Gasirea solutiei (a intervalelor selectate) are complexitatea O(N) (parcurgem matricea pe linii si diagonala).
		Astfel, complexitatea gasirii solutiei este O(N^2).