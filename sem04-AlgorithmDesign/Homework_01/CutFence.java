/* -----------------------------------------------------------
 *  Autor:        Gula Tanase, 324CA
 * 
 *		Proiectarea Algoritmilor. Tema 1. Problema 1.
 * ----------------------------------------------------------- */
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class CutFence {
	int N;
	int M;
	int N2;				// numarul de stalpi ramasi
	int max;			// distanta maxima
	int L;
	int[] DistToOne;	// distanta fata de primul stalp
	int[] DistToPrev;	// distanta fata de stalpul anterior
	
	/* -------------------------------------------------------
	 * Constructor
	 * ------------------------------------------------------- */
	CutFence(String filename) {
		readData(filename);
		
		// construim vectorul DistToPrev
		DistToPrev = new int[N];
		DistToPrev[0] = 0;
		for (int i = 1; i < N; i++) {
			DistToPrev[i] = DistToOne[i] - DistToOne[i-1];
		}
		N2 = N;		// initial numarul de poli ramasi e egal cu nr de poli
	}
	
	/* -------------------------------------------------------
	 * citeste datele din fisierul de input 
	 * si initializeaza variabilele cu valorile date
	 * ------------------------------------------------------- */
	private void readData(String filename) {
		Scanner scanner = null;
		
		try {
			scanner = new Scanner(new File(filename));
			N = scanner.nextInt();
			M = scanner.nextInt();
			L = scanner.nextInt();
			DistToOne = new int[N];
			for (int i = 0; i < N; i++){
				DistToOne[i] = scanner.nextInt();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (scanner != null) scanner.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	/* -------------------------------------------------------
	 * scrie datele in fisierul de output
	 * ------------------------------------------------------- */
	private void writeData(String filename, int[] v) {
		try {
			File file = new File(filename);
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(""+v[0]+"\n");
			bw.write(""+v[N-1]+"\n");
			bw.write(""+0+"\n");
			for(int i = 1; i < v[N-1]; i++) {
				bw.write(""+v[i]+"\n");
			}
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/* -------------------------------------------------------
	 * testeaza daca e posibil ca distanta minima "dist" sa
	 * fie atinsa taind maxim M stalpi
	 * ------------------------------------------------------- */
	private int[] testDist (int dist) {
		int[] DistOne = new int[N];
		
		int k = 1;
		int aux = 0;
		
		for (int i = 1; i < N; i++) {
			if (DistToPrev[i] + aux >= dist) {
				DistOne[k] = DistToOne[i];
				aux = 0;
				k++;
				continue;
			}
			aux += DistToPrev[i];
		}		
		if (aux != 0) {
			DistOne[k] = DistToOne[N-1];
			k++;
		}
		if (DistOne[k-1] - DistOne[k-2] < dist) {
			DistOne[0] = 0;
			return DistOne;
		}
		
		if (N-k > M) {
			DistOne[0] = 0;
		}
		else {
			DistOne[0] = k;
		}
		return DistOne;
	}
	
	/* -------------------------------------------------------
	 * afla mijlocul intervalului [first, last]
	 * ------------------------------------------------------- */
	private int findMid(int first, int last) {
		return (last + first) / 2;
	}
	
	/* -------------------------------------------------------
	 * afla distanta maxima si o returneaza
	 * ------------------------------------------------------- */
	int[] findDist() {
		int first = 0;
		int last = L;
		int[] DistOne = new int[N];
		int dist = 0;
				
		while (true) {
			dist = findMid(first, last);
			DistOne = testDist(dist);
			
			if (first + 1 == last) {
				break;
			}
			
			if (DistOne[0] == 0) {
				last = dist;
			}
			else {
				first = dist;
			}
		}
		
		DistOne[N-1] = DistOne[0];
		DistOne[0] = dist;
		
		return DistOne;
	}
			
	public static void main(String[] args) {
		CutFence cf = new CutFence("date.in");
		
		cf.writeData("date.out", cf.findDist());
	}
}
