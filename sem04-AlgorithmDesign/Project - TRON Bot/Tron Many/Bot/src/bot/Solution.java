/*---------------------------------------------------------------------
 *  Authors:        TRON Ninjas
 *		Daniel Popovici,	324CA
 *		Andreea FLorescu,	324CA
 *		Dragos Sava,		324CA
 *		Tanase Gula,		324CA
 *
 *	Proiectarea Algoritmilor. Project: TRON / Stage 1.
 *---------------------------------------------------------------------*/
package bot;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

class Move {

        int X, Y, cost;
        String direction;

        public Move() {

        }

        public Move(Move m) {
                X = m.X;
                Y = m.Y;
                direction = new String(m.direction);
        }

        public Move(int x, int y, String dir) {
                X = x;
                Y = y;
                direction = new String(dir);
        }
        
        public void set(Move m){
                X = m.X;
                Y = m.Y;
                direction = new String(m.direction);
                cost = m.cost;
        }

        public void setX(int x) {
                X = x;
        }

        public int getX() {
                return X;
        }

        public void setY(int y) {
                Y = y;
        }

        public int getY() {
                return Y;
        }

        public void setDirection(String direction) {
                this.direction = direction;
        }

        public String getDirection() {
                return direction;
        }

        public String toString() {
                String s = "(" + Y + ", " + X + ", " + direction+")";
                return s;
        }
}

public class Solution {

	private static final int INF = Integer.MAX_VALUE;
	private static final int WALL = 999;
	private static final int R = 1001;
	private static final int G = 1002;
	private static int myPlayer;
	public static int distante[][];
	public static int zone[][];
	private static int n;
	private static int m;
	private static ArrayList<Move> frontier = new ArrayList<Move>(); 
	private static String res = "UP";
	private static int MAXDEPTH = 1;

	

	public static void main(String[] args) throws FileNotFoundException {
		String s_temp = new String();
		int x, y;
		Scanner sc = new Scanner(System.in);
		int[][] board;
        Move myMove,opMove;

		// read current Player
		s_temp = sc.next();
		if (s_temp.charAt(0) == 'r') {
			myPlayer = R;
		} 
		else {
			myPlayer = G;
		}
        if (myPlayer == R) {
		// read first player position in the map
		    x = sc.nextInt();
		    y = sc.nextInt();
		    myMove = new Move(y, x, "");
        
		// read second player position in the map
		    x = sc.nextInt();
		    y = sc.nextInt();
		    opMove = new Move(y, x, "");
        }
		else {
            x = sc.nextInt();
		    y = sc.nextInt();
		    opMove = new Move(y, x, "");
            
            x = sc.nextInt();
		    y = sc.nextInt();
		    myMove = new Move(y, x, "");
        }
		// read board
		n = sc.nextInt();
		m = sc.nextInt();
		
		board = new int[n][m];
        
		for (int i = 0; i < n; i++) {
			s_temp = sc.next();
			for (int j = 0; j < m; j++) {
				switch (s_temp.charAt(j)) {
					case '#': {
						board[i][j] = WALL;
						break;
					}
					case '-': {
						board[i][j] = 0;
						break;
					}
					case 'r': {
						board[i][j] = R;
						break;
					}
					case 'g': {
						board[i][j] = G;
						break;
					}
				}
			}
		}
                
		sc.close();
		// in s_temp we're putting the direction that we've chosen for the player
		getMove(board, myMove, opMove, myPlayer, MAXDEPTH, s_temp);

		System.out.println(res);
	}

	// this function makes a BFS strategy, calculating the distances between ME (the player)
	// and every other positions accesible from the board;
	// frontier has the role of a queue
	static void generateDistancesForMe(int[][] my_board, Move myPos, int depth) {
		Move move = new Move();
                
                
		
		if (frontier.size() == 0) {
			return;
		}
        
		// move is the next position that must be visited
		move = frontier.remove(0);  
        
		// finds all the accesible positions
		ArrayList<Move> moves = getPossibleMoves(my_board, move);
                for (Move m: moves) {
			// update the cost of position
			// mark the position as visited and add it in frontier
			m.cost = move.cost + 1;          
			my_board[m.getY()][m.getX()] = WALL;
			frontier.add(m);
		}

		// add the cost of the current position in the distances matrix;
		// mark the area as part of my area and go forward with the recursion
		distante[move.getY()][move.getX()] = move.cost;
		zone[move.getY()][move.getX()] = 2;
		generateDistancesForMe(my_board, move, depth + 1);

	}

	// this function makes a limited BFS strategy: 
	// when it hits a positions with the same cost as the opponent, it stops;
	// the passing only explores the opponents area and marks it in consequence
	static void generateInfluenceZones(int[][] board, Move opPos, int depth) {

		Move move = new Move();

		if (frontier.size() == 0) {
			return;
		}

		move = frontier.remove(0);  
		
		// add posible moves in queue
		ArrayList<Move> moves = getPossibleMoves(board, move);
        
		for (Move m: moves) {
			m.cost = move.cost + 1; 
			// if the opponent gets faster to the respective area, mark it as "owner"
			// and move forward with the recursion
			if (m.cost < distante[m.getY()][m.getX()]) {
				board[m.getY()][m.getX()] = WALL;
				zone[m.getY()][m.getX()] = 1;
				frontier.add(m);
				continue;
			}
            
			// if it gets to a position with an equal distance between the two players
			// mark it with 0 (zero) in order to stop;
			// explore forward
			if (m.cost == distante[m.getY()][m.getX()]) {
				zone[m.getY()][m.getX()] = 3;
                                distante[m.getY()][m.getX()] = 0;
				continue;
			}

		}

		distante[move.getY()][move.getX()] = move.cost;
		generateInfluenceZones(board, move, depth + 1);

	}

	static int evaluate(int[][] board, Move myPos, Move opPos) {
		distante = new int[n][m];
		zone = new int[n][m];
		int myCount = 0;      
		int aux_board[][] = new int[n][m];

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				aux_board[i][j] = board[i][j];
			}
		}

		// calculate the distances between me and any other position on the map
		myPos.cost = 0;
		frontier.clear();
		frontier.add(myPos);       
		generateDistancesForMe(board, myPos, 0);

		// calculate the distances for the opponent
		frontier.clear();
		opPos.cost = 0;
		frontier.add(opPos);       
		generateInfluenceZones(aux_board, opPos, 0);
                

		// counting the positions from my area of influence
		for (int i = 0; i < zone.length; i++) {
			for (int j = 0; j < zone[i].length; j++) {
				if (zone[i][j] == 2) {	// if it's on my area of influence
					myCount++;
				}
			}
		}

		return myCount;

	}

	static int getMove(int[][] board, Move myPos, Move opponentPos,
			int player, int depth, String move) {

		int score = 0;
		int best_score = -INF;
		int aux_board[][] = new int[n][m];
		ArrayList<Move> moves = getPossibleMoves(board, myPos);
		Move bestMove = new Move();
        
		// check if the bots did hit
		if (myPos.X == opponentPos.X && myPos.Y == opponentPos.Y) {
			return 6666;
		}

		// check if we reached maximum depth
		if (depth == 0) {
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < m; j++) {
					aux_board[i][j] = board[i][j];
				}
			}
			
			score = evaluate(aux_board, myPos, opponentPos);
			if (player == myPlayer) {
				return score;
			} 
			else {
				return -score;
			}
		}
        int pl;
		// check all the moves
		for (Move m : moves) {
			// mark the position that it wants to go
			board[m.getY()][m.getX()] = player;
                        
                        score = getMove(board, m, opponentPos, player, depth - 1, move);
			// check if the bots did hit
			if (score == 6666 || score == -6666) {
				continue;
			}

			// keep the best move
			if (score > best_score) {
				best_score = score;
                                bestMove.set(m);
				if (depth == MAXDEPTH) {
					res = m.direction;
				}
			}
                        
                        
			board[m.getY()][m.getX()] = 0;
		}

		return score;
	}
    
	private static boolean isValid(int[][] board ,Move m){
		if (board[m.getY()][m.getX()] != WALL && board[m.getY()][m.getX()] != G
                && board[m.getY()][m.getX()] != R) {
			return true;
		}
		
		return false;
	}

	static ArrayList<Move> getPossibleMoves(int[][] my_board, Move pos) {
		ArrayList<Move> rez = new ArrayList<Move>();
		if (pos.getY() > 1 && 
				isValid(my_board, new Move(pos.getX(), (pos.getY() - 1), "UP"))) {
			rez.add(new Move(pos.getX(), (pos.getY() - 1), "UP"));
		}

		if (pos.getY() < (n - 1) && 
				isValid(my_board, new Move(pos.getX(), (pos.getY() + 1), "UP"))) {
			rez.add(new Move(pos.getX(), (pos.getY() + 1), "DOWN"));
		}

		if (pos.getX() > 1 && 
				isValid(my_board, new Move(pos.getX() - 1, pos.getY(), "UP"))) {
			rez.add(new Move(pos.getX() - 1, pos.getY(), "LEFT"));
		}

		if (pos.getX() < (m - 1) && 
				isValid(my_board, new Move(pos.getX() + 1, (pos.getY()), "UP"))) {
			rez.add(new Move(pos.getX() + 1, (pos.getY()), "RIGHT"));
		}

		return rez;
	}
}
