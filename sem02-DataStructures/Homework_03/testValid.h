#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

#include "bitmap.h"
#include "queue.h"
#include "tree.h"

#include <string.h>

// Prints the program usage
void printUsage(){
	fprintf(stderr, "Program usage:\n");
	fprintf(stderr, "\tmycompbmp -c|-d <nume_fisier_in> [<nume_fisier_out>]\n");
	fprintf(stderr, "\n");
}

// Intoarce parametrii liniei de comanda (in aux)
char** parameters(int argc, char **argv){
    char **aux = new char*[3];
    aux[1] = new char[3];
    aux[2] = new char[20];
    aux[3] = new char[20];
    switch(argc){
        case 3:
            strcpy(aux[1], argv[1]);
            strcpy(aux[2], argv[2]);
            if(strcmp(aux[1], "-c") == 0){
                aux[3] = strtok(argv[2],".");
                aux[3] = strcat(aux[3],".mcb");
            }
            else{
                aux[3] = strtok(argv[2],".");
                aux[3] = strcat(aux[3],".bmp");
            }
            break;
        case 4:
            strcpy(aux[1], argv[1]);
            strcpy(aux[2], argv[2]);
            strcpy(aux[3], argv[3]);
            break;
        default:
            printUsage();
            return NULL;
    }
    return aux;
}

// Eroare la citirea din fisier
void error(FILE *input, int i){
	if (input != NULL)
		fclose(input);

		fprintf(stderr, "ERROR: Invalid FILE format!\n");

	switch(i){
        case 1:
            fprintf(stderr, "The file does not have the BITMAPFILEHEADER!\n");
            break;
        case 2:
            fprintf(stderr, "The file does not have the BITMAPINFOHEADER!\n");
            break;
        case 3:
            fprintf(stderr, "The colorTable is not valid!\n");
            break;
        case 4:
            fprintf(stderr, "The bfType is not valid!\n");
            break;
	}
}

int testBIH (BITMAPINFOHEADER bih){
    // A bitmap must have 8 bits of color (256 index table), and be
    // not compressed, in order to be converted to My Compressed Bitmap format
    if (bih.biBitCount != 8 || bih.biClrUsed > 256 ||
        bih.biClrImportant > 256 || bih.biCompression != BI_RGB) {
            printf("This bitmap is not suitable for My Compressed Bitmap format!\n");
			return 1;
	}
return 0;
}

// Checkes the other "stuff" in the headers, which are specific for compression
int testComp(FILE *input, BITMAPFILEHEADER bfh, BITMAPINFOHEADER bih){
	// We check for the bitmap format ID
	// The word is stored in little endian format
	if (( bfh.bfType       & 0x00FF) != 'B' ||
		((bfh.bfType >> 8) & 0x00FF) != 'M') {
        error(input, 4);
		return 2;
	}

    // Se testeaza daca sunt 8biti de culoare
    int i = testBIH(bih);
    if(i == 1){
        return 2;
    }
return 0;
}

// Checkes the other "stuff" in the headers, which are specific for decompression.
int testDecomp(FILE *input, BITMAPFILEHEADER bfh, BITMAPINFOHEADER bih){
    // We check for the bitmap format ID
	// The word is stored in little endian format
	if (( bfh.bfType       & 0x00FF) != 'M' ||
		((bfh.bfType >> 8) & 0x00FF) != 'C') {
		    error(input, 4);
		    return 2;
	}
return 0;
}


#endif // FUNCTIONS_H
