#ifndef QUEUE_H
#define QUEUE_H

#include <cstdio>
#include <cstdlib>
#include<iostream>
#include<stdio.h>
#include<stdint.h>

struct Node{
    int lvl;
    int prior;
    uint8_t info;
    Node *parent;
    Node *right;
    Node *left;
};

template <typename T>
class PriorQueue{
public:
    PriorQueue();
    ~PriorQueue();

// Inseareaza un nou nod in vector
    void insert(T *x);

// Returneaza nodul radacina si rearanjeaza arborele
    T* extract();

// Compara doua noduri, conform cerintei
    int compare(T *n1, T *n2);

// Printeaza lista; doar cu rol de verificare
    void print();

    T **values;
    int dimVect;
    int capVect;
};

#endif // QUEUE_H
