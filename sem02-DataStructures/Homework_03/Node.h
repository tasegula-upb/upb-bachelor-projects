#ifndef NODE_H_
#define NODE_H_

#include "queue.h"

class Node{
public:
    int lvl;
    int prior;
    uint8_t info;
    Node *parent;
    Node *right;
    Node *left;

    // Constructor
    Node();

    // Destructor
    ~Node();

    // Create the Huffman Tree

}

#endif // NODE_H
