#ifndef BITMAP_H_
#define BITMAP_H_

#include <stdint.h>

//////////////////////////////////////////////////
// The Bitmap Format Header Structures
//
// Further details here:
//  * http://msdn2.microsoft.com/en-us/library/ms532311(VS.85).aspx
//  * http://www.fortunecity.com/skyscraper/windows/364/bmpffrmt.html
//  * http://www.fileformat.info/format/bmp/egff.htm

// The first header in the Bitmap file
typedef struct tagBITMAPFILEHEADER {
  uint16_t	bfType;
  uint32_t	bfSize;
  uint16_t	bfReserved1;
  uint16_t	bfReserved2;
  uint32_t	bfOffBits;
} __attribute__((__packed__)) BITMAPFILEHEADER, *PBITMAPFILEHEADER;

// The second header in the Bitmap file
typedef struct tagBITMAPINFOHEADER {
  uint32_t	biSize;
  int32_t	biWidth;
  int32_t	biHeight;
  uint16_t	biPlanes;
  uint16_t	biBitCount;
  uint32_t	biCompression;
  uint32_t	biSizeImage;
  int32_t	biXPelsPerMeter;
  int32_t	biYPelsPerMeter;
  uint32_t	biClrUsed;
  uint32_t	biClrImportant;
} __attribute__((__packed__)) BITMAPINFOHEADER, *PBITMAPINFOHEADER;

// An entry in the color table
typedef struct tagRGBQUAD {
	uint8_t rgbBlue;
	uint8_t rgbGreen;
	uint8_t rgbRed;
	uint8_t rgbReserved;
} __attribute__((__packed__)) RGBQUAD, *PRGBQUAD;

typedef struct tagSerialHuffmanNode {
  uint8_t isTerminal;
  union {
    uint8_t colorIndex;
    struct {
      uint16_t leftChld;
      uint16_t rightChld;
    } __attribute__((__packed__)) chldData;
  } __attribute__((__packed__));
} __attribute__((__packed__)) SerialHuffmanNode, *PSerialHuffmanNode;

// Constants for the biCompression field
#define BI_RGB        0L
#define BI_RLE8       1L
#define BI_RLE4       2L
#define BI_BITFIELDS  3L
#define BI_JPEG       4L
#define BI_PNG        5L

// Custom MCB constant
#define BI_MCB		  0xC0FFEEL


#endif /*BITMAP_H_*/
