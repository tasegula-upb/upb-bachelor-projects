#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "bitmap.h"
#include "queue.h"
#include "tree.h"
#include "testValid.h"
#include "compression.h"
#include "decompression.h"

using namespace std;

int main(int argc, char **argv){
	FILE *bmpFile = NULL;
	FILE *output = NULL;
	BITMAPFILEHEADER bfh;   // First bitmap header
	BITMAPINFOHEADER bih;   // Second bitmap header
	RGBQUAD ctable[256];    // The color table
	int tableSize = 0;      // The actual size of the table

    char **param = NULL;

    param = parameters(argc, argv); // Read the parameters

    if(param == NULL){
        return 2;
    }

    puts(param[1]);
    puts(param[2]);
    puts(param[3]);

	bmpFile = fopen(param[2], "rb"); // We open the input file...
    output = fopen(param[3], "wb"); // We open the  file

	if (bmpFile == NULL) {
		fprintf(stderr, "ERROR: %s cannot be open!\n", param[2]);
		return 2;
	}

	// Read the first header
	if (!fread(&bfh, sizeof(BITMAPFILEHEADER),1, bmpFile)) {
		error(bmpFile, 1);
		return 2;
	}

    // Read the next header in the file
    if (!fread(&bih, sizeof(BITMAPINFOHEADER),1, bmpFile)) {
		error(bmpFile, 2);
        return 2;
	}

	tableSize = bih.biClrUsed;
	if (tableSize == 0){
		tableSize = 256;
	}

	// Then read the bitmap color table
	if (fread(ctable, sizeof(RGBQUAD), tableSize, bmpFile) < tableSize) {
		error(bmpFile, 3);
		return 2;
	}

    if(strcmp(param[1], "-c") == 0){
        uint8_t **dict = new uint8_t*[256];
        int size;

        if(testComp(bmpFile, bfh, bih) != 0){
            return 2;
        }

        SerialHuffmanNode *SH = new SerialHuffmanNode[65000];
 //       SerialHuffmanNode *v = new SerialHuffmanNode[512];

        bfh.bfType = 17229;
        bih.biCompression = 0xC0FFEE;

        size = bih.biWidth * bih.biHeight;

        // Return the vector of SerialHuffmanNode
        // Also, in dict, is kept the dictionary of codes.
        SH = createSerial(bmpFile, bih, dict);

//        v = compressVector(SH, 1000000);

        writeHeaders(output, bfh, bih, ctable, SH, 65000);

        //writeHeaders(output, bfh, bih, ctable, v, 512);

        // Seek at the beginning of the bitmap pixel data
        fseek(bmpFile, bfh.bfOffBits, SEEK_SET);

        compression(bmpFile, output, size, dict);

    return 0;
    }

    if(strcmp(param[1], "-d") == 0){
        if(testDecomp(bmpFile, bfh, bih) != 0)
            return 2;

        decompresion(bmpFile, output, ctable, bfh, bih);

    return 0;
    }
}
