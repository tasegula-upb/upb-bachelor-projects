#ifndef DECOMPRESION_H_
#define DECOMPRESION_H_

#include "bitmap.h"
#include "testValid.h"

void decompresion(FILE* input, FILE* output, RGBQUAD ctable[256],
                 BITMAPFILEHEADER bfh, BITMAPINFOHEADER bih){
    SerialHuffmanNode *SH;
    uint16_t n;
    uint8_t buffer;
    uint8_t bit;
    int contor = 0, k=0, c=0;
    int size = bih.biWidth * bih.biHeight;

    fread(&n, sizeof(uint16_t), 1, input);

    SH = new SerialHuffmanNode[n];
    for(int i = 0; i < n; i++){
        fread(&SH[i], sizeof(SerialHuffmanNode),1, input);
    }

    bfh.bfType = 19778;
    bih.biCompression = 0;

    fwrite(&bfh, 1, sizeof(BITMAPFILEHEADER), output);
    fwrite(&bih, 1, sizeof(BITMAPINFOHEADER), output);
    fwrite(ctable, 256, sizeof(RGBQUAD), output);

    // The decompression algorithm starts here
    fread(&buffer, sizeof(uint8_t), 1, input);
    while(1){
        bit = buffer & pow2(contor);
        if(bit){
            k = SH[k].chldData.leftChld;
        }
        else{
            k = SH[k].chldData.rightChld;
        }
        contor++;
        if(SH[k].isTerminal == 1){
            fwrite(&SH[k].colorIndex,1,sizeof(uint8_t),output);
            c++;
            k = 0;
        }
        if(contor == 8){
            fread(&buffer,sizeof(uint8_t),1,input);
            contor = 0;
        }
        if(c == size)
            break;
    }
return;
}

#endif // DECOMPRESION_H
