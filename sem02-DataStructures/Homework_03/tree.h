#ifndef TREE_H_
#define TREE_H_

#include "queue.h"
#include "bitmap.h"

#include <stdint.h>

using namespace std;

// Checkes if a Node is terminal or not
int isTerminal(Node *n){
    if(n->left != NULL || n->right != NULL) return 1;
return 0;
}

// Auxiliar function
// Helps creating the Huffman tree
void add(PriorQueue<Node> *x){

    Node *Node1 = new struct Node;
    Node *Node2 = new struct Node;
    Node *Parent = new struct Node;

    Node1 = x->extract();
    Node2 = x->extract();

    Parent->left = Node1;
    Parent->right = Node2;
    Parent->prior = Node1->prior + Node2->prior;

    if(Node1->lvl >= Node2->lvl){
        Parent->lvl = Node1->lvl + 1;
    }
    else{
        Parent->lvl = Node2->lvl + 1;
    }

    x->insert(Parent);
}

// Creates the Huffman Tree
Node* CreateHuffmanTree(PriorQueue<Node> *x){
    while(x->dimVect > 1){
        add(x);
    }
    Node *n = x->extract();
return n;
}

// Creates the vector that will be written in the .mcb
void CreateHuffmanVector(SerialHuffmanNode *HuffVect, int i, Node *n){
    if(isTerminal(n) == 1){
        CreateHuffmanVector(HuffVect,2*i+1,n->left);
    }
    if(isTerminal(n) == 0){
        HuffVect[i].isTerminal = 1;
        HuffVect[i].colorIndex = n->info;
    }
    else{
        HuffVect[i].chldData.leftChld = 2*i+1;
        HuffVect[i].chldData.rightChld = 2*i+2;
        HuffVect[i].isTerminal = 0;
    }

    if(isTerminal(n) == 1){
        CreateHuffmanVector(HuffVect,2*i+2,n->right);
    }
}

// Creates a dictionary in which will be remembered every code
// dictionary[Index][0] = how long is the code;
void dictionary(Node *n, uint8_t *v, uint8_t i, uint8_t **matrix){
    int j;

    if(n->left != NULL){
        v[i] = 1;
        dictionary(n->left, v, i+1, matrix);
    }

    if(isTerminal(n) == 0){
        matrix[n->info][0]=i;
        for(j = 0; j < i; j++){
            matrix[n->info][j+1]=v[j];
        }
    }

	if(n->right != NULL){
		v[i] = 0;
		dictionary(n->right, v, i+1, matrix);
	}
}

#endif // TREE_H
