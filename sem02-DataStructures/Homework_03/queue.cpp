#include"queue.h"

using namespace std;

template <typename T>
PriorQueue<T> :: PriorQueue(){
    int i;
    capVect = 256;
    dimVect = 0;
    values = new T*[capVect];
    for(i = 0; i < capVect; i++){
        values[i] = new T;
    }
    for(i = 0; i < capVect; i++){
        values[i]->prior = 1000000;
        values[i]->lvl = 10;
    }
}

template<typename T>
PriorQueue<T> :: ~PriorQueue(){
}

template <typename T>
int PriorQueue<T> :: compare(T *node1, T *node2){
    if(node1->prior < node2->prior) return 1;
    else
        if(node1->prior == node2->prior){
            if(node1->lvl < node2->lvl) return 1;
        }
return 0;
}

template <typename T>
void PriorQueue<T> :: insert(T *x){

    T *n;
    int aux,c;
    if(dimVect == 0){
        values[dimVect] = x;
        dimVect++;
//        printf("FIRST\n");
        return;
    }

    values[dimVect] = x;
    dimVect++;

    aux = dimVect-1;
    c = compare(values[aux], values[aux-1]);
    while(c != 0){
        switch(c){
            case 1:
                n = values[aux];
                values[aux] = values[aux-1];
                values[aux-1] = n;
                if(aux > 1) aux--;
                break;
            default:
                if(aux > 1) aux--;
                break;
        }
        c = compare(values[aux],values[aux-1]);
    }
return;
}

template <typename T>
T* PriorQueue<T> :: extract(){
    T *aux = values[0];
    for(int i = 0; i < dimVect-1; i++){
        values[i] = values[i+1];
    }
    dimVect--;
return aux;
}

template <typename T>
void PriorQueue<T> :: print(){
    int a = dimVect, i;
    for(i = 0; i < a; i++){
        printf("INFO: %u\n", values[i]->info);
        printf("PRIOR: %u\n", values[i]->prior);
        printf("\n");
    }
}

template class PriorQueue<Node>;
