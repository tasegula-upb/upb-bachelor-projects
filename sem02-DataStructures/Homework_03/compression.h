#ifndef COMPRESSION_H_
#define COMPRESSION_H_

#include "queue.h"
#include "bitmap.h"
#include "tree.h"

// Reads all the pixels from the image
// Returns a vector that have the number of appearances of an index
int* readPixels(FILE *input, BITMAPINFOHEADER bih){
    int size = bih.biWidth*bih.biHeight, i;
    int *vectorPrior = new int[256];
    uint8_t Index;

    for(i = 0; i < 256; i++){
        vectorPrior[i] = 0;
    }

    for(i = 0; i < size; i++){
        fread(&Index, sizeof(uint8_t), 1, input);
        vectorPrior[Index]++;
    }
return vectorPrior;
}

// Initialize the SerialHuffmanNode vector
void initializeSerial(SerialHuffmanNode *S, int n){
    for(int i = 0; i < n; i++){
        S[i].isTerminal = 2;
    }
}

// Initialize the dictionary aand the auxiliar vector,
// which will be used for creating the dictionary
void initializeDictionary(uint8_t* v, uint8_t** dict){
    int i,j;

    for(i = 0; i < 256; i++){
        dict[i]=new uint8_t[16];
        dict[i][0]=0;
        for(j = 1; j < 16; j++){
            dict[i][j]=2;
        }
    }

    for(i = 0; i < 16; i++){
        v[i]=2;
    }
}

// Just for testing
// It prints the dictioanry;
void printDictionary(uint8_t **dict){
    for(int i = 0; i < 256; i++){
        printf("%d\t%u\t",i,dict[i][0]);
        for(int j = 1; j <= dict[i][0]; j++){
            printf("%u",dict[i][j]);
        }
        printf("\n");
    }
}

// Creates the SerialHuffmanNode vector, which will be put in the .mcb file
SerialHuffmanNode* createSerial(FILE *input, BITMAPINFOHEADER bih, uint8_t **dict){
    PriorQueue<Node> *q = new class PriorQueue<Node>;
    Node *HuffTree = new struct Node;
    SerialHuffmanNode *SH = new SerialHuffmanNode[65000];
	uint8_t *v = new uint8_t[16];
    int i;

    int *vectorPrior = readPixels(input, bih);

    // Creates the priority queue
    for(i = 0; i < 256 ; i++){
        if(vectorPrior[i] != 0){
            Node *aux = new struct Node;
            aux->info = i;
            aux->prior = vectorPrior[i];
            q->insert(aux);
        }
    }

    initializeDictionary(v, dict);

    // special case in which there is a picture with only one color
    if(q->dimVect == 1){
        Node *n = new struct Node;
        SH[0].isTerminal = 0;
        SH[0].chldData.leftChld = 1;
        SH[1].isTerminal = 1;
        n = q->extract();
        SH[1].colorIndex = n->info;
        dict[n->info][0] = 1;
        dict[n->info][1] = 1;
        return SH;
    }

    // Here is the root of the tree
    HuffTree = CreateHuffmanTree(q);
    dictionary(HuffTree,v,0,dict);

    initializeSerial(SH, 65000);
    CreateHuffmanVector(SH,0,HuffTree);

return SH;
}

// pow(2,a); it returns an int;
int pow2(int a){
    int pow = 1;
    for(int i = 0; i < a; i++){
        pow = pow * 2;
    }
return pow;
}

// Writes in the output file all the headers
void writeHeaders(FILE* output, BITMAPFILEHEADER bfh, BITMAPINFOHEADER bih,
                  RGBQUAD ctable[256], SerialHuffmanNode *SH, uint16_t n){

    fwrite(&bfh, 1, sizeof(BITMAPFILEHEADER), output);
    fwrite(&bih, 1, sizeof(BITMAPINFOHEADER), output);
    fwrite(ctable, 256, sizeof(RGBQUAD), output);
    fwrite(&n, 1, sizeof(uint16_t), output);

    for(int i = 0; i < n; i++){
        fwrite(&SH[i], 1, sizeof(SerialHuffmanNode), output);
	}
}

// Compress the SerialHuffmanNode vector, removing the blank spots
SerialHuffmanNode* compressVector(SerialHuffmanNode *v, int n){
    SerialHuffmanNode *aux = new SerialHuffmanNode[512];
    int contor, k, chld, parent, i;
    contor = k = chld = parent = 0;

    for(i = 0; i < n; i++){
        if(v[i].isTerminal == 2){
            continue;
        }

        aux[k] = v[i];
        k++;
        parent = (int)(i/2) - chld;

        if(chld == 0)
            aux[parent].chldData.leftChld = k;
        if(chld == 1)
            aux[parent].chldData.rightChld = k;

        chld++;
        if(chld > 1)
            chld = 0;
    }

cout<<k<<endl;

return aux;
}

void compression(FILE* input, FILE *output, int size, uint8_t **dict){
    uint8_t Index, buffer = 0;
    int i, j, contor = 0;

    for(i = 0; i < size; i++){
        fread(&Index, sizeof(uint8_t), 1, input);
        for(j = 1; j <= dict[Index][0]; j++){
            if(dict[Index][j] == 1){
                buffer |= pow2(contor);
                contor++;
            }
            else{
                contor++;
            }
            if(contor == 8) {
                fwrite(&buffer, sizeof(uint8_t), 1, output);
                contor = 0;
                buffer = 0;
            }
        }
    }
    if(contor > 0){
        fwrite(&buffer, sizeof(uint8_t), 1, output);
    }
return;
}

#endif // COMPRESSION_H
