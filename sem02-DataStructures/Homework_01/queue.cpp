#include"queue.h"
#include<stdio.h>

template <typename T, int N>
Queue<T, N>::Queue() {
	head=-1;
        tail=0;
        size=0;
}

template <typename T, int N>
Queue<T, N>::~Queue() {
	
}

template <typename T, int N>
void Queue<T, N>::enqueue(T e) {
	head++;
	queueArray[head]=e;
	size++;
}

template <typename T, int N>
T Queue<T, N>::dequeue() {
	size--;
	tail++;
	return queueArray[tail-1];
}

template <typename T, int N>
T Queue<T, N>::front() {
	return queueArray[head];
}

template <typename T, int N>
bool Queue<T, N>::isEmpty() {
	if(size==0) return 1;
        else return 0;
}

template class Queue<process,100>;
