class process{
public:
	int pid;
	int t;
	int prior;
	int run;

	void init(){
		pid=t=prior=run=0;
	}
};

template <typename T, int N>
class Queue {
private:
	int head;
	int tail;
	int size;
	T queueArray[N];

public:
	// Constructor
	Queue();

	// Destructor
	~Queue();

	void enqueue(T e);
	T dequeue();
	T front();
	bool isEmpty();

};
