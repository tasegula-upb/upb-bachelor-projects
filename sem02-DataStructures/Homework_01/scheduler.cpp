#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"queue.h"

Queue<process,100> R[5];	// cozile de READY
Queue<process,100> W[3];	// cozile de WAITING
process x;					// procesul din RUNNING
int T;						// "timpul" procesorului

//
//	Functia pentru comanda WAIT
//
//	primeste ca parametru prioritatea (n) din comanda (al doilea numar);
//	daca exista proces in RUNNING (x.pid!=0), atunci:
//		- x.run=0;
//		- adauga procesul x la coada WAITING conform lui n;
//		- reseteaza valorile procesului RUNNING la 0.
//
void wait(int n){
	if(x.pid){	
		x.run=0;
		W[n].enqueue(x);
		x.init();
		return;
	}
return;
}

//
//	Functia pentru comanda SIGNAL
//		
//		primeste ca parametru prioritatea (n) din comanda (al doilea numar);
//		citeste din coada WAITING(n) un element si il adauga intr.o coada
//	de READY in functie de prioritatea sa;
//		citirea, respectiv adaugarea, continua pana cand nu mai exista elemente
//	de citit din WAITING(n).
//
void signal(int n){
process p;
p.init();
int ok=1,i;
while(ok==1){
	ok=0;
	if(W[n].isEmpty()==0){
		p=W[n].dequeue();
		ok=1;
	}
	if(p.pid!=0 && ok==1){
		if(p.prior!=0){
			p.prior--;
		}
		R[p.prior].enqueue(p);
	}
}
return;
}

//
//	Functia pentru comanda SIGNAL
//
//	se observa doua cazuri: exita proces in RUNNING sau nu exista;
//		- daca nu exista, cauta in cozile de READY pana gaseste proces de rulat:
//			- cand gaseste, il introduce in RUNNING;
//			- daca ajunge la coada READY[4] (de prioritate 5), si in continuare
//			nu gaseste proces de rulat, iese din program;
//		- daca (cand) exista proces in RUNNING, atunci se verifica ca el sa nu
//		fi rulat de mai mult de 3 ori si sa mai aibe durata de rulare;
//			- daca aceste conditii se respecta, procesul se ruleaza inca o data,
//			rezultand cresterea numarului de rulari (x.run++) si scaderea
//			duratei de rulare (x.t--); de asemenea, creste timpul procesorului
//			cu o unitate (T++);
//			- daca aceste conditii nu sunt respectate, dar procesul RUNNING mai
//			are durata de executie, atunci acesta se adauga la coada READY
//			conform prioritatii sale; se reseteaza procesul RUNNING.
//
void finalize(){
int ok=1,i;

	while(ok==1){
		if(x.pid==0){
			for(i=0;i<5;i++){
				if(R[i].isEmpty()==0){
					x=R[i].dequeue();
					break;
				}
				else{
					if(i==4){
						ok=0;
					}
				}
			}
		}
		else{
			if(x.run<3 && x.t>0){
				printf("[%d] PID %d\n", T, x.pid);
				T++;
				x.run++;
				x.t--;
			}
			else{
				if(x.t>0){
					x.run=0;
					R[x.prior].enqueue(x);
				}
				x.init();
			}
		}
	}
}

//
//	Functia de SCHEDULER
//
//		primeste ca parametru "time", reprezentand timpul procesorului atunci
//	cand se primeste o comanda;
//		astfel de creeaza o variabila d=time-T+1;
//		functioneaza asemanator functiei FINALIZE, doar ca atunci cand cauta
//	procese in cozile de READY si ajunge la READY[4] si nu gaseste proces,
//	afiseaza [] NOP de "d" ori.
// 
//	PS: d se decrementeaza la fiecare afisare, iar T se incrementeaza;
//
void scheduler(int time){
int d,i,j;

	d=time-T+1;
	while(d>0){
		if(x.pid==0){
			for(i=0;i<5;i++){
				if(R[i].isEmpty()==0){
					x=R[i].dequeue();
					break;
				}
				else{
					if(i==4){
						for(j=0;j<d;j++){
							printf("[%d] NOP\n",T);
							T++;
						}
						d=0;
					}
				}
			}
		}
		else{
			if(x.run<3 && x.t>0){
				printf("[%d] PID %d\n", T, x.pid);
				T++;
				x.run++;
				x.t--;
				d--;
			}
			else{
				if(x.t>0){
					x.run=0;
					R[x.prior].enqueue(x);
				}
				x.init();
			}
		}
	}
}

int main(){

char *command; // sir de caractere unde se vor retine, pe rand, comenzile
char *p;	// comanda

int time,priority; // se retin argumentele comenzilor
int id=1, OK=1;	// se retine PID.ul ultimului proces introdus

process proc_new;
proc_new.init();

	command=(char *)malloc(20);
	fgets(command,20,stdin);
	p=strtok(command," \n");
	
	if(strcmp(p,"FINALIZE")==0){
		return 0;
	}

	while(OK==1){
		if(strcmp(p,"NEW")==0){
			p=strtok(NULL," \n");
			time=atoi(p);
			p=strtok(NULL," \n");
			proc_new.t=atoi(p);
			p=strtok(NULL," \n");
			proc_new.prior=atoi(p);
			proc_new.pid=id;
			proc_new.run=0;
			id++;
			scheduler(time);
			R[proc_new.prior].enqueue(proc_new);
		}
		if(strcmp(p,"WAIT")==0){
			p=strtok(NULL," \n");
			time=atoi(p);
			p=strtok(NULL," \n");
			priority=atoi(p);
			scheduler(time);
			wait(priority);
		}
		if(strcmp(p,"SIGNAL")==0){
			p=strtok(NULL," \n");
			time=atoi(p);
			p=strtok(NULL," \n");
			priority=atoi(p);
			scheduler(time);
			signal(priority);
		}
		if(strcmp(p,"FINALIZE")==0){
			finalize();
			OK=0;
		}
		else{
			free(command);
			command=(char *)malloc(20);
			fgets(command,20,stdin);
			p=strtok(command," \n");
		}
	}

free(command);

}
