#!/bin/bash

EXE=sched
TOTAL=0

for i in $(seq -w 0 15); do
	./$EXE < "tests/test$i.in" > "tests/out$i"
	diff -wiB "tests/out$i" "tests/test$i.out_ref" > __stdout
	if [ $? != 0 ]; then
		echo "Test $i "
		cat __stdout | head -n 20
		echo -e "File truncated!               ... test failed!\n"
	else
		echo "Test $i                       ... OK!"
		TOTAL=$(expr $TOTAL + 5)
	fi
done

echo "----------------------------------------------"
echo "TOTAL: $TOTAL/80 puncte"

rm -f tests/out*
