#include<stdio.h>
#include<stdlib.h>
#include<string.h>


/*
# Functia DUMP; 
#
# se parcurge arena; in functie de index%16 (modulo 16), se afiseaza un anumit lucru;
*/
void dump(unsigned char *v, int n){
int i;
	for(i=0;i<n;i++){
		if(i%16==0){
			printf("%.8X\t", i);
		}
		printf("%.2X " , *(v+i));
		if(i%16==7){
			printf(" ");
		}
		if(i%16==15){
			printf("\n");
		}
	}
	if((n-1)%16!=15){ 
		printf("\n");
	}
	printf("%.8X\n",n);
}

/*
# Functia pentru comanda ALLOC; n-numarul de elemente din vector; a-numar luat din comanda.
#
# avem doua cazuri: v[i]!=0 sau v[i]==0;
# caz v[i]==0: inseamna ca arena este goala, deci se va aloca blocul dorit incepand cu pozitia 4+12(biti de gestiune)
# caz v[i]!=0: se verifica, pe rand, daca exista loc pentru alocarea blocului intre:
#	indexul de start si primul bloc;
#	urmatoarele doua blocuri etc;
#	la sfarsitul arenei;
*/
int alloc(unsigned char *v, int n, int a){

// folosite pentru parcurgerea arenei; 
// ok: se foloseste pentru a sti daca s-a alocat blocul(=1);	
	int i=0, ant, ok;
// numarul care se pune in sectiunea de gestiune pe pozitia 3 reprezentand marimea totala a blocului;
	int x=12+a; 
// se vor folosi pentru a vedea daca intre doua blocuri ocupate exista loc pentru un nou bloc de marime totala x;
	int aux, aux_urm, aux_ant;
	
	if((*(int *)(v+i))==0){
		if(n>=x){
			*(int *)(v)=4;
			*(int *)(v+4)=0;
			*(int *)(v+8)=0;
			*(int *)(v+12)=x;
			return 16;
		}
		else{
			return 0;
		}
	}
	else{
		ok=0;
		ant=0;
		aux=*(int *)(v+i)-4;
		if(aux>=x){
			ok=1;
			aux_urm=*(int *)(v+i);
			*(int *)(v+4)=*(int *)(v+i);
			*(int *)(v+8)=0;
			*(int *)(v+12)=x;
			*(int *)(v+i)=4;
			*(int *)(v+aux_urm+4)=4;
			i=*(int *)(v+i);
			return 16;
		}
//		i=*(int *)(v+i);
		while(*(int *)(v+i)!=0 && ok!=1){
			aux_urm=*(int *)(v+i);
			aux_ant=i+(*(int *)(v+i+8));
			aux=aux_urm-aux_ant;
			if(aux>=x){
				ok=1;
				*(int *)(v+aux_ant)=aux_urm;
				*(int *)(v+aux_ant+4)=i;
				*(int *)(v+aux_ant+8)=x;
				*(int *)(v+i)=aux_ant;
				*(int *)(v+aux_urm+4)=aux_ant;
				i=*(int *)(v+i);
				return i+12;
			}
			else{
				ant=i;
				i=*(int *)(v+i);
			}
		}
		if((n-1-i-*(int *)(v+i+8))>=x){
			if(ok==0){
				*(int *)(v+i)=i+*(int *)(v+i+8);
				*(int *)(v+i+4)=ant;
				ant=i;
				i=*(int *)(v+i);
				*(int *)(v+i)=0;
				*(int *)(v+i+4)=ant;
				*(int *)(v+i+8)=x;
				return i+12;
			}
			else{
				return 0;
			}
		}
		else{
			return 0;
		}
	}
}

/*
# Functia pentru comanda FILL: i-index; s-size; val-value; n-numar elemente din vector; 
#
# doar introduce valoarea "value", incepand cu indexul "index", pana se ajunge la index+size;
*/
void fill(unsigned char *v, int n, int i, int s, int val){
	int j=0;
	int aux=*(int *)(v+i+s);
	for(j=0;j<s;j++){
		*(int *)(v+i+j)=val;
	}
	*(int *)(v+i+s)=aux;
}

/*
# Functia pentru comanda FREE: va sterge continutul din v[i], v[i+1*sizeof(int)] si v[i+2*sizeof(int)]
#
# functioneaza dupa principiul de stergere al unui nod dintr-o lista dublu inlantuita;
*/
void fri(unsigned char *v, int n, int i){
	int urm, ant;
	urm=*(int *)(v+i-12);
	ant=*(int *)(v+i-8);
	*(int *)(v+ant)=urm;
	*(int *)(v+urm+4)=ant;
}
/*
# Functia pentru comanda SHOW FREE
#
# verifica orice bloc liber (se considera zona ocupata atat zonele de gestiune, precum si zonele de date utilizator);
# in cazul in care se gaseste bloc liber, o variabila k se incrementeaza, iar la o alta, numita sum, se aduna numarul de biti din acel bloc;
*/
void show_free(unsigned char *v, int n){
	int i=0;
	int aux_urm, aux_ant;
	int x;
	int k=0;
	int sum=0;

	if(*(int *)(v+i)!=0){
		x=*(int *)(v+i)-4;
		if(x>0){
			k++;
			sum+=x;
		}
		i=*(int *)(v+i);
	
		while(*(int *)(v+i)!=0 && i<n){
			aux_urm=*(int *)(v+i);
			aux_ant=i+*(int *)(v+i+8);
			x=aux_urm-aux_ant;
			if(x>0){
				k++;
				sum+=x;
				i=*(int *)(v+i);
			}
			else{
				i=*(int *)(v+i);
			}
		}
		
		x=n-i-(*(int *)(v+i+8));

		if(x>0){
			k++;
			sum+=x;
		}

		printf("%d blocks (%d bytes) free\n", k, sum);
	}
	else{
		printf("1 blocks (%d bytes) free\n", n-4);
	}
}

/*
# Functia pentru comanda SHOW USAGE
#
# se verifica toate spatiile ocupate de utilizator! (deci spatiul ocupat de indexul de start nu intra in calcul);
# se foloseste algoritmul de la functia show_free pentru a putea afla eficienta si fragmentarea;
*/
void show_usage(unsigned char *v, int n){
	int i=0;
	int j=0;
	int sum_ocupat=0;
	int ef,fr;
	
	if(*(int *)(v+i)!=0){
		i=*(int *)(v+i);
	}

	while(*(int *)(v+i)!=0 && i<n){
		j++;
		sum_ocupat+=*(int *)(v+i+8)-12;
		i=*(int *)(v+i);
	}
	j++;
	sum_ocupat+=*(int *)(v+i+8)-12;

	printf("%d blocks (%d bytes) used\n", j, sum_ocupat);			

// NEPRACTIC; am copiat codul din show_free pentru a afla numarul de blocuri libere si de octeti liberi;	
	i=0;
	int aux_urm, aux_ant, aux;
	int k=0, sum_liber=0;
	
	if(*(int *)(v+i)!=0){
		aux=*(int *)(v+i)-4;
		if(aux>0){
			k++;
			sum_liber+=aux;
		}
		i=*(int *)(v+i);
	
		while(*(int *)(v+i)!=0 && i<n){
			aux_urm=*(int *)(v+i);
			aux_ant=i+*(int *)(v+i+8);
			aux=aux_urm-aux_ant;
			if(aux>0){
				k++;
				sum_liber+=aux;
				i=*(int *)(v+i);
			}
			else{
				i=*(int *)(v+i);
			}
		}

		aux=n-i-(*(int *)(v+i+8));

		if(aux>0){
			k++;
			sum_liber+=aux;
		}
	}

	ef=(int)(100.0*((float)sum_ocupat/(100-sum_liber)));
	fr=(int)(100.0*(((float)k-1)/j));
	
	printf("%d%% efficiency\n",ef);
	printf("%d%% fragmentation\n",fr);
}

/*
# Functia pentru SHOW ALLOCATIONS
#
# blocul pentru indexul de start se considera ocupat cu 4 biti;
# se verifica daca intre indexul de start si blocul urmator(daca exista), este un bloc liber;
# se continua pana cand se ajunge va v[i]=0 (blocul la care am ajuns in momentul acesta nu are urmator), caz in care se afiseaza diferenta ramasa in biti pana la finalul arenei (daca aceasta este diferita de 0).
*/
void show_alloc(unsigned char *v, int n){
	int i=0;
	int aux, aux_urm, aux_ant;
	
	if(*(int *)(v+i)!=0){
		printf("OCCUPIED 4 bytes\n");
		if((*(int *)(v+i)-4)>0){
			printf("FREE %d bytes\n", *(int *)(v+i)-4);
		}
		i=*(int *)(v+i);
	
		while(*(int *)(v+i)!=0){
			aux_urm=*(int *)(v+i);
			aux_ant=i+*(int *)(v+i+8);
			aux=aux_urm-aux_ant;	
				
			printf("OCCUPIED %d bytes\n", *(int *)(v+i+8));
			if(aux>0){
				printf("FREE %d bytes\n", aux);
			}
	
			i=*(int *)(v+i);
		}	
	
		printf("OCCUPIED %d bytes\n", *(int *)(v+i+8));
		aux=n-i-(*(int *)(v+i+8));

		if(aux>0){
			printf("FREE %d bytes\n", aux);
		}
	}
	else{
		printf("OCCUPIED 4 bytes\n");
		printf("FREE %d bytes\n", n-4);
	}
}

int main(){

unsigned char *v; // vectorul "mare" cu rol de intreaga memorie
char *s; // sir de caractere unde se vor retine, pe rand, comenzile
int n; // numarul de elemente din vector

char *p;
int *r;
int n1,n2,n3,i;
int aux;

	s=(char *)malloc(20);
	fgets(s,20,stdin);
	p=strtok(s," \n");
	
	if(strcmp(p,"INITIALIZE")==0){
		p=strtok(NULL," ");
		n=atoi(p);
		v=(unsigned char *)malloc(n*sizeof(unsigned char));
		for(i=0;i<n;i++){
			r=(int *)(v+i);
			*r=0;
		}
	}

	free(s);
	s=(char *)malloc(20);
	fgets(s,20,stdin);
	p=strtok(s," \n");

	while(strcmp(p,"FINALIZE")!=0){
		if(strcmp(p,"DUMP")==0){
			dump(v,n);
		}
		if(strcmp(p,"ALLOC")==0){
			p=strtok(p+strlen(p)+1," \n");
			n1=atoi(p);
			aux=alloc(v,n,n1);
			printf("%d\n", aux);
		}
		if(strcmp(p,"FREE")==0){
			p=strtok(NULL," \n");
			n1=atoi(p);
			fri(v,n,n1);
		}
		if(strcmp(p,"FILL")==0){
			p=strtok(NULL," \n");
			n1=atoi(p);
			p=strtok(NULL," \n");
			n2=atoi(p);
			p=strtok(NULL," \n");
			n3=atoi(p);
			fill(v, n, n1, n2, n3);
		}
		if(strcmp(p,"SHOW")==0){
			p=strtok(NULL," \n");
				if(strcmp(p,"FREE")==0){
				show_free(v,n);
			}
			if(strcmp(p,"USAGE")==0){
			show_usage(v,n);
			}
			if(strcmp(p,"ALLOCATIONS")==0){
			show_alloc(v,n);
			}
		}
		free(s);
		s=(char *)malloc(20);
		fgets(s,20,stdin);
		p=strtok(s," \n");
	}

free(v);
free(s);

return 0;
}
