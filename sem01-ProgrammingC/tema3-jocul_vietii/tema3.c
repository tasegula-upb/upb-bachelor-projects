#include<stdio.h>
#include<stdlib.h>
//#include"biti.h"

//
// pentru bitul 0
//
int bit0(unsigned char **m, int i, int j, int n){
	int ok=0, k=0;
	if((*(*(m+i)+j)&1)==1) ok=1;

	if(i-1>=0){
		if((*(*(m+i-1)+j)&1)==1) k++;
		if((*(*(m+i-1)+j)&2)==2) k++;
		if(j-1>=0){
			if((*(*(m+i-1)+j-1)&128)==128) k++;
		}
	}
	if(j-1>=0){
		if((*(*(m+i)+j-1)&128)==128) k++;
	}
	if((*(*(m+i)+j)&2)==2) k++;
	if(i+1<8*n){
		if((*(*(m+i+1)+j)&1)==1) k++;
		if((*(*(m+i+1)+j)&2)==2) k++;
		if(j-1>=0){
			if((*(*(m+i+1)+j-1)&128)==128) k++;
		}
	}

	if(ok==1){
		if(k!=2 && k!=3) ok=0;
	}
	else
		if(k==3) ok=1;

return ok;
}

//
// pentru bitul 1
//
int bit1(unsigned char **m, int i, int j, int n){
	int ok=0, k=0;
	if((*(*(m+i)+j)&2)==2) ok=1;

	if(i-1>=0){
		if((*(*(m+i-1)+j)&1)==1) k++;
		if((*(*(m+i-1)+j)&2)==2) k++;
		if((*(*(m+i-1)+j)&4)==4) k++;
	}
	if((*(*(m+i)+j)&1)==1) k++;
	if((*(*(m+i)+j)&4)==4) k++;
	if(i+1<8*n){
		if((*(*(m+i+1)+j)&1)==1) k++;
		if((*(*(m+i+1)+j)&2)==2) k++;
		if((*(*(m+i+1)+j)&4)==4) k++;
	}

	if(ok==1){
		if(k!=2 && k!=3) ok=0;
	}
	else
		if(k==3) ok=1;

return ok;
}

//
// pentru bitul 2
//
int bit2(unsigned char **m, int i, int j, int n){
	int ok=0, k=0;
	if((*(*(m+i)+j)&4)==4) ok=1;

	if(i-1>=0){
		if((*(*(m+i-1)+j)&2)==2) k++;
		if((*(*(m+i-1)+j)&4)==4) k++;
		if((*(*(m+i-1)+j)&8)==8) k++;
	}
	if((*(*(m+i)+j)&2)==2) k++;
	if((*(*(m+i)+j)&8)==8) k++;
	if(i+1<8*n){
		if((*(*(m+i+1)+j)&2)==2) k++;
		if((*(*(m+i+1)+j)&4)==4) k++;
		if((*(*(m+i+1)+j)&8)==8) k++;
	}

	if(ok==1){
		if(k!=2 && k!=3) ok=0;
	}
	else
		if(k==3) ok=1;

return ok;
}

//
// pentru bitul 3
//
int bit3(unsigned char **m, int i, int j, int n){
	int ok=0, k=0;
	if((*(*(m+i)+j)&8)==8) ok=1;

	if(i-1>=0){
		if((*(*(m+i-1)+j)&4)==4) k++;
		if((*(*(m+i-1)+j)&8)==8) k++;
		if((*(*(m+i-1)+j)&16)==16) k++;
	}
	if((*(*(m+i)+j)&4)==4) k++;
	if((*(*(m+i)+j)&16)==16) k++;
	if(i+1<8*n){
		if((*(*(m+i+1)+j)&4)==4) k++;
		if((*(*(m+i+1)+j)&8)==8) k++;
		if((*(*(m+i+1)+j)&16)==16) k++;
	}

	if(ok==1){
		if(k!=2 && k!=3) ok=0;
	}
	else
		if(k==3) ok=1;

return ok;
}

//
// pentru bitul 4
//
int bit4(unsigned char **m, int i, int j, int n){
	int ok=0, k=0;
	if((*(*(m+i)+j)&16)==16) ok=1;

	if(i-1>=0){
		if((*(*(m+i-1)+j)&8)==8) k++;
		if((*(*(m+i-1)+j)&16)==16) k++;
		if((*(*(m+i-1)+j)&32)==32) k++;
	}
	if((*(*(m+i)+j)&8)==8) k++;
	if((*(*(m+i)+j)&32)==32) k++;
	if(i+1<8*n){
		if((*(*(m+i+1)+j)&8)==8) k++;
		if((*(*(m+i+1)+j)&16)==16) k++;
		if((*(*(m+i+1)+j)&32)==32) k++;
	}

	if(ok==1){
		if(k!=2 && k!=3) ok=0;
	}
	else
		if(k==3) ok=1;

return ok;
}

//
// pentru bitul 5
//
int bit5(unsigned char **m, int i, int j, int n){
	int ok=0, k=0;
	if((*(*(m+i)+j)&32)==32) ok=1;

	if(i-1>=0){
		if((*(*(m+i-1)+j)&16)==16) k++;
		if((*(*(m+i-1)+j)&32)==32) k++;
		if((*(*(m+i-1)+j)&64)==64) k++;
	}
	if((*(*(m+i)+j)&16)==16) k++;
	if((*(*(m+i)+j)&64)==64) k++;
	if(i+1<8*n){
		if((*(*(m+i+1)+j)&16)==16) k++;
		if((*(*(m+i+1)+j)&32)==32) k++;
		if((*(*(m+i+1)+j)&64)==64) k++;
	}

	if(ok==1){
		if(k!=2 && k!=3) ok=0;
	}
	else
		if(k==3) ok=1;

return ok;
}

//
// pentru bitul 6
//
int bit6(unsigned char **m, int i, int j, int n){
	int ok=0, k=0;
	if((*(*(m+i)+j)&64)==64) ok=1;

	if(i-1>=0){
		if((*(*(m+i-1)+j)&32)==32) k++;
		if((*(*(m+i-1)+j)&64)==64) k++;
		if((*(*(m+i-1)+j)&128)==128) k++;
	}
	if((*(*(m+i)+j)&32)==32) k++;
	if((*(*(m+i)+j)&128)==128) k++;
	if(i+1<8*n){
		if((*(*(m+i+1)+j)&32)==32) k++;
		if((*(*(m+i+1)+j)&64)==64) k++;
		if((*(*(m+i+1)+j)&128)==128) k++;
	}

	if(ok==1){
		if(k!=2 && k!=3) ok=0;
	}
	else
		if(k==3) ok=1;

return ok;
}

//
// pentru bitul 7
//
int bit7(unsigned char **m, int i, int j, int n){
	int ok=0, k=0;
	if((*(*(m+i)+j)&128)==128) ok=1;

	if(i-1>=0){
		if((*(*(m+i-1)+j)&64)==64) k++;
		if((*(*(m+i-1)+j)&128)==128) k++;
		if(j+1<n){
			if((*(*(m+i-1)+j+1)&1)==1) k++;
		}
	}
	if(j+1<n){
		if((*(*(m+i)+j+1)&1)==1) k++;
	}
	if((*(*(m+i)+j)&64)==64) k++;
	if(i+1<8*n){
		if((*(*(m+i+1)+j)&64)==64) k++;
		if((*(*(m+i+1)+j)&128)==128) k++;
		if(j+1<n){
			if((*(*(m+i+1)+j+1)&1)==1) k++;
		}
	}

	if(ok==1){
		if(k!=2 && k!=3) ok=0;
	}
	else
		if(k==3) ok=1;

return ok;
}

int main(){
	int n,g; // numarul de coloane si de generatii;
	unsigned char **m, **m1;
	int i,j,k; // contoare pentru matrici;

	scanf("%d", &n);

	m=(unsigned char **)malloc((8*n)*sizeof(unsigned char *));
	for(i=0;i<8*n;i++)
		m[i]=(unsigned char *)calloc(n,sizeof(unsigned char));

	m1=(unsigned char **)malloc((8*n)*sizeof(unsigned char *));
	for(i=0;i<8*n;i++)
		m1[i]=(unsigned char *)calloc(n,sizeof(unsigned char));


	for(i=0;i<8*n;i++){
		for(j=0;j<n;j++){
			scanf("%d", (int *)(&*(*(m+i)+j)));
		}
	}

	scanf("%d", &g);

	for(k=1;k<=g;k++){
		for(i=0;i<8*n;i++){
			for(j=0;j<n;j++){
				if(bit0(m,i,j,n)==1) m1[i][j]+=1;
				if(bit1(m,i,j,n)==1) m1[i][j]+=2;
				if(bit2(m,i,j,n)==1) m1[i][j]+=4;
				if(bit3(m,i,j,n)==1) m1[i][j]+=8;
				if(bit4(m,i,j,n)==1) m1[i][j]+=16;
				if(bit5(m,i,j,n)==1) m1[i][j]+=32;
				if(bit6(m,i,j,n)==1) m1[i][j]+=64;
				if(bit7(m,i,j,n)==1) m1[i][j]+=128;
			}
		}
		for(i=0;i<8*n;i++){
			for(j=0;j<n;j++){
				*(*(m+i)+j)=*(*(m1+i)+j);
				*(*(m1+i)+j)=0;
			}
		}
	}
		
	for(i=0;i<8*n;i++){
		for(j=0;j<n;j++){
			printf("%d ", m[i][j]);
		}
		printf("\n");
	}

free(m);
free(m1);	
return 0;
}	
