#include<stdio.h>

int lucas(long x){
int a=2,b=1,k=0;
if(x==a || x==b){
	printf("Numarul %ld face parte din seria Lucas\n", x);
	k=1;}
else{
	while(x>a){
		a=a+b;
		b=b+a;
		if(x==a || x==b){
			printf("Numarul %ld face parte din seria Lucas\n", x);
			k=1;}
		}
	}
if(k==0){ 
	printf("Numarul %ld nu face parte din seria Lucas\n", x);}
return 0;
}

int inv(long x){
int a=0;
while(x){
	a=a*10+x%10;
	x=x/10;
	}
return a;
}

int sump(long x){
int s=0,a=0;
long v=x;
while(v!=0){
	a=v%10;
	switch(a){
		case 2: s=s+4; break;
		case 3: s=s+9; break;
		case 5: s=s+25; break;
		case 7: s=s+49; break;
		default: s+=0; break;
		}
	v=v/10;
	}
printf("%ld | %d\n",x,s);
return 0;
}

int binar(long x){
short i=0,j=0;
int a=0;
long v=x;
while(v){
	if(v%2==0) i++;
	if(v%2==1) j++;
	v/=2;
	}
if(i==j){
	printf("%ld\n",x);
	a=1;
	}
return a;
}

int main(){

unsigned long n;
long i;
int k=0;

scanf("%ld" , &n);

//Cerinta 1: Seria Lucas
lucas(n);

//Cerinta 2: Suma patratelor cifrelor prime din palindroame
for(i=1;i<n;i++){
	if(i==inv(i)) sump(i);
	}

//Cerinta 3: Numere care in binar au numar egal de 0 si 1
for(i=1;i<=n;i++){
	if(binar(i)==1) k++;
	}
printf("%d\n",k);

return 0;
}
