Functia "main":
# se declara urmatoarele variabile:
### n (tip unsigned long, ales deoarece numarul trebuie sa fie pozitiv si mai mare decat 65000 cat poate lua o valoare de tip unsigned int); aici se va retine valoarea citita de la tastatura;
### i (tip long, variabila care se va folosi in FOR pentru cerintele 2 si 3 unde e nevoie de a se trece prin toate numerele de la 1 la n);
### k (tip int, initial avand valoarea 0; aici se vor retine numarul de numere care in binar au acelasi numar de 0 si 1);

# se citeste n de la tastatura;

# se apeleaza functia "lucas" pentru numarul citit n;

Functia LUCAS: parametri: x de tip long.
- se verifica daca numarul apartine seriei Lucas; se declara doua variabile a si b initializate cu primele doua numere din serie: a=2, b=1; in cele doua variabile se va genera seria (cu b consecutiv lui a) pana cand x va fi egal cu unul dintre numere sau pana cand a>x (cazul de egalitate va fi discutat in interiorul lui WHILE, in IF, rezultand in afisarea mesajului "Numarul face parte din seria Lucas");
- se declara si o variabila k=0 (presupunem ca numarul nu apartine seriei Lucas); daca numarul apartine seriei Lucas, k=1; astfel, la sfarsitul functiei se verifica daca valoarea lui k ramane 0, caz in care se afiseaza mesajul "Numarul nu face parte din seria Lucas";

# se apeleaza functia sump(i) pentru fiecare numar de la 1 la n-1 care este egal cu inversul sau (pentru fiecare palindrom);

Functia INV: parametri: x de tip long.
- se declara o variabila a, initializata cu 0; in a se va calcula inversul numarului dat ca parametru dupa urmatorul algoritm:
--- cat timp x va fi mai mare decat 0: a se va inmulti cu 10; la numarul astfel rezultat se va adauga restul impartirii lui x la 10; la sfarsit, x se va imparti la 10 (fiind de tip int, se va face convertirea implicita, astfel ca in x se va retine doar catul impartirii lui x la 10);

Functia SUMP: parametri: x de tip long.
- se declara o variabila s, initializata cu 0; in s se va calcula suma cifrelor prime a numarului palimdrom;
- se declara o variabila a, initializata cu 0; in a se va retine restul impartirii numarului la 10; de fapt, in se va retine o cifra (ultima) a nuamrului.
- o variabila v de tip long, care se initializeaza cu variabila data ca parametru;
- cat timp v mai mare decat 0 (sau diferit de 0, avand in vedere ca v este mereu pozitiv) a retine ultima cifra. Se apeleaza la SWITCH(a) pentru cazurile posibile de cifre prime si anume 2,3,5,7; cand a este egal cu unul dintre ele, la suma s se aduna 4,9,25,49 (patratul cifrei prime); la sfarsitul lui WHILE, v devine catul impartirii lui la 10 (fiind de tip integer, se face convertirea implicita);
- la sfarsitul functiei se afiseaza numarul initial si suma cifrelor prime; daca numarul nu are cifre prime, se afiseaza 0, deoarece suma s a fost initializata cu 0;

# se apeleaza functia binar(i) pentru fiecare numar i de la 1 la n; cum functia binar returneaza 0 (daca numarul are numar diferit de 0 si 1 in binar) sau 1 (daca numarul are acelasi numar de 0 si 1 in binar), se verifica daca binar(i)=1, caz in care k (variabila declarata la inceput in main) se incrementeaza; astfel, in k se retine numarul de numere care respecta functia "binar";

Functia BINAR: parametri: x de tip long.
- o variabila i, initializata cu 0, in care se va retine numarul de cifre de 0 din numarul in binar;
- o variabila j, initializata cu 0, in care se va retine numarul de cifre de 1 din numarul in binar;
- o variabila a, initializata cu 0 (se presupune ca numarul nu respecta cerinta: numarul de cifre de 0 si 1 in binar sa fie egal); a=1 daca se respecta cerinta data.
- o variabila v, care se initializeaza cu valoarea din parametru;
- cat timp v este diferit de 0 (de fapt mai mare ca 0, deoarece el este intotdeauna pozitiv):
--- se verifica daca restul impartirii numarului v la 2 este egal cu 0, caz in care i se incrementeaza;
--- se verifica daca restul impartirii numarului v la 2 este egal cu 1, caz in care j se incrementeaza;
- v isi schimba valoarea in catul impartirii lui la 2 (fiind de timp integer, se apeleaza la conversia implicita);
- daca i=j (numarul de cifre de 0 este egal cu numarul de cifre de 1) se afiseaza numarul, iar a ia valoarea 1;
- functia returneaza valoarea lui a (0 sau 1);
- OBS: functia nu afla valoarea numarului in binar niciodata, doar trece prin toate cifrele numarului folosind algoritmul de transformare a unui numar zecimal in binar;

# se afiseaza numarul de numere care in bianr au acelasi numar de cifre de 0 si 1.
