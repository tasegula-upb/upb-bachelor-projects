#include<stdio.h>
#include"automat.h"

int main(){

	//
	//Se configureaza automatul;
	//

	//Se introduc bancnotele existente in automat;
	int nr_banc=0;
	printf("Introduceti numarul de bancnote de %d :", _50_DOLLARS);
	scanf("%d", &nr_banc);
	set_available_bills(_50_DOLLARS, nr_banc);
	printf("Introduceti numarul de bancnote de %d :", _10_DOLLARS);
	scanf("%d", &nr_banc);
	set_available_bills(_10_DOLLARS, nr_banc);
	printf("Introduceti numarul de bancnote de %d :", _5_DOLLARS);
	scanf("%d", &nr_banc);
	set_available_bills(_5_DOLLARS, nr_banc);
	printf("Introduceti numarul de bancnote de %d :", _1_DOLLAR);
	scanf("%d", &nr_banc);
	set_available_bills(_1_DOLLAR, nr_banc);
	
	printf("\n");

	//Se introduc produsele;
	int no_prod, cod, pret, cant, var_prod=1; 
	printf("Configurati numarul de produse disponibile in automat:");
	scanf("%d", &no_prod);
	set_number_of_products(no_prod);

	printf("\n");
	
	//Se introduc datele despre produse;
	int i;
	printf("Introduceti datele despre produse: \n");
		for(i=0;i<get_number_of_products();i++){
			printf("Produsul %d \n", i);
			printf("PRET: ");
			scanf("%d", &pret);
			printf("CANTITATE: ");
			scanf("%d", &cant);
			set_product_cost(cod, pret);
			set_product_cost(cod, cant);
		
	}

	//
	// Se configureaza alegerile cumparatorului
	//
	
	int b, bani; //b=bancnota introdusa; bani=creditul pe care il are utilizatorul;
	int p_code, p_cost, p_cant; //informatiile despre produsul dorit;
	char opt; //retine optiunea pentru continuare in caz de erori;
	printf("Bancnote acceptate: %d, %d, %d, %d \n", _1_DOLLAR, _5_DOLLARS, _10_DOLLARS, _50_DOLLARS);
	printf("\n");
	printf("Introduceti bancnotele (pentru oprire, apasati 0) \n");
	scanf("%d", &b);
	while(b!=0){	
		if(insert_bill(b)==-1){
			printf("Bancnota nu poate fi acceptata. Introduceti una din bancnotele de mai sus! \n");
			scanf("%d", &b);
		}
		else{
			bani=insert_bill(b);
			printf("Credit: %d", bani);
			printf("Introduceti bancnota (pentru oprire, apasati 0) \n");
			scanf("%d", &b);
		}
	}
	printf("Introduceti codul produsului \n");
	scanf("%d", &p_code);
	
/*se verifica in mod invers erorile posibile (-4,-3,-2,-1)
 -4: Informeaza utilizatorul ca nu poate returna rest si 
 intreaba daca doreste sa continue prin apelarea force_request_product;
 -3: Informeaza utilizatorul ca nu are credit suficient pentru produsul selectat si 
 intreaba daca doreste sa continue prin introducerea mai multor bancnote pana se ajunge la suma necesara;
 -2: Informeaza utilizatorul ca nu exista cantitate din produsul selectat si 
 intreaba daca doreste sa continue prin selectarea unui nou produs;
 -1: Informeaza utilizatorul ca nu exista produsul dorit si
 intreaba daca doreste sa continue prin selectarea unui nou produs;
*/

	while(request_product(p_code)==-4){
		while(request_product(p_code)==-3){
			while(request_product(p_code)==-2){
				while(request_product(p_code)==-1){
					printf("Cod invalid! Continuati? Y/N \n");
					scanf("%c", &opt);
					switch(opt){
						case 'Y': 
							printf("Introduceti codul produsului \n");
							scanf("%d", &p_code);
							break;
						case 'N': request_change(); break;
						default: break;
					}
				}
				printf("Cantitate zero! Continuati? Y/N \n");
				scanf("%c", &opt);
				switch(opt){
					case 'Y': 
						printf("Introduceti codul produsului \n");
						scanf("%d", &p_code);
						break;
					case 'N': request_change(); break;
					default: break;
				}
			}
			printf("Credit insuficient!\n");
			printf("Cost produs: %d", get_product_cost(p_code));
			printf("Credit: %d", bani);
			printf("Continuati? Y/N \n");
			scanf("%c", &opt);
			switch(opt){
				case 'Y':
					while(bani<get_product_cost(p_code)){
						printf("Introduceti bancnote \n");
						scanf("%d", &b);
						insert_bill(b);
					}
					break;
				case 'N': request_change(); break;
				default: break;
			}
		}
		printf("Nu se poate returna rest! Continuati? Y/N \n");
		scanf("%c", &opt);
		switch(opt){
			case 'Y': 
				force_request_product(p_code);
				break;
			case 'N': request_change(); break;
			default: break;
		}
	}	

	return 0;
}
