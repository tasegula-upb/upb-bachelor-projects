cg=user_money - prod[code].cost;
				if(cg/_50_DOLLARS==0 &&	cg/_10_DOLLARS==0 && cg/_5_DOLLARS==0 && cg/_1_DOLLAR==0) return -4;
				else{
					/*dupa parcurgerea tuturor celor 4 if-uri, se va returna restul pe care nu il pot da;
					* ideea este urmatoarea:
					* - in x se retine catul impartirii restului la o anumita bancnota,
					* rezultand astfel numarul de bancnote necesare pentru a plati o parte din rest;
					* bancnotele se iau descrescator, astfel incat restul e platit in bancnote maxime.
					* - apoi se verifica daca se poate returna numarul de bancnote de care avem nevoie
					* pentru a plati acea parte din rest;
					* - daca nu avem numarul necesar de bancnote cerute, se returneaza numarul total
					* de bancnote cerute, existente in automat;
					* - din restul pe care trebuie sa il dam, scadem valoarea bancnotelor date;
					* - se repeta pasii pentru fiecare bancnota existenta, in mod descrescator;
					*
					* aici cg isi va schimba valoarea, ajungand la final sa reprezinte numarul pe care
					* nu o putem inapoia ca rest;
					*
					*/
					x=cg/_50_DOLLARS;
					if(x!=0){
						if(x <= get_available_bills(_50_DOLLARS)){
							cg_pos+=x*_50_DOLLARS;
							cg-=cg_pos;
						}
						else{
							cg_pos+=_50_DOLLARS*get_available_bills(_50_DOLLARS);
							cg-=cg_pos;
						}
					}
					x=cg/_10_DOLLARS;
					if(x!=0){
						if(x <= get_available_bills(_10_DOLLARS)){
							cg_pos+=x*_10_DOLLARS;
							cg-=cg_pos;
						}
						else{
							cg_pos+=_10_DOLLARS*get_available_bills(_10_DOLLARS);
							cg-=cg_pos;
						}
					}
					x=cg/_5_DOLLARS;
					if(x!=0){
						if(x <= get_available_bills(_5_DOLLARS)){
							cg_pos+=x*_5_DOLLARS;
							cg-=cg_pos;
						}
						else{
							cg_pos+=_5_DOLLARS*get_available_bills(_5_DOLLARS);
							cg-=cg_pos;
						}
					}
					x=cg/_1_DOLLAR;
					if(x!=0){
						if(x <= get_available_bills(_1_DOLLAR)){
							cg_pos+=x*_1_DOLLAR;
							cg-=cg_pos;
						}
						else{
							cg_pos+=_1_DOLLAR*get_available_bills(_1_DOLLAR);
							cg-=cg_pos;
						}
					}
					if(cg==0){
						product=code;
						return user_money - prod[code].cost;
					}
				
