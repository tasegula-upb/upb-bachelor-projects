#include<stdio.h>
#include"automat.h"

//definim o structura pentru produse, ce va contine codul si costul produsului;
struct products{
	int id; //codul produsului;
	int cant; //numarul de bucati din produs;
	int cost; //pretul produsului;
};

int b1, b5, b10, b50; //bi=numarul de bancnote disponibile de _i_DOLLARS;
struct products prod[100]; //vector de structura products pentru detaliile despre produse;
int nr_prod; //numarul total de produse (diferite);
int user_money; //valoarea totala a bancnotelor introduse de utilizator;
int prod_code=-1; //se va retine codul produsului selectat;
int i; //

void set_available_bills(bill_type bill, int count){
	if(count>=0){
		if(bill==_1_DOLLAR) b1=count;
		else
			if(bill==_5_DOLLARS) b5=count;
			else
				if(bill==_10_DOLLARS) b10=count;
				else
					if(bill==_50_DOLLARS) b50=count;
	}
}

int get_available_bills(bill_type bill){
	switch (bill){
		case _1_DOLLAR: return b1; break;
		case _5_DOLLARS: return b5; break;
		case _10_DOLLARS: return b10; break;
		case _50_DOLLARS: return b50; break;
		default: return 0;
	}
}

void set_number_of_products(int count){
	if(count<=100) nr_prod=count;
}

int get_number_of_products(){
	return nr_prod;
}

void set_product_cost(int code, int cost){
	prod[code].cost=cost;
	prod[code].id=code;
}

int get_product_cost(int code){
	return prod[code].cost;
}

void set_product_quantity(int code, int quantity){
	prod[code].cant=quantity;
}

int get_product_quantity(int code){
//	printf("cant %d \n", prod[code].cant);	
	return prod[code].cant;
}

int request_product(int code){
	// prescurtare de la change; se retine valoarea restului ce trebuie returnat;
	int cg;
	
	if(code>=get_number_of_products(code)) {return -1;}
	else{
		if(get_product_quantity(code)==0) {return -2;}
		else{
			if(prod[code].cost > user_money) {return -3;}
			else{
				if(user_money-prod[code].cost!=0){
					cg=user_money-prod[code].cost;
					if(cg/_50_DOLLARS>0 && get_available_bills(_50_DOLLARS)>0){
						if(cg/_50_DOLLARS <= get_available_bills(_50_DOLLARS)){
							cg-=(cg/_50_DOLLARS)*_50_DOLLARS;
						}
						else{ cg-=_50_DOLLARS*get_available_bills(_50_DOLLARS); }
					}
					if(cg/_10_DOLLARS>0 && get_available_bills(_10_DOLLARS)>0){
						if(cg/_10_DOLLARS <= get_available_bills(_10_DOLLARS)){
							cg-=(cg/_10_DOLLARS)*_10_DOLLARS;
						}
						else{ cg-=_10_DOLLARS*get_available_bills(_10_DOLLARS); }
					}
					if(cg/_5_DOLLARS>0 && get_available_bills(_5_DOLLARS)>0){
						if(cg/_5_DOLLARS <= get_available_bills(_5_DOLLARS)){
							cg-=(cg/_5_DOLLARS)*_5_DOLLARS;
						}
						else{ cg-=_5_DOLLARS*get_available_bills(_5_DOLLARS); }
					}
					if(cg/_1_DOLLAR>0 && get_available_bills(_1_DOLLAR)>0){
						if(cg/_1_DOLLAR <= get_available_bills(_1_DOLLAR)){
							cg-=(cg/_1_DOLLAR)*_1_DOLLAR;
						}
						else{ cg-=_1_DOLLAR*get_available_bills(_1_DOLLAR); }
					}
					if(cg!=0) {return -4; }
					else{
						prod_code=code;
						request_change();
						set_product_quantity(code, get_product_quantity(code)-1);
						return user_money-prod[code].cost;
					}
				}
				else{
					prod_code=code;
					request_change();
					set_product_quantity(code, get_product_quantity(code)-1);
					return user_money-prod[code].cost;
				}
			}
		}
	}
}

int request_change(){
	//verific daca s-a ales produs, sau se doreste returnarea banilor;
	//restul e egal cu diferenta dintre banii introdusi de utilizator si costul produsului;
	int rest;	
	if(prod_code==-1){ rest=user_money; }
	else{
		rest=user_money-prod[prod_code].cost;
	}
	
	if(rest/_50_DOLLARS>0 && get_available_bills(_50_DOLLARS)>0){
		if(rest/_50_DOLLARS <= get_available_bills(_50_DOLLARS)){
			set_available_bills(_50_DOLLARS, get_available_bills(_50_DOLLARS)-(rest/_50_DOLLARS));
			rest-=(rest/_50_DOLLARS)*_50_DOLLARS;
		}
		else{
			rest-=_50_DOLLARS*get_available_bills(_50_DOLLARS);
			set_available_bills(_50_DOLLARS, 0);
		}
	}
	if(rest/_10_DOLLARS>0 && get_available_bills(_10_DOLLARS)>0){
		if(rest/_10_DOLLARS <= get_available_bills(_10_DOLLARS)){
			set_available_bills(_10_DOLLARS, get_available_bills(_10_DOLLARS)-(rest/_10_DOLLARS));
			rest-=(rest/_10_DOLLARS)*_10_DOLLARS;
		}
		else{
			rest-=_10_DOLLARS*get_available_bills(_10_DOLLARS);
			set_available_bills(_10_DOLLARS, 0);
		}
	}
	if(rest/_5_DOLLARS>0 && get_available_bills(_5_DOLLARS)>0){
		if(rest/_5_DOLLARS <= get_available_bills(_5_DOLLARS)){
			set_available_bills(_5_DOLLARS, get_available_bills(_5_DOLLARS)-(rest/_5_DOLLARS));
			rest-=(rest/_5_DOLLARS)*_5_DOLLARS;
		}
		else{
			rest-=_5_DOLLARS*get_available_bills(_5_DOLLARS);
			set_available_bills(_5_DOLLARS, 0);
		}
	}
	if(rest/_1_DOLLAR>0 && get_available_bills(_1_DOLLAR)>0){
		if(rest/_1_DOLLAR <= get_available_bills(_1_DOLLAR)){
			set_available_bills(_1_DOLLAR, get_available_bills(_1_DOLLAR)-(rest/_1_DOLLAR));
			rest-=(rest/_1_DOLLAR)*_1_DOLLAR;
		}
		else{
			rest-=_1_DOLLAR*get_available_bills(_1_DOLLAR);
			set_available_bills(_1_DOLLAR, 0);
		}
	}
	if(rest==0) return user_money-prod[prod_code].cost;
	else return -1;
}

int force_request_product(int code){
	if(code>=get_number_of_products(code)) {return -1;}
	else{
		if(get_product_quantity(code)==0) {return -2;}
		else{
			if(prod[code].cost > user_money) {return -3;}
			else{
				prod_code=code;
				set_product_quantity(code, get_product_quantity(code)-1);
				return 0;
			}	
		}
	}
}

int insert_bill(bill_type bill){
	switch (bill){
		case _1_DOLLAR: 
			user_money+=_1_DOLLAR; 
			set_available_bills(_1_DOLLAR, get_available_bills(_1_DOLLAR)+1); 
			return user_money; break;
		case _5_DOLLARS: 
			user_money+=_5_DOLLARS; 
			set_available_bills(_5_DOLLARS, get_available_bills(_5_DOLLARS)+1); 
			return user_money; break;
		case _10_DOLLARS: 
			user_money+=_10_DOLLARS; 
			set_available_bills(_10_DOLLARS, get_available_bills(_10_DOLLARS)+1); 
			return user_money; break;
		case _50_DOLLARS: 
			user_money+=_50_DOLLARS; 
			set_available_bills(_50_DOLLARS, get_available_bills(_50_DOLLARS)+1); 
			return user_money; break;
		default: return -1;
	}
}

void reset(){
    int i;
	for(i=0;i<get_number_of_products();i++){
		prod[i].id=0;
		prod[i].cost=0;
		prod[i].cant=0;
	}
	set_available_bills(_1_DOLLAR, 0);;
	set_available_bills(_5_DOLLARS, 0);
	set_available_bills(_10_DOLLARS, 0);
	set_available_bills(_50_DOLLARS, 0);
	set_number_of_products(0);
	user_money=0;
	prod_code=0;
}

