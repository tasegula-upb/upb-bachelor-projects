#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "t5.h"

#define NUM_INITIAL_MATRICES 60
#define NUM_EXTRA_SUBMATRICES 1000
#define NUM_EXTENSIONS 180
#define NUM_CHECKS 1000
#define NUM_FINAL_CHECKS 500
#define VMAX 10000

int tmp_buffer[100000];

int CreateMatrixWithSpecificSizes(int nrows, int ncols, int x) {
	int mid = CreateMatrix();

	int i = 0, j = 0, k;
	unsigned int rv = 0xdeadbeef ^ (x * x + x * nrows + ncols);
	char op = x % 2;

	while (i < nrows || j < ncols) {
		rv = (rv >> 1) + (1 << 30) * (rv % 2);
		op = rv % 2;

		if (op == 0) {
			if (i == nrows)
				continue;
			for (k = 0; k < j; k++)
				tmp_buffer[k] = (x + (i % VMAX) * (ncols % VMAX) + (j % VMAX) * (k % VMAX)) % VMAX;
			AppendRow(mid, tmp_buffer);
			i++;
		} else {
			if (j == ncols)
				continue;
			for (k = 0; k < i; k++)
				tmp_buffer[k] = (k * x + (i % VMAX) * (j % VMAX)) % VMAX;
			AppendColumn(mid, tmp_buffer);
			j++;
		}
	}

	return mid;
}

void CheckUniqueMatrixId(int* v, int poz) {
	int i;
	for (i = 0; i < poz; i++)
		if (v[i] == v[poz]) {
			printf("[EROARE: Identificatorii a doua matrici diferite sunt identici]\n");
			exit(0);		
		}
}

int ComputeSubMatrixSummary(int** elems, int nrows, int ncols) {
	int summary = 0;
	int i, j;
	for (i = 0; i < nrows; i++)
		for (j = 0; j < ncols; j++)
			summary ^= elems[i][j];
	return summary;
}

#define TIME_LIMIT_1 130

void test1() {
	int tstart = time(NULL);

	int* mid = (int*) malloc(NUM_INITIAL_MATRICES * sizeof(int));

	printf("[Incepe TESTUL 1]\n");
	printf("[Se initializeaza sistemul]\n");
	Init("data");

	printf("[Se creeaza %d matrici initiale de dimensiuni variate]\n", NUM_INITIAL_MATRICES);
	int i;
	for (i = 0; i < NUM_INITIAL_MATRICES; i++) {
		int nrows, ncols;
		if (i % 3 == 0) {
			nrows = ncols = 1024;		
		} else if (i % 3 == 1) {
			nrows = 100000;
			ncols = 12;		
		} else {
			nrows = 12;
			ncols = 100000;
		}

		mid[i] = CreateMatrixWithSpecificSizes(nrows, ncols, i + 1);
		CheckUniqueMatrixId(mid, i);		
		printf("[S-a creat matricea initiala %d]\n", i);
	}

	freopen("mids.txt", "w", stderr);
	for (i = 0; i < NUM_INITIAL_MATRICES; i++)
		fprintf(stderr, "%d\n", mid[i]);

	freopen("ans-test1.txt", "r", stdin);

	printf("[Se verifica dimensiunile matricilor initiale]\n");
	for (i = 0; i < NUM_INITIAL_MATRICES; i++) {
		int nrows, ncols;
		if (i % 3 == 0) {
			nrows = ncols = 1024;		
		} else if (i % 3 == 1) {
			nrows = 100000;
			ncols = 12;		
		} else {
			nrows = 12;
			ncols = 100000;
		}
		
		int real_nrows, real_ncols;
		GetMatrixDimensions(mid[i], &real_nrows, &real_ncols);
		if (real_nrows != nrows || real_ncols != ncols) {
			printf("[EROARE: Cel putin una din matricile initiale nu are dimensiunile asteptate]\n");
			exit(0);
		}
	}

	printf("[Se verifica numarul de elemente din diferite intervale ale matricilor initiale]\n");
	int k;
	for (k = 0; k < NUM_CHECKS; k++) {		
		int m, vmin, vmax;		
		scanf("%d %d %d", &m, &vmin, &vmax);
		int nelems = GetNumberOfElementsBetweenValues(mid[m], vmin, vmax);
		int expected_nelems;
		scanf("%d", &expected_nelems);
		if (nelems != expected_nelems) {
			printf("[EROARE: Numarul de elemente ale unei matrici dintr-un interval dat nu este corect]\n");
			exit(0);
		}
	}

	int*** subm = (int***) malloc(NUM_INITIAL_MATRICES * sizeof(int**));
	int* base_matrix = (int*) malloc(NUM_INITIAL_MATRICES * sizeof(int));
	int* start_row = (int*) malloc(NUM_INITIAL_MATRICES * sizeof(int));
	int* end_row = (int*) malloc(NUM_INITIAL_MATRICES * sizeof(int));
	int* start_col = (int*) malloc(NUM_INITIAL_MATRICES * sizeof(int));
	int* end_col = (int*) malloc(NUM_INITIAL_MATRICES * sizeof(int));
	int* s1 = (int*) malloc(NUM_INITIAL_MATRICES * sizeof(int));
	int* s2 = (int*) malloc(NUM_INITIAL_MATRICES * sizeof(int));

	printf("[Se creeaza %d submatrici initiale de dimensiuni variate]\n", NUM_INITIAL_MATRICES);
	for (i = 0; i < NUM_INITIAL_MATRICES; i++) {
		base_matrix[i] = mid[i];

		if (i % 3 == 0) {
			start_row[i] = 599; end_row[i] = 703;
			start_col[i] = 498; end_col[i] = 601;	
		} else if (i % 3 == 1) {
			start_row[i] = 30123; end_row[i] = 59001;
			start_col[i] = 7; end_col[i] = 9;
		} else {
			start_row[i] = 7; end_row[i] = 9;
			start_col[i] = 30123; end_col[i] = 59001;
		}

		subm[i] = GetSubMatrix1(base_matrix[i], start_row[i], start_col[i], end_row[i], end_col[i]);
		printf("[S-a obtinut submatricea %d]\n", i);

		s1[i] = ComputeSubMatrixSummary(subm[i], end_row[i] - start_row[i] + 1, end_col[i] - start_col[i] + 1);
		int expected_s1;
		scanf("%d", &expected_s1);
		
		if (s1[i] != expected_s1) {
			printf("[EROARE: Elementele submatricii obtinute sunt incorecte]\n");
			exit(0);
		}

		printf("[Se modifica submatricea %d]\n", i);
		int l, c;
		for (l = start_row[i]; l <= end_row[i]; l++)
			for (c = start_col[i]; c <= end_col[i]; c++)
				subm[i][l - start_row[i]][c - start_col[i]] = (subm[i][l - start_row[i]][c - start_col[i]] + ((l * c + i) % 1000)) % 1000;

		s2[i] = ComputeSubMatrixSummary(subm[i], end_row[i] - start_row[i] + 1, end_col[i] - start_col[i] + 1);

		printf("[Se scrie inapoi in fisier submatricea %d]\n", i);
		PutSubMatrix(subm[i]);
	}

	printf("[Se verifica scrierea corecta in fisier a submatricilor initiale]\n");
	for (i = 0; i < NUM_INITIAL_MATRICES; i++) {
		subm[i] = GetSubMatrix1(base_matrix[i], start_row[i], start_col[i], end_row[i], end_col[i]);
		printf("[S-a reobtinut submatricea %d]\n", i);
		int sum = ComputeSubMatrixSummary(subm[i], end_row[i] - start_row[i] + 1, end_col[i] - start_col[i] + 1);
		if (sum != s2[i]) {
			printf("[EROARE: Modificarile din submatricea %d nu au fost scrise corespunzator in fisier]\n", i);
			exit(0);		
		}
	}


	for (k = 0; k < NUM_CHECKS; k++) {
		int m, vmin, vmax;
		scanf("%d %d %d", &m, &vmin, &vmax);
		int nelems = GetNumberOfElementsBetweenValues(mid[m], vmin, vmax);
		int expected_nelems;
		scanf("%d", &expected_nelems);
		if (nelems != expected_nelems) {
			printf("[EROARE: Numarul de elemente ale unei matrici dintr-un interval dat nu este corect dupa scrierea submatricilor]\n");
			exit(0);
		}
	}

	Finish();

	free(mid);
	free(subm);
	free(base_matrix);
	free(start_row);
	free(end_row);
	free(start_col);
	free(end_col);
	free(s1);
	free(s2);

	int duration = time(NULL) - tstart;
	printf("[Durata=%d sec / Limita=%d sec]\n", duration, TIME_LIMIT_1);
	if (duration > TIME_LIMIT_1) {
		printf("[EROARE: Testul 1 a durat prea mult]\n");
		exit(0);
	}

	printf("[OK - TESTUL 1]\n");
}

#define TIME_LIMIT_2 80

void test2() {
	int tstart = time(NULL);

	int* mid = (int*) malloc(NUM_INITIAL_MATRICES * sizeof(int));
	int i;
	freopen("mids.txt", "r", stdin);
	for (i = 0; i < NUM_INITIAL_MATRICES; i++)
		scanf("%d", &mid[i]);

	freopen("ans-test2.txt", "r", stdin);

	printf("[Incepe TESTUL 2]\n");
	printf("[Se initializeaza sistemul]\n");
	Init("data");

	printf("[Se verifica dimensiunile matricilor initiale]\n");
	for (i = 0; i < NUM_INITIAL_MATRICES; i++) {
		int nrows, ncols;
		if (i % 3 == 0) {
			nrows = ncols = 1024;		
		} else if (i % 3 == 1) {
			nrows = 100000;
			ncols = 12;		
		} else {
			nrows = 12;
			ncols = 100000;
		}

		int real_nrows = -1, real_ncols = -1;
		GetMatrixDimensions(mid[i], &real_nrows, &real_ncols);
		if (real_nrows != nrows || real_ncols != ncols) {
			printf("[EROARE: Cel putin una din matricile initiale nu are dimensiunile asteptate]\n");
			exit(0);
		}
	}

	printf("[Se verifica numarul de elemente din diferite intervale ale matricilor initiale]\n");
	int k;
	for (k = 0; k < NUM_CHECKS; k++) {
		int m, vmin, vmax;		
		scanf("%d %d %d", &m, &vmin, &vmax);
		int nelems = GetNumberOfElementsBetweenValues(mid[m], vmin, vmax);
		int expected_nelems;
		scanf("%d", &expected_nelems);
		if (nelems != expected_nelems) {
			printf("[EROARE: Numarul de elemente ale unei matrici dintr-un interval dat nu este corect]\n");
			exit(0);
		}
	}

	int*** subm = (int***) malloc(2 * (NUM_EXTRA_SUBMATRICES + NUM_INITIAL_MATRICES) * sizeof(int**));
	int* base_matrix = (int*) malloc(2 * (NUM_EXTRA_SUBMATRICES + NUM_INITIAL_MATRICES) * sizeof(int));
	int* start_row = (int*) malloc(2 * (NUM_EXTRA_SUBMATRICES + NUM_INITIAL_MATRICES) * sizeof(int));
	int* end_row = (int*) malloc(2 * (NUM_EXTRA_SUBMATRICES + NUM_INITIAL_MATRICES) * sizeof(int));
	int* start_col = (int*) malloc(2 * (NUM_EXTRA_SUBMATRICES + NUM_INITIAL_MATRICES) * sizeof(int));
	int* end_col = (int*) malloc(2 * (NUM_EXTRA_SUBMATRICES + NUM_INITIAL_MATRICES) * sizeof(int));
	int* s1 = (int*) malloc(2 * (NUM_EXTRA_SUBMATRICES + NUM_INITIAL_MATRICES) * sizeof(int));
	int* s2 = (int*) malloc(2 * (NUM_EXTRA_SUBMATRICES + NUM_INITIAL_MATRICES) * sizeof(int));

	printf("[Se verifica cele %d submatrici initiale]\n", NUM_INITIAL_MATRICES);
	for (i = 0; i < NUM_INITIAL_MATRICES; i++) {
		base_matrix[i] = mid[i];

		if (i % 3 == 0) {
			start_row[i] = 499; end_row[i] = 713;
			start_col[i] = 532; end_col[i] = 641;	
		} else if (i % 3 == 1) {
			start_row[i] = 20123; end_row[i] = 52321;
			start_col[i] = 6; end_col[i] = 8;
		} else {
			start_row[i] = 8; end_row[i] = 10;
			start_col[i] = 40123; end_col[i] = 69837;
		}

		subm[i] = GetSubMatrix1(base_matrix[i], start_row[i], start_col[i], end_row[i], end_col[i]);
		printf("[S-a obtinut submatricea %d]\n", i);

		int summary = ComputeSubMatrixSummary(subm[i], end_row[i] - start_row[i] + 1, end_col[i] - start_col[i] + 1);
		int expected_summary;
		scanf("%d", &expected_summary);

		if (summary != expected_summary) {
			printf("[EROARE: Elementele submatricii obtinute sunt incorecte]\n");
			exit(0);
		}
	}	

	printf("[Se creeaza %d submatrici suplimentare]\n", NUM_EXTRA_SUBMATRICES);

	int* parent = (int*) malloc(2 * (NUM_EXTRA_SUBMATRICES + NUM_INITIAL_MATRICES) * sizeof(int));
	for (i = 0; i < NUM_INITIAL_MATRICES; i++)
		parent[i] = -1;

	srand(10234);
	for (i = NUM_INITIAL_MATRICES; i < NUM_INITIAL_MATRICES + NUM_EXTRA_SUBMATRICES; i++) {
		scanf("%d %d %d %d %d", &parent[i], &start_row[i], &start_col[i], &end_row[i], &end_col[i]);	
		subm[i] = GetSubMatrix2(subm[parent[i]], start_row[i], start_col[i], end_row[i], end_col[i]);
		s1[i] = ComputeSubMatrixSummary(subm[i], end_row[i] - start_row[i] + 1, end_col[i] - start_col[i] + 1);
		int expected_summary;
		scanf("%d", &expected_summary);
		if (s1[i] != expected_summary) {
			printf("[EROARE: Elementele submatricii obtinute sunt incorecte]\n");
			exit(0);
		}

		int l, c;
		for (l = start_row[i]; l <= end_row[i]; l++)
			for (c = start_col[i]; c <= end_col[i]; c++) {
				int ll = l - start_row[i], cc = c - start_col[i];
				subm[i][ll][cc]++;
				int p1 = i, p2 = i;
				while (parent[p1] >= 0) {
					p2 = p1;
					p1 = parent[p1];
					if (subm[p1][ll + start_row[p2]][cc + start_col[p2]] != subm[p2][ll][cc]) {
						printf("[EROARE: Modificarea unei submatrici nu aduce dupa ea si modificarea celorlalte submatrici peste care se suprapune]\n");
						exit(0);
					}

					ll += start_row[p2];
					cc += start_col[p2];
				}
			}
	}

	printf("[Se testeaza scrierea in fisier a tuturor submatricilor]\n");
	for (i = 0; i < NUM_INITIAL_MATRICES + NUM_EXTRA_SUBMATRICES; i++) {
		s2[i] = ComputeSubMatrixSummary(subm[i], end_row[i] - start_row[i] + 1, end_col[i] - start_col[i] + 1);
		PutSubMatrix(subm[i]);
	}

	for (i = NUM_INITIAL_MATRICES + NUM_EXTRA_SUBMATRICES; i < 2 * NUM_INITIAL_MATRICES + NUM_EXTRA_SUBMATRICES; i++) {
		base_matrix[i] = base_matrix[i - (NUM_INITIAL_MATRICES + NUM_EXTRA_SUBMATRICES)];
		start_row[i] = start_row[i - (NUM_INITIAL_MATRICES + NUM_EXTRA_SUBMATRICES)];
		start_col[i] = start_col[i - (NUM_INITIAL_MATRICES + NUM_EXTRA_SUBMATRICES)];
		end_row[i] = end_row[i - (NUM_INITIAL_MATRICES + NUM_EXTRA_SUBMATRICES)];
		end_col[i] = end_col[i - (NUM_INITIAL_MATRICES + NUM_EXTRA_SUBMATRICES)];

		subm[i] = GetSubMatrix1(base_matrix[i], start_row[i], start_col[i], end_row[i], end_col[i]);
		int summary = ComputeSubMatrixSummary(subm[i], end_row[i] - start_row[i] + 1, end_col[i] - start_col[i] + 1);
		if (summary != s2[i - (NUM_INITIAL_MATRICES + NUM_EXTRA_SUBMATRICES)]) {
			printf("[EROARE: Elementele submatricilor initiale nu au fost scrise corect in fisier]\n");
			exit(0);
		}
	}

	for (i = 2 * NUM_INITIAL_MATRICES + NUM_EXTRA_SUBMATRICES; i < 2 * (NUM_INITIAL_MATRICES + NUM_EXTRA_SUBMATRICES); i++) {
		parent[i] = parent[i - (NUM_INITIAL_MATRICES + NUM_EXTRA_SUBMATRICES)] + NUM_INITIAL_MATRICES + NUM_EXTRA_SUBMATRICES;
		start_row[i] = start_row[i - (NUM_INITIAL_MATRICES + NUM_EXTRA_SUBMATRICES)];
		start_col[i] = start_col[i - (NUM_INITIAL_MATRICES + NUM_EXTRA_SUBMATRICES)];
		end_row[i] = end_row[i - (NUM_INITIAL_MATRICES + NUM_EXTRA_SUBMATRICES)];
		end_col[i] = end_col[i - (NUM_INITIAL_MATRICES + NUM_EXTRA_SUBMATRICES)];

		subm[i] = GetSubMatrix2(subm[parent[i]], start_row[i], start_col[i], end_row[i], end_col[i]);
		int summary = ComputeSubMatrixSummary(subm[i], end_row[i] - start_row[i] + 1, end_col[i] - start_col[i] + 1);
		if (summary != s2[i - (NUM_INITIAL_MATRICES + NUM_EXTRA_SUBMATRICES)]) {
			printf("[EROARE: Elementele submatricilor suplimentare nu au fost scrise corect in fisier]\n");
			exit(0);
		}
	}

	for (k = 0; k < NUM_CHECKS; k++) {
		int m, vmin, vmax;		
		scanf("%d %d %d", &m, &vmin, &vmax);
		int nelems = GetNumberOfElementsBetweenValues(mid[m], vmin, vmax);
		int expected_nelems;
		scanf("%d", &expected_nelems);
		if (nelems != expected_nelems) {
			printf("[EROARE: Numarul de elemente ale unei matrici dintr-un interval dat nu este corect dupa scrierea submatricilor]\n");
			exit(0);
		}
	}

	Finish();

	free(mid);
	free(subm);
	free(base_matrix);
	free(start_row);
	free(end_row);
	free(start_col);
	free(end_col);
	free(s1);
	free(s2);

	int duration = time(NULL) - tstart;
	printf("[Durata=%d sec / Limita=%d sec]\n", duration, TIME_LIMIT_2);
	if (duration > TIME_LIMIT_2) {
		printf("[EROARE: Testul 2 a durat prea mult]\n");
		exit(0);
	}

	printf("[OK - TESTUL 2]\n");
}

#define TIME_LIMIT_3 260

void test3() {
	int tstart = time(NULL);

	int* mid = (int*) malloc(2 * NUM_INITIAL_MATRICES * sizeof(int));
	int i;
	freopen("mids.txt", "r", stdin);
	for (i = 0; i < NUM_INITIAL_MATRICES; i++)
		scanf("%d", &mid[i]);

	freopen("ans-test3.txt", "r", stdin);

	printf("[Incepe TESTUL 3]\n");
	printf("[Se initializeaza sistemul]\n");
	Init("data");

	int* nrows = (int*) malloc(2 * NUM_INITIAL_MATRICES * sizeof(int));
	int* ncols = (int*) malloc(2 * NUM_INITIAL_MATRICES * sizeof(int));

	printf("[Se verifica dimensiunile matricilor initiale]\n");
	for (i = 0; i < NUM_INITIAL_MATRICES; i++) {
		if (i % 3 == 0) {
			nrows[i] = ncols[i] = 1024;		
		} else if (i % 3 == 1) {
			nrows[i] = 100000;
			ncols[i] = 12;		
		} else {
			nrows[i] = 12;
			ncols[i] = 100000;
		}

		int l, c;
		GetMatrixDimensions(mid[i], &l, &c);
		if (l != nrows[i] || c != ncols[i]) {
			printf("[EROARE: Matricile initiale nu au dimensiunile asteptate]\n");
			exit(0);
		}
	}

	printf("[Se creeaza inca %d matrici]\n", NUM_INITIAL_MATRICES);
	for (i = NUM_INITIAL_MATRICES; i < 2 * NUM_INITIAL_MATRICES; i++) {
		if (i % 3 == 0) {
			nrows[i] = ncols[i] = 1024;		
		} else if (i % 3 == 1) {
			nrows[i] = 100000;
			ncols[i] = 12;		
		} else {
			nrows[i] = 12;
			ncols[i] = 100000;
		}

		mid[i] = CreateMatrixWithSpecificSizes(nrows[i], ncols[i], i + 1);
		CheckUniqueMatrixId(mid, i);		
		printf("[S-a creat matricea %d]\n", i);
	}

	printf("[Se extind matricile existente]\n");

	srand(1334);
	int k;
	for (k = 0; k < NUM_EXTENSIONS; k++) {
		int ext_type, i1, i2;		
		scanf("%d %d %d", &ext_type, &i1, &i2);
		if (ext_type == 0) {
			ExtendRight(mid[i1], mid[i2]);
			ncols[i1] += ncols[i2];
		} else {
			ExtendDown(mid[i1], mid[i2]);
			nrows[i1] += nrows[i2];
		}
	}

	printf("[Se verifica dimensiunile tuturor matricilor]\n");
	for (i = 0; i < NUM_INITIAL_MATRICES; i++) {
		int l, c;
		GetMatrixDimensions(mid[i], &l, &c);
		if (l != nrows[i] || c != ncols[i]) {
			printf("[EROARE: Matricile nu au dimensiunile asteptate]\n");
			exit(0);
		}
	}

	printf("[Se verifica numarul de elemente din diferite intervale ale tuturor matricilor]\n");
	for (k = 0; k < NUM_FINAL_CHECKS; k++) {
		int m, vmin, vmax;
		scanf("%d %d %d", &m, &vmin, &vmax);
		int nelems = GetNumberOfElementsBetweenValues(mid[m], vmin, vmax);
		int expected_nelems;
		scanf("%d", &expected_nelems);
		if (nelems != expected_nelems) {
			printf("[EROARE: Numarul de elemente ale unei matrici dintr-un interval dat nu este corect]\n");
			exit(0);
		}
	}

	printf("[Se verifica anumite elemente din diferite pozitii ale tuturor matricilor]\n");
	for (k = 0; k < NUM_FINAL_CHECKS; k++) {
		int m, l, c;
		scanf("%d %d %d", &m, &l, &c);
		int** subm = GetSubMatrix1(mid[m], l, c, l, c);		
		int expected_elem;
		scanf("%d", &expected_elem);
		if (subm[0][0] != expected_elem) {
			printf("[EROARE: Elementele unei matrici sunt incorecte dupa extindere]\n");
			exit(0);
		}
	}

	Finish();

	free(mid);
	free(nrows);
	free(ncols);

	int duration = time(NULL) - tstart;
	printf("[Durata=%d sec / Limita=%d sec]\n", duration, TIME_LIMIT_3);
	if (duration > TIME_LIMIT_3) {
		printf("[EROARE: Testul 3 a durat prea mult]\n");
		exit(0);
	}

	printf("[OK - TESTUL 3]\n");
}

int main(int argc, char* argv[]) {
	if (argc != 2) {
		printf("Eroare de utilizare a programului de test\n");
		return 1;
	}

	if (strcmp(argv[1], "1") == 0) {
		test1();
	} else 	if (strcmp(argv[1], "2") == 0) {
		test2();
	} else 	if (strcmp(argv[1], "3") == 0) {
		test3();
	}

	return 0;
}


