#ifndef __T5_H
#define __T5_H

#include<stdlib.h>
#include<string.h>

typedef struct{
	float col;
	float row;
} mat;

mat *m;
int mat_id;
char *name;
int **aux;

//
// Functii extra
//
void filename(int id, char *fname){
	char s[4];
	sprintf(s,"%d",id);
	strcpy(fname, name);
	strcat(fname, s);
}

void free_memory(int **matrix){
int i;
	for(i=0; matrix[i]!=NULL; i++){
		free(matrix[i]);
	}
	if(matrix!=NULL) {
		free(matrix);
	}
}

int** read_from_file(int id, char c, int id2){
	int i;
	FILE *f;
	char *fname=(char *)malloc(24*sizeof(int));

	free_memory(aux);

	filename(id, fname);
	f=fopen(fname,"rb");

	switch(c){
		case 'r': 
			aux=(int **)malloc((m[id].row+1)*sizeof(int *));
			for(i=0;i<(m[id].row+1);i++){
				aux[i]=(int *)malloc(m[id].col*sizeof(int));
			}
			break;
		case 'c':
			aux=(int **)malloc(m[id].row*sizeof(int *));
			for(i=0;i<m[id].row;i++){
				aux[i]=(int *)malloc((m[id].col+1)*sizeof(int));
			}
			break;
		case 's': 
			aux=(int **)malloc((m[id].row)*sizeof(int *));
			for(i=0;i<m[id].row;i++){
				aux[i]=(int *)malloc((m[id].col+m[id2].col)*sizeof(int));
			}
			break;
		case 't':
			aux=(int **)malloc((m[id].row+m[id2].row)*sizeof(int *));
			for(i=0;i<(m[id].row+m[id2].row);i++){
				aux[i]=(int *)malloc(m[id].col*sizeof(int));
			}
			break;
		default: 
			aux=(int **)malloc(m[id].row*sizeof(int *));
			for(i=0;i<m[id].row;i++){
				aux[i]=(int *)malloc(m[id].col*sizeof(int));
			}
			break;
	}
	
	fread(aux,sizeof(int),m[id].row*m[id].col,f);

fclose(f);
free(fname);
return aux;
}

//
// Functia INIT
//
void Init(char* prefix){
	FILE *f;

	name=(char *)malloc(20);
	strcpy(name, prefix);

	m=(mat *)malloc(1061*sizeof(mat));

	if(fopen("data","b")==NULL){
		f=fopen("data", "wb");
		fwrite(m,sizeof(mat),1061,f);
	}
	else{
		f=fopen("data", "rb");
		fread(m,sizeof(mat),1061,f);
	}

}

//
// Functia CREATE_MATRIX
//
int CreateMatrix(){
	FILE *f;
	char s[4];
	char *fname=(char *)malloc(24);
	sprintf(s,"%d",mat_id);
	strcpy(fname, name);
	strcat(fname, s);
	f=fopen(fname,"wb");
	m[mat_id].col=0;
	m[mat_id].row=0;
	mat_id++;
fclose(f);
free(fname);
return mat_id-1;
}

//
// Functia GET_MATRIX_DIMENSIONS
//
void GetMatrixDimensions(int id, int* nrows, int* ncols){
	*nrows=m[id].row;
	*ncols=m[id].col;
}

//
// Functia APPEND_ROW
//
void AppendRow(int id, int* elems){
	int i=0,j=0,r,c;
	FILE *f;
	char *fname=(char *)malloc(24);

	free_memory(aux);
	
	if(elems!=NULL){
		r=m[id].row;
		c=m[id].col;

		aux=read_from_file(id,'r',0);
		filename(id, fname);

		m[id].row++;

		for(i=0;i<c;i++){
			aux[r][i]=*(elems+i);
		}
		
		f=fopen(fname,"wb");
		fwrite(aux,sizeof(int),(r+1)*c,f);
		
		fclose(f);
	}
	else{
		m[id].row++;
	}

//se elibereaza zona alocata pentru matricea cu identificatorul id;	
free_memory(aux);
free(fname);
}

//
// Functia APPEND_COLUMN
//
void AppendColumn(int id, int* elems){
	int i=0,j=0,r,c;
	FILE *f;
	char *fname=(char *)malloc(24);

	free_memory(aux);

	if(elems!=NULL){
		r=m[id].row;
		c=m[id].col;

		aux=read_from_file(id,'c',0);
		filename(id, fname);

		m[id].col++;

		for(i=0;i<r;i++){
			aux[i][c]=*(elems+i);
		}
		
		f=fopen(fname,"wb");
		fwrite(aux,sizeof(int),r*c+1,f);
		
		fclose(f);
	}
	else{
		m[id].col++;
	}

//se elibereaza zona alocata pentru matricea cu identificatorul id;	
free_memory(aux);
free(fname);
}

int **sm; //va fi folosita pentru generarea submatricelor;

//
// Functia GET_SUBMATRIX_1
//
int ** GetSubMatrix1(int id, int start_row, int start_col, int end_row, int end_col){
	int i,j,r,c;
	
	free_memory(aux);
	
//se va retine in matricea dinamica aux, matricea cu identificatorul id;
	aux=read_from_file(id, 'x',0);

//definim submatricea; aceasta va contine  doua randuri in plus (ultimele) unde vor fi retinute date despre aceasta;
	r=end_row-start_row+1; //numarul de randuri pe care il va contine submatricea;
	c=end_col-start_col+1; //numarul de coloane pe care il va contine submatricea;

	free_memory(sm);

	sm=(int **)malloc((r+2)*sizeof(int *));
	for(i=0;i<r;i++){
		sm[i]=(int *)calloc(c,sizeof(int));
	}
	sm[r]=(int *)calloc(5,sizeof(int));
	sm[r+1]=(int *)calloc(1,sizeof(int));
	
	//sm[r]: zona de "gestiune" a submatricei; s[r+1]: in GetSubmatrix2, se va parcurge base_submatrix pana se ajunge la NULL;
	sm[r][0]=id;
	sm[r][1]=start_row;
	sm[r][2]=end_row;
	sm[r][3]=start_col;
	sm[r][4]=end_col;
	sm[r+1]=NULL;
	
	for(i=0;i<r;i++){
		for(j=0;j<c;j++){
			sm[i][j]=aux[start_row+i][start_col+j];
		}
	}

//se elibereaza zona alocata pentru matricea cu identificatorul id;	
free_memory(aux);

return sm;
}

//
// Functia GET_SUBMATRIX_2
//
int ** GetSubMatrix2(int** base_submatrix, int start_row, int start_col, int end_row, int end_col){
	int i,j,r,c;

//definim submatricea; aceasta va contine doua randuri in plus (ultimele) unde vor fi retinute date despre aceasta;
//vor fi retinute date despre locatia submatricei in matricea originara;
	r=end_row-start_row+1; //numarul de randuri pe care il va contine submatricea;
	c=end_col-start_col+1; //numarul de coloane pe care il va contine submatricea;

	free_memory(sm);
	sm=(int **)malloc((r+2)*sizeof(int *));
	for(i=0;i<r;i++){
		sm[i]=(int *)calloc(c,sizeof(int));
	}
	sm[r]=(int *)calloc(5,sizeof(int));
	sm[r+1]=(int *)calloc(1,sizeof(int));
	
	//sm[r]: zona de "gestiune" a submatricei; s[r+1]: in GetSubmatrix2, se va parcurge base_submatrix pana se ajunge la NULL;
	sm[r][0]=base_submatrix[r][0];
	sm[r][1]=base_submatrix[r][1]+start_row;
	sm[r][2]=base_submatrix[r][2]+end_row;
	sm[r][3]=base_submatrix[r][3]+start_col;
	sm[r][4]=base_submatrix[r][4]+end_col;
	sm[r+1]=NULL;

	for(i=0;i<r;i++){
		for(j=0;j<c;j++){
			sm[i][j]=&base_submatrix[start_row+i][start_col+j];
		}
	}

return sm;
}

//
// Functia PUT_SUBMATRIX
//
void PutSubMatrix(int** submatrix){
	int mid;
	int start_row, start_col, end_row, end_col; //coordonatele intre care se va realiza scrierea;
	int r,c; //se va retine numarul de linii/coloane ce vor scrise;
	int i,j,x,y; //variabile pentru parcurgerea matricelor;
	FILE *f;
	char *fname=(char *)malloc(24);

	free_memory(aux);

	i=0;
	while(submatrix[i]!=NULL){
		i++;
	}

	mid=submatrix[i-1][0];
	start_row=submatrix[i-1][1];
	end_row=submatrix[i-1][2];
	start_col=submatrix[i-1][3];
	end_col=submatrix[i-1][4];

	r=end_row-start_row+1;
	c=end_col-start_col+1;
	
	aux=read_from_file(mid, 'x',0);
	filename(mid, fname);
	
	for(i=start_row, x=0; i<=end_row, x<r; i++, x++){
		for(j=start_col, y=0; j<=end_col, y<c; j++, y++){
			aux[i][j]=submatrix[x][y];
		}
	}

	f=fopen(fname,"wb");
	fwrite(aux,sizeof(int),m[mid].row*m[mid].col,f);
		
fclose(f);
free(fname);

//se elibereaza zona alocata pentru matricea cu identificatorul id;	
free_memory(aux);
}

//
// Functia EXTEND_RIGHT
//
void ExtendRight(int id1, int id2){
	int i,j,x;
	int **aux1, **aux2;

	aux1=read_from_file(id1,'s',id2);
	aux2=read_from_file(id2,'x',0);
	
	for(i=0;i<m[id1].row;i++){
		for(j=m[id1].col, x=0; j<(m[id1].col+m[id2].col), x<m[id2].col; j++, x++){
			aux1[i][j]=aux2[i][x];
		}
	}
//se elibereaza zona alocata pentru matricea cu identificatorul id1 si id2;	
free_memory(aux1);
free_memory(aux2);
}

//
// Functia EXTEND_DOWN
//
void ExtendDown(int id1, int id2){
	int i,j,x;
	int **aux1, **aux2;

	aux1=read_from_file(id1,'t',id2);
	aux2=read_from_file(id2,'x',0);

	for(i=m[id1].row, x=0; i<(m[id1].row+m[id2].row), x<m[id2].row; i++, x++){
		for(j=0; j<m[id1].col; j++){
			aux1[i][j]=aux2[x][j];
		}
	}
//se elibereaza zona alocata pentru matricea cu identificatorul id1 si id2;	
free_memory(aux1);
free_memory(aux2);
}

//
// Functia GET_NUMBER_OF_ELEMENTS_BETWEEN_VALUES
//
int GetNumberOfElementsBetweenValues(int id, int vmin, int vmax){
	int i,j,k;
	
	free_memory(aux);

	aux=read_from_file(id,'x',0);

	for(i=0;i<m[id].row;i++){
		for(j=0;j<m[id].col;j++){
			if(aux[i][j]>=vmin && aux[i][j]<=vmax) k++;
		}
	}
	
//se elibereaza zona alocata pentru matricea cu identificatorul id;	
free_memory(aux);
return k;
}

//
// Functia FINISH
//
void Finish(){
free_memory(aux);
free_memory(sm);

	FILE *f;
	f=fopen("data", "wb");
	fwrite(m,sizeof(mat),1061,f);

fclose(f);
free(m);
}


#endif /* __T5_H */
