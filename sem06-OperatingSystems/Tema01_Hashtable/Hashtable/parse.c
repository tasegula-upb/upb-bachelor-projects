/**	---------------------------------------------------------------------------
 **	Gula Tanase, 334CA
 **		Sisteme de Operare - Tema 01: Multi-platform Development
 **	------------------------------------------------------------------------ */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "parse.h"
#include "utils.h"

char** parse(char *line) {
	char toks[5] = " \n";
	char **result;

	if (strlen(line) == 1) {
		return NULL;
	}

	result = (char **) calloc (3, sizeof(char*));

	result[0] = strtok(line, toks);
	result[1] = strtok(NULL, toks);
	result[2] = strtok(NULL, toks);

	return result;
}

void get_input(char *line, char **command, char **param1, char **param2) {
	char toks[5] = " \n";

	if (strlen(line) == 1) {
		return;
	}

	*command = strtok(line, toks);
	*param1 = strtok(NULL, toks);
	*param2 = strtok(NULL, toks);
}