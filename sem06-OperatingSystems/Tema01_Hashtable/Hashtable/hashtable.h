/**	---------------------------------------------------------------------------
 **	Gula Tanase, 334CA
 **		Sisteme de Operare - Tema 01: Multi-platform Development
 **	------------------------------------------------------------------------ */

#ifndef HASHTABLE_H_
#define HASHTABLE_H_

typedef struct Node {
	char *data;
	struct Node *next;
} Node;

/**	---------------------------------------------------------------------------
 **		HASHTABLE FRAMEWORK
 **	------------------------------------------------------------------------ */

//	RETURNS the last element of the bucket;
Node* get_last(Node *bucket);

//	RETURNS the element that contains the given word, NULL otherwise;
//	can act as a contains() function;
Node* get_element(Node *bucket, char *word);

//	Test if the bucket contains a given word; RETURNS 1 is found, 0 otherwise;
unsigned int contains(Node *bucket, char *word);

//	Prints in the FILE named file, if the word is found;
void find(Node *bucket, char *word, char *file);

//	ADD a Node containing word, as the the last in its bucket;
void add_last(Node **hashtable, char *word, unsigned int size);

//	REMOVE the element with "word";
void delete_element(Node **hashtable, char *word, unsigned int size);

//	REMOVE the elements from the entire bucket;
void remove_bucket(Node *bucket);

//	REMOVE the elements from the entire hashtable;
void clear(Node **hashtable, unsigned int size);

//	PRINTS the words stored in bucket;
void print_bucket(Node *bucket, char *file);

//	PRINTS the entire hashtable;
void print_hashtable(Node **hashtable, unsigned int size, char *file);

//	USED for debugging: same as print_bucket, but prints in the given file;
void print_bucket_in_file(Node *bucket, FILE *file);

//	RESIZE the hashtable, depending on the type given;
//	RETURNS the new hashtable, and its size by referencing;
Node** resize(Node **hashtable, unsigned int *size, char *type);

#endif
