/**	---------------------------------------------------------------------------
 **	Gula Tanase, 334CA
 **		Sisteme de Operare - Tema 01: Multi-platform Development
 **	------------------------------------------------------------------------ */

#ifndef HASH_H_
#define HASH_H_

unsigned int hash(const char *str, unsigned int hash_length);

#endif
