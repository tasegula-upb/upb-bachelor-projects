/**	---------------------------------------------------------------------------
 **	Gula Tanase, 334CA
 **		Sisteme de Operare - Tema 01: Multi-platform Development
 **	------------------------------------------------------------------------ */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hash.h"
#include "hashtable.h"
#include "parse.h"
#include "utils.h"

#define MAX_LINE 20000

int main(int argc, char* argv[]) {
	int k = 0;
	unsigned int size = 0;
	char *line;
	char **input;
	Node **hashtable = NULL;

	if (argc == 1) {
		printf("USAGE: ./EXE SIZE <INPUT FILE>*\n");
		return 0;
	}

	line = (char*) malloc (MAX_LINE * sizeof(char));
	DIE(line == NULL, "MALLOC ERROR\n");
	
	input = (char**) malloc (3 * sizeof(char*));
	DIE(input == NULL, "MALLOC ERROR\n");

	size = atoi(argv[1]);
	hashtable = (Node**) calloc (size, sizeof(Node*));
	// no need for DIE() testing


	for (k = 2; k <= argc; k++) {
		if (argv[k] != NULL) {
			freopen (argv[k], "r", stdin);
		}

		while (fgets(line, MAX_LINE, stdin) != NULL) {
			if ((input = parse(line)) != NULL) {
				
				if (strcmp(input[0], "add") == 0) {
					add_last(hashtable, input[1], size);
				}
				else if (strcmp(input[0], "remove") == 0) {
					delete_element(hashtable, input[1], size);
				}
				else if (strcmp(input[0], "clear") == 0) {
					clear(hashtable, size);	
					free(hashtable);
					hashtable = (Node**) calloc (size, sizeof(Node*));
				}
				else if (strcmp(input[0], "find") == 0) {
					find(hashtable[hash(input[1], size)], input[1], input[2]);
				}
				else if (strcmp(input[0], "print_bucket") == 0) {
					print_bucket(hashtable[atoi(input[1])], input[2]);
				}
				else if (strcmp(input[0], "print") == 0) {
					print_hashtable(hashtable, size, input[1]);
				}
				else if (strcmp(input[0], "resize") == 0) {
					hashtable = resize(hashtable, &size, input[1]);
				}
			}
		}
	}

free(line);
clear(hashtable, size); free(hashtable);

fclose(stdin);
return 0;
}
