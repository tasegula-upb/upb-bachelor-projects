/**	---------------------------------------------------------------------------
 **	Gula Tanase, 334CA
 **		Sisteme de Operare - Tema 01: Multi-platform Development
 **	------------------------------------------------------------------------ */

#ifndef PARSE_H_
#define PARSE_H_

char** parse(char *line);

void get_input(char *line, char **command, char **param1, char **param2);

#endif
