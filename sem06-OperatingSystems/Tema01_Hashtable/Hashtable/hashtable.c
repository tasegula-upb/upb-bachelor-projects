/**	---------------------------------------------------------------------------
 **	Gula Tanase, 334CA
 **		Sisteme de Operare - Tema 01: Multi-platform Development
 **	------------------------------------------------------------------------ */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "hashtable.h"
#include "hash.h"
#include "utils.h"


//	RETURNS the last element of the bucket;
Node* get_last(Node *bucket) {
	Node *p = bucket;

	while (p->next != NULL) {
		p = p->next;
	}

	return p;
}

//	RETURNS the element that contains the given word, NULL otherwise;
//	can act as a contains() function;
Node* get_element(Node *bucket, char *word) {
	Node *p = bucket;

	if (bucket == NULL) {
		return 0;
	}

	while (p != NULL && p->data != NULL) {
		if (strcmp(p->data, word) == 0) {
			return p;
		}

		p = p->next;
	}
	return NULL;
}

//	Test if the bucket contains a given word; RETURNS 1 is found, 0 otherwise;
unsigned int contains(Node *bucket, char *word) {
	Node *p = bucket;

	if (bucket == NULL) {
		return 0;
	}

	while (p != NULL && p->data != NULL) {
		if (strcmp(p->data, word) == 0) {
			return 1;
		}

		p = p->next;
	}
	
	return 0;
}

//	Prints in the FILE named file, if the word is found;
void find(Node *bucket, char *word, char *file) {
	FILE *out = stdout;

	if (file != NULL) {
		out = fopen(file, "a");
	}

	if (contains(bucket, word) == 1) {
		fprintf(out, "True\n");
	}
	else {
		fprintf(out, "False\n");
	}

	if (file != NULL) {
		fclose(out);
	}
}

//	ADD a Node containing word, as the the last in its bucket;
void add_last(Node **hashtable, char *word, unsigned int size) {
	unsigned int hashn;
	Node *p, *list, *last;

	p = malloc(sizeof(Node));
	DIE(p == NULL, "MALLOC ERROR\n");
	p->data = malloc((strlen(word) + 1) * sizeof(char));
	DIE(p->data == NULL, "MALLOC ERROR\n");

	hashn = hash(word, size);
	list = hashtable[hashn];

	if (list == NULL) {
		strcpy(p->data, word);
		p->next = NULL;

		hashtable[hashn] = p;
	}
	else if (contains(hashtable[hashn], word) == 0) { // the list doesn't contains the word
		last = get_last(list);

		strcpy(p->data, word);

		last->next = p;
		p->next = NULL;
		hashtable[hashn] = list;
	}
}

//	REMOVE the element with "word";
void delete_element(Node **hashtable, char *word, unsigned int size) {
	unsigned int hashn = hash(word, size);
	Node *elem = get_element(hashtable[hashn], word);
	Node *p = (Node*) malloc (sizeof(Node));
	DIE(p == NULL, "MALLOC ERROR\n");

	if (elem == NULL) {
		return;
	}

	// delete last element
	if (elem->next == NULL) {
		// test if the last element is also the first
		if (hashtable[hashn]->next == NULL) {
			hashtable[hashn] = NULL;
			return;
		}

		p = hashtable[hashn];
		while (p->next->next != NULL) {
			p = p->next;
		}

		p->next = NULL;
		free (elem);
	}
	else {
		// delete non-last element :D
		p = elem->next;
		elem->data = elem->next->data;
		elem->next = elem->next->next;

		free(p);
	}
}

//	REMOVE the elements from the entire bucket;
void remove_bucket(Node *bucket) {
	Node *p = bucket;
	Node *q = (Node*) malloc (sizeof(Node));
	DIE(q == NULL, "MALLOC ERROR\n");

	while (p != NULL) {
		q = p;
		p = p -> next;
		free(q);
	}
	
}

//	REMOVE the elements from the entire hashtable;
void clear(Node **hashtable, unsigned int size) {
	unsigned int i = 0;

	for (i = 0; i < size; i++) {
		remove_bucket(hashtable[i]);
	}
}

//	PRINTS the words stored in bucket;
void print_bucket(Node *bucket, char *file) {
	Node *list = bucket;
	FILE *out = stdout;

	// test if it should print in file or at stdout;
	if (file != NULL) {
		out = fopen(file, "a");
	}

	while (list != NULL && list->data != NULL) {
		fprintf(out, "%s ", list->data);		
		list = list->next;
	}

	// test if it's an empty list;
	// if not, we already printed the elements => go to the next line
	if (bucket != NULL) {
		fprintf(out, "\n");
	}

	// close the file
	if (file != NULL) {
		fclose(out);
	}
	list = NULL;
}

//	PRINTS the entire hashtable;
void print_hashtable(Node **hashtable, unsigned int size, char *file) {
	unsigned int i = 0;

	for (i = 0; i < size; i++) {
		print_bucket(hashtable[i], file);
	}
}

//	RESIZE the hashtable, depending on the type given;
//	RETURNS the new hashtable, and its size by referencing;
Node** resize(Node **hashtable, unsigned int *_size, char *type) {
	Node **result;
	Node *list;

	unsigned int size = 0;
	unsigned int i = 0;

	if (strcmp(type, "double") == 0) {
		size = *_size * 2;
	}
	else if (strcmp(type, "halve") == 0) {
		size = *_size / 2;
	}
	else {
		return NULL;
	}

	result = (Node**) calloc (size, sizeof(Node*));
	for (i = 0; i < *_size; i++) {		
		list = hashtable[i];
		
		while (list != NULL) {
			add_last(result, list->data, size);
			list = list->next;
		}
		remove_bucket(hashtable[i]);
	}

	*_size = size;
	free(hashtable);
	
	return result;
}

//	USED for debugging: same as print_bucket, but prints in the given file;
void print_bucket_in_file(Node *bucket, FILE *file) {
	Node *list = bucket;

	while (list != NULL && list->data != NULL) {
		fprintf(file, "%s ", list->data);
		
		list = list->next;
	}

	fprintf(file, "\n");
	list = NULL;
}