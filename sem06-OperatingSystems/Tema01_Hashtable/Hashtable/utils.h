/**	---------------------------------------------------------------------------
 **	Gula Tanase, 334CA
 **		Sisteme de Operare - Tema 01: Multi-platform Development
 **	------------------------------------------------------------------------ */

#ifndef UTILS_H_
#define UTILS_H_

#define DIE(assertion, call_description)				\
	do {								\
		if (assertion) {					\
			fprintf(stderr, "(%s, %d): ",			\
					__FILE__, __LINE__);		\
			perror(call_description);			\
			exit(EXIT_FAILURE);				\
		}							\
	} while(0)

#endif