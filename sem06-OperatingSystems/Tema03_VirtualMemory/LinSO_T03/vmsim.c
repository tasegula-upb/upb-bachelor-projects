/*
 *	Gula Tanase, 334CA *	
 *	Operating Systems - Assignment 03
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "vmsim.h"
#include "common.h"
#include "helpers.h"

#include "debug.h"
#include "util.h"

static node_t* memory = NULL;

/* my handlers :) */
static void _vmsim_handler(int signum, siginfo_t* info, void* context);
static void _vmsim_sigsegv(w_ptr_t page_addr, entry_t* entry);

/* swaping */
static void _vmsim_swap_out(entry_t* entry, w_size_t page_no);

/* initialization methods */
static void _init_page(page_t* page, w_ptr_t start, int i);
static void _init_frame(frame_t* frame);

/* creates and opens the ram/swap files */
static w_handle_t _handle(char type, w_size_t num_pages);


/* initialize and cleanup library -- consider exception handler */
w_boolean_t vmsim_init(void)
{
	return w_set_exception_handler(_vmsim_handler);
}

w_boolean_t vmsim_cleanup(void)
{
	w_exception_handler_t handler;
	w_get_previous_exception_handler(&handler);

	return w_set_exception_handler(handler);
}

/*
 * allocate physical pages in RAM mapped by virt_pages in swap
 * map is to be filled with start address and handle to RAM and swap
 * files
 */
w_boolean_t vm_alloc(w_size_t num_pages, w_size_t num_frames, vm_map_t *map)
{
	if (num_frames > num_pages) {
		return FALSE;
	}

	unsigned int i = 0;

	node_t* entry = malloc(sizeof(node_t));
	entry->data = malloc(sizeof(entry_t));
	entry->next = NULL;

	// creates files (ram and swap)
	map->ram_handle = _handle('R', num_frames);
	map->swap_handle = _handle('S', num_pages);

	// allocate virtual memory
	map->start = mmap(NULL, 
					  w_get_page_size() * num_pages,
					  PROT_NONE,
					  MAP_SHARED | MAP_ANONYMOUS,
					  -1,
					  0);
	DIE(map->start == MAP_FAILED, "mmap @ alloc");

	entry->data->start       = map->start;
	entry->data->ram_handle  = map->ram_handle;
	entry->data->swap_handle = map->swap_handle;

	entry->data->num_pages = num_pages;
	entry->data->num_frames = num_frames;

	entry->data->ram_pages = 0;

	// creates the disk and ram memories
	entry->data->disk_mem = malloc(num_pages * sizeof(page_t));
	entry->data->ram_mem = malloc(num_frames * sizeof(frame_t));

	for (i = 0; i < num_pages; i++) {
		_init_page(&entry->data->disk_mem[i], entry->data->start, i);
	}

	for (i = 0; i < num_frames; i++) {
		_init_frame(&entry->data->ram_mem[i]);
	}

	// test if memory is NULL
	if (memory == NULL) {
		memory = entry;
	}
	else {
		node_t* list = memory;

		// find the last node;
		while (list->next != NULL) {
			list = list->next;
		}
		// add as the new last node
		list->next = entry;
	}


	return TRUE;
}

/*
 * free space previously allocated with vm_alloc
 * start is the start address of the previously allocated area
 *
 * implementation has to also close handles corresponding to RAM and
 * swap files
 */
w_boolean_t vm_free(w_ptr_t start)
{
	if (start == NULL || memory == NULL) {
		return FALSE;
	}

	int rc = 0;		// return

	node_t* list = memory;
	node_t* p = memory;

	while(list->data->start != start) {
		list = list->next;

		if (list == NULL) return FALSE;
	}

	rc = munmap(start, w_get_page_size() * list->data->num_pages);
	DIE(rc < 0, "munmap");

	w_close_file(list->data->ram_handle);
	w_close_file(list->data->swap_handle);

	free(list->data->disk_mem);
	free(list->data->ram_mem);
	free(list->data);

	if (list == memory) {
		p = list->next;
		list = list->next;
		free(p);
	}
	else {
		p = list;
		list = list->next;

		free(p);
	}

	return TRUE;
}

/*	---------------------------------------------------------------------------
 *	Implementing the static methods declared above
 */

void _vmsim_handler(int signum, siginfo_t *info, void *context)
{
	// check if the signal is SIGSEGV or the list is NULL
	if (signum != SIGSEGV || memory == NULL) {
		return;
	}

	w_ptr_t page_addr;
	w_ptr_t start, end;
	w_size_t page_size;

	node_t* list = memory;

	page_size = w_get_page_size();

	// align the address
	page_addr = (w_ptr_t*)info->si_addr;
	page_addr = (w_ptr_t)((unsigned long)page_addr - 
						  (unsigned long) page_addr % page_size);

	// start and end of the search interval
	start = list->data->start;
	end   = (w_ptr_t)((unsigned long)list->data->start +
					  (unsigned long)list->data->num_pages * 
					  (unsigned long)page_size);

	// find the address
	while (list != NULL &&
		   !((unsigned long)page_addr >= (unsigned long)start &&
			 (unsigned long)page_addr < (unsigned long)end)) {

		list = list->next;

		start = list->data->start;
		end   = (w_ptr_t)((unsigned long)list->data->start +
						  (unsigned long)list->data->num_pages * 
						  (unsigned long)page_size);
	}

	// address not found
	if (list == NULL) {
		printf("Address not found\n");
		DIE(1 == 1, "Address not found");
	}

	dprintf("Address %p found in interval [%p - %p]\n", page_addr, start, end);

	_vmsim_sigsegv(page_addr, list->data);
}

void _vmsim_sigsegv(w_ptr_t page_addr, entry_t *entry)
{
	// returns;
	int rc;
	w_ptr_t rcp;

	// get page size for future use
	w_size_t page_size = w_get_page_size();

	// get the page number
	w_size_t page_no = (int)(((unsigned long)page_addr - 
							  (unsigned long)entry->start) / 
							 (unsigned long)page_size);

	w_size_t frame_no = -1;

	// get the page entry from the table;
	page_t* page = &(entry->disk_mem[page_no]);

	dprintf("Address %p\n", page_addr);
	dprintf("\t page number: %d\n", (int) page_no);
	dprintf("\t start:       %p\n", page->start);
	dprintf("\t protection:  %d\n", (int)page->protection);
	dprintf("\t state        %d\n", page->state);

	// basic case:
	//	the virtual page will be mapeed to a ram frame
	//	 >	the swap memory is empty
	//	 >	the state is STATE_NOT_ALLOC
	if (entry->ram_pages < entry->num_frames &&
		page->protection == PROTECTION_NONE)
	{
		dprintf("\t [segfault][0] update to READ\n");

		frame_no = entry->ram_pages;
		entry->ram_pages++;

		rc = munmap(page_addr, page_size);
		DIE(rc < 0, "[segfault][0] munmap");

		rcp = mmap(page_addr,
					page_size,
					PROT_READ,
					MAP_FIXED | MAP_SHARED,
					entry->ram_handle,
					frame_no * page_size);
		DIE(rcp == MAP_FAILED, "[segfault][0] mmap");

		rc = w_protect_mapping(page->start, 1, PROTECTION_READ);
		DIE(rc < 0, "[segfault][0] protect + READ");

		// change the page properties
		page->state = STATE_IN_RAM;
		page->protection = PROTECTION_READ;
		page->dirty = FALSE;

		page->frame_no = frame_no;
		entry->ram_mem[frame_no].page_no = page_no;

		return;
	}

	// if in ram, then it's protection is read and should be write;
	else if (page->state == STATE_IN_RAM)
	{
		dprintf("\t [segfault][1] update to WRITE in RAM\n");

		frame_no = page->frame_no;

		rc = w_protect_mapping(page->start, 1, PROTECTION_WRITE);

		page->protection = PROTECTION_WRITE;
		page->dirty = TRUE;

		return;
	}

	// ram memory is FULL
	// we have to put a frame from ram to swap
	// then, see if the segfault page is in swap or on disk
	else if (entry->ram_pages == entry->num_frames)
	{
		dprintf("\t [segfault][2] swaping\n");

		frame_no = 0;	// for simplicity

		// swap out the frame_no frame
		_vmsim_swap_out(entry, page_no);

		// map the page which got segfault
		rc = munmap(page->start, page_size);
		DIE(rc < 0, "[segfault][2] munmap");

		// protection is read|write because the page can be in swap
		// 	=> may need to be written
		rcp = mmap(page->start,
				   page_size,
				   PROT_READ | PROT_WRITE,
				   MAP_SHARED,
				   entry->ram_handle,
				   frame_no * page_size);
		DIE(rcp == MAP_FAILED, "[segfault][2] mmap");

		// if in swap, copy the data from swap (swap in)
		if (page->state == STATE_IN_SWAP)
		{
			// copy the entry from swap ...
			rc = w_set_file_pointer(entry->swap_handle,
									page_no * page_size);
			DIE(rc < 0, "[segfault][2.2] seek");

			// ... by reading from the swap handle into the mapped location in ram
			rc = w_read_file(entry->swap_handle,
							 rcp,
							 page_size);
		}

		rc = w_protect_mapping(page->start, 1, PROTECTION_READ);
		DIE(rc < 0, "[segfault][2] prot - READ");

		// change the page properties
		page->prev_state = page->state;
		page->state = STATE_IN_RAM;
		page->protection = PROTECTION_READ;
		page->dirty = FALSE;

		page->frame_no = frame_no;

		// change the frame properties
		entry->ram_mem[frame_no].page_no = page_no;

		return;
	}

	else {
		printf("unknown: PROT-%d    STATE-%d\n", 
				(int)page->protection,
				(int)page->state);
		DIE(1 == 1, "unknown");
	}
	printf("the end?\n");
}

void _vmsim_swap_out(entry_t* entry, w_size_t page_no)
{
	w_size_t page_size = w_get_page_size();
	w_size_t frame_no = 0;	// for simplicity

	int rc;			// return;
	w_ptr_t rcp;	// return;

	// the page to be swapped
	w_size_t swap_page_no = entry->ram_mem[frame_no].page_no;
	page_t* swap_page = &(entry->disk_mem[swap_page_no]);

	// test if swap is necessary
	if (swap_page->dirty == TRUE ||
		swap_page->prev_state == STATE_NOT_ALLOC)
	{
		dprintf("\t [swap out] swaping frame\n");

		// write in swap memory
		rc = w_set_file_pointer(entry->swap_handle,
								swap_page_no * page_size);
		DIE(rc == FALSE, "[swap out] seek");

		rc = w_write_file(entry->swap_handle,
						  swap_page->start,
						  page_size);
		DIE(rc == FALSE, "[swap out] write");

		swap_page->prev_state = swap_page->state;
		swap_page->state = STATE_IN_SWAP;
		swap_page->dirty = FALSE;
		swap_page->frame_no = -1;
	}

	// remap the memory which was swapped
	rc = munmap(swap_page->start, page_size);
	DIE(rc < 0, "[swap out] munmap");

	rcp = mmap(swap_page->start,
				page_size,
				PROT_NONE,
				MAP_FIXED | MAP_SHARED | MAP_ANONYMOUS,
				-1,
				0);
	DIE(rcp == MAP_FAILED, "[swap out] mmap");

	swap_page->protection = PROTECTION_NONE;
}

w_handle_t _handle(char type, w_size_t num_entries)
{
	char template[15];
	w_handle_t handle;
	int rc;

	if (type == 'R') {
		sprintf(template, "RAM_XXXXXX");
	}
	else if (type == 'S') {
		sprintf(template, "SWAP_XXXXXX");
	}

	handle = mkstemp(template);
	DIE(handle == INVALID_HANDLE, "mkstemp");

	rc = ftruncate(handle, w_get_page_size() * num_entries);
	DIE(rc < 0, "ftruncate");

	return handle;
}

void _init_page(page_t *page, w_ptr_t start, int i)
{
	page->state      = STATE_NOT_ALLOC;
	page->prev_state = STATE_NOT_ALLOC;
	page->dirty      = FALSE;
	page->protection = PROTECTION_NONE;		// 0
	page->start      = (w_ptr_t)(start + i * w_get_page_size());

	page->frame_no   = -1;
}

void _init_frame(frame_t *frame)
{
	frame->index   = -1;
	frame->page_no = -1;
}