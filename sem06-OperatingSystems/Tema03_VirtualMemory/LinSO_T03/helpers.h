/*
 *	Gula Tanase, 334 CA
 *	Operating Systems - Assignment 03
 *
 *	New data types + Some useful functions
 */

#ifndef HELPERS_H_
#define HELPERS_H_

#include "common.h"
#include "vmsim.h"

/* State of a page */
typedef enum page_state {
	STATE_NOT_ALLOC,	// 0
	STATE_IN_RAM,		// 1
	STATE_IN_SWAP		// 2
} page_state;

struct frame;

/* handle pages (virtual pages) */
typedef struct page_table_entry {
	page_state state;
	page_state prev_state;

	w_boolean_t dirty;
	w_prot_t protection;
	w_ptr_t start;

	// RAM frame number; -1 if not mapped
	w_size_t frame_no;
} page_t;

/* handle frames (physical pages) */
typedef struct frame {
	w_size_t index;

	/* "backlink" to page_table_entry; -1 in case of free frame */
	w_size_t page_no;
} frame_t;

/* handle mapping */
typedef struct table_entry {
	page_t* disk_mem;
	frame_t* ram_mem;

	/* vm_map_t; used so to shrink up the line size */
	/* p->data->start as p->data->map->start; */
	w_ptr_t start;
	w_handle_t ram_handle;
	w_handle_t swap_handle;

	w_size_t num_pages;
	w_size_t num_frames;

	int ram_pages;		// number of pages in ram
} entry_t;

/* LIST node; */
typedef struct node {
	struct node* next;
	entry_t* data;			// all the mapping data
} node_t;

/* virtual memory mapping encapsulation; initialized by vm_alloc
typedef struct vm_map {
	w_ptr_t start;
	w_handle_t ram_handle;
	w_handle_t swap_handle;
} vm_map_t;
//*/


#endif