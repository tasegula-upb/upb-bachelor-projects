/*
 *	Gula Tanase, 334CA *	
 *	Operating Systems - Assignment 03
 */

#define DLL_EXPORTS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "vmsim.h"
#include "common.h"
#include "helpers.h"

#include "debug.h"
#include "util.h"

#define DLL_EXPORTS

static node_t* memory = NULL;
static w_handle_t segfault_handle = NULL;

/* my handlers :) */
static LONG _vmsim_handler(PEXCEPTION_POINTERS ExceptionInfo);
static void _vmsim_sigsegv(w_ptr_t page_addr, entry_t* entry);

/* swaping */
static void _vmsim_swap_out(entry_t* entry, w_size_t page_no);

/* initialization methods */
static void _init_page(page_t* page);
static void _init_frame(frame_t* frame);

/* creates and opens the ram/swap files */
static w_handle_t _handle(char type, w_size_t num_pages);


/* initialize and cleanup library -- consider exception handler */
w_boolean_t vmsim_init(void)
{
	segfault_handle = w_add_exception_handler(_vmsim_handler);
	return segfault_handle != NULL;
}

w_boolean_t vmsim_cleanup(void)
{
	return w_remove_exception_handler(segfault_handle);
}

/*
 * allocate physical pages in RAM mapped by virt_pages in swap
 * map is to be filled with start address and handle to RAM and swap
 * files
 */
w_boolean_t vm_alloc(w_size_t num_pages, w_size_t num_frames, vm_map_t *map)
{


	unsigned int i = 0;
	int rc;		// return;

	w_size_t page_size = w_get_page_size();

	node_t* node;
	entry_t* entry;

	if (num_frames > num_pages) {
		return FALSE;
	}

	node = malloc(sizeof(node_t));
	node->data = malloc(sizeof(entry_t));
	node->next = NULL;
	entry = node->data;

	entry->num_pages = num_pages;
	entry->num_frames = num_frames;
	entry->ram_pages = 0;

	// creates the disk and ram memories
	entry->disk_mem = malloc(num_pages * sizeof(page_t));
	entry->ram_mem = malloc(num_frames * sizeof(frame_t));

	for (i = 0; i < num_pages; i++) {
		_init_page(&entry->disk_mem[i]);
	}

	for (i = 0; i < num_frames; i++) {
		_init_frame(&entry->ram_mem[i]);
	}

	// create files (ram and swap)
	map->ram_handle = _handle('R', num_frames);
	map->swap_handle = _handle('S', num_pages);

	entry->ram_handle = CreateFileMapping(map->ram_handle,
										  NULL,
										  PAGE_READWRITE,
										  0,
										  num_frames * page_size,
										  NULL);
	DIE(entry->ram_handle  == INVALID_HANDLE_VALUE, "CreateFileMapping");

	entry->swap_handle = CreateFileMapping(map->swap_handle,
										   NULL,
										   PAGE_READWRITE,
										   0,
										   num_pages * page_size,
										   NULL);
	DIE(entry->swap_handle  == INVALID_HANDLE_VALUE, "CreateFileMapping");

	// map the memory
	map->start = VirtualAlloc(NULL,
							  page_size * num_pages,
							  MEM_RESERVE,
							  PAGE_NOACCESS);
	DIE(map->start == NULL, "VirtualAlloc");

	rc = VirtualFree(map->start, 0, MEM_RELEASE);
	DIE(rc == 0, "VirtualFree");

	for (i = 0; i < num_pages; i++) {
		entry->disk_mem[i].start = 
			VirtualAlloc((char*)map->start + i * page_size,
						  page_size,
						  MEM_RESERVE,
						  PAGE_NOACCESS);
		DIE(entry->disk_mem[i].start == NULL, "VirtualAlloc");
	}

	// copy the map (for the map handles)
	entry->map = map;

	// test if memory is NULL
	if (memory == NULL) {
		memory = node;
	}
	else {
		node_t* list = memory;

		// find the last node;
		while (list->next != NULL) {
			list = list->next;
		}
		// add as the new last node
		list->next = node;
	}

	return TRUE;
}

/*
 * free space previously allocated with vm_alloc
 * start is the start address of the previously allocated area
 *
 * implementation has to also close handles corresponding to RAM and
 * swap files
 */
w_boolean_t vm_free(w_ptr_t start)
{
	int rc = 0;		// return

	node_t* list = memory;
	node_t* p = memory;

	if (start == NULL || memory == NULL) {
		return FALSE;
	}

	// find the entry
	while(list->data->map->start != start) {
		list = list->next;

		if (list == NULL) return FALSE;
	}

	rc = VirtualFree(start, 0, MEM_RELEASE);
	DIE(rc < 0, "VirtualFree");

	// close ALL the handles
	w_close_file(list->data->ram_handle);
	w_close_file(list->data->swap_handle);
	w_close_file(list->data->map->ram_handle);
	w_close_file(list->data->map->swap_handle);

	free(list->data->disk_mem);
	free(list->data->ram_mem);
	free(list->data);

	if (list == memory) {
		p = list->next;
		list = list->next;
		free(p);
	}
	else {
		p = list;
		list = list->next;

		free(p);
	}

	return TRUE;
}

/*	---------------------------------------------------------------------------
 *	Implementing the static methods declared above
 */

LONG _vmsim_handler(PEXCEPTION_POINTERS ExceptionInfo)
{
	w_ptr_t page_addr;
	w_ptr_t start, end;
	w_size_t page_size;

	node_t* list = memory;

	page_size = w_get_page_size();

	// align the address
	page_addr = (w_ptr_t*)(ExceptionInfo->ExceptionRecord->ExceptionInformation[1]);
	page_addr = (w_ptr_t)((unsigned long)page_addr - 
						  (unsigned long) page_addr % page_size);

	// start and end of the search interval
	start = list->data->map->start;
	end   = (w_ptr_t)((unsigned long)list->data->map->start +
					  (unsigned long)list->data->num_pages * 
					  (unsigned long)page_size);

	// find the address
	while (list != NULL &&
		   !((unsigned long)page_addr >= (unsigned long)start &&
			 (unsigned long)page_addr < (unsigned long)end)) {

		list = list->next;

		start = list->data->map->start;
		end   = (w_ptr_t)((unsigned long)list->data->map->start +
						  (unsigned long)list->data->num_pages * 
						  (unsigned long)page_size);
	}

	// address not found
	if (list == NULL) {
		dprintf("Address not found\n");
		DIE(1 == 1, "Address not found");
	}

	dprintf("Address %p found in interval [%p - %p]\n", page_addr, start, end);

	_vmsim_sigsegv(page_addr, list->data);

	return EXCEPTION_CONTINUE_EXECUTION;
}

void _vmsim_sigsegv(w_ptr_t page_addr, entry_t *entry)
{
	// returns;
	int rc;
	w_ptr_t rcp;

	// get page size for future use
	w_size_t page_size = w_get_page_size();

	// get the page number
	w_size_t page_no = (int)(((unsigned long)page_addr - 
							  (unsigned long)entry->map->start) / 
							 (unsigned long)page_size);

	w_size_t frame_no = -1;

	// get the page entry from the table;
	page_t* page = &(entry->disk_mem[page_no]);

	dprintf("Address %p\n", page_addr);
	dprintf("\t page number: %d\n", (int) page_no);
	dprintf("\t start:       %p\n", page->start);
	dprintf("\t protection:  %d\n", (int)page->protection);
	dprintf("\t state        %d\n", page->state);

	// basic case:
	//	the virtual page will be mapeed to a ram frame
	//	 >	the swap memory is empty
	//	 >	the state is STATE_NOT_ALLOC
	if (entry->ram_pages < entry->num_frames &&
		page->protection == PROTECTION_NONE)
	{
		dprintf("\t [segfault][0] update to READ: \t\t %p\n", page_addr);

		frame_no = entry->ram_pages;
		entry->ram_pages++;

		rc = VirtualFree(page_addr, 0, MEM_RELEASE);
		DIE(rc < 0, "[segfault][0] VirtualFree");

		rcp = MapViewOfFileEx(entry->ram_handle,
							  FILE_MAP_READ,
							  0,
							  frame_no * page_size,
							  page_size,
							  page_addr);
		DIE(rcp == NULL, "[segfault][0] MapViewOfFileEx");

		rc = w_protect_mapping(page->start, 1, PROTECTION_READ);
		DIE(rc < 0, "[segfault][0] protect + READ");

		// change the page properties
		page->state = STATE_IN_RAM;
		page->protection = PROTECTION_READ;
		page->dirty = FALSE;

		page->frame_no = frame_no;
		entry->ram_mem[frame_no].page_no = page_no;

		return;
	}

	// if in ram, then it's protection is read and should be write;
	else if (page->state == STATE_IN_RAM)
	{
		dprintf("\t [segfault][1] update to WRITE in RAM: \t %p\n", page_addr);

		frame_no = page->frame_no;

		rc = UnmapViewOfFile(page_addr);
		DIE (rc == 0, "[segfault][1] Unmap");

		rcp = MapViewOfFileEx(entry->ram_handle,
							 FILE_MAP_WRITE,
							 0,
							 frame_no * page_size,
							 page_size,
							 page_addr);
		DIE(rcp == NULL, "[segfault][0] MapViewOfFileEx");

		rc = w_protect_mapping(page_addr, 1, PROTECTION_WRITE);

		page->protection = PROTECTION_WRITE;
		page->dirty = TRUE;

		return;
	}

	// ram memory is FULL
	// we have to put a frame from ram to swap
	// then, see if the segfault page is in swap or on disk
	else if (entry->ram_pages == entry->num_frames)
	{
		dprintf("\t [segfault][2] swaping: \t\t %p\n", page_addr);

		frame_no = 0;	// for simplicity

		// swap out the frame_no frame
		_vmsim_swap_out(entry, page_no);

		rc = VirtualFree(page_addr, 0, MEM_RELEASE);
		DIE(rc < 0, "[segfault][2] VirtualFree");

		rcp = MapViewOfFileEx(entry->ram_handle,
							  FILE_MAP_WRITE,
							  0,
							  frame_no * page_size,
							  page_size,
							  page_addr);
		DIE(rcp == NULL, "[segfault][2] MapViewOfFileEx");

		// if in swap, copy the data from swap (swap in)
		if (page->state == STATE_IN_SWAP)
		{
			// copy the entry from swap ...
			rc = w_set_file_pointer(entry->map->swap_handle,
									page_no * page_size);
			DIE(rc < 0, "[segfault][2.2] seek");

			// ... by reading from the swap handle into the mapped location in ram
			rc = w_read_file(entry->map->swap_handle,
							 rcp,
							 page_size);
		}

		rc = w_protect_mapping(page->start, 1, PROTECTION_READ);
		DIE(rc < 0, "[segfault][2] protect + READ");

		// change the page properties
		page->prev_state = page->state;
		page->state = STATE_IN_RAM;
		page->protection = PROTECTION_READ;
		page->dirty = FALSE;

		page->frame_no = frame_no;

		// change the frame properties
		entry->ram_mem[frame_no].page_no = page_no;

		return;
	}

	else {
		dprintf("unknown: PROT-%d    STATE-%d\n", 
				(int)page->protection,
				(int)page->state);
		DIE(1 == 1, "unknown");
	}

	DIE(1 == 1, "FORCED OUT");
}

void _vmsim_swap_out(entry_t* entry, w_size_t page_no)
{
	w_size_t page_size = w_get_page_size();
	w_size_t frame_no = 0;	// for simplicity

	int rc;			// return;
	w_ptr_t rcp;	// return;

	// the page to be swapped
	w_size_t swap_page_no = entry->ram_mem[frame_no].page_no;
	page_t* swap_page = &(entry->disk_mem[swap_page_no]);

	// test if swap is necessary
	if (swap_page->dirty == TRUE ||
		swap_page->prev_state == STATE_NOT_ALLOC)
	{
		dprintf("\t [swap out] swaping frame\n");

		// write in swap memory
		rc = w_set_file_pointer(entry->map->swap_handle,
								swap_page_no * page_size);
		DIE(rc == FALSE, "[swap out] seek");

		rc = w_write_file(entry->map->swap_handle,
						  swap_page->start,
						  page_size);
		DIE(rc == FALSE, "[swap out] write");

		swap_page->prev_state = swap_page->state;
		swap_page->state = STATE_IN_SWAP;
		swap_page->dirty = FALSE;
		swap_page->frame_no = -1;
	}

	rc = UnmapViewOfFile(swap_page->start);
		DIE (rc == 0, "[swap out] Unmap");

	rcp = VirtualAlloc(swap_page->start,
					   page_size,
					   MEM_RESERVE,
					   PAGE_NOACCESS);
	DIE(rcp == NULL, "[swap out] VirtualAlloc");

	swap_page->protection = PROTECTION_NONE;
}

w_handle_t _handle(char type, w_size_t num_entries)
{
	char filename[15];
	w_handle_t handle;

	int rc;

	if (type == 'R') {
		rc = GetTempFileName(".", "ram_", 0, filename);
	}
	else if (type == 'S') {
		rc = GetTempFileName(".", "swap_", 0, filename);
	}
	DIE(rc == 0, "GetTempFileName");
	handle = w_open_file(filename, MODE_FULL_OPEN);

	rc = w_set_file_pointer(handle, w_get_page_size() * num_entries);
	DIE(rc == FALSE, "[swap out] seek");

	SetEndOfFile(handle);

	rc = w_set_file_pointer(handle, 0);
	DIE(rc == FALSE, "[swap out] seek");

	return handle;
}

void _init_page(page_t *page)
{
	page->state      = STATE_NOT_ALLOC;
	page->prev_state = STATE_NOT_ALLOC;
	page->dirty      = FALSE;
	page->protection = PROTECTION_NONE;		// 0

	page->frame_no   = -1;
}

void _init_frame(frame_t *frame)
{
	frame->index   = -1;
	frame->page_no = -1;
}
