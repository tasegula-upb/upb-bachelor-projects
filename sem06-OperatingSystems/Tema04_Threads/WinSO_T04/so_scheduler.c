#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "so_scheduler.h"
#include "./_test/scheduler_test.h"

#define basic_test(t) if (t) printf("PASS\n"); else printf("FAIL\n");

/*
 * If tested thread is not set to wait, then schedule it next.
 * @thread_id	the id of the thread to be tested if executed next
 * @j			the new position in priority array
 * @wait		semaphore variable: should the sem be set to wait or not
 */
#define call_scheduler_if_test(thread_id, j, wait)		\
	do {												\
		if (th_waiting[thread_id] == _FALSE) {			\
			priority_pos[th_priorities[thread_id]] = j;	\
			_scheduler(thread_id, wait);				\
			return;										\
		}												\
	} while(0);

// struct for a simple array
typedef struct {
	unsigned *array;
	unsigned capacity;
	unsigned size;
} array;

// boolean datatype
enum BOOLEAN {
	_TRUE,
	_FALSE
};

// received at init
unsigned quantum;
unsigned max_io;

unsigned count;				// number of threads
unsigned array_size;		// the arrays capacity

unsigned current_thread;	// id of current thread
unsigned current_quantum;	// value of the current quantum

array *devent;				// max_io * max_io array for devices

HANDLE **semaphores;		// array of semaphore(s)
so_handler **handlers;		// array of handler(s)

HANDLE *threads;			// array of threads id
BOOLEAN *th_waiting;		// array of threads waiting status (true or false)
unsigned *th_priorities;	// array of threads priorities; the given priority is unsigned

array priority_list[SO_MAX_PRIO + 1];		// a simple array for each priority (0 to MAX);
unsigned priority_pos[SO_MAX_PRIO + 1];		// position in that array; may need to reach 0 again

/*
 *	Thread function: blocks the thread until it's scheduled
 *	@thread_id	the id of the thread that executes the function
 */
DWORD WINAPI start_thread(void * thread_id);

/*
 *	The scheduler: computes what thread should be executed next
 *	@wait	semaphore variable: should the sem be set to wait or not
 */
void scheduler(BOOLEAN wait);

/*
 *	The scheduler's helper: modifies variables in order to execute next the scheduled thread
 *	@thread		the id of the thread that WILL be executed next
 *	@wait		semaphore variable: should the sem be set to wait or not
 */
void _scheduler(unsigned thread, BOOLEAN wait);


int so_init(unsigned quant, unsigned io_max)
{
	unsigned i = 0;

	if (quantum != 0 || quant == 0 || io_max > 256) {
		return -1;
	}

	max_io = io_max;
	quantum = quant;

	devent = calloc(max_io, sizeof(array));
	for (i = 0; i < max_io; i++) {
		devent[i].capacity = 64;
		devent[i].array = calloc(devent[i].capacity, sizeof(unsigned));
		devent[i].size = 0;
	}

	count = 0;			// no thread
	array_size = 64;	// initial dimension for arrays; double when full

	// initialize 
	semaphores    = calloc(array_size, sizeof(HANDLE));
	handlers      = calloc(array_size, sizeof(so_handler*));
	threads       = calloc(array_size, sizeof(HANDLE));
	th_waiting    = calloc(array_size, sizeof(BOOLEAN));
	th_priorities = calloc(array_size, sizeof(unsigned));

	// priorities arrays will not be reallocated the same as the other arrays
	for (i = 0; i <= SO_MAX_PRIO; i++) {
		priority_list[i].capacity = array_size;
		priority_list[i].array = calloc(priority_list[i].capacity, sizeof(unsigned));
		priority_list[i].size = 0;
	}

	return 0;
}

tid_t so_fork(so_handler* handler, unsigned priority)
{
	HANDLE sem;
	tid_t tid;
	HANDLE ret;

	// sanity checks
	if (handler == NULL) {
		return INVALID_TID;
	}

	// semaphore initialization
	sem = CreateSemaphore(NULL, 0, 1, NULL);
	if (sem == NULL) {
		return INVALID_TID;
	}

	// test if capacity exceeded
	if (count == array_size) {
		array_size *= 2;

		semaphores    = realloc(semaphores,    array_size * sizeof(HANDLE));
		handlers      = realloc(handlers,      array_size * sizeof(so_handler*));
		threads       = realloc(threads,       array_size * sizeof(HANDLE));
		th_waiting    = realloc(th_waiting,    array_size * sizeof(BOOLEAN));
		th_priorities = realloc(th_priorities, array_size * sizeof(unsigned));
	}

	// set the variables for the new thread
	semaphores[count]    = sem;
	handlers[count]      = handler;
	th_priorities[count] = priority;
	th_waiting[count]    = _FALSE;

	// add the specific priority_list
	priority_list[priority].array[priority_list[priority].size++] = count;

	// test if priority array capacity exceeded
	if (priority_list[priority].size == priority_list[priority].capacity) {
		priority_list[priority].capacity *= 2;
		priority_list[priority].array = realloc(priority_list[priority].array,
												priority_list[priority].capacity * sizeof(unsigned));
	}

	// create thread
	ret = CreateThread(
		NULL, 
		0, 
		start_thread, 
		(void*)count, 
		0, 
		&tid);
	if (ret == NULL) {
		return INVALID_TID;
	}
	threads[count] = ret;

	// one more thread, so the current thread executed one quantum
	current_quantum--;
	count++;

	// first thread is special
	if (count == 1) {
		current_thread = count - 1;
		current_quantum = quantum;
		ReleaseSemaphore(semaphores[current_thread], 1, NULL);
	}
	else {
		scheduler(_TRUE);
	}

	return tid;
}

int so_wait(unsigned id)
{
	// sanity checks: the id can't be bigger that maximum
	if (id >= max_io) {
		return -1;
	}

	// set waiting flag to _TRUE
	th_waiting[current_thread] = _TRUE;

	// add thread on that device
	devent[id].array[devent[id].size++] = current_thread;

	// test if devent array capacity exceeded
	if (devent[id].size == devent[id].capacity) {
		devent[id].capacity *= 2;
		devent[id].array = realloc(devent[id].array, 
								   devent[id].capacity * sizeof(unsigned));
	}

	scheduler(_TRUE);
	return 0;
}

int so_signal(unsigned id)
{
	int i = 0;
	unsigned size = 0;

	// sanity check: the id can't be bigger that maximum
	if (id >= max_io) {
		return -1;
	}

	// how many threads where awakened
	size = devent[id].size;

	// no more waiting for these threads
	for (i = 0; i < (int)size; i++) {
		th_waiting[devent[id].array[i]] = _FALSE;
	}

	// this device has no more threads waiting
	devent[id].size = 0;
	current_quantum--;

	scheduler(_TRUE);
	return size;
}

void so_exec()
{
	// it's executed, so quantum goes down
	current_quantum--;
	scheduler(_TRUE);
}

void so_end()
{
	int i = 0;

	// join threads and free threads stuff
	for (i = 0; i < (int)count; i++) {
		WaitForSingleObject(threads[i], INFINITE);
	}
	free(threads);
	threads = NULL;
	free(th_waiting);
	free(th_priorities);

	// destroy semaphores and free memory
	for (i = 0; i < (int)count; i++) {
		CloseHandle(semaphores[i]);
	}
	free(semaphores);
	semaphores = NULL;

	// free io
	for (i = 0; i < (int)max_io; i++) {
		free(devent[i].array);
	}
	free(devent);
	devent = NULL;

	// free priorities arrays
	for (i = 0; i <= SO_MAX_PRIO; i++) {
		free(priority_list[i].array);
	}

	// no more devices; quantum is zero
	// no thread, so no current_thread
	// no quantum, so no current_quantum
	quantum = max_io = 0;
	current_quantum = current_thread = 0;
}

DWORD WINAPI start_thread(void * thread_id)
{
	unsigned id = (unsigned)(long)thread_id;

	WaitForSingleObject(semaphores[id], INFINITE);
	handlers[id](th_priorities[id]);

	// set waiting
	th_waiting[id] = _TRUE;
	current_quantum = 0;
	scheduler(_FALSE);
	return 0;
}

void scheduler(BOOLEAN wait)
{
	int i = 0, j = 0, end = 0;

	unsigned priority = th_priorities[current_thread];

	unsigned thread_id = -1;
	unsigned* th_priority_list = NULL;

	// test for those threads with GREATER priority
	for (i = SO_MAX_PRIO; i > (int)priority; i--) {

		// from the last position in that array to the end
		end = priority_list[i].size;
		th_priority_list = priority_list[i].array;

		for (j = priority_pos[i]; j < end; j++) {
			thread_id = th_priority_list[j];
			call_scheduler_if_test(thread_id, j, wait);
		}

		// then from the first to the last position
		end = priority_pos[i];
		for (j = 0; j < end; j++) {
			thread_id = th_priority_list[j];
			call_scheduler_if_test(thread_id, j, wait);
		}
	}

	// only is quantum is 0 or the current thread is waiting
	if (current_quantum == 0 || th_waiting[current_thread] == _TRUE) {

		// first priority checked is this thread's
		// go to the next position in array
		priority_pos[priority]++;
		// if that position doesn't exist, start again from 0
		if (priority_pos[priority] >= priority_list[priority].size) {
			priority_pos[priority] = 0;
		}

		// test for those threads with EQUAL or LOWER priority
		i = 0;
		for (i = priority; i >= 0; i--) {
			// from the last position in that array to the end
			end = priority_list[i].size;
			th_priority_list = priority_list[i].array;

			for (j = priority_pos[i]; j < end; j++) {
				thread_id = th_priority_list[j];
				call_scheduler_if_test(thread_id, j, wait);
			}

			// then from the first to the last position
			end = priority_pos[i];
			for (j = 0; j < end; j++) {
				thread_id = th_priority_list[j];
				call_scheduler_if_test(thread_id, j, wait);
			}
		}
	}
}

void _scheduler(unsigned thread_id, BOOLEAN wait)
{
	unsigned th = 0;
	current_quantum = quantum;	// reset quantum;

	// the thread to be scheduled cannot be the same as current
	if (thread_id != current_thread) {
		th = current_thread;
		current_thread = thread_id;

		// give thread permision to be executed
		ReleaseSemaphore(semaphores[thread_id], 1, NULL);

		// block the previous thread
		if (wait == _TRUE) {
			WaitForSingleObject(semaphores[th], INFINITE);
		}
	}
}
