/**
 * Operating Systems 2013 - Assignment 2
 *
 */


#ifndef _UTILS_H
#define _UTILS_H

#include <windows.h>

#include "parser.h"

#define CHUNK_SIZE 100
#define ERR_ALLOCATION "unable to allocate memory"

#define SHELL_EXIT -100

/* useful macro for handling error codes */
#define DIE(assertion, call_description)				\
	do {												\
			if (assertion) {							\
					fprintf(stderr, "(%s, %s, %d): ",	\
					__FILE__, __FUNCTION__, __LINE__);	\
					PrintLastError(call_description);	\
					exit(EXIT_FAILURE);					\
			}											\
	} while(0)


/**
 * Readline from mini-shell.
 */
char *read_line();

/**
 * Parse and execute a command.
 */
int parse_command(command_t *, int, command_t *, HANDLE *, HANDLE *);


/* ------------------------------------------------------------------------- */
/* 							MY METHODS										 */
/* ------------------------------------------------------------------------- */

/**
 *	Write an error message to the given file
 */
int write_error(char *cmd_name);


HANDLE MyOpenFile(PCSTR filename, DWORD cMode, BOOL type);
void redirect(STARTUPINFO *psi, simple_command_t *s);
void open_files(char *in, char *out, char *err);


#define FREE(cmd, in, out, err) free(cmd); free(in); free(out); free(err);


#define READ		0
#define WRITE		1

#define OPENF(in, out, err) {							\
	HANDLE hFile;										\
														\
	if (in != NULL) {									\
		hFile = MyOpenFile(in, CREATE_ALWAYS, READ);	\
		CloseHandle(hFile);								\
	}													\
														\
	if (out != NULL) {									\
		hFile = MyOpenFile(out, CREATE_ALWAYS, WRITE);	\
		CloseHandle(hFile);								\
														\
	}													\
														\
	if (err != NULL) {									\
		hFile = MyOpenFile(err, CREATE_ALWAYS, WRITE);	\
		CloseHandle(hFile);								\
	}													\
}



#endif
