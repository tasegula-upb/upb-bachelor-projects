/**
 * Operating Systems 2013 - Assignment 2
 *
 */

//-------------------------------------------------------------------------------------------------
#define _CRT_SECURE_NO_WARNINGS
#include <windows.h>
#include <process.h>

#include <assert.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

/* do not use UNICODE */
#undef _UNICODE
#undef UNICODE

#define READ		0
#define WRITE		1

#define INF			1000000000

#define MAX_SIZE_ENVIRONMENT_VARIABLE 100

/**
 * Debug method, used by DIE macro.
 */
static VOID PrintLastError(const PCHAR message)
{
	CHAR errBuff[1024];

	FormatMessage(
		FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_MAX_WIDTH_MASK,
		NULL,
		GetLastError(),
		0,
		errBuff,
		sizeof(errBuff) - 1,
		NULL);

	fprintf(stderr, "%s: %s\n", message, errBuff);
}

/**
 * Internal change-directory command.
 */
static bool shell_cd(word_t *dir)
{
	/* execute cd */
	bool ret = SetCurrentDirectory(dir->string);
	if (ret != 0) {
		return write_error("Invalid folder");
	}
	return 0;
}

/**
 * Internal exit/quit command.
 */
static int shell_exit()
{
	/* execute exit/quit */
	return SHELL_EXIT;
}

/**
 * Concatenate parts of the word to obtain the command
 */
static LPTSTR get_word(word_t *s)
{
	DWORD string_length = 0;
	DWORD substring_length = 0;

	LPTSTR string = NULL;
	CHAR substring[MAX_SIZE_ENVIRONMENT_VARIABLE];

	DWORD dwret;

	while (s != NULL) {
		strcpy(substring, s->string);

		if (s->expand == true) {
			dwret = GetEnvironmentVariable(substring, substring, MAX_SIZE_ENVIRONMENT_VARIABLE);
			if (!dwret)
				/* Environment Variable does not exist. */
				strcpy(substring, "");
		}

		substring_length = strlen(substring);

		string = realloc(string, string_length + substring_length + 1);
		memset(string + string_length, 0, substring_length + 1);

		strcat(string, substring);
		string_length += substring_length;

		s = s->next_part;
	}

	return string;
}

/**
 * Parse arguments in order to succesfully process them using CreateProcess
 */
static LPTSTR get_argv(simple_command_t *command)
{
	LPTSTR argv = NULL;
	LPTSTR substring = NULL;
	word_t *param;

	DWORD string_length = 0;
	DWORD substring_length = 0;

	argv = get_word(command->verb);
	assert(argv != NULL);

	string_length = strlen(argv);

	param = command->params;
	while (param != NULL) {
		substring = get_word(param);
		substring_length = strlen(substring);

		argv = realloc(argv, string_length + substring_length + 4);
		assert(argv != NULL);

		strcat(argv, " ");

		/* Surround parameters with ' ' */
		strcat(argv, "'");
		strcat(argv, substring);
		strcat(argv, "'");

		string_length += substring_length + 3;
		param = param->next_word;

		free(substring);
	}

	return argv;
}

/**
 * Parse and execute a simple command, by either creating a new processing or
 * internally process it.
 */
bool parse_simple(simple_command_t *s, 
				int level, command_t *father, 
				HANDLE *pipeIn, HANDLE *pipeOut)
{

	char *command = get_word(s->verb);

	char *input = get_word(s->in);
	char *output = get_word(s->out);
	char *error = get_word(s->err);

	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	DWORD dwRes;
	BOOL bRes;

	int ret = 0;
	char *argv = get_argv(s);

	/* create flag */
	DWORD cMode = CREATE_ALWAYS;

	/* TODO sanity checks */
	if (s == NULL) return write_error("NULL simple command");

	/* if builtin command, execute the command */
	if ((strcmp(command, "exit") == 0) || (strcmp(command, "quit") == 0)) {
		return shell_exit();
	}

	if (strcmp(command, "cd") == 0) {
		open_files(input, output, error);
		return shell_cd(s->params);
	}

	/* if variable assignment, execute the assignment and return
	 * the exit status */
	if ((s->verb->next_part != NULL) && (s->verb->next_part->string[0] == '=')) {
		return !SetEnvironmentVariable(s->verb->string, 
									  s->verb->next_part->next_part->string);
	}

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));
	si.dwFlags |= STARTF_USESTDHANDLES;

	si.hStdInput = GetStdHandle(STD_INPUT_HANDLE);
	si.hStdOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	si.hStdError = GetStdHandle(STD_ERROR_HANDLE);

	if (pipeIn != NULL) {
		si.hStdInput = pipeIn;
	}
	else if (input != NULL) {
		si.hStdInput = MyOpenFile(input, OPEN_EXISTING, READ);
	}

	if (pipeOut != NULL) {
		si.hStdOutput = pipeOut;
	}
	else if (output != NULL) {
		if (s->io_flags == 1) {
			cMode = OPEN_ALWAYS;
		}
		si.hStdOutput = MyOpenFile(output, cMode, WRITE);
	}

	if (error != NULL) {
		if (output != NULL && strcmp (output, error) == 0) {
			si.hStdError = si.hStdOutput;
		}
		else {
			if (s->io_flags == 2) {
				cMode = OPEN_ALWAYS;
			}
			si.hStdError = MyOpenFile(error, cMode, WRITE);
		}
	}

	bRes = CreateProcess(
			NULL, 
			argv, 
			NULL, 
			NULL, 
			TRUE, 
			0, 
			NULL, 
			NULL, 
			&si, 
			&pi);
	if (!bRes) {
		write_error(command);

		if (input != NULL || pipeIn != NULL)	CloseHandle(si.hStdInput);
		if (output != NULL || pipeOut != NULL)	CloseHandle(si.hStdOutput);
		if (error != NULL)						CloseHandle(si.hStdError);

		FREE(command, input, output, error);
		return -1;
	}

	dwRes = WaitForSingleObject(pi.hProcess, INFINITE);
	DIE(dwRes == WAIT_FAILED, "WaitForSingleObject");

	GetExitCodeProcess(pi.hProcess, &ret);

	CloseHandle(pi.hProcess);

	if (input != NULL || pipeIn != NULL)	CloseHandle(si.hStdInput);
	if (output != NULL || pipeOut != NULL)	CloseHandle(si.hStdOutput);
	if (error != NULL)						CloseHandle(si.hStdError);

	FREE(command, input, output, error);
	return ret;
}


static unsigned int _stdcall parallel(LPVOID args)
{
	unsigned ret = 0;
	parse_command((command_t *)args, 0, NULL, NULL, NULL);
	_endthreadex(ret);

	return ret;
}

/**
 * Process two commands in parallel, by creating two children.
 */
static bool do_in_parallel(command_t *cmd1, command_t *cmd2, 
						int level, command_t *father, 
						HANDLE *pipeIn, HANDLE *pipeOut)
{
	/* execute cmd1 and cmd2 simultaneously */

	int ret;
	unsigned th;

	th = _beginthreadex(0, 0, parallel, cmd1, 0, 0);
	ret = parse_command(cmd2, level, father, pipeIn, pipeOut);
	CloseHandle((HANDLE) th);

	return ret;
}

/**
 * Run commands by creating an annonymous pipe (cmd1 | cmd2)
 */
static bool do_on_pipe(command_t *cmd1, command_t *cmd2, 
					int level, command_t *father, 
					HANDLE *pipeIn, HANDLE *pipeOut)
{
	HANDLE pRead, pWrite;
	SECURITY_ATTRIBUTES sa;
	BOOL bRes;

	int ret = 0;

	/* redirect the output of cmd1 to the input of cmd2 */
	if (cmd1 == NULL) return write_error("NULL command 1");
	if (cmd2 == NULL) return write_error("NULL command 2");

	ZeroMemory(&sa, sizeof(sa));
	sa.nLength = sizeof(SECURITY_ATTRIBUTES);
	sa.bInheritHandle = TRUE;
	sa.lpSecurityDescriptor = NULL; 

	bRes = CreatePipe(&pRead, &pWrite, &sa, INFINITE);
	if(!bRes) return write_error("PIPE");

	ret = parse_command(cmd1, level, father, pipeIn, pWrite);
	CloseHandle(pWrite);

	ret = parse_command(cmd2, level, father, pRead, pipeOut);
	CloseHandle(pRead);
	return ret;
}

/**
 * Parse and execute a command.
 */
int parse_command(command_t *c, int level, command_t *father, HANDLE *pipeIn, HANDLE *pipeOut)
{
	int ret = 0;

	/* sanity checks */
	if (c == NULL) return write_error("NULL command");

	if (c->op == OP_NONE) {
		/* execute a simple command */
		return parse_simple(c->scmd, level, c, pipeIn, pipeOut);
	}


	switch (c->op) {
	case OP_SEQUENTIAL:
		/* ; - execute the commands one after the other */
		ret = parse_command(c->cmd1, level, c, pipeIn, pipeOut);
		return parse_command(c->cmd2, level, c, pipeIn, pipeOut);

	case OP_PARALLEL:
		/* & - execute the commands simultaneously */
		return do_in_parallel(c->cmd1, c->cmd2, level, c, pipeIn, pipeOut);

	case OP_CONDITIONAL_NZERO:
		/* || -  execute the second command only if 
		 *       the first one returns non zero */
		ret = parse_command(c->cmd1, level, c, pipeIn, pipeOut);
		if (ret != 0) {
			ret = parse_command(c->cmd2, level, c, pipeIn, pipeOut);
		}
		return ret;

	case OP_CONDITIONAL_ZERO:
		/* && - execute the second command only if 
		 *      the first one returns zero */
		ret = parse_command(c->cmd1, level, c, pipeIn, pipeOut);
		if (ret == 0) {
			ret = parse_command(c->cmd2, level, c, pipeIn, pipeOut);
		}
		return ret;

	case OP_PIPE:
		/* | - redirect the output of the first command 
		 *     to the input of the second */
		return do_on_pipe(c->cmd1, c->cmd2, level, c, pipeIn, pipeOut);

	default:
		return SHELL_EXIT;
	}

	return -1; // nothing has been done
}

/**
 * Readline from mini-shell.
 */
char *read_line()
{
	char *instr;
	char *chunk;
	char *ret;

	int instr_length;
	int chunk_length;

	int endline = 0;

	chunk = calloc(CHUNK_SIZE, sizeof(char));
	if (chunk == NULL) {
		fprintf(stderr, ERR_ALLOCATION);
		exit(EXIT_FAILURE);
	}

	instr = NULL;
	instr_length = 0;

	while (!endline) {
		ret = fgets(chunk, CHUNK_SIZE, stdin);
		if (ret == NULL) {
			break;
		}

		chunk_length = strlen(chunk);
		if (chunk[chunk_length - 1] == '\n') {
			chunk[chunk_length - 1] = 0;
			endline = 1;
		}

		instr = realloc(instr, instr_length + CHUNK_SIZE);
		if (instr == NULL) {
			free(ret);
			return instr;
		}

		memset(instr + instr_length, 0, CHUNK_SIZE);
		strcat(instr, chunk);
		instr_length += chunk_length;
	}

	free(chunk);

	return instr;
}


/**
 *	Write an error message and return -1;
 *	The message is written at STDERR
 */
int write_error(char *cmd_name)
{
	fprintf(stderr, "Execution failed for '%s'\n", cmd_name);
	fflush(stderr);
	return -1;
}


HANDLE MyOpenFile(PCSTR filename, DWORD cMode, BOOL type)
{
	SECURITY_ATTRIBUTES sa;
	DWORD aMode;
	DWORD sMode;

	HANDLE hFile;

	char *errmsg = _strdup("open-");

	ZeroMemory(&sa, sizeof(sa));
	sa.bInheritHandle = TRUE;

	if (type == WRITE) {
		aMode = GENERIC_WRITE;
		sMode = FILE_SHARE_WRITE;
	}
	else {
		aMode = GENERIC_READ;	
		sMode = FILE_SHARE_READ;	
	}

	hFile =  CreateFile(
				filename,
				aMode,
				sMode,
				&sa,
				cMode,
				FILE_ATTRIBUTE_NORMAL,
				NULL);

	if (hFile == INVALID_HANDLE_VALUE) {
		strcat(errmsg, filename);
		write_error(errmsg);
		return hFile;
	}

	if (cMode == OPEN_ALWAYS) {
		SetFilePointer(hFile, 0, NULL, FILE_END);
	}

	return hFile;
}

/*
 *	Redirects stdin, stdout and stderr to the given file(names), if not NULL;
 *	Mode describes which flags to use when opening the files
 */
void redirect(STARTUPINFO *psi, simple_command_t *s)
{
//	printf("(%s - %s - %s) %d\n", in, out, error, mode);

	int ret = 0;
	int mode = s->io_flags;
	char *in = get_word(s->in);
	char *out = get_word(s->out);
	char *err = get_word(s->err);

	/* Find the flags */
	DWORD cMode = CREATE_ALWAYS;

	psi->hStdInput = GetStdHandle(STD_INPUT_HANDLE);
	psi->hStdOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	psi->hStdError = GetStdHandle(STD_ERROR_HANDLE);

	if (in != NULL) {
//		printf("in %s\n", in);
		psi->hStdInput = MyOpenFile(in, OPEN_EXISTING, READ);
	}

	if (out != NULL) {
		if (err != NULL && strcmp (out, err) == 0) {
//			printf("out-err %s %s\n", out, err);
			psi->hStdOutput = MyOpenFile(out, cMode, WRITE);
			psi->hStdError = psi->hStdOutput;

			return;
		}

		if (mode == 1) {
			cMode = OPEN_ALWAYS;
		}

//		printf("out %s\n", out);
		psi->hStdOutput = MyOpenFile(out, cMode, WRITE);

	}

	if (err != NULL) {
//		printf("err %s\n", err);

		if (mode == 2) {
			cMode = OPEN_ALWAYS;
		}

		psi->hStdError = MyOpenFile(err, cMode, WRITE);
	}
}

void open_files(char *in, char *out, char *err) {
	HANDLE hFile;

	if (in != NULL) {
		hFile = MyOpenFile(in, CREATE_ALWAYS, READ);
		CloseHandle(hFile);
	}

	if (out != NULL) {
		hFile = MyOpenFile(out, CREATE_ALWAYS, WRITE);
		CloseHandle(hFile);

	}

	if (err != NULL) {
		hFile = MyOpenFile(err, CREATE_ALWAYS, WRITE);
		CloseHandle(hFile);
	}
}