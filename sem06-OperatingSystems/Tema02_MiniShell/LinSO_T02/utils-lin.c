/**
 * Operating Sytems 2013 - Assignment 2
 *
 */

#include <assert.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

#include <fcntl.h>
#include <unistd.h>

#include "utils.h"

#define READ		0
#define WRITE		1

#define CHDIR 		-1
#define SIMPLE 		1

/**
 * 	Internal change-directory command.
 */
static bool shell_cd(word_t *dir)
{
	/* execute cd */
	return chdir(dir->string);
}

/**
 * Internal exit/quit command.
 */
static int shell_exit()
{
	/* execute exit/quit */
	return SHELL_EXIT;
}

/**
 * Concatenate parts of the word to obtain the command
 */
static char *get_word(word_t *s)
{
	int string_length = 0;
	int substring_length = 0;

	char *string = NULL;
	char *substring = NULL;

	while (s != NULL) {
		substring = strdup(s->string);

		if (substring == NULL) {
			return NULL;
		}

		if (s->expand == true) {
			char *aux = substring;
			substring = getenv(substring);

			/* prevents strlen from failing */
			if (substring == NULL) {
				substring = calloc(1, sizeof(char));
				if (substring == NULL) {
					free(aux);
					return NULL;
				}
			}

			free(aux);
		}

		substring_length = strlen(substring);

		string = realloc(string, string_length + substring_length + 1);
		if (string == NULL) {
			if (substring != NULL)
				free(substring);
			return NULL;
		}

		memset(string + string_length, 0, substring_length + 1);

		strcat(string, substring);
		string_length += substring_length;

		if (s->expand == false) {
			free(substring);
		}

		s = s->next_part;
	}

	return string;
}

/**
 * Concatenate command arguments in a NULL terminated list in order to pass
 * them directly to execv.
 */
static char **get_argv(simple_command_t *command, int *size)
{
	char **argv;
	word_t *param;

	int argc = 0;
	argv = calloc(argc + 1, sizeof(char *));
	assert(argv != NULL);

	argv[argc] = get_word(command->verb);
	assert(argv[argc] != NULL);

	argc++;

	param = command->params;
	while (param != NULL) {
		argv = realloc(argv, (argc + 1) * sizeof(char *));
		assert(argv != NULL);

		argv[argc] = get_word(param);
		assert(argv[argc] != NULL);

		param = param->next_word;
		argc++;
	}

	argv = realloc(argv, (argc + 1) * sizeof(char *));
	assert(argv != NULL);

	argv[argc] = NULL;
	*size = argc;

	return argv;
}

/* test if the command is a variable assignment, and
 * if it is, set it */
static bool set_env(char *command) 
{
	char *key = strtok(command, "=");
	char *value = strtok(NULL, "=");

	if (key != NULL && value != NULL) {
		setenv(key, value, 1);
		return true;
	}
	return false;
}

/**
 * Parse a simple command (internal, environment variable assignment,
 * external command).
 */
static int parse_simple(simple_command_t *s, int level, command_t *father)
{
	/* sanity checks */
	if (s == NULL) return write_error("NULL command: simple_command");

	char *command = get_word(s->verb);

	/* if builtin command, execute the command */
	if ((strcmp(command, "exit") == 0) || (strcmp(command, "quit") == 0)) {
		return shell_exit();
	}

	// get the redirection files
	char *input = get_word(s->in);
	char *output = get_word(s->out);
	char *error = get_word(s->err);

	if (strcmp(command, "cd") == 0) {
		/* redirect standard output/error if needed */	
		redirect(input, output, error, s->io_flags, CHDIR);
		FREE(input, output, error);
		return shell_cd(s->params);
	}

	/* if variable assignment, execute the assignment and 
	 * return the exit status */
	if (set_env(command)) {
		return 0;
	}

	/* if external command */
	pid_t pid, wait_ret;
	int status, i;

	int argc = 0;
	char **argv = get_argv(s, &argc);

	switch (pid = fork()) {
	case -1:
		/* error */
		return write_error("fork");

	case 0:
		/* redirect standard output/error if needed */
		redirect(input, output, error, s->io_flags, SIMPLE);
		FREE(input, output, error);

		/* child process */
		execvp(command, argv);

		/* if failed */
		exit(write_error(command));

		for (i = 0; i < argc; i++) free(argv[i]);
		free(argv);
		free(command);

	default:
		/* parent process */
		wait_ret = waitpid(pid, &status, 0);
		if(wait_ret < 0) return write_error("waitpid");

		for (i = 0; i < argc; i++) free(argv[i]);
		free(argv);
		free(command);

		return status;
	}
}

/**
 * Process two commands in parallel, by creating two children.
 */
static bool do_in_parallel(command_t *cmd1, command_t *cmd2, int level, command_t *father)
{
	if (cmd1 == NULL) return write_error("NULL command: parallel 1");
	if (cmd2 == NULL) return write_error("NULL command: parallel 2");

	pid_t pid1, pid2;
	int wait_ret1, status1;
	int wait_ret2, status2;

	/* execute cmd1 and cmd2 simultaneously */
	switch(pid1 = fork()) {
	case 0:
		exit(parse_command(cmd1, level, father));
	default:
		switch(pid2 = fork()) {
		case 0:
			exit(parse_command(cmd2, level, father));
		default:
			wait_ret1 = waitpid(pid1, &status1, 0);
			if(wait_ret1 < 0) return write_error("waitpid-1");

			wait_ret2 = waitpid(pid2, &status2, 0);
			if(wait_ret2 < 0) return write_error("waitpid-2");

			return status1 || status2;
		}
	}
}

/**
 * Run commands by creating an anonymous pipe (cmd1 | cmd2)
 */
static bool do_on_pipe(command_t *cmd1, command_t *cmd2, int level, command_t *father)
{
	if (cmd1 == NULL) return write_error("NULL command: pipe 1");
	if (cmd2 == NULL) return write_error("NULL command: pipe 2");

	int ret;
	int pipefd[2];

	pid_t pid1, pid2;
	int wait_ret1, status1;
	int wait_ret2, status2;
	int ret1, ret2;

	/* redirect the output of cmd1 to the input of cmd2 */
	ret = pipe(pipefd);
	if(ret == -1) return write_error("pipe-pipefd");
	
	switch(pid1 = fork()) {
	case 0:
		close(pipefd[READ]);
		dup2(pipefd[WRITE], STDOUT_FILENO);
		ret1 = parse_command(cmd1, level, father);
		close(pipefd[WRITE]);
		exit(ret1);
		
	default:
		switch(pid2 = fork()) {
		case 0:	
			close(pipefd[WRITE]);
			dup2(pipefd[READ], STDIN_FILENO);
			ret2 = parse_command(cmd2, level, father);
			close(pipefd[READ]);
			exit(ret2);

		default:
			close(pipefd[READ]);
			close(pipefd[WRITE]);

			wait_ret1 = waitpid(pid1, &status1, 0);
			if(wait_ret1 < 0) return write_error("waitpid-1-pipe");
			
			wait_ret2 = waitpid(pid2, &status2, 0);
			if(wait_ret2 < 0) return write_error("waitpid-2-pipe");
			
			return status2;
		}
	}

}

/**
 * Parse and execute a command.
 */
int parse_command(command_t *c, int level, command_t *father)
{
	/* sanity checks */
	if(c == NULL) return write_error("NULL command: parse_command");

	int ret = 0;

	if (c->op == OP_NONE) {
		/* execute a simple command */
		return parse_simple(c->scmd, level, c);
	}

	switch (c->op) {
	case OP_SEQUENTIAL:
		/* ; - execute the commands one after the other */
		parse_command(c->cmd1, level, c);
		return parse_command(c->cmd2, level, c);

	case OP_PARALLEL:
		/* & - execute the commands simultaneously */
		return do_in_parallel(c->cmd1, c->cmd2, level, c);

	case OP_CONDITIONAL_NZERO:
		/* || - execute the second command only if 
		 *      the first one returns non zero */
		switch(parse_command(c->cmd1, level, c)) {
		case 0:
			return 0;
		default:
			return parse_command(c->cmd2, level, c);
		}

	case OP_CONDITIONAL_ZERO:
		/* && - execute the second command only if 
		 *      the first one returns zero */
		switch(ret = parse_command(c->cmd1, level, c)) {
		case 0:
			return parse_command(c->cmd2, level, c);
		default:
			return ret;
		}
		break;

	case OP_PIPE:
		/* | - redirect the output of the first command 
		 *     to the input of the second */
		return do_on_pipe(c->cmd1, c->cmd2, level + 1, c);

	default:
		assert(false);
	}

	return -1;	// nothing has been done;
}


/**
 * Readline from mini-shell.
 */
char *read_line()
{
	char *instr;
	char *chunk;
	char *ret;

	int instr_length;
	int chunk_length;

	int endline = 0;

	instr = NULL;
	instr_length = 0;

	chunk = calloc(CHUNK_SIZE, sizeof(char));
	if (chunk == NULL) {
		fprintf(stderr, ERR_ALLOCATION);
		return instr;
	}

	while (!endline) {
		ret = fgets(chunk, CHUNK_SIZE, stdin);
		if (ret == NULL) {
			break;
		}

		chunk_length = strlen(chunk);
		if (chunk[chunk_length - 1] == '\n') {
			chunk[chunk_length - 1] = 0;
			endline = 1;
		}

		ret = instr;
		instr = realloc(instr, instr_length + CHUNK_SIZE + 1);
		if (instr == NULL) {
			free(ret);
			return instr;
		}
		memset(instr + instr_length, 0, CHUNK_SIZE);
		strcat(instr, chunk);
		instr_length += chunk_length;
	}

	free(chunk);

	return instr;
}


/**
 *	Write an error message and return -1;
 *	The message is written at STDERR
 */
int write_error(char *cmd_name)
{
	char *str;
	str = strdup("Execution failed for '");
	strcat(str, cmd_name);
	strcat(str, "'\n");

	write(STDERR_FILENO, str, strlen(str));

//	free(str);
	return -1;
}


/*
 * @filedes  - file descriptor to be redirected
 * @filename - filename used for redirection
 * @flags    - how to open the file (APPEND, READ, TRUNC etc.)
 * @type     - the type of redirection: 
 *   -1/0: none    - no redirection (for chdir) 
 *      1: normal  - redirect just the filedes
 *      2: mutiple - redirect the filedes and STDERR
 */
int redirect_file(int filedes, char *filename, int flags, int type)
{
	int ret, fd;

	/* Redirect filedes into file filename */
	fd = open(filename, flags, 0644);
	if(fd < 0) return write_error("REDIRECT: open");

	if (type > 0) {
		ret = dup2(fd, filedes);
		if(ret < 0) return write_error("REDIRECT: dup2");
	}

	if (type == 2) {
		ret = dup2(fd, STDERR_FILENO);
		if(ret < 0) return write_error("REDIRECT: dup2 error");
	}

	close(fd);
	return 0;
}


/*
 *	Redirects stdin, stdout and stderr to the given file(names), if not NULL;
 *	Mode describes which flags to use when opening the files
 */
int redirect(char *input, char *output, char *error, int mode, int type)
{
	int ret = 0;
	/* Find the flags */
	int flags = O_WRONLY | O_CREAT;
	(mode == 0) ? (flags |= O_TRUNC) : (flags |= O_APPEND);

	if (input != NULL) {
		ret += redirect_file(STDIN_FILENO, input, O_RDONLY, type);
	}

	if (output != NULL) {
		if (error != NULL && strcmp (output, error) == 0) {
			ret += redirect_file(STDOUT_FILENO, output, flags, type + 1);
			return 0;
		}
		ret += redirect_file(STDOUT_FILENO, output, flags, type);
	}

	if (error != NULL) {
		ret += redirect_file(STDERR_FILENO, error, flags, type);
	}

	return ret;	/* will be 0 or less (by adding multiple -1s); */
}
