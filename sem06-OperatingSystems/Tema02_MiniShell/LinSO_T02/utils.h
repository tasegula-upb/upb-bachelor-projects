/**
 * Operating Sytems 2013 - Assignment 2
 *
 */

#ifndef _UTILS_H
#define _UTILS_H

#include <stdio.h>
#include <stdlib.h>

#include "parser.h"

#define CHUNK_SIZE 100
#define ERR_ALLOCATION "unable to allocate memory"

#define SHELL_EXIT -100

/**
 * 	Readline from mini-shell.
 */
char *read_line();

/**
 * 	Parse and execute a command.
 */
int parse_command(command_t *, int, command_t *);


/* ------------------------------------------------------------------------- */
/* 							MY METHODS										 */
/* ------------------------------------------------------------------------- */


/**
 *	Write an error message to the given file
 */
int write_error(char *cmd_name);


/*
 * @filedes  - file descriptor to be redirected
 * @filename - filename used for redirection
 * @flags    - how to open the file (APPEND, READ, TRUNC etc.)
 * @type     - the type of redirection: 
 *   -1/0: none    - no redirection (for chdir) 
 *      1: normal  - redirect just the filedes
 *      2: mutiple - redirect the filedes and STDERR
 */
int redirect_file(int filedes, char *filename, int flags, int type);


/*
 *	Redirects stdin, stdout and stderr to the given file(names), if not NULL;
 *	Mode describes which flags to use when opening the files;
 *	Type will be send to redirect_file
 */
int redirect(char *input, char *output, char *error, int mode, int type);


#define REDIRECT(input, output, error, mode)					\
	if (input != NULL) {										\
		redirect_input(STDIN_FILENO, input);					\
	}															\
																\
	if (output != NULL && error != NULL) {						\
		if (error != NULL && strcmp (output, error) == 0) {		\
			redirect_outerr(output, mode);						\
		}														\
	}															\
	else {														\
		if (output != NULL) {									\
			redirect_file(STDOUT_FILENO, output, mode);			\
		}														\
		if (error != NULL) {									\
			redirect_file(STDERR_FILENO, error, mode);			\
		}														\
	}


#define FREE(input, output, error) free(input); free(output); free(error);


#endif
