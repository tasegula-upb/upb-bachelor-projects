#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <sys/sendfile.h>

#include <netinet/in.h>
#include <arpa/inet.h>

#include "aws.h"
#include "util.h"
#include "debug.h"
#include "sock_util.h"
#include "w_epoll.h"
#include "http_parser.h"

#define VSIZE 1024
#define STATIC 0
#define DYNAMIC 1

/* server socket file descriptor */
static int listenfd;

/* epoll file descriptor */
static int epollfd;

enum connection_state {
	STATE_DATA_RECEIVED,
	STATE_DATA_SENT,
	STATE_CONNECTION_CLOSED
};

/* structure acting as a connection handler */
typedef struct connection {
	int sockfd;

	char request[BUFSIZ];	// request received
	int filefd;		// file descriptor
	int request_type;	// request type
	int ok;

	/* buffers used for receiving messages and then echoing them back */
	char recv_buffer[BUFSIZ];
	size_t recv_len;
	char send_buffer[BUFSIZ];
	size_t send_len;
	char resp_buffer[BUFSIZ];
	size_t resp_len;

	enum connection_state state;
} connection;


/*
 * Http request parser
 */
static int parse_request(connection* conn, int flags)
{
	struct http_parser_url *url;
	char *token;

	if (strstr(conn->recv_buffer, "\n") == NULL) {
		if (conn->ok == 0) {
			strcat(conn->request, conn->recv_buffer);
			printf("PARSE: %s\n", conn->request);
			return -1;
		}
	}
	else {
		if (conn->ok == 0) {
			strcat(conn->request, conn->recv_buffer);
			conn->ok = 1;
		}
		else {
			return 0;
		}
	}

	url = malloc(sizeof(struct http_parser_url));
	http_parser_parse_url(conn->request, strlen(conn->request), 0, url);
	token = strtok(conn->request + url->field_data[0].len, " \n");

	if (strstr(token, "static/")) {
		conn->request_type = STATIC;
	}
	else {
		conn->request_type = DYNAMIC;
	}

	conn->filefd = open(token + 1, O_RDONLY, 0666);
	printf("URL: %s - %d\n", token, conn->filefd);
	return 0;
}


static void set_non_blocking(int sockfd)
{
	int flags = fcntl(sockfd, F_GETFL, 0);
	if (flags < 0) {
		flags = O_NONBLOCK;
	}
	else {
		flags |= O_NONBLOCK;
	}

	fcntl(sockfd, F_SETFL, flags);
}


/*
 * Initialize connection structure on given socket.
 */
static struct connection *connection_create(int sockfd)
{
	struct connection *conn = malloc(sizeof(*conn));
	DIE(conn == NULL, "malloc");

	conn->sockfd = sockfd;
	memset(conn->recv_buffer, 0, BUFSIZ);
	memset(conn->send_buffer, 0, BUFSIZ);
	memset(conn->request, 0, BUFSIZ);
	memset(conn->resp_buffer, 0, BUFSIZ);
	conn->ok = 0;

	return conn;
}


/*
 * Copy receive buffer to send buffer (echo).
 */
static void connection_copy_buffers(struct connection *conn)
{
	conn->send_len = conn->recv_len;
	memcpy(conn->send_buffer, conn->recv_buffer, conn->send_len);
}


/*
 * Remove connection handler.
 */
static void connection_remove(struct connection *conn)
{
	close(conn->sockfd);
	conn->state = STATE_CONNECTION_CLOSED;
	free(conn);
}


/*
 * Handle a new connection request on the server socket.
 */
static void handle_new_connection(void)
{
	static int sockfd;
	socklen_t addrlen = sizeof(struct sockaddr_in);
	struct sockaddr_in addr;
	struct connection *conn;
	int rc;

	/* accept new connection */
	sockfd = accept(listenfd, (SSA *) &addr, &addrlen);
	DIE(sockfd < 0, "accept");
	set_non_blocking(sockfd);

	dlog(LOG_ERR, "Accepted connection from: %s:%d\n", inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));

	/* instantiate new connection handler */
	conn = connection_create(sockfd);

	/* add socket to epoll */
	rc = w_epoll_add_ptr_in(epollfd, sockfd, conn);
	DIE(rc < 0, "w_epoll_add_in");
}


/*
 * Receive message on socket.
 * Store message in recv_buffer in struct connection.
 */
static enum connection_state receive_message(struct connection *conn)
{
	ssize_t bytes_recv;
	int rc;
	char abuffer[64];

	rc = get_peer_address(conn->sockfd, abuffer, 64);
	if (rc < 0) {
		ERR("get_peer_address");
		goto remove_connection;
	}

	do {
		bytes_recv = recv(conn->sockfd, conn->recv_buffer, BUFSIZ, 0);

		if (bytes_recv < 0) {		/* error in communication */
			dlog(LOG_ERR, "Error in communication from: %s\n", abuffer);
			goto remove_connection;
		}
		if (bytes_recv == 0) {		/* connection closed */
			dlog(LOG_INFO, "Connection closed from: %s\n", abuffer);
			goto remove_connection;
		}

		dlog(LOG_DEBUG, "Received message from: %s\n", abuffer);

		printf("---===\n%s===---\n", conn->recv_buffer);
	} while (parse_request(conn, bytes_recv) != 0);
	printf("out of while\n----------------------------\n----------------------------\n");


	conn->recv_len = bytes_recv;
	conn->state = STATE_DATA_RECEIVED;

	return STATE_DATA_RECEIVED;

remove_connection:
	rc = w_epoll_remove_ptr(epollfd, conn->sockfd, conn);
	DIE(rc < 0, "w_epoll_remove_ptr");

	/* remove current connection */
	connection_remove(conn);

	return STATE_CONNECTION_CLOSED;
}


/*
 * Send message on socket.
 * Store message in send_buffer in struct connection.
 */
static enum connection_state send_message(struct connection *conn)
{
	ssize_t bytes_sent;
	int rc;
	char abuffer[64];
	struct stat aux_stat;
	char send_buf[100];

	if (conn->filefd < 0)
		strcpy(send_buf, "HTTP/1.0 404 Not Found\r\n\r\n");
	else
		strcpy(send_buf, "HTTP/1.0 200 OK\r\n\r\n");

	rc = get_peer_address(conn->sockfd, abuffer, 64);
	if (rc < 0) {
		ERR("get_peer_address");
		goto remove_connection;
	}

	// header response
	int total_sent = 0;
	while (total_sent < strlen(send_buf)) {
		bytes_sent = send(conn->sockfd, send_buf + total_sent, strlen(send_buf) - total_sent, 0);

		if (bytes_sent < 0) {		/* error in communication */
			dlog(LOG_ERR, "Error in communication to %s\n", abuffer);
			goto remove_connection;
		}
		if (bytes_sent == 0) {		/* connection closed */
			dlog(LOG_INFO, "Connection closed to %s\n", abuffer);
			goto remove_connection;
		}

		total_sent += bytes_sent;
	}


	dlog(LOG_DEBUG, "Sending message to %s\n", abuffer);

	if (conn->filefd > 0) {
		fstat(conn->filefd, &aux_stat);

		if (conn->request_type == STATIC) {
			// static request
			total_sent = 0;

			while (total_sent <= aux_stat.st_size) {
				bytes_sent = sendfile(conn->sockfd, conn->filefd, NULL, aux_stat.st_size);
				if (bytes_sent > 0) {
					total_sent += bytes_sent;
				}
				else {
					break;
				}
				printf("sended: %d / %d\n", (int)total_sent, (int)aux_stat.st_size);
			}
			printf("out of while\n");
		}
		else {

		}
		close(conn->filefd);
	}
	else {
		goto remove_connection;
	}


	printf("--\n%s--\n", conn->send_buffer);

	/* all done - remove out notification */
	rc = w_epoll_update_ptr_in(epollfd, conn->sockfd, conn);
	DIE(rc < 0, "w_epoll_update_ptr_in");

	conn->state = STATE_DATA_SENT;

remove_connection:
	rc = w_epoll_remove_ptr(epollfd, conn->sockfd, conn);
	DIE(rc < 0, "w_epoll_remove_ptr");

	/* remove current connection */
	connection_remove(conn);

	return STATE_CONNECTION_CLOSED;
}


/*
 * Handle a client request on a client connection.
 */
static void handle_client_request(struct connection *conn)
{
	int rc;
	enum connection_state ret_state;

	ret_state = receive_message(conn);
	if (ret_state == STATE_CONNECTION_CLOSED)
		return;

	connection_copy_buffers(conn);

	/* add socket to epoll for out events */
	rc = w_epoll_update_ptr_inout(epollfd, conn->sockfd, conn);
	DIE(rc < 0, "w_epoll_add_ptr_inout");
}


int main(void)
{
	int rc;

	/* Init multiplexing */
	epollfd = w_epoll_create();
	DIE(epollfd < 0, "w_epoll_create");

	/* Create server socket */
	listenfd = tcp_create_listener(AWS_LISTEN_PORT, DEFAULT_LISTEN_BACKLOG);
	DIE(listenfd < 0, "tcp_create_listener");
	set_non_blocking(listenfd);

	rc = w_epoll_add_fd_in(epollfd, listenfd);
	DIE(rc < 0, "w_epoll_add_fd_in");

	dlog(LOG_INFO, "Server waiting for connections on port %d\n", AWS_LISTEN_PORT);

	/* Server main loop */
	while (1) {
		struct epoll_event rev;

		/* Wait for events */
		rc = w_epoll_wait_infinite(epollfd, &rev);
		DIE(rc < 0, "w_epoll_wait_infinite");

		/*
		 * Switch event types; consider
		 *   - new connection requests (on server socket)
		 *   - socket communication (on connection sockets)
		 */
		if (rev.data.fd == listenfd) {
			dlog(LOG_DEBUG, "New connection\n");
			if (rev.events & EPOLLIN)
				handle_new_connection();
		}
		else {
			if (rev.events & EPOLLIN) {
				dlog(LOG_DEBUG, "New message\n");
				handle_client_request(rev.data.ptr);
			}
			if (rev.events & EPOLLOUT) {
				dlog(LOG_DEBUG, "Ready to send message\n");
				send_message(rev.data.ptr);
			}
		}
	}

	return 0;
}