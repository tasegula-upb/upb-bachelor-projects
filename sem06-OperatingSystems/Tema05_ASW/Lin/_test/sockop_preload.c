#define _GNU_SOURCE
#include <dlfcn.h>
#include <sys/types.h>
#include <unistd.h>

ssize_t (*orig_send)(int sockfd, const void *buf, size_t len, int flags);
ssize_t (*orig_recv)(int sockfd, void *buf, size_t len, int flags);

ssize_t send(int sockfd, const void *buf, size_t len, int flags)
{
	if (len > 1) {
		len /= 2;
	}

	return orig_send(sockfd, buf, len, flags);
}

ssize_t recv(int sockfd, void *buf, size_t len, int flags)
{
	if (len > 20)
		len = 20;
	else if (len > 1) {
		len /= 2;
	}

	return orig_recv(sockfd, buf, len, flags);
}

void _init(void)
{
	orig_send = dlsym(RTLD_NEXT, "send");
	orig_recv = dlsym(RTLD_NEXT, "recv");
}
