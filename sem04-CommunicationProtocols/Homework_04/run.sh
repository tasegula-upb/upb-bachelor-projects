#!/bin/bash

make clean
make build
./my_dns_client google.com A
./my_dns_client google.com MX
./my_dns_client google.com SOA
./my_dns_client google.com TXT
./my_dns_client google.com NS
./my_dns_client google.com CNAME
./my_dns_client www.facebook.com CNAME
./my_dns_client www.youtube.com	NS
./my_dns_client 9gag.com A
./my_dns_client feesh-sticks.blogspot.ro A
./my_dns_client feesh-sticks.blogspot.ro MX
./my_dns_client feesh-sticks.blogspot.ro SOA
./my_dns_client feesh-sticks.blogspot.ro TXT
./my_dns_client feesh-sticks.blogspot.ro NS
./my_dns_client feesh-sticks.blogspot.ro CNAME
