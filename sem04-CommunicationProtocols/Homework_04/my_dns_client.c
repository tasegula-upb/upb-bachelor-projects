/*
 * GULA TANASE, 324CA
 *
 * PROTOCOALE DE COMUNICATIE. TEMA 4.
 */
 
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include<sys/select.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<sys/time.h>
#include<arpa/inet.h>
#include<netinet/in.h>
#include<fcntl.h>
#include<unistd.h>
#include<netdb.h>

#include"dns_message.h"

#define BUFLEN 1024

void error(char *m) {
	perror(m);
	exit(0);
}

void make_qname(unsigned char *site, unsigned char *qname) {
	char *contor = (char*) qname;
	char *tok = strtok(site, ".");

	int length = 0;
	int dim = 0;

	memset(contor, 0, strlen(site) + 2);

	while (tok != NULL) {
		dim = strlen(tok);
		*contor = dim;
		length += 1;

		contor = (char *) &qname[length];
		strcpy(contor, tok);
		length += strlen(tok);

		contor = (char *) &qname[length];
		tok = strtok(NULL, ".");
	}

	*contor = 0;
	qname[length + 1] = '\0';
}

unsigned char* make_msg(unsigned char *site, unsigned char *type) {
	int length = sizeof(dns_header_t) + sizeof(dns_question_t) + strlen(site) + 3;

	unsigned char *qname = (unsigned char *)malloc(strlen(site) + 3);
	unsigned char *buffer = (unsigned char *)malloc(length * sizeof(unsigned char));

	dns_header_t *header;
	dns_question_t *ques;

	header = (dns_header_t*) &buffer[0];
	header->id = htons(getpid());
	header->rd = 1;
	header->qdcount = htons(1);

	qname = (unsigned char*) &buffer[sizeof(dns_header_t)];

	make_qname(site, qname);

	ques = (dns_question_t*) &buffer[sizeof(dns_header_t) + strlen(qname) + 1];
	ques->qtype = htons(get_type(type));
	ques->qclass = htons(1);

	return buffer;
}

int get_type(unsigned char *t) {
	if (strcmp(t, "A") == 0) return A;
	if (strcmp(t, "NS") == 0) return NS;
	if (strcmp(t, "CNAME") == 0) return CNAME;
	if (strcmp(t, "SOA") == 0) return SOA;
	if (strcmp(t, "MX") == 0) return MX;
	if (strcmp(t, "TXT") == 0) return TXT;
}

// creaza din buffer, incepand de la pozitia resp, site-ul rname
// intoarce pozitia de unde se termina site-ul
int make_rname(unsigned char *buffer, unsigned char *resp, unsigned char *rname) {
	int count = 1;
	int pos = 0;
	int offset;
	int jumped = 0; 	// valoari booleene, 0 = false, 1 = true
	int i, j;

	rname[0] = '\0';

	while (*resp != 0) {
		// 192 = 1100 0000 0000 0000
		// valoarea se scade din offset
		if (*resp >= 192) {
			offset = (*resp) * (1<<8) + *(resp + 1)	- (((1<<16) - 1) - ((1<<14) -1));
			resp = buffer + offset - 1;
			jumped++;	// nu mai creste count-ul
		}
		else {
			rname[pos++] = *resp;
		}

		// trecerea la urmatorul caracter
		resp += 1;

		if ( !jumped ) {
			count++;
		}
	}

	// sfarsit de sir
	rname[pos] = '\0';	
	if (jumped) {
		count++;
	}

	// conversia din "3www" in "www."
	for (i = 0; i < (int)strlen((const char*)rname); ++i) {
		pos = rname[i];
		for (j = 0; j < (int)pos; ++j) {
			rname[i] = rname[i + 1];
			++i;
		}
		rname[i] = '.';
	}
	rname[i] = '\0';

	return count;
}

void process_rdata(int tip, int rdlen, unsigned char *recvbuf, unsigned char *resp, unsigned char *rname, FILE *file, int length) {
	int x;

	if (tip == SOA) {
		// printam in fisier NAME-ul primit
		fprintf(file, "%s IN SOA ", rname);

		memset(rname, 0, 256);
		x = make_rname(recvbuf, resp, rname);
		fprintf(file, "%s ", rname);

		// repozitionam "cursoru" in RDATA / recvbuffer
		resp += x;

		memset(rname, 0, 256);
		x = make_rname(recvbuf, resp, rname);
		fprintf(file, "%s ", rname);

		resp += x;

		// aflam numerele: Serial Refresh Retry Expiration Minimum
		int aux = 0;
		int i, j;
		for (i = 0; i < 5; i++) {
			for (j = 0; j < 5; j++) {
				aux *= 256;
				aux += resp[j];
			}
			aux /= 256;
			fprintf(file, "%d\t", aux);
			aux = 0;
			resp += 4;
		}

		fprintf(file, "\n");

		return;
	}

	if (tip == A) {
		// aflam ip-ul
		unsigned char *ip = (unsigned char *)malloc(20 * sizeof(char));
		sprintf(ip, "%d.%d.%d.%d", resp[0], resp[1], resp[2], resp[3]);
		
		fprintf(file, "%s IN A %s\n", rname, ip);
		
		return;
	}

	if (tip == MX) {
		// aflam preference-ul
		unsigned short pref;
		pref = resp[0] * 256 + resp[1];

		// repozitionam "cursorul" in RDATA / recvbuffer
		resp += 2;
		
		// printam in fisier NAME-ul primit si PREFERENCE-ul
		fprintf(file, "%s IN MX %d ", rname, pref);
		
		// aflam numele serverului de mail si il printam in fisier
		memset(rname, 0, 256);
		x = make_rname(recvbuf, resp, rname);
		fprintf(file, "%s\n", rname);
		
		return;
	}

	if (tip == TXT) {
		// trecem peste primul octet
		resp +=1;

		char *txt = (unsigned char*)malloc((rdlen + 1) * sizeof(char));
		memcpy(txt, resp, rdlen);

		fprintf(file, "%s IN TXT %s\n", rname, resp);

		return;
	}

	if (tip == NS) {
		fprintf(file, "%s IN NS ", rname);

		// aflam numele DNS-ului si il printam in fisier
		memset(rname, 0, 256);
		x = make_rname(recvbuf, resp, rname);
		fprintf(file, "%s\n", rname);

		return;
	}		

	if (tip == CNAME) {
		fprintf(file, "%s IN CNAME ", rname);

		// aflam numele canonic si il printam in fisier
		memset(rname, 0, 256);
		x = make_rname(recvbuf, resp, rname);
		fprintf(file, "%s\n", rname);

		return;
	}
}

int main(int argc, char *argv[]) {
	int length = 0;	// auxiliar pentru diverse lungimi de variabile
	int fd;			// file descriptor
	int sck;		// socket
	int listensck;	// socket pentru select
	int x;			// auxiliar

	// adresa in format "www." si tipul inerogarii
	char *site = (char *)malloc(strlen(argv[1]));
	strcpy(site, argv[1]);
	char *type = argv[2];

	// mesajul ce va fi transmis (buffer) si mesajul ce va fi primit (recvbuf)
	unsigned char *buffer;
	unsigned char *recvbuf = (unsigned char *)malloc(BUFLEN);

	struct sockaddr_in serv_addr;

	fd_set readfds;
	struct timeval t;

	int i, j, k;

	/*
	 * VERIFICARE MOD DE UTILIZARE
	 */
	if (argc != 3) {
		fprintf(stderr, "UTILIZARE:\n\t ./%s ADRESA COD_CERINTA\n", argv[0]);
		exit(0);
	}

	// deschidere socket
	sck = socket(AF_INET, SOCK_DGRAM, 0);

	/*
	 * CREARE INTEROGARE
	 */
	buffer = make_msg(site, type);

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(53);

	/*
	 * DESCHIDERE FISIER ADRESE DNS
	 */
	FILE *f = fopen("dns_servers.conf", "r");
	if (f == NULL) {
		fprintf(stderr, "EROARE @ deschiderea fisierului dns_servers.conf");
		exit(0);
	}

	/*
	 * TRIMITERE MESAJ LA DNS
	 */
	unsigned char *aux = (unsigned char *)malloc(100);
	memset(aux, 0, 100);
	while (fgets(aux, 100, f) != NULL) {
		if (aux[0] == '#' || aux[0] == '\n') {
			continue;
		}
		serv_addr.sin_addr.s_addr = inet_addr(aux);
		
		length = sizeof(dns_header_t) + sizeof(dns_question_t) + strlen(argv[1]) + 3;
		if (sendto(sck, buffer, length, 0, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) < 0) {
			printf("EROARE @ %s - trimitere\n", aux);
			continue;
		}

		// verifica daca avem timeout, caz in care returneaza eroare
		FD_ZERO(&readfds);
		FD_SET(sck, &readfds);

		t.tv_sec = 2;
		t.tv_usec = 0;

		if (select(sck + 1, &readfds, NULL, NULL, &t) <= 0) {
			printf("EROARE @ %s - select\n", aux);
			continue;
		}

		memset(recvbuf, 0, BUFLEN);
		x = sizeof(serv_addr);
		if (recvfrom(sck, recvbuf, BUFLEN, 0, (struct sockaddr*) &serv_addr, (socklen_t*) &x) < 0) {
			printf("EROARE @ %s - recvfrom\n", aux);
			continue;
		}

		aux[strlen(aux) - 1] = '\0';
		printf("RESPONSE RECEIVED from %s\n", aux);

		fclose(f);
		break;
	}

	/*
	 * PRELUCRARE RASPUNS PRIMIT
	 */
	dns_header_t *header;
	dns_question_t *ques;
	dns_rr_t *rr_resp;
	unsigned char *rname, *resp;
	int ancount, nscount, arcount;
	unsigned short tip, rdlen;

	// header = prima parte a raspunsului
	header = (dns_header_t*) &recvbuf[0];

	// preluam din header numarul de raspunsuri
	ancount = ntohs(header->ancount);
	nscount = ntohs(header->nscount);
	arcount = ntohs(header->arcount);

	// mutam "cursorul" din raspuns la NAME-ul primit
	length = sizeof(dns_header_t) + strlen(argv[1]) + 2 + sizeof(dns_question_t);

	// rname = numele retinut in structura dns_rr_t
	rname = (unsigned char *)calloc(256, sizeof(char));

	// deschide fisierul logfile
	FILE *file = fopen("logfile", "a");
	fprintf(file, "; %s - %s %s\n\n", aux, argv[1], argv[2]);

	/** 
	 ** INCEPE PRELUCRAREA MESAJULUI
	 **/

	/***
	 *** INCEPE PRELUCRAREA SECTIUNII ANSWER 
	 ***/
	for (i = 0; i < ancount; ++i) {
		if (i == 0) {
			fprintf(file, ";; ANSWER SECTION:\n");
		}
		
		// aflu NAME-ul (din structura dns_rr_t)
		resp = (unsigned char*) &recvbuf[length];
		x = make_rname(recvbuf, resp, rname);
		
		// incep citirea din structura dns_rr_t
		length += x;
		rr_resp = (dns_rr_t*) &recvbuf[length];

		// retin tipul si lungimea mesajului RDATA
		tip = ntohs(rr_resp->type);
		rdlen = ntohs(rr_resp->rdlength);

		// mutare "cursor" la RDATA
		length += sizeof(dns_rr_t) - x;
		resp = (unsigned char *) &recvbuf[length];
		
		// prelucrez RDATA
		process_rdata(tip, rdlen, recvbuf, resp, rname, file, length);

		// ducem "cursorul" la urmatoarea structura dns_rr_t
		length += rdlen;
	}

	/***
	 *** INCEPE PRELUCRAREA SECTIUNII AUTHORITY 
	 ***/
	for (i = 0; i < nscount; ++i) {
		if (i == 0) {
			fprintf(file, ";; AUTHORITY SECTION:\n");
		}
		
		// aflu NAME-ul (din structura dns_rr_t)
		resp = (unsigned char*) &recvbuf[length];
		x = make_rname(recvbuf, resp, rname);
		
		// incep citirea din structura dns_rr_t
		length += x;
		rr_resp = (dns_rr_t*) &recvbuf[length];

		// retin tipul si lungimea mesajului RDATA
		tip = ntohs(rr_resp->type);
		rdlen = ntohs(rr_resp->rdlength);

		// mutare "cursor" la RDATA
		length += sizeof(dns_rr_t) - x;
		resp = (unsigned char *) &recvbuf[length];
		
		// prelucrez RDATA
		process_rdata(tip, rdlen, recvbuf, resp, rname, file, length);

		// ducem "cursorul" la urmatoarea structura dns_rr_t
		length += rdlen;
	}

	/***
	 *** INCEPE PRELUCRAREA SECTIUNII ADDITIONAL
	 ***/
	for (i = 0; i < arcount; ++i) {
		if (i == 0) {
			fprintf(file, ";; ADDITIONAL SECTION:\n");
		}
		
		// aflu NAME-ul (din structura dns_rr_t)
		resp = (unsigned char*) &recvbuf[length];
		x = make_rname(recvbuf, resp, rname);
		
		// incep citirea din structura dns_rr_t
		length += x;
		rr_resp = (dns_rr_t*) &recvbuf[length];

		// retin tipul si lungimea mesajului RDATA
		tip = ntohs(rr_resp->type);
		rdlen = ntohs(rr_resp->rdlength);

		// mutare "cursor" la RDATA
		length += sizeof(dns_rr_t) - x;
		resp = (unsigned char *) &recvbuf[length];
		
		// prelucrez RDATA
		process_rdata(tip, rdlen, recvbuf, resp, rname, file, length);

		// ducem "cursorul" la urmatoarea structura dns_rr_t
		length += rdlen;
	}

	fprintf(file, "\n");
	fclose(file);
}
