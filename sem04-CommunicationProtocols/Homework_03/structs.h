#define BUFLEN 400
#define CL_NR 10

typedef struct {
	char name[255];				// nume
	struct in_addr ip;			// adresa
	unsigned short int port;	// port ascultare
	time_t connect_at;			// timpul conectarii
	double connect_time;		// durata conectarii
	int socket;
	int online;					// 0 = offline, 1 = online
} infoc;

typedef struct {
	char type[25];			// tipul mesajului
	char sender[255];		// numele clientului care trimite
	char receiver[255];		// numele clientului care primeste
	char payload[BUFLEN];
	unsigned short int len;
} msg;

ssize_t Readline(int sockd, void *vptr, size_t maxlen) {
	ssize_t n, rc;
	char c, *buffer;
	buffer = vptr;

	for (n = 1; n < maxlen; n++) {	
		if ((rc = read(sockd, &c, 1)) == 1) {
			if (c == '\n') {
				break;
			}
			*buffer++ = c;
		}
		else {
			if (rc == 0) {
				if (n == 1) {
					return 0;
				}
				else {
					break;
				}
			}
			else {
				if (errno == EINTR) {
					continue;
				}
				return -1;
			}
		}
	}

    *buffer = 0;
    return n;
}
