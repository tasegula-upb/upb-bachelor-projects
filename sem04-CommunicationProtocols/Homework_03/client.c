#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "structs.h"

void error(char *m) {
	perror(m);
	exit(0);
}

void init_timeval(struct timeval *t) {
	t->tv_sec = 1;
	t->tv_usec = 0;
}

void timestamp(char *date_time){
	time_t now = time(0);
	struct tm dtm = *localtime(&now);
	strftime(date_time, 25, "%d.%m.%Y, %X", &dtm);
}

void printlog(char *comm, char *p1, char *p2) {
	if (p1 == NULL) {
		fprintf(stderr, "Comanda %s a fost efectuata\n", comm, p1);
		return;
	}
	if (p2 == NULL) {
		fprintf(stderr, "Comanda %s a fost trimisa la %s\n", comm, p1);
		return;
	}
	fprintf(stderr, "Comanda %s %s %s a fost trimisa la server\n", comm, p1, p2);
}

int main(int argc, char *argv[]) {
	infoc client;
	infoc file_cl;
	infoc auxcl;
	msg message;
	
	struct sockaddr_in serv_addr, cl_addr, lst_addr;
	struct hostent *server;
	struct timeval t;
	
	fd_set read_fds;	// multimea de citire folosita in select()
	fd_set tmp_fds;		// multime folosita temporar
	
	char fhistory[275];		// nume fisier de history-log
	char file_recv[300];	// nume fisier de primit
	char msg_bcast[500];	// retine mesajul de broadcast
	char filename[100];
	char buffer[250];
	
	int sockfd, sockfd2, port;
//	int msg_2send = 0;	// numar mesaje de trimis
	int file_2send = 0;	// numar fisiere de trimis
	int timeout;		//
	int fdmax;			// valoare maxima file descriptor din read_fds
	int sp;				// port pe care asculta clientul
	int position;		// auxiliar pt parcurgere fisier
	int aux;			// pentru diverse conectari, socketi etc;
	int i;
	
	if (argc != 5) {
		fprintf(stderr, "UTILIZARE:\n\t./client <nume_client> <port_client> <ip_server> <port_server>\n");
	}
	
	FD_ZERO(&read_fds);
	FD_ZERO(&tmp_fds);

	// Conectare cu serverul
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) {
		error("ERROR 001: could not open socket");
	}
	
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(atoi(argv[4]));
	inet_aton(argv[3], &serv_addr.sin_addr);
	
	if (connect(sockfd, (struct sockaddr*) &serv_addr, 
				sizeof(serv_addr)) < 0) {
		error("ERROR 002: could not connect");
	}
	
	// Salvare informatii client
	strcpy(client.name, argv[1]);
	port = htons(atoi(argv[2]));
	//memcpy(&client.port, &port, sizeof(unsigned short int));
	client.port = port;
	client.online = 1;
	
	// Trimitere informatii client
	strcpy(message.type, "client");
	memcpy(message.payload, &client, sizeof(infoc));
	message.len = sizeof(infoc);
	send(sockfd, &message, sizeof(msg), 0);
	
	strcpy(message.sender, argv[1]);
	
	// REDO Ascultare pe port primit
	lst_addr.sin_family = AF_INET;
	lst_addr.sin_port = htons(atoi(argv[2]));
	lst_addr.sin_addr.s_addr = INADDR_ANY;
	
	sp = socket(AF_INET, SOCK_STREAM, 0);
	fdmax = sp;
	
	// REDO
	if (bind(sp, (struct sockaddr *) &lst_addr, sizeof(struct sockaddr)) < 0) {
		error ("ERROR 003: could not bind");
	}
	
	listen(sp, CL_NR);
	
	FD_SET(sp, &read_fds);
	FD_SET(0, &read_fds);
	FD_SET(sockfd, &read_fds);
	
	// Creare fisier history-log
	// Nu avem nevoie ca fisierul sa fie deschis
	strcpy(fhistory, argv[1]);
	strcat(fhistory, ".hist");
	int file = open(fhistory, O_CREAT|O_TRUNC, S_IRWXU);
	close(file);
	
	init_timeval(&t);

	while (1) {
		tmp_fds = read_fds;
		
		timeout = select(fdmax + 1, &tmp_fds, NULL, NULL, &t);
		if (timeout == -1) {
			error("ERROR 010: select");
		}
		
		// Trimitere eventuale fisiere (daca timeout-ul dupa select este 0)
		if (!timeout) {
			if (file_2send) {
				int f = open(filename, O_RDONLY);
				lseek(f, position, SEEK_SET);
				
				message.len = read(f, message.payload, BUFLEN);
				if (message.len > 0) {
					strcpy(message.type, "fsegment");
				
					struct sockaddr_in destination;
					destination.sin_family = AF_INET;
					destination.sin_addr = file_cl.ip;
					destination.sin_port = file_cl.port;
				
					aux = socket(AF_INET, SOCK_STREAM, 0);
					if (connect(aux, (struct sockaddr*) &destination, sizeof(destination)) < 0) {
						memset(message.payload, 0, BUFLEN);
						file_2send = 0;
						position = 0;
						init_timeval(&t);
						continue;
					}
				
					send(aux, &message, sizeof(msg), 0);
				
					position = lseek(f, 0, SEEK_CUR);
				
					close(f);
					close(aux);
				}
				else {
					memset(message.payload, 0, BUFLEN);
					file_2send = 0;
					position = 0;
				}
				
				init_timeval(&t);
			}
			else {
				init_timeval(&t);
			}
			continue;
		}
		
		/* -----------------------------------------------------------------
		 * pentru toti file descriptorii,verificam daca un client e conectat
		 * apoi verificam tipul mesajului in functie de tipul comenzii: 
		 * de la server, de la alt client sau comanda de la utilizator
		 * ----------------------------------------------------------------- */
		for (i = 0; i <= fdmax; i++) {
			if (FD_ISSET(i, &tmp_fds)) {
				
				// comanda utilizator: fileno(stdin) = 0;
				if (i == 0) {
					// citire de la tastatura
					memset(buffer, 0, BUFLEN);
					Readline(0, buffer, BUFLEN - 1);
					
					char *command, *p1, *p2;
					command = strtok(buffer, " ");
					p1 = strtok(NULL, " ");
					p2 = strtok(NULL, " ");

					strcpy(message.sender, argv[1]);

					if (!strcmp(command, "listclients")) {
						if (p1 != NULL) {
							fprintf(stderr, "UTILIZARE: listclients\n");
							continue;
						}
						
						// Trimitere mesaj: type = listclients
						// receiver = (none), payload = (none)
						strcpy(message.type, "listclients");
						send(sockfd, &message, sizeof(msg), 0);
						
						printlog(command, "", "");

						//primire mesaj de la server
						recv(sockfd, &message, sizeof(msg), 0);
						printf("%s", message.payload);
						
						printlog(message.type, NULL, "");
						continue;
					}
					
					if (!strcmp(command, "infoclient")) {
						if (p1 == NULL || p2 != NULL) {
							fprintf(stderr, "UTILIZARE: infoclient <nume_client>\n");
							continue;
						}
						
						// Trimitere mesaj: type = infoclient
						// receiver = (none)
						strcpy(message.type, "infoclient");
						// payload = <nume_client>
						memset(message.receiver, 0, BUFLEN);
						strcpy(message.receiver, p1);
						
						send(sockfd, &message, sizeof(msg), 0);
						printlog(command, p1, "");

						//primire mesaj de la server
						recv(sockfd, &message, sizeof(msg), 0);

						if (!strcmp(message.payload, "null")) {
							printf("Clientul %s nu este conectat in retea\n", p1);
						}
						else {
							printf("%s", message.payload);
						}

						printlog(command, NULL, "");
						continue;
					}
					
					if (!strcmp(command, "message")) {
						if (p1 == NULL || p2 == NULL) {
							fprintf(stderr, "UTILIZARE: message <nume_client> <mesaj>\n");
							continue;
						}
						
						char msgx[500];
						
						// Trimitere mesaj: type = message
						// payload = (none)
						strcpy(message.type, "message");
						// receiver = <nume_client>
						memset(message.receiver, 0, BUFLEN);
						strcpy(message.receiver, p1);
						
						// copiem mesajul in msg
						memset(msgx, 0, BUFLEN);
						strcpy(msgx, p2);
						strcat(msgx, " ");
						p2 = strtok(NULL, " ");
						while (p2 != NULL) {
							strcat(msgx, p2);
							strcat(msgx, " ");
							p2 = strtok(NULL, " ");
						}
						
						send(sockfd, &message, sizeof(msg), 0);
						printlog(command, p1, "");

						// Primire mesaj cu date client-destinatar
						// payload = informatii client
						recv(sockfd, &message, sizeof(msg), 0);
						memcpy(&auxcl, &message.payload, sizeof(infoc));
						
						if (auxcl.online) {
							struct sockaddr_in destination;
							destination.sin_family = AF_INET;
							destination.sin_addr = auxcl.ip;
							destination.sin_port = auxcl.port;
							
							int sck = socket(AF_INET, SOCK_STREAM, 0);
							
							aux = connect(sck, (struct sockaddr*) &destination, sizeof(destination));
							if (aux < 0) {
								error("ERROR 021: could not connect");
							}
							
							memset(message.payload, 0, BUFLEN);
							strcpy(message.payload, msgx);
							send(sck, &message, sizeof(msg), 0);
							close(sck);
						}
						else {
							printf("Clientul %s nu este conectat!\n", message.receiver);
						}						
						
						printlog(command, NULL, "");
						continue;
					}
					
					if (!strcmp(command, "broadcast")) {
						if (p1 == NULL) {
							fprintf(stderr, "UTILIZARE: broadcast <mesaj>\n");
							continue;
						}
						
						// Trimitere mesaj: type = broadcast
						// receiver = (none), payload = (none)
						strcpy(message.type, "broadcast");
						// copiem mesajul
						memset(msg_bcast, 0, BUFLEN);
						strcpy(msg_bcast, p1);
						strcat(msg_bcast, " ");
						
						if (p2 != NULL) {
							strcat(msg_bcast, p2);
						}

						p1 = strtok(NULL, " ");
						while (p1 != NULL) {
							strcat(msg_bcast, p1);
							strcat(msg_bcast, " ");
							p1 = strtok(NULL, " ");
						}
						
						send(sockfd, &message, sizeof(msg), 0);
						
						printlog(command, "", "");
						continue;
					}
					
					if (!strcmp(command, "sendfile")) {
						if (p1 == NULL || p2 == NULL) {
							fprintf(stderr, "UTILIZARE: sendfile <client_destinatie> <nume_fisier>\n");
							continue;
						}
						
						// Trimitere mesaj: type = sendfile
						// payload = (none)
						strcpy(message.type, "sendfile");
						// receiver = <client_destinatie>
						memset(message.receiver, 0, BUFLEN);
						strcpy(message.receiver, p1);
						// salvam numele fisierului
						memset(filename, 0, BUFLEN);
						strcpy(filename, p2);
						
						send(sockfd, &message, sizeof(msg), 0);
						
						// Primire mesaj cu date client-destinatar
						// payload = informatii client
						recv(sockfd, &message, sizeof(msg), 0);
						memcpy(&file_cl, &message.payload, sizeof(infoc));
						
						if (file_cl.online) {
							struct sockaddr_in destination;
							destination.sin_family = AF_INET;
							destination.sin_addr = file_cl.ip;
							destination.sin_port = file_cl.port;
							
							int sck = socket(AF_INET, SOCK_STREAM, 0);
							
							strcpy(message.payload, filename);
							
							aux = connect(sck, (struct sockaddr*) &destination, sizeof(destination));
							if (aux < 0) {
								error("ERROR 022: could not connect");
							}
							file_2send = 1;
							send(sck, &message, sizeof(msg), 0);
							close(sck);
						}
						else {
							printf("Clientul %s nu este conectat!\n", message.receiver);
						}
						
						printlog(command, p1, p2);
						continue;	
					}
					
					if (!strcmp(command, "history")) {
						if (p1 != NULL) {
							fprintf(stderr, "UTILIZARE: history\n");
							continue;
						}
						
						char line[BUFLEN];
						int file = open(fhistory, O_RDONLY);
						while (read(file, line, BUFLEN) > 0) {
							printf("%s", line);
						}
						close(file);
						
						printlog(command, "", "");
						continue;
					}
					
					if (!strcmp(command, "quit")) {
						if (p1 != NULL) {
							fprintf(stderr, "UTILIZARE: quit\n");
						continue;
						}
						
						// Trimitere mesaj: type = quit
						// receiver = (none), payload = (none)
						strcpy(message.type, "quit");
						
						send(sockfd, &message, sizeof(msg), 0);
						FD_ZERO(&read_fds);
						close(sockfd);
						close(sp);
						
						printlog(command, "", "");
						return 0;
					}
				}
					
				// de la server
				if (i == sockfd) {
					aux = recv(i, &message, sizeof(msg), 0);
					if (aux <= 0) {
						error("ERROR 030: receving message");
					}
					
					if (!strcmp(message.type, "kick")) {
						printf("KICK from server\n");
						FD_ZERO(&read_fds);
						close(sockfd);
						close(sp);
						return 0;
					}

					if (!strcmp(message.type, "serverout")) {
						printf("Server is out\n");
						FD_ZERO(&read_fds);
						close(sockfd);
						close(sp);
						return 0;
					}
					
					// verificam daca clientul era deja inregistrat de server
					// caz in care trebuie sa inchidem conexiunea
					if (!strcmp(message.type, "in_use")) {
						printf("username unavailable\n");
						FD_ZERO(&read_fds);
						close(sockfd);
						close(sp);
						return 0;
					}
					
					if (!strcmp(message.type, "broadcast")) {
						// Primire mesaj cu date client-destinatar
						// payload = informatii client
						memcpy(&auxcl, &message.payload, sizeof(infoc));
						
						struct sockaddr_in destination;
						destination.sin_family = AF_INET;
						destination.sin_addr = auxcl.ip;
						destination.sin_port = auxcl.port;
						
						int sck = socket(AF_INET, SOCK_STREAM, 0);
							
						aux = connect(sck, (struct sockaddr*) &destination, sizeof(destination));
						if (aux < 0) {
							error("ERROR 031: could not connect");
						}
						
						strcpy(message.type, "message");
						strcpy(message.payload, msg_bcast);
						send(sck, &message, sizeof(msg), 0);
						close(sck);
						continue;
					}
					
				}
				
				// de la alt client
				if (i == sp) {
					aux = sizeof(cl_addr);
					sockfd2 = accept(sp, (struct sockaddr*) &cl_addr, &aux);
					aux = recv(sockfd2, &message, sizeof(msg), 0);
					if (aux <= 0) {
						error("ERROR 040: receiving message");
					}
					
					if (!strcmp(message.type, "message")) {
						char msj[BUFLEN];
						char *date_time;
						date_time = (char *)malloc(50 * sizeof(char));
						timestamp(date_time);
						sprintf(msj, "[%s][%s] %s\n", 
							date_time, message.sender, message.payload);
						printf("%s", msj);
						
						// Scriem in fisierul history-log
						int file = open(fhistory, O_WRONLY | O_APPEND);
						write(file, msj, strlen(msj));
						close(file);
						close(sockfd2);

						printlog(message.type, message.receiver, NULL); 
						continue;
					}
					
					if (!strcmp(message.type, "sendfile")) {
						strcpy(file_recv, message.payload);
						strcat(file_recv, "_primit");
						
						int file = open(file_recv, O_CREAT|O_WRONLY|O_TRUNC, S_IRWXU);
						close(file);
						
						char msj[BUFLEN];
						char *date_time;
						date_time = (char *)malloc(50 * sizeof(char));
						timestamp(date_time);
						sprintf(msj, "[%s][%s] Fisier: %s\n", 
							date_time, message.sender, message.payload);
						printf("%s", msj);
						
						int hist = open(fhistory, O_WRONLY|O_APPEND);
						write(hist, msj, strlen(msj));
						close(hist);
						close(sockfd2);

						printlog(message.type, message.receiver, NULL);
						continue;
					}
					
					if (!strcmp(message.type, "fsegment")) {
						int file = open(file_recv, O_WRONLY|O_APPEND);
						write(file, message.payload, message.len);
						close(file);
						continue;
					}
				}				
			}
		}
		
	}
	
}
