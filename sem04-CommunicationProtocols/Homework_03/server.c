#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "structs.h"

void error(char *m) {
	perror(m);
	exit(1);
}

void timestamp(char *date_time){
	time_t now = time(0);
	struct tm dtm = *localtime(&now);
	strftime(date_time, 25, "%X", &dtm);
}

int main(int argc, char *argv[]) {
	infoc client[CL_NR];
	infoc auxcl;
	msg message;
	
	struct sockaddr_in serv_addr, cl_addr;
	
	fd_set read_fds;	// multimea de citire folosita in select()
	fd_set tmp_fds;		// multime folosita temporar
	
	time_t t;

	char buffer[100];
	
	int sockfd, sockfd2, port, clilen;
	int fdmax;			// valoare maxima file descriptor din read_fds
	int aux;			// pentru diverse conectari, socketi etc;
	int i, j;
	
	if (argc != 2) {
		fprintf(stderr, "UTILIZARE: ./server <port>\n");
	}
	
	// golim multimea de descriptori
	FD_ZERO(&read_fds);
	FD_ZERO(&tmp_fds);
	
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) {
		error("ERROR 101: opening socket");
	}
	
	port = atoi(argv[1]);
	
	memset((char *)&serv_addr, 0, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(port);
	
	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(struct sockaddr)) < 0) {
		error("ERROR 102: could not bind");
	}
	
	listen(sockfd, CL_NR);
	
	// adaugare file descriptor (socket pe care se asculta) in read_fds
	// adaugare descripti stdin: fileno(stdin) = 0
     FD_SET(sockfd, &read_fds);
     fdmax = sockfd;
     FD_SET(0, &read_fds);

     // initializare baza de date cu clienti
     for (i = 0; i < CL_NR; i++) {
     	client[i].online = 0;
     }

	 while (1) {
		tmp_fds = read_fds;
		
		aux = select(fdmax + 1, &tmp_fds, NULL, NULL, NULL);
		if (aux == -1) {
			error("ERROR 110: select");
		}
		
		/* -----------------------------------------------------------------
		 * pentru toti file descriptorii,verificam daca un client e conectat
		 * apoi verificam tipul mesajului in functie de tipul comenzii: 
		 * de la server, de la alt client sau comanda de la utilizator
		 * ----------------------------------------------------------------- */
		for (i = 0; i <= fdmax; i++) {

			if (FD_ISSET(i, &tmp_fds)) {
				// comanda utilizator: fileno(stdin) = 0;
				if (i == 0) {
					// citire de la tastatura
					memset(&buffer, 0, BUFLEN);
					Readline(0, buffer, BUFLEN-1);
					
					char *command, *p1;
					command = strtok(buffer, " ");
					p1 = strtok(NULL, " ");
					
					if (!strcmp(command, "status")) {
						if (p1 != NULL) {
							fprintf(stderr, "UTILIZARE: status\n");
						}
						for (j = 0; j < CL_NR; j++) {
							if (client[j].online) {
								printf("Client: %s\n\tip: %s\t\tport: %d\n", 
									client[j].name, inet_ntoa(client[j].ip), ntohs(client[j].port));
							}
						}
						continue;
					}
					
					if (!strcmp(command, "kick")) {
						if (p1 == NULL) {
							fprintf(stderr, "UTILIZARE: kick <nume_Client>\n");
							continue;
						}
						for (j = 0; j < CL_NR; j++) {
							if (!strcmp(client[j].name, p1) && client[j].online) {
								// primind kick, clientul devine offline
								client[j].online = 0;
								strcpy(message.type, "kick");
								send(client[j].socket, &message, sizeof(msg), 0);
								FD_CLR(client[j].socket, &read_fds);
								break;
							}
						}
						continue;
					}
					
					if (!strcmp(command, "quit")) {
						if (p1 != NULL) {
							fprintf(stderr, "UTILIZARE: quit\n");
							continue;
						}
						for (j = 0; j < CL_NR; j++) {
							if (client[j].online) {
								strcpy(message.type, "serverout");
								send(client[j].socket, &message, sizeof(msg), 0);
							}
						}
						FD_ZERO(&read_fds);
						close(sockfd);
						return 0;
					}

					continue;
				}
				
				// comanda socket inactiv: listen a receptionat ceva
				if (i == sockfd) {
					clilen = sizeof(cl_addr);
					sockfd2 = accept(sockfd, (struct sockaddr *)&cl_addr, &clilen);
					if (sockfd2 == -1) {
						error("ERROR 131: could not accept command");
					}
					else {
						// adaug socketul in read_fds
						// NU SE POATE DUPA FOR DE VERIFICARE UTILIZATOR EXISTENT?
						FD_SET(sockfd2, &read_fds);
						if (sockfd2 > fdmax) {
							fdmax = sockfd2;
						}
					}
					
					printf("Conexiune noua: %s (port: %d)\n", 
						inet_ntoa(cl_addr.sin_addr), ntohs(cl_addr.sin_port));
					
					aux = recv(sockfd2, &message, sizeof(msg), 0);
					if (aux <= 0) {
						error("ERROR 132: could not receive message");
					}
					
					// copiem informatiile clientului din payload
					memcpy(&auxcl, &message.payload, sizeof(infoc));
					// verificam daca utilizatorul exista (online) in sistem
					for (j = 0; j < CL_NR; j++) {
						if (!strcmp(client[j].name, auxcl.name) && client[j].online) {
							strcpy(message.type, "in_use");
							send(sockfd2, &message, sizeof(msg), 0);
							FD_CLR(sockfd2, &read_fds);
							printf("\tusername existent, online\n");
							continue;
						}
					}
					
					for (j = 0; j < CL_NR; j++) {
						if (!client[j].online) {
							memcpy(&client[j], &message.payload, message.len);
							
							memcpy(&client[j].ip, &cl_addr.sin_addr, sizeof(struct in_addr));
							client[j].socket = sockfd2;
							client[j].online = 1;
							
							t = time(NULL);
							memcpy(&client[j].connect_at, &t, sizeof(time_t));
							break;
						}
					}
					continue;					
				}
				
				aux = recv(i, &message, sizeof(msg), 0);
				if (aux <= 0) {
					error("ERROR 140: could not receive message");
					close(i);
					FD_CLR(i, &read_fds);
					for (j = 0; j < CL_NR; j++) {
						if (!strcmp(client[j].name, message.sender)) {
							client[j].online = 0;
							break;
						}
					}
				}

				if (!strcmp(message.type, "listclients")) {
					printf("Procesez comanda %s\n", message.type);
					memset(message.payload, 0, BUFLEN);
					strcpy(message.payload, "Lista clienti:\n");
					for (j = 0; j < CL_NR; j++) {
						if (client[j].online) {
							strcat(message.payload, "\t");
							strcat(message.payload, client[j].name);
							strcat(message.payload, "\n");
						}
					}

					send(i, &message, sizeof(msg), 0);
					continue;
				}
				
				if (!strcmp(message.type, "infoclient")) {
					printf("Procesez comanda %s\n", message.type);
					char msj[300];
					strcpy(msj, "null");
					memcpy(&message.payload, msj, sizeof(msj));

					// caut in vector clientul
					for(j = 0; j < CL_NR; j++){
						if (!strcmp(client[j].name, message.receiver) && client[j].online) {
							time(&t);
							struct tm auxt = *localtime(&t);
							
							client[j].connect_time = difftime(mktime(&auxt),client[j].connect_at);
							
							// creez mesajul cu informatiile despre client
							memset(msj, 0, sizeof(msj));
							sprintf(msj, "Client: %s\n\tPort: %d\n\tUpTime: %.f[s]\n", 
								client[j].name, ntohs(client[j].port), client[j].connect_time);

							memcpy(&message.payload, &msj, sizeof(msj));
							break;
						}
					}

					send(i, &message, sizeof(msg), 0);
					continue;
				}
				
				if (!strcmp(message.type, "message")) {
					printf("Procesez comanda %s\n", message.type);

					// caz de baza: clientul cautat nu exista/ nu este online
					auxcl.online = 0;
					memcpy(&message.payload, &auxcl, sizeof(infoc));

					// caut in vector clientul
					for (j = 0; j < CL_NR; j++) {
						if (!strcmp(client[j].name, message.receiver) && client[j].online) {
							// copiem in payload datele utilizatorului
							memcpy(&message.payload, &client[j], sizeof(infoc));
							break;
						}
					}

					send(i, &message, sizeof(msg), 0);
					continue;
				}
				
				if (!strcmp(message.type, "broadcast")) {
					printf("Procesez comanda %s\n", message.type);

					for (j = 0; j < CL_NR; j++) {
						if (client[j].online) {
							// in payload copiem datele utilizatorului destinatie
							memcpy(&message.payload, &client[j], sizeof(infoc));
							send(i, &message, sizeof(msg), 0);
						}
					}
					continue;
				}
				
				if (!strcmp(message.type, "sendfile")) {
					printf("Procesez comanda %s\n", message.type);

					// caz de baza: clientul cautat nu exista/ nu este online
					auxcl.online = 0;
					memcpy(&message.payload,&auxcl,sizeof(infoc));

					// caut in vector clientul
					for (j = 0; j < CL_NR; j++) {
						if (!strcmp(client[j].name, message.receiver) && client[j].online) {
							// copiem in payload datele utilizatorului
							memcpy(&message.payload, &client[j], sizeof(infoc));
							break;
						}
					}

					send(i, &message, sizeof(msg), 0);
					continue;
				}
				
				if (!strcmp(message.type, "quit")) {
					printf("Procesez comanda %s\n", message.type);

					FD_CLR(i, &read_fds);
					for (j = 0; j < CL_NR; j++) {
						if (!strcmp(client[j].name, message.sender)) {
							client[j].online = 0;
							break;
						}
					}
				}
				
			}
		}
	 }
	
}
