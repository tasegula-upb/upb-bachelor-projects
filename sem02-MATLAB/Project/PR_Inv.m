function B = PR_Inv(A)
	% Functia care calculeaza inversa matricii A folosind factorizari Gram-Schmidt
	% Se va inlocui aceasta linie cu descrierea algoritmului de inversare
	
	% Algortimul de factorizare Gram-Schmidt (alternativ)
	n=length(A);
	R=zeros(n);
	Q=zeros(n);
	x=zeros(n);
	
	[Q R]=gramSc(A);
	x=invSST(R);
	B=x*(Q');
	
endfunction	
