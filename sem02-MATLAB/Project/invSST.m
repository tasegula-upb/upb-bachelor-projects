function B=invSST(A)
	% inverseaza o matrice superior triunghiulara
	n=length(A);
	B=zeros(n,n);
	
	for j=1:n
		B(j,j)=1/A(j,j);
		for i=j-1:-1:1
			s=A(i,i+1:j)*B(i+1:j,j);
			B(i,j)=-1/A(i,i)*s;
		endfor
	endfor

end