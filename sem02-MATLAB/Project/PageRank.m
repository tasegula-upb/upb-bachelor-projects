function [R1 R2] = PageRank(nume, d, eps)
	% Calculeaza indicii PageRank pentru cele 3 cerinte
	% Scrie fisierul de iesire nume.out
	
	f=fopen(nume,"r");
	N=fscanf(f,"%d",1);
	output=strcat(nume,".out");
	g=fopen(output,"w");
	
	% Aflarea matricelor pt metodele Iterative si Algebraic
	Ri=Iterative(nume,d,eps);	
	Ra=Algebraic(nume,d,eps);
	
	% Sortarea vectorului;
	PR=sort(Ra);
	
	% Citirea valorilor val1 si val2
	for i=1:N
		x=fscanf(f,"%d",1);
		x=fscanf(f,"%d",1);
		for j=1:x
			x=fscanf(f,"%d",1);
		endfor
	endfor
	val1=fscanf(f,"%f",1);
	val2=fscanf(f,"%f",1);
	
	% Scrierea in fisier
	fprintf(g,"%d\n",N);
	for i=1:N
		fprintf(g,"%f\n",Ri(i));
	endfor
	fprintf(g,"\n");
	for i=1:N
		fprintf(g,"%f\n",Ra(i));
	endfor
	fprintf(g,"\n");
	
	for i=1:N
		y=Apartenenta(PR(i,1),val1,val2);
		fprintf(g,"%d %d %f\n",i,PR(i,2),y);
	endfor
	
	fclose(f);
	fclose(g);
endfunction	
	