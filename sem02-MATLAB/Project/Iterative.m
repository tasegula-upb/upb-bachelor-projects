function R = Iterative(nume, d, eps)
	% Functia care calculeaza matricea R folosind algoritmul iterativ.
	% Intrari:
	%	-> nume: numele fisierului din care se citeste;
	%	-> d: coeficentul d, adica probabilitatea ca un anumit navigator sa continue navigarea (0.85 in cele mai multe cazuri)
	%	-> eps: eruarea care apare in algoritm.
	% Iesiri:
	%	-> R: vectorul de PageRank-uri acordat pentru fiecare pagina.
	
	f=fopen(nume,"r");
	
	N=0;	% nr de linii
	y=0;	% primul nr de pe o linie
	x=0;	% auxiliara pt link.urile dintre pagini.
	
	N=fscanf(f,"%d",1);
	
	R=zeros(N,1);
	R_aux=zeros(N,1);
	A=zeros(N);		% Matricea de adiacenta a grafului link.urilor
	M=zeros(N);		% Matricea in care sunt retinute link.urile dintre pagini
	K=zeros(N);		% numarul de linkuri de pe o pagina (al doilea nr de pe fiecare linie)
	coef=ones(N,1);	% vector coloana fix, ce se va aduna la fiecare pas la R;
	coef=((1-d)/N)*coef;
	
	% Se fac citirile din fisiere, actualizand matricele K si A
	for i=1:N
		R(i)=1/N;
		y=fscanf(f,"%d",1);
		K(y,y)=fscanf(f,"%d",1);
		for j=1:K(y,y)
			x=fscanf(f,"%d",1);
			if(x!=y)
				A(y,x)=1;
			else
				K(y,y)-=1;
			endif
		endfor
		A(i,i)=0;
	endfor

	fclose(f);
	
	% Aflam M
	M=(inv(K)*A)';
	R_aux=R;
	% Vector care ia valoare 1 atunci cand se respecta conditia de oprire
	% abs(R_nou(i) - R(precedent))<eps;
	ok=zeros(N,1);
	
	% Aflam vectorul R, folosind algoritmul matriceal, dar calculand pe elemente;
	while(1)
		for i=1:N
			if(ok(i)==0)
				R_aux(i)=d*M(i,:)*R+coef(i);
				if(abs(R_aux(i)-R(i))<eps)
					ok(i)=1;
				endif
			endif
		endfor
		if(ok==1)
			break;
		endif
		R=R_aux;
	endwhile
endfunction
