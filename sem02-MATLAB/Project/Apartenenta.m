function y = Apartenenta(x, val1, val2)
	% Functia care primeste ca parametrii x, val1, val2 si care calculeaza valoarea functiei membru in punctul x.
	% Stim ca 0 <= x <= 1
	% Valorile lui a si b sunt date astfel incat functia sa fie continua.

	a = 1/(val2 - val1);
	b = -val1 * a;
	
	% Rezolvam functia
	if(x<val1)
		y=0;
	else
		if(x<=val2)
			y=a*x+b;
		else
			y=1;
		endif
	endif

endfunction
