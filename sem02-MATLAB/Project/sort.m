function PR=sort(A)
	% Sorteaza vectorul A;
	% Returneaza o matrice cu doua coloane:
		% pe prima coloana se afla vectorul A
		% pe a doua coloana se afla pozitia in vectorul original a elementului de pe pozitia 1
	n=length(A);
	PR=zeros(n,2);
	aux=zeros(1,2);
	
	for i=1:n
		PR(i,1)=A(i);
		PR(i,2)=i;
	endfor
	
	for i=1:n
		for j=i+1:n
			if(PR(j,1)>PR(i,1))
				aux=PR(j,:);
				PR(j,1)=PR(i,1);
				PR(j,2)=PR(i,2);
				PR(i,:)=aux;
			endif
		endfor
	endfor
				
endfunction