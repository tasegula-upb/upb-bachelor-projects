function R = Algebraic(nume, d)
	% Functia care calculeaza vectorul PageRank folosind varianta algebrica de calcul.
	% Intrari: 
	%	-> nume: numele fisierului in care se scrie;
	%	-> d: probabilitatea ca un anumit utilizator sa continue navigarea la o pagina urmatoare.
	% Iesiri:
	%	-> R: vectorul de PageRank-uri acordat pentru fiecare pagina.
	
	f=fopen(nume,"r");
	
	N=0;	% nr de linii
	y=0;	% primul nr de pe o linie
	x=0;	% auxiliara pt link.urile dintre pagini
	
	N=fscanf(f,"%d",1);
	
	R=zeros(N,1);
	R_aux=zeros(N,1);
	A=zeros(N);		% Matricea de adiacenta a grafului link.urilor
	M=zeros(N);		% Matricea in care sunt retinute link.urile dintre pagini
	K=zeros(N);		% numarul de linkuri de pe o pagina (al doilea nr de pe fiecare linie)
	coef=ones(N,1);	% vector coloana fix, ce se va aduna la fiecare pas la R;
	coef=((1-d)/N)*coef;
	
	% Se fac citirile din fisiere, actualizand matricele K si A
	for i=1:N
		R(i)=1/N;
		y=fscanf(f,"%d",1);
		K(y,y)=fscanf(f,"%d",1);
		for j=1:K(y,y)
			x=fscanf(f,"%d",1);
			if(x!=y)
				A(y,x)=1;
			else
				K(y,y)-=1;
			endif
		endfor
		A(i,i)=0;
	endfor

	fclose(f);
	
	I=zeros(N);
	I=PR_Inv(K);
	
	M=(I*A)';
	% Se calculeaza R folosind algoritmul matriceal.
	R=(inv(eye(N)-d*M))*coef;
	
endfunction