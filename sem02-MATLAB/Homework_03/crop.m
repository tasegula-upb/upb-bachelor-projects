function crop(image)

	img=imread(image);
	[m n]=size(img);
	x=m/4+1;
	y=n/4+1;
	img_crop=img(x:x+(m/2)-1,y:y+(n/2)-1);

	sir=strcat("out_crop_",image);
	imwrite(img_crop,sir);

endfunction