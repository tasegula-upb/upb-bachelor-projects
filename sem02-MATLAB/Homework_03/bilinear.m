function bilinear(image)

	img=imread(image);
	[m n]=size(img);
	img_nn=zeros(2*m,2*n);
	img=uint16(img);
	
	img_nn(1:2:2*m,1:2:2*n)=img;
	img_nn(1:2:2*m-1,2:2:2*n-2)=(img(:,1:n-1)+img(:,2:n))/2;
	img_nn(2:2:2*m-2,1:2:2*n-1)=(img(1:m-1,:)+img(2:m,:))/2;
	img_nn(2:2:2*m-2,2:2:2*n-2)=(img(1:m-1,1:n-1)+img(2:m,1:n-1)+img(1:m-1,2:n)+img(2:m,2:n))/4;
	
	img_nn=img_nn(1:2*m-1,1:2*n-1);
	
	s=length(image);
	sir=strcat("out_bilinear_",image(10:s));
	imwrite(uint8(img_nn),sir);

endfunction