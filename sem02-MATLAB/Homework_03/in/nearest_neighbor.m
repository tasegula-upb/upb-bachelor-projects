function nearest_neighbor(image)

	img=imread(image);
	[m n]=size(img);
	img_nn=zeros(2*m,2*n);
	
	img_nn(1:2:2*m,1:2:2*n)=img;
	img_nn(1:2:2*m,2:2:2*n)=img;
	img_nn(2:2:2*m,1:2:2*n)=img;
	img_nn(2:2:2*m,2:2:2*n)=img;
	
	s=length(image);
	sir=strcat("out_nn_",image(10:s));
	imwrite(uint8(img_nn),sir);

endfunction