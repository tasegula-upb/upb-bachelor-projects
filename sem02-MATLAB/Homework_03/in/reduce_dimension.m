function reduce_dimension(image,rate)

	img=imread(image);
	img=img*(1-rate/100);
	
	sir=strcat("out_rd_",num2str(rate),"_",image);
	imwrite(img,sir);

endfunction