function add_zero(image)

	img=imread(image);
	[m n]=size(img);
	img0=zeros(2*m,2*n);
	
	img0(1:2:2*m,1:2:2*n)=img;
	
	img0=img0(1:2*m-1,1:2*n-1);
	
	s=length(image);
	sir=strcat("out_add_zero_",image(10:s));
	imwrite(uint8(img0),sir);

endfunction