#include <iostream>
#include <fstream>
#include <cstdlib>
#include <limits>
#include <math.h>

using namespace std;

const int max_int = numeric_limits<int>::max();

int** init_matrix(int** matrix, int n) {
	matrix = (int**)malloc(n * sizeof(int*));
	for (int i = 0; i < n; i++) {
		matrix[i] = (int*)malloc(n * sizeof(int));
	}
	return matrix;
}

void test_files(const ios& in, char* iname, const ios& out, char* oname) {
    if (!in.good()) {
		cerr << "Can't open input file: " << iname << endl;
  		exit(1);
	}
	if (!out.good()) {
		cerr << "Can't open output file: " << oname << endl;
		exit(1);
	}
}

int main(int argc, char* argv[]) {
	ifstream input;         // fisier de input
	input.open(argv[2]);
	ofstream output;        // fisier de output
	output.open(argv[3]);

	// testez validitatea fisierelor
	test_files(input, argv[2], output, argv[3]);

	/* ------------------------------------------------------------------------
	** N - latura salii (N x N = numar senatori)
	** C - numar culori ;   S - numar saptamani
	** senate[N][N] 		- matricea senatorilor
	** next_senate[N][N]	- noua matrice
	** colors[C]	- colors[i] = numar membri partid i (0 = nu exista)
	** dist 		- aux pentru a calcula distanta de la s(i,j) la s(k,l)
	** dmin[C]		- vector aux pentru a calcula dist min pt fiecare culoare
	** --------------------------------------------------------------------- */
	int N, C;
	input >> N >> C;			// citim N, C
	int S = atoi(argv[1]);

	int dist;
	int i, j, k, l;

    int* colors = (int*)malloc(C * sizeof(int));
	int* dmin = (int*)malloc(C * sizeof(int));

	int** senate = init_matrix(senate, N);
	int** next_senate = init_matrix(next_senate, N);

	// Citesc matricea
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			input >> senate[i][j];
		}
	}

	while (S != 0) {
		// colors se initilizeaza cu 0;
		for (i = 0; i < C; i++) {
			colors[i] = 0;
		}

		// prelucrarea efectiva
		for (i = 0; i < N; i++) {
			for (j = 0; j < N; j++) {
				// initalizez distantele minime cu maximul posibil;
				for (k = 0; k < C; k++) {
						dmin[k] = max_int;
				}

				// aflu distanta de la s(i,j) la fiecare s(k,l)
				// si apoi verific daca este cea mai mica
				for (k = 0; k < N; k++) {
					for (l = 0; l < N; l++) {
						dist = max(abs(i-k), abs(j-l));
						if (dmin[senate[k][l]] > dist && dist > 0) {
							dmin[senate[k][l]] = dist;
						}
					}
				}

				// aleg culoarea pentru senatorul (i,j)
				dist = 0;
				k = 0;
				for (l = 0; l < C; l++) {
					if (dmin[l] != max_int) {
						if (dmin[l] > dist) {
							dist = dmin[l];
							k = l;
						}
						else if (dmin[l] == dist) {
							if (l < k) {
								k = l;
							}
						}
					}
				}

				next_senate[i][j] = k;
			}
		}

		for (i = 0; i < N; i++) {
			for (j = 0; j < N; j++) {
				senate[i][j] = next_senate[i][j];
				colors[senate[i][j]] ++;
				next_senate[i][j] = 0;
			}
		}

		for (i = 0; i < C; i++) {
			output << colors[i] << " ";
		}
		output << endl;

		S--;
    }

    for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			output << senate[i][j] << " ";
		}
		output << endl;
	}

input.close();
output.close();

return 0;
}
