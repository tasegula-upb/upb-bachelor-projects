#include <iostream>
#include <fstream>
#include <cstdlib>
#include <limits>

using namespace std;

const int max_int = numeric_limits<int>::max();

void test_files(const ios& in, char* iname, const ios& out, char* oname);

int main(int argc, char* argv[]) {
	ifstream input;         // fisier de input
	input.open(argv[2]);
	ofstream output;        // fisier de output
	output.open(argv[3]);

	// testez validitatea fisierelor
	test_files(input, argv[2], output, argv[3]);

	int N, C, S;
	/* ------------------------------------------------------------------------
	** N - latura salii (N x N = numar senatori)
	** C - numar culori ;   S - numar saptamani
	** senate[N][N] 		- matricea senatorilor
	** next_senate[N][N]	- noua matrice
	** colors[C]	- colors[i] = numar membri partid i (0 = nu exista)
	** dist 		- aux pentru a calcula distanta de la s(i,j) la s(k,l)
	** dmin[C]		- vector aux pentru a calcula dist min pt fiecare culoare
	** --------------------------------------------------------------------- */
	input >> N >> C;			// citim N, C
	S = atoi(argv[1]);

	int dist;
	int i, j, k, l;
	int step;	
	int x, y;
	int rem_colors, total_colors;

	// initializez matricile
	int old[C];
    int colors[C];
	int dmin[C];

	int senate[N][N];
	int next_senate[N][N];

	// Citesc matricea
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			input >> senate[i][j];
		}
	}

	for (i = 0; i < C; i++) {
		colors[i] = max_int;
		dmin[i] = max_int;
		old[i] = max_int;

	}
	total_colors = C;
	rem_colors = C;

	while (S != 0) {

		total_colors = C;
		// colors se initilizeaza cu 0;
		for (i = 0; i < C; i++) {
			old[i] = colors[i];
			if (colors[i] == 0 || colors[i] == -1) {
				colors[i] = -1;
				total_colors--;
			}
			else {
				colors[i] = 0;
			}
		}
		
		// prelucrarea efectiva
		for (i = 0; i < N; i++) {
			for (j = 0; j < N; j++) {

				rem_colors = total_colors;
				for (k = 0; k < C; k++) {
					dmin[k] = max_int;
				}

				step = 1;
				k = -1;	

				if (old[senate[i][j]] == 1) {
					rem_colors--;
					dmin[senate[i][j]] = 0;
				}

				while (k == -1) {

						if (rem_colors == 1) {
							for (x = 0; x < C; x++) {
								if (colors[x] > 0 && dmin[x] == max_int) {
									if (senate[i][j] != x || old[x] != 1) {
										rem_colors--;
										k = x;
										dmin[x] = step;
										break;
									}
								}
							}
						}				

						if (i - step >= 0) {
							for (y = -step; y <= step; y++) {
								if (j + y >= 0 && j + y < N) {
									if (dmin[senate[i-step][j+y]] > step) {
										dmin[senate[i-step][j+y]] = step;
										rem_colors--;
									}
								}
							}
						}

						if (i + step < N) {
							for (y = -step; y <= step; y++) {
								if (j + y >= 0 && j + y < N) {
									if (dmin[senate[i+step][j+y]] > step) {
										dmin[senate[i+step][j+y]] = step;
										rem_colors--;
									}
								}
							}
						}

						if (j - step >= 0) {
							for (x = -step + 1; x < step; x++) {
								if (i + x >= 0 && i + x < N) {
									if (dmin[senate[i+x][j-step]] > step) {
										dmin[senate[i+x][j-step]] = step;
										rem_colors--;
									}
								}
							}
						}

						if (j + step < N) {
							for (x = -step + 1; x < step; x++) {
								if (i + x >= 0 && i + x < N) {
									if (dmin[senate[i+x][j+step]] > step) {
										dmin[senate[i+x][j+step]] = step;
										rem_colors--;
									}
								}
							}
						}

						if ((rem_colors != 0 && step == (N - 1)) || (rem_colors == 0)) {
							k = 0;
							dist = 0;
							for (x = 0; x < C; x++) {
								if (dmin[x] != max_int) {
									if (dmin[x] > dist) {
										dist = dmin[x];
										k = x;
									}
								}
							}
							break;
						}

					step++;
				}

				next_senate[i][j] = k;
				colors[k] ++;
			}
		}

		// copiez senatul
		for (i = 0; i < N; i++) {
			for (j = 0; j < N; j++) {
				senate[i][j] = next_senate[i][j];
				next_senate[i][j] = 0;
			}
		}

		// scriu partidele in fisier
		for (i = 0; i < C; i++) {
			if (colors[i] == -1) {
				output << "0 ";
			}
			else {
				output << colors[i] << " ";
			}
		}
		output << endl;

		S--;
    }

    for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			output << senate[i][j] << " ";
		}
		output << endl;
	}

input.close();
output.close();

return 0;
}

void test_files(const ios& in, char* iname, const ios& out, char* oname) {
    if (!in.good()) {
		cerr << "Can't open input file: " << iname << endl;
  		exit(1);
	}
	if (!out.good()) {
		cerr << "Can't open output file: " << oname << endl;
		exit(1);
	}
}