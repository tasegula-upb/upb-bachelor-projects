#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sstream>
#include <vector>
#include <set>
#include <mpi.h>

#define INF 100000
#define CHOOSE(a, b) a < b ? a : b

using namespace std;

struct message {
	int to;
	std::string text;	
};

int main(int argc, char* argv[]) {
	srand(time(NULL));

	if (argc < 2) {
		return 0;
	}

	int i, j, k, l, rank, size;
	MPI_Status status;
	MPI_Request reqs;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	int topology[size][size];
	int recv_top[size][size];
	int cycles[size][size];
	int routeTable[size];

	//initializari matrici
	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			topology[i][j] = recv_top[i][j] = cycles[i][j] = 0;
		}
		routeTable[i] = INF;
	}

	// pentru parsarea fisierelor
	std::string fileline;
	std::string line;
	char c;
	int x = 0;

	int parent = -1;
	int source;
	int leaf = 0;

	std::vector<message> vm;
	std::vector<int> neighbors;
	char *buffer = new char[1000];
	int neigh = 0;

	/**	-----------------------------------------------------------------------
	 **		CITIRE VECINI DIN FISIER
	 **	-------------------------------------------------------------------- */
	ifstream input;
	input.open(argv[1]);

	while(std::getline(input, fileline)) {
		std::stringstream ss(fileline);
		ss >> x;
		if (x == rank) {
			ss >> c;
			while (ss >> x) {
				topology[rank][x] = topology[x][rank] = 1;
				neigh++;
			}
			break;
		}
	}
	input.close();

	/**	-----------------------------------------------------------------------
	 **		CREARE TOPOLOGIE PENTRU FIECARE BUNCAR
	 **	-------------------------------------------------------------------- */
	leaf = neigh;		//folosesc leaf ca un aux;
	if (rank == 0) {
		// trimit semnal de trimitere a topologiei catre copii
		for (i = 1; i < size; i++) {
			if (topology[rank][i] != 0) {
				MPI_Send(&rank, 1, MPI_INT, i, 1, MPI_COMM_WORLD);
			}
		}

		// asteapta sa primeasca de la copii topologia
		for (i = 1; i < size; i++) {
			if (topology[rank][i] != 0) {								
				MPI_Recv(&recv_top, size * size, MPI_INT, i, 1, MPI_COMM_WORLD, &status);

				// si modifica topologia curenta
				for (k = 0; k < size; k++) {
					if (k != rank) {
						for (l = 0; l < size; l++) {
							topology[k][l] = topology[k][l] | recv_top[k][l];
						}
					}
				}
			}
		} // end for
	}
	else {
		// primesc semnal de trimitere a topologiei
		MPI_Recv(&source, 1, MPI_INT, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &status);

		if (parent == -1) {
			parent = source;
		}
		else {
			cycles[rank][source] = cycles[source][rank] = 1;
		}

		// trimit semnal de trimitere a topologiei catre copii
		for (i = 0; i < size; i++) {
			if (i != source && topology[rank][i] != 0) {
				MPI_Send(&rank, 1, MPI_INT, i, 1, MPI_COMM_WORLD);
			}
		}

		// asteapta sa primeasca de la copii topologia
		for (i = 0; i < size; i++) {
			if (i != source && topology[rank][i] != 0) {	
				MPI_Recv(&recv_top, size * size, MPI_INT, i, 1, MPI_COMM_WORLD, &status);

				// si modifica topologia curenta
				for (k = 0; k < size; k++) {					
					if (k != rank) {
						for (l = 0; l < size; l++) {
							topology[k][l] = topology[k][l] | recv_top[k][l];
						}
					}
				}
			}
			}
		// trimit topologia catre parinte		
		for (i = 0; i < size; i++) {		
			MPI_Send(&topology, size * size, MPI_INT, source, 1, MPI_COMM_WORLD);
		}
	}

	//	TRIMITE TOPOLOGIE LA FIECARE BUNCAR	
	if (rank == 0) {
		for (i = 1; i < size; i++) {
			if (topology[rank][i] != 0) {
				MPI_Send(&topology, size * size, MPI_INT, i, 1, MPI_COMM_WORLD);
			}
		}
	}
	else {		
		MPI_Recv(&topology, size * size, MPI_INT, parent, 1, MPI_COMM_WORLD, &status);

		for (i = 0; i < size; i++) {
			if (i != parent && topology[rank][i] != 0) {
				MPI_Send(&topology, size * size, MPI_INT, i, 1, MPI_COMM_WORLD);
			}
		}
	}

	// afiseaza matricea de adiacenta
//*	
	if (rank == 0) {
		cout << "MATRICEA DE ADIACENTA A TOPOLOGIEI DATE\n";
		for (i = 0; i < size; i++) {
			cout << "\t";
			for (j = 0; j < size; j++) {
				cout << topology[i][j] << " ";
			}
			cout << endl;
		}
		cout << endl;
		cout << "TABELA DE RUTARE\n";
	}
//*/
/*	
	for (i = 0; i < size; i++) {
	MPI_Barrier(MPI_COMM_WORLD);
		if (rank == i) {
			cout << rank << "\t";
			for (j = 0; j < size; j++) {
				cout << cycles[rank][j] << " ";
			}
			cout << endl;
		}
	}
//*/
	MPI_Barrier(MPI_COMM_WORLD);

	/**	-----------------------------------------------------------------------
	 **		CREARE TABELA DE RUTARE PENTRU FIECARE BUNCAR
	 **			ALGORITM DIJKSTRA
	 **	-------------------------------------------------------------------- */
	//initializare tabela de rutare
	routeTable[rank] = 0;
	int dist[size][size];
	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			dist[i][j] = topology[i][j];
		}
	}

	for (k = 0; k < size; k++) {
		for (i = 0; i < size; i++) {
			for (j = 0; j < size; j++) {
				if (i != j && dist[i][k] && dist[k][j] && (dist[i][j] > dist[i][k] + dist[k][j] || dist[i][j] == 0)) {
					dist[i][j] = dist[i][k] + dist[k][j];
				}
			}
		}
	}
	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			if (topology[rank][j] && dist[rank][i] - 1 == dist[j][i]) {
				routeTable[i] = j;
				break;
			}
		}
	}

	// afiseaza tabela de rutare
	for (i = 0; i < size; i++) {
		MPI_Barrier(MPI_COMM_WORLD);
		if (i == rank) {
			printf("P%d:\t", rank);
			for (j = 0; j < size; j++) {
				cout << routeTable[j] << " ";
			}
			cout << endl;
			if (rank == size - 1) {
				cout << endl;
				cout << "Mesajele ce vor fi trimite: sursa -> destinatie : mesaj\n";
			}
		}
	}

	/**	-----------------------------------------------------------------------
	 **		CITIRE MESAJE DIN FISIER
	 **	-------------------------------------------------------------------- */
	ifstream messages;
	messages.open(argv[2]);
	int n = 0;
	messages >> n;

	for (i = 0; i <= n; i++) {
		std::getline(messages, fileline);
		std::stringstream ss(fileline);
		ss >> x;
		if (x == rank) {
			message m;
			ss >> c;

			if (c == 'B') {
				m.to = -1;		// mesaj de broadcast;
			}
			else {
				ss.seekg(1 ,ios::beg);	// inapoi la pozitia 1, 
				ss >> x;				// pentru a citi destinatia
				m.to = x;
			}

			ss >> line;
			m.text = line;
			while (ss >> line) {
				m.text += " " + line;
			}

			vm.push_back(m);
			break;
		}
	}
	messages.close();

	// afiseaza mesajele
	for (i = 0; i < size; i++) {
		MPI_Barrier(MPI_COMM_WORLD);
		if (i == rank) {
			for (j = 0; j < vm.size(); j++) {
				if (vm.at(j).to != -1) {
					cout << rank << " -> " << vm.at(j).to << " : " << vm.at(j).text + "\n";
				}
				else {
					cout << rank << " -> B : " << vm.at(j).text + "\n";					
				}
			}
		}
	}
	if (rank == 0) { 
		cout << "\nProcesul de trimitere a mesajelor folosind topologia\n";
	}

	/**	-----------------------------------------------------------------------
	 **		BROADCAST -> ANUNTA INCEPEREA COMUNICARII!
	 **	-------------------------------------------------------------------- */
	for (i = 0; i < size; i++) {
		if (cycles[rank][i] != 1 && topology[rank][i] != 0) {
			MPI_Isend(&rank, 1, MPI_INT, i, 2, MPI_COMM_WORLD, &reqs);
			MPI_Irecv(&source, 1, MPI_INT, i, 2, MPI_COMM_WORLD, &reqs);
		}
	}
	MPI_Barrier(MPI_COMM_WORLD);

	/**	-----------------------------------------------------------------------
	 **		INCEPE COMUNICAREA!
	 **	-------------------------------------------------------------------- */
	int dest;

	while (!vm.empty()) {
		message mess = vm.back();
		vm.pop_back();

		dest = mess.to;

		if (dest != -1) {
			sprintf(buffer, "%d %d %s", mess.to, rank, mess.text.c_str());
			printf("[SEND][P%d]  [S%d][D%d] -> [H%d] \t %s\n", rank, rank, dest, routeTable[dest], mess.text.c_str());
			MPI_Isend(&buffer[0], 1000, MPI_CHAR, routeTable[dest], 1, MPI_COMM_WORLD, &reqs);
		}
		else {
			sprintf(buffer, "B %d %s", rank, mess.text.c_str());
			printf("[BCAST][P%d] \t\t\t %s\n", rank, mess.text.c_str());
			for (i = 0; i < size; i++) {				
				if (topology[rank][i] != 0) {
					printf("[BCAST][P%d] [SEND][S%d][D%d] \t %s\n", rank, i, rank, mess.text.c_str());
					MPI_Isend(&buffer[0], 1000, MPI_CHAR, i, 1, MPI_COMM_WORLD, &reqs);
				}
			}
		}
	}
	if (rank == 0) { cout << endl; }

	// S-a terminat comunicarea	
	for (i = 0; i < size; i++) {
		if (cycles[rank][i] != 1 && topology[rank][i] != 0) {
			sprintf(buffer, "%s", "Incepe comunicatia!");
			MPI_Isend(&buffer[0], 1000, MPI_CHAR, i, 1, MPI_COMM_WORLD, &reqs);
		}
	}

	int bcast = size - 1;
	while (true) {
		MPI_Recv(&buffer[0], 1000, MPI_CHAR, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &status);
		c = buffer[0];

		if (c == 'I') {					// Mesaj de tip broadcast de final
			for (i = 0; i < size; i++) {
				if (i != status.MPI_SOURCE && cycles[rank][i] != 1 && topology[rank][i] != 0) {
					sprintf(buffer, "%s", "Incepe comunicatia!");
					MPI_Isend(&buffer[0], 1000, MPI_CHAR, i, 1, MPI_COMM_WORLD, &reqs);
				}
			}

			bcast--;
			if (bcast == 0) {
				break;
			}
			continue;
		}
		
		if (c == 'B') {					// Mesaj de tip broadcast
			std::string str(buffer);
			std::stringstream ss(str);
			ss >> c;
			ss >> source;

			ss >> line;
			fileline = line;
			while (ss >> line) {
				fileline += " " + line;
			}

			printf("[BCAST][P%d] [RECV][P%d][S%d] \t %s \n", rank, source, status.MPI_SOURCE, fileline.c_str());
			for (i = 0; i < size; i++) {
				if (i != status.MPI_SOURCE && cycles[rank][i] != 1 && topology[rank][i] != 0) {
					printf("[BCAST][P%d] [SEND][P%d][D%d] \t %s\n", rank, source, i, fileline.c_str());	
					MPI_Isend(&buffer[0], 1000, MPI_CHAR, i, 1, MPI_COMM_WORLD, &reqs);
				}
			}
			continue;				
		}
		
		if (isdigit(c)) {				// Mesaj normal de rutat
			std::string str(buffer);
			std::stringstream ss(str);
			
			ss >> dest;
			ss >> source;

			ss >> line;
			fileline = line;
			while (ss >> line) {
				fileline += " " + line;
			}

			if (dest == rank) {
				printf("[RECV][P%d]  [S%d][D%d] -> [H%d] \t %s\n", rank, source, rank, status.MPI_SOURCE, fileline.c_str());
			}
			else {
				printf("[ROUT][P%d]  [S%d][D%d] -> [H%d] \t %s\n", rank, source, dest, routeTable[dest], fileline.c_str());
				MPI_Isend(&buffer[0], 1000, MPI_CHAR, routeTable[dest], 1, MPI_COMM_WORLD, &reqs);
			}
		}

	}
	MPI_Barrier(MPI_COMM_WORLD);

	/**	-----------------------------------------------------------------------
	 **		GASIREA VECINILOR SI NOTAREA FRUNZELOR
	 **	-------------------------------------------------------------------- */
	int children = 0;
	for (i = 0; i < size; i++) {
		recv_top[rank][i] = 0;

		if (cycles[rank][i] != 1 && topology[rank][i] != 0) {
			neighbors.push_back(i);
			recv_top[rank][i] = 1;			// folosit pentru a tine evidenta vecinilor vizitati de fiecare nod
		}
	}
	if (neighbors.size() == 1) {
		leaf = 1;
	}
	MPI_Barrier(MPI_COMM_WORLD);
	if (rank == 0) { printf("\n=========== LIDERII ===========\n===============================\n"); }
	MPI_Barrier(MPI_COMM_WORLD);

	/**	-----------------------------------------------------------------------
	 **		ALEGEREA LIDERULUI (PRESEDINTELUI)!
	 **	-------------------------------------------------------------------- */
	int recvDec = 0;
	int lastSend = 0;
	int leader = 0;

	leader = rand() % size;
	for (i = 0; i < neighbors.size() - 1; i++) {
		MPI_Recv(&recvDec, 1, MPI_INT, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &status);
		leader = CHOOSE(leader, recvDec);
		recv_top[rank][status.MPI_SOURCE] = 0;
	}

	for (i = 0; i < size; i++) {
		if (recv_top[rank][i] != 0) {
			MPI_Isend(&leader, 1, MPI_INT, i, 1, MPI_COMM_WORLD, &reqs);
			MPI_Recv(&recvDec, 1, MPI_INT, i, 1, MPI_COMM_WORLD, &status);
			leader = CHOOSE(leader, recvDec);
			lastSend = i;
			break;
		}	
	}

	for (i = 0; i < size; i++) {
		if (cycles[rank][i] != 1 && topology[rank][i] != 0 && i != lastSend) {
			MPI_Isend(&leader, 1, MPI_INT, i, 1, MPI_COMM_WORLD, &reqs);
		}
	}

	// THE LIDER HAS BEEN CHOOSEN
	printf("[P%d] President:\t %d\n", rank, leader); 
	MPI_Barrier(MPI_COMM_WORLD);
	if (rank == 0) { printf("\n"); }
	MPI_Barrier(MPI_COMM_WORLD);


	/**	-----------------------------------------------------------------------
	 **		ALEGEREA VICEPRESEDINTELUI!
	 **	-------------------------------------------------------------------- */
	int vice = rand() % size;

	while (vice == leader) {
		vice = rand() % size;
	}

	for (i = 0; i < neighbors.size() - 1; i++) {
		MPI_Recv(&recvDec, 1, MPI_INT, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &status);
		vice = CHOOSE(vice, recvDec);
		recv_top[rank][status.MPI_SOURCE] = 0;
	}

	for (i = 0; i < size; i++) {
		if (recv_top[rank][i] != 0) {
			MPI_Isend(&vice, 1, MPI_INT, i, 1, MPI_COMM_WORLD, &reqs);
			MPI_Recv(&recvDec, 1, MPI_INT, i, 1, MPI_COMM_WORLD, &status);
			vice = CHOOSE(vice, recvDec);
			lastSend = i;
		}	
	}

	for (i = 0; i < size; i++) {
		if (cycles[rank][i] != 1 && topology[rank][i] != 0 && i != lastSend) {
			MPI_Isend(&vice, 1, MPI_INT, i, 1, MPI_COMM_WORLD, &reqs);
		}
	}

	// THE VICE HAS BEEN CHOOSEN
	printf("[P%d] Vicepresident:\t %d\n", rank, vice); 
	MPI_Barrier(MPI_COMM_WORLD);

MPI_Finalize();	

return 0;
}