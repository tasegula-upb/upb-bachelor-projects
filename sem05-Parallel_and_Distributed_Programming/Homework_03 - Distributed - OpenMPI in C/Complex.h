#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <cmath>

class Complex {
public:
	double x;
	double y;

	Complex() {
		x = 0.00;
		y = 0.00;
	}

	Complex(double _x, double _y) {
		x = _x;
		y = _y;
	}

	double absolute() {
		double q = std::sqrt(x * x + y * y);
	//	printf("SQRT: %1$.16f\n", q);
		return q;
	}

	void print() {
		printf("Complex No: (%1$.16f + %1$.16fi)\n", x, y);
	}

	Complex square() {
		Complex z = Complex();
		z.x = x * x - y * y;
		z.y = 2 * x * y;
		return z;
	}

	Complex operator*(Complex z) {
		Complex q = Complex();
		q.x = (x * z.x) - (y * z.y);
		q.y = (x * z.y) + (y * z.x);
		return q;
	}

	Complex operator+(Complex z) {
		Complex q = Complex(x, y);
		q.x += z.x;
		q.y += z.y;
		return q;
	}
};

