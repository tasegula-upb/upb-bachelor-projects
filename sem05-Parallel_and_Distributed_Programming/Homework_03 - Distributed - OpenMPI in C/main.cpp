#include <iostream>
#include <utility>
#include <fstream>
#include <cstdlib>
#include <stdio.h>
#include <cmath>
#include <mpi.h>
#include "Complex.h"

#define MAX_COLORS	256

using namespace std;

void test_file(const ios& in, char* iname) {
    if (!in.good()) {
		cerr << "Can't open input file: " << iname << endl;
  		exit(1);
	}
}

int main(int argc, char* argv[]) {
	/**	-----------------------------------------------------------------------
	 **		DESCHIDERE FISIERE
	 **	-------------------------------------------------------------------- */
	ifstream input;         // fisier de input
	input.open(argv[1]);
	ofstream output;        // fisier de output
	output.open(argv[2]);

	// testez validitatea fisierelor
	test_file(input, argv[1]);

	/**	-----------------------------------------------------------------------
	 **		VARIABILE
	 **	-------------------------------------------------------------------- */
	//	structura primita
	int type;						// tipul -> va fi dat ca tag;
	double xmin, xmax, ymin, ymax;	// coordonatele subspatiului complex folosit
	double scan;					// rezolutia
	int iter;						// numar de iteratii
	double j1, j2;					// daca tipul este Julia, cele doua valori

	//	matricea imaginii
	int width, height;
	int chunck;
	int **auxImage;
	int **image;

	//	auxiliare
	int i, j, x, y;		//	auxiliare - contoare
	int step;			//	contor de iteratii in bucla while
	int h1, h2;			//	[h1, h2) = intervalul de linii pe care il proceseaza un proces;
	Complex z, c;

	// variabile MPI
	int rank, size;  
	MPI_Status status;

	/**	-----------------------------------------------------------------------
	 **		INITIALIZARE MPI
	 **	-------------------------------------------------------------------- */
	MPI_Init(&argc, &argv);					// start MPI
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);	// ID-ul procesului curent
	MPI_Comm_size (MPI_COMM_WORLD, &size);	// numarul de procese

	if (size < 2) {
		printf("Don't serialize me!\n");
		exit(1);
	}

	/**	-----------------------------------------------------------------------
	 **		MASTER (RANK 0)
	 **	-------------------------------------------------------------------- */
	if (rank == 0){
		//	CITIRE DIN MATRICE
		input >> type;
		input >> xmin >> xmax >> ymin >> ymax;
		input >> scan;
		input >> iter;

		if (type == 1) {
			input >> j1;
			input >> j2;
		}

		/**	-------------------------------------------------------------------
		 **		INCEPERE PROGRAM
		 **	---------------------------------------------------------------- */
		//	Initializari
		width = static_cast<int>((xmax - xmin) / scan);
		height = static_cast<int>((ymax - ymin) / scan);
		chunck = static_cast<int>(height / size);
		h1 = 0;
		h2 = chunck + (height - chunck * size);

		//	Initializare matrice imagine si matrice auxialiara (ce va fi trimisa la toate procesele)
		image = (int**)malloc(height * sizeof(int*));
		for (i = 0; i < height; i++) {
			image[i] = (int*)malloc(width * sizeof(int));
		}
		auxImage = (int**)malloc(height * sizeof(int*));
		for (i = 0; i < height; i++) {
			auxImage[i] = (int*)malloc(width * sizeof(int));
		}

		for (i = 1; i < size; i++) {
			MPI_Send(&type, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
			MPI_Send(&xmin, 1, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
			MPI_Send(&xmax, 1, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
			MPI_Send(&ymin, 1, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
			MPI_Send(&ymax, 1, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
			MPI_Send(&scan, 1, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
			MPI_Send(&iter, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
			if (type == 1) {				
				MPI_Send(&j1, 1, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
				MPI_Send(&j2, 1, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
			}
		}

		c = Complex(j1, j2);										//	JULIA
		for (x = h1; x < h2; x++) {
			for (y = 0; y < width; y++) {				
				step = 0;

				if (type == 0) {
					c = Complex(xmin + y * scan, ymin + x * scan);	//	MANDELBROT
					z = Complex();
				}
				else {
					z = Complex(xmin + y * scan, ymin + x * scan);
				}

				while (z.absolute() < 2 && step < iter) {
					z = z * z + c;
					step++;
				}
				
				auxImage[x][y] = step % MAX_COLORS;
			}
		}
	}

	/* 	-----------------------------------------------------------------------
	 **		RESTUL PROCESELOR:	CALCULEAZA VALORILE PE PARTEA DE MATRICE DATA
	 **	-------------------------------------------------------------------- */
	if (rank != 0) {

		MPI_Recv(&type, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
		MPI_Recv(&xmin, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &status);
		MPI_Recv(&xmax, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &status);
		MPI_Recv(&ymin, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &status);
		MPI_Recv(&ymax, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &status);
		MPI_Recv(&scan, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &status);
		MPI_Recv(&iter, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
		if (type == 1) {				
			MPI_Recv(&j1, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &status);
			MPI_Recv(&j2, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &status);
		}

		width = static_cast<int>((xmax - xmin) / scan);	
		height = static_cast<int>((ymax - ymin) / scan);
		chunck = static_cast<int>(height / size);

		h1 = chunck * rank + (height - chunck * size);
		h2 = chunck + h1;

		auxImage = (int**)malloc(height * sizeof(int*));
		for (i = 0; i < height; i++) {
			auxImage[i] = (int*)malloc(width * sizeof(int));
		}

		c = Complex(j1, j2);										//	JULIA
		for (x = h1; x < h2; x++) {
			for (y = 0; y < width; y++) {				
				step = 0;

				if (type == 0) {
					c = Complex(xmin + y * scan, ymin + x * scan);	//	MANDELBROT
					z = Complex();
				}
				else {
					z = Complex(xmin + y * scan, ymin + x * scan);
				}

				while (z.absolute() < 2 && step < iter) {
					z = z * z + c;
					step++;
				}
				
				auxImage[x][y] = step % MAX_COLORS;
			}
		}

		MPI_Send(&auxImage[0][0], height * width, MPI_INT, 0, 0, MPI_COMM_WORLD);
	}

	/* 	-----------------------------------------------------------------------
	 **		MASTER (RANK 0):	SCRIU IN FISIER
	 **	-------------------------------------------------------------------- */
	if (rank == 0) {
		
		for (x = h1; x < h2; x++) {
			for (y = 0; y < width; y++) {
				image[x][y] = auxImage[x][y];
			}
		}

		for (i = 1; i < size; i++) {	
			MPI_Recv(&(auxImage[0][0]), height * width, MPI_INT, i, 0, MPI_COMM_WORLD, &status);

			h1 = chunck * i + (height - chunck * size);
			h2 = chunck + h1;
			for (x = h1; x < h2; x++) {
				for (y = 0; y < width; y++) {
					image[x][y] = auxImage[x][y];
				}
			}
		}

		output << "P2\n";
		output << width << " " << height << "\n";
		output << MAX_COLORS - 1 << "\n";
		for (x = height - 1; x >= 0; x--) {
			for (y = 0; y < width; y++) {
				output << image[x][y] << " ";
			}
			output << "\n";
		}
	}


MPI_Finalize();
return 0;
}