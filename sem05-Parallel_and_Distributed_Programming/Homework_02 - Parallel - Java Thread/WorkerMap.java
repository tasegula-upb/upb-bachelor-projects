
import java.util.Hashtable;
import java.util.StringTokenizer;

/* ----------------------------------------------------------------------------
**	Algoritmi Paraleli si Distribuiti:   TEMA 02
**	GULA TANASE, 334CA 
** ------------------------------------------------------------------------- */

public class WorkerMap extends Thread {
	WorkPool wpm;
	static Hashtable<String, Hashtable<String, Integer>> freqs = new Hashtable<String, Hashtable<String, Integer>>();
	static Hashtable<String, Integer> totalCount = new Hashtable<String, Integer>();

	public WorkerMap(WorkPool workpool, String files[]) {
		this.wpm = workpool;
		for (int i = 0; i < files.length; i++) {
			if (files[i] == null) {
				continue;
			}
			freqs.put(files[i], new Hashtable<String, Integer>());
			totalCount.put(files[i], 0);
		}
		
	}		// end of constructor(...)

	/* ------------------------------------------------------------------------
	** Procesarea unei solutii partiale. Aceasta poate implica generarea unor
	** noi solutii partiale care se adauga in workpool folosind putWork().
	** Daca s-a ajuns la o solutie finala, aceasta va fi afisata.
	** --------------------------------------------------------------------- */
	void processPartialSolution(PartialSolution ps) {
		StringTokenizer st = new StringTokenizer(ps.text, " _().,;:-\\'\\\"\n\r\t\f");
		String token;

		synchronized(freqs) {	
			Hashtable<String, Integer> hashDoc = freqs.get(ps.doc);		// shorter form, but need sync before it
		
			while (st.hasMoreTokens()) {
				token = st.nextToken().toLowerCase();

				Integer n = hashDoc.get(token);
				if (n == null) {
					hashDoc.put(token, 1);
				}
				else {
					hashDoc.put(token, n + 1);
				}
				totalCount.put(ps.doc, totalCount.get(ps.doc) + 1);
			}

			freqs.put(ps.doc, hashDoc);
		}
	
	}		// end of method processPartialSolution(...)
	
	public void run() {
//		System.out.println("Thread-ul worker " + this.getName() + " a pornit...");
		while (true) {
			PartialSolution ps = wpm.getWork();
			if (ps == null)
				break;
			
			processPartialSolution(ps);
		}
//		System.out.println("Thread-ul worker " + this.getName() + " s-a terminat...");
	}		// end of method run()
}
