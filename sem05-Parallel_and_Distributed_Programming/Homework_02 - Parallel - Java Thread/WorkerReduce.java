
import java.util.Hashtable;
import java.util.StringTokenizer;

/* ----------------------------------------------------------------------------
**	Algoritmi Paraleli si Distribuiti:   TEMA 02
**	GULA TANASE, 334CA 
** ------------------------------------------------------------------------- */

public class WorkerReduce extends Thread {
	WorkPool wpr;
	Hashtable<String, Hashtable<String, Integer>> freqs = new Hashtable<String, Hashtable<String, Integer>>();
	Hashtable<String, Integer> totalCount = new Hashtable<String, Integer>();
	static Hashtable<String,  Double> simil = new Hashtable<String, Double>();

	public WorkerReduce(WorkPool workpool, 
						String files[], 
						Hashtable<String, Hashtable<String, Integer>> freqs, 
						Hashtable<String, Integer> totalCount) {
		this.wpr = workpool;
		this.freqs = freqs;
		this.totalCount = totalCount;
		
		simil.put(files[0], 10000.0);
		for (int i = 1; i < files.length; i++) {
			if (files[i] == null) {
				continue;
			}
			simil.put(files[i], 0.0);
		}
		
	}		// end of constructor(...)

	/* ------------------------------------------------------------------------
	** Procesarea unei solutii partiale. Aceasta poate implica generarea unor
	** noi solutii partiale care se adauga in workpool folosind putWork().
	** Daca s-a ajuns la o solutie finala, aceasta va fi afisata.
	** --------------------------------------------------------------------- */
	void processPartialSolution(PartialSolution ps) {
		if (ps.doc == null) {
			return;
		}
		Integer n = freqs.get(ps.doc).get(ps.text);		// numarul de aparitii a cuvantului ps.text in ps.doc
		if (n != null) {			
			synchronized(simil) {
				simil.put(ps.doc, simil.get(ps.doc) + ((double)n / (double)totalCount.get(ps.doc) * 100.0) * (double)ps.freq * 100.0);
			}
		}
	
	}		// end of method processPartialSolution(...)
	
	public void run() {
//		System.out.println("Thread-ul worker " + this.getName() + " a pornit...");
		while (true) {
			PartialSolution ps = wpr.getWork();
			if (ps == null)
				break;
			
			processPartialSolution(ps);
		}
//		System.out.println("Thread-ul worker " + this.getName() + " s-a terminat...");
	}		// end of method run()
}
