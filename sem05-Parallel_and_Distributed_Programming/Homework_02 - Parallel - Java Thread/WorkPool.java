
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Vector;

/* ----------------------------------------------------------------------------
**	Algoritmi Paraleli si Distribuiti:   TEMA 02
**	GULA TANASE, 334CA 
** ------------------------------------------------------------------------- */

public class WorkPool {
    int nThreads;					// numarul de thread-uri
    int nWaiting;					// numarul de thread-uri care sunt blocate
    public boolean ready = false;	// s-a terminat rezolvarea problemei?
    
    LinkedList<PartialSolution> tasks = new LinkedList<PartialSolution>();
	
	public WorkPool(int nTh) {
		this.nThreads = nTh;
	}
	
	/* ------------------------------------------------------------------------
	** Functie care incearca obtinera unui task din workpool.
	** Daca nu sunt task-uri disponibile, functia se blocheaza pana cand 
	** poate fi furnizat un task sau pana cand rezolvarea problemei este 
	** complet terminata
	** @return:	Un task de rezolvat, sau 
	**			null daca rezolvarea problemei s-a terminat.
	** --------------------------------------------------------------------- */
	
	public synchronized PartialSolution getWork() {
		if (tasks.size() == 0) { // workpool gol
			nWaiting++;
			/* condtitie de terminare:
			** nu exista task in workpool si nici worker activ 
			*/
			if (nWaiting == nThreads) {
				ready = true;
				/* problema s-a terminat, anunt toti ceilalti workeri */
				notifyAll();
				return null;
			} 
			else {
				while (!ready && tasks.size() == 0) {
					try {
						this.wait();
					} catch(Exception e) {e.printStackTrace();}
				}
				
				if (ready) {
				    /* s-a terminat prelucrarea */
				    return null;
				}
				nWaiting--;
				
			}
		}
		return tasks.remove();

	}
	
	/*
	** Functie care introduce un task in workpool.
	** @param sp:	task-ul care trebuie introdus 
	** */
	synchronized void putWork(PartialSolution sp) {
	//	System.out.println("WorkPool - adaugare task: " + sp);
		tasks.add(sp);
		/* anuntam unul dintre workerii care asteptau */
		this.notify();

	}
}
