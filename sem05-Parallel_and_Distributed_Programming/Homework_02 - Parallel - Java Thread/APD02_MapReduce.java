
import java.util.ArrayList;
import java.util.Hashtable;
import java.io.*;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;

/* ----------------------------------------------------------------------------
**	Algoritmi Paraleli si Distribuiti:   TEMA 02
**	GULA TANASE, 334CA 
**								Main Class
** ------------------------------------------------------------------------- */


public class APD02_MapReduce {
	
	ArrayList<Hashtable<String, Integer>> freqs = new ArrayList<Hashtable<String, Integer>>();
	
	static String file;
	static int buflen;
	static float maxSim;
	static int nFiles;
	static String[] files;
	
	static void readInput(String input) throws FileNotFoundException, IOException {
		BufferedReader inputFile = new BufferedReader(new FileReader(input));
		file = inputFile.readLine();
		buflen = Integer.parseInt(inputFile.readLine());
		maxSim = Float.parseFloat(inputFile.readLine()) - 0.001f;
		nFiles = Integer.parseInt(inputFile.readLine());
		files = new String[nFiles + 1];
		files[0] = file;
		for (int i = 1; i <= nFiles; i++) {
			files[i] = inputFile.readLine();
			if (files[i].equals(files[0])) {
				files[i] = null;
			}
		}
		inputFile.close();
	}
	
	static ArrayList<Map.Entry<String, Double>> sortValue(Hashtable<String, Double> t) {
		//Transfer as List and sort it
		ArrayList<Map.Entry<String, Double>> l = new ArrayList(t.entrySet());
		
		Collections.sort(l, 
						new Comparator<Map.Entry<String, Double>>() {
							public int compare(Map.Entry<String, Double> o1, Map.Entry<String, Double> o2) {
								return o2.getValue().compareTo(o1.getValue());
							}
						});

		return l;
    }

	public static void main(String args[]) throws IOException, InterruptedException {
		//	retinem argumentele din linia de comanda
		int nThreads = Integer.parseInt(args[0]);
		String input = args[1];
		String output = args[2];

		//	retinem datele din fisierul de input
		readInput(input);

		WorkerMap[] wm = new WorkerMap[nThreads];
		WorkerReduce[] wr = new WorkerReduce[nThreads];
		WorkPool wp = new WorkPool(nThreads);
		WorkPool wpr = new WorkPool(nThreads);
		PartialSolution ps;
		
/* ----------------------------------------------------------------------------
**										MAP
** ------------------------------------------------------------------------- */
		int offset, aux;
		byte[] buf;
		for (int i = 0; i <= nFiles; i++) {
			if (files[i] == null) {
				continue;
			}
			FileInputStream fis = new FileInputStream(new File(files[i]));
			offset = 0;

			while(fis.available() != 0) {
				buf = new byte[buflen + 50];
				aux = fis.read(buf, 0, buflen);
				if (aux == buflen) {
					while (buf[aux - 1] != 32 && buf[aux - 1] != 10) {
						buf[aux] = (byte) fis.read();
						aux++;
					}
				}
				ps = new PartialSolution(files[i] , new String(buf, 0, aux), 0.0);
				wp.putWork(ps);
				
			}	// end of while()
		}		// end of for()
		// Give the map-workers something to do
		WorkerMap wk[] = new WorkerMap[nThreads];
        for (int i = 0; i < nThreads; i++) {
            wk[i] = new WorkerMap(wp, files);
            wk[i].start();
        }
		// wait for the others to finish too
        for (int i = 0; i < nThreads; i++) {
            wk[i].join();
        }

		
/* ----------------------------------------------------------------------------
**									REDUCE
** ------------------------------------------------------------------------- */
		Hashtable<String, Integer> hashInput = WorkerMap.freqs.get(files[0]);
		Integer totalInput = WorkerMap.totalCount.get(files[0]);
		for (int i = 1; i <= nFiles; i++) {
			if (files[i] == null) {
				continue;
			}

			for (String key : hashInput.keySet()) {
				ps = new PartialSolution(files[i] , key, (double)hashInput.get(key) / (double)totalInput);
				wp.putWork(ps);
			}
		}		// end of for()
		// Give the reduce-workers something to do
        for (int i = 0; i < nThreads; i++) {
            wr[i] = new WorkerReduce(wp, files, WorkerMap.freqs, WorkerMap.totalCount);
            wr[i].start();
        }
		// wait for the others to finish too
        for (int i = 0; i < nThreads; i++) {
            wr[i].join();
        }


/* ----------------------------------------------------------------------------
**							COMPARE AND SORT THE RESULT
** ------------------------------------------------------------------------- */
		ArrayList<Map.Entry<String, Double>> list;
		list = sortValue(WorkerReduce.simil);

/* ----------------------------------------------------------------------------
**								PRINT THE OUTPUT
** ------------------------------------------------------------------------- */		
		DecimalFormat dec = new DecimalFormat("0.000");
		BufferedWriter bw = new BufferedWriter(new FileWriter(output));
		bw.write("Rezultate pentru: (" + files[0] + ")\n");
		bw.newLine();
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getKey().equals(files[0])) {
				continue;
			}
			if ((list.get(i).getValue()) >= maxSim) {
				bw.write(list.get(i).getKey() + " (" + dec.format(list.get(i).getValue() / 100.0) + "%)\n");
			}
		}
		bw.close();
			
		
	}		// end of public static void main 
}			// end of main class
