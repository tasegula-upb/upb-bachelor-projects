/* ----------------------------------------------------------------------------
**	Algoritmi Paraleli si Distribuiti:   TEMA 02
**	GULA TANASE, 334CA 
** ------------------------------------------------------------------------- */


public class PartialSolution {
	String text;
	String doc;
	Double freq;
	
	PartialSolution(String doc, String text, Double freq) {
		this.doc = doc;
		this.text = text;
		this.freq = freq;
	}
	
	@Override
	public String toString() {
		if (freq.equals(0.0)) {
			return "Din " + doc + " proceseaza: \n" + text + "\n";
		}
		return "In " + doc + " exista " + freq.toString() + " intrari ale cuvantului " + text + "\n";
	}
}
