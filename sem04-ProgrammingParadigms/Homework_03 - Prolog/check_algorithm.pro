%% CHECK_ALGORITHM.PL
%% Tudor Berariu, 2013

:-consult('ports.pl').
:-consult('match.pl').

%% -------------------------------------------------------------------------- %%
%% -------------------------------------------------------------------------- %%
%% Fișierul conține definițiile predicatelor folosite pentru a puncta
%% strategia de joc.
%% -------------------------------------------------------------------------- %%
%% -------------------------------------------------------------------------- %%

verb(true):-
    current_predicate(verbose/0),
    !.
verb(false).

%% -------------------------------------------------------------------------- %%
%% -------------------------------------------------------------------------- %%

%% random_score/2
%% random_score(-Score)
random_score(2):-
    verb(Verbose),
    match(50, random_player, my_player, Opp, Student, Verbose),
    format("~nPLAYER vs RANDOM : ~d - ~d~n~n",[Student, Opp]),
    Student >= 45,
    !.
random_score(0).

%% dummy1_score/2
%% dummy1_score(-Score)
dummy1_score(2):-
    verb(Verbose),
    match(50, dummy_player_1, my_player, Opp, Student, Verbose),
    format("~nPLAYER vs DUMMY1 : ~d - ~d~n~n",[Student, Opp]),
    Student >= 40,
    Student >= Opp,
    !.
dummy1_score(0).

%% dummy2_score/2
%% dummy2_score(-Score)
dummy2_score(3):-
    verb(Verbose),
    match(50, dummy_player_2, my_player, Opp, Student, Verbose),
    format("~nPLAYER vs DUMMY2 : ~d - ~d~n~n",[Student, Opp]),
    Student >= 40,
    Student >= Opp,
    !.
dummy2_score(0).

%% algorithm_score/2
%% algorithm_score(-Score)
algorithm_score(Score):-
    random_score(S1),
    dummy1_score(S2),
    dummy2_score(S3),
    Score is S1 + S2 + S3.
