%% COLLAPSE.PL
%% Tudor Berariu, 2013

:-ensure_loaded('utils.pl').

%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
%% În acest fișier se va scrie definiția predicatului collapse/3.
%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%


%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
%% collapse/3
%% collapse(-Entanglements, +[Index, Position], -LeftEntanglements)
%% Predicatul collapse/3 primește o lista simbolurilor aflate în stare
%% cuantică  și o măsurătoare în forma unei liste [Index, Position] și
%% este satisfăcut prin legarea tuturor variabilelor corespunzătoare
%% pozițiilor ce sunt fixate prin colaps în urma măsurătorii. De asemenea,
%% în LeftEntanglements sunt păstrate acele legări din lista inițială
%% care nu sunt afectate de măsurătoarea curentă. Elementele listelor
%% Entanglements și LeftEntanglements sunt de forma
%% entanglement(Symbol, Index, Pos1, Pos2).

%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%

%% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %
%% START OF CODE  %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %
%% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %

% --------------------------------------------------------------------------- %
% returneaza in Element, variabila din lista de legaturi care contine Indexul %
% --------------------------------------------------------------------------- %
% getEntanglementByIndex(+Entanglements, +Index, -Element).					  %
% --------------------------------------------------------------------------- %
getEntanglementByIndex([H|Tail], Index, Element):-
	arg(2, H, Y),
	(Y == Index -> Element = H ;
		getEntanglementByIndex(Tail, Index, Element)
	).

% --------------------------------------------------------------------------- %
% returneaza pozitia pe care elementul o are in interiorul listei 			  %
% pozitia va fi folosita pentru a sterge elementul respectiv de acolo         %
% --------------------------------------------------------------------------- %
% getEntanglementPosition(+Entaglements, +Index, +Contor, -PositionNo).       %
% --------------------------------------------------------------------------- %
getEntanglementPosition([H|Tail], Index, N, Pos):-
	arg(2, H, Y),
	Y is Index,
	Pos = N,
	!;
	N1 is N + 1,
	getEntanglementPosition(Tail, Index, N1, Pos).

% --------------------------------------------------------------------------- %
% returneaza lista de legaturi fara cea care contine Indexul                  %
% adica, sterge din lista de legaturi, legatura de pe pozitia N,              %
% pozitie determinata prin functia de mai sus                                 %
% --------------------------------------------------------------------------- %
% removeNth(+N, +Entanglements, -LeftEntanglements).                          %
% --------------------------------------------------------------------------- %
removeNth(1, [H|Tail], Tail).
removeNth(N, [H|L1], [H|L2]):-
	N1 is N - 1,
	removeNth(N1, L1, L2).

% --------------------------------------------------------------------------- %
% cauta restul elementelor care mai erau in pozitia data                      %
% --------------------------------------------------------------------------- %
% getNeighbours(+Position, +EntanglementsToSearchIn, -Neighbours)             %
% --------------------------------------------------------------------------- %
getNeighbours(_, [], []).
getNeighbours(Position, [H|Tail], Neigh):-
	H = entanglement(_, _, P1, P2),
	(Position == P1 -> getNeighbours(Position, Tail, NeighAux),
		Neigh = [H|NeighAux];
	Position == P2 -> getNeighbours(Position, Tail, NeighAux),
		Neigh = [H|NeighAux];
	getNeighbours(Position, Tail, NeighAux), Neigh = NeighAux
	).

% --------------------------------------------------------------------------- %
% cauta Indexul si Pozitia dintr-un element de tip entanglement				  %
% --------------------------------------------------------------------------- %
% getData(+Entanglement, +PositionToSearch, -[Index, Position])               %
% --------------------------------------------------------------------------- %
getData(Elem, Position, [I,P]):-
	Elem = entanglement(Sym, Idx, P1, P2),
	I = Idx,
	(Position == P1 -> P = P2;
	Position == P2 -> P = P1
	).
			
% --------------------------------------------------------------------------- %
% primeste vecinii, si returneaza o lista de forma [ceva(Index, Position),..] %
% --------------------------------------------------------------------------- %
% getNext(+EntanglementsToWorkOn, +PositionToSearch, -IndicesAux, -Indices).  %
% --------------------------------------------------------------------------- %
getNext([], _, Indices, Indices).
getNext([H|Tail], Pos, Indices, All):-
	X = [something(I,P)],
	getData(H, Pos, [I, P]),
	append(Indices, X, NewLine),
	getNext(Tail, Pos, NewLine, All).

% --------------------------------------------------------------------------- %
% primeste o lista de [Index, Position] care trebuiesc in continuare          %
% folosite in collapse                                                        %
% --------------------------------------------------------------------------- %
% continueCollapse(+Entanglements, +List[Index,Position], -LeftEntanglements) %
% --------------------------------------------------------------------------- %
continueCollapse(Entags, [], _, Entags).
continueCollapse([], [], Left, Left).
continueCollapse(Entags, [H|Tail], Aux1, Left):-
	H = something(I,P),
	getEntanglementByIndex(Entags, I, Element),
	arg(1, Element, Sym),
	getEntanglementPosition(Entags, I, 1, N),
	removeNth(N, Entags, LeftEnt),
	getNeighbours(P, LeftEnt, Neigh),
	Aux = LeftEnt,
	(Neigh == [] -> continueCollapse(LeftEnt, Tail, Aux, Left);
		getNext(Neigh, P, NextAux, Next),
		append(Tail, Next, NewList),
		continueCollapse(LeftEnt, NewList, Aux, Left)
	),
	P = Sym.
	
% --------------------------------------------------------------------------- %
%                                                                             %
% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %
collapse(Entags, [Index, Pos], LeftEnt):-
	getEntanglementByIndex(Entags, Index, Element),
	arg(1, Element, Sym),
	getEntanglementPosition(Entags, Index, 1, N),
	removeNth(N, Entags, Left),
	getNeighbours(Pos, Left, Neigh),
	getNext(Neigh, Pos, Aux, Next),
	continueCollapse(Left, Next, Auxiliar, Lefts),
	Pos = Sym,
	LeftEnt = Lefts, !.


	
	