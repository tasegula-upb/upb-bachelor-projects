#!/usr/bin/swipl -s

%% TESTER.PL
%% Tudor Berariu, 2013

%% -------------------------------------------------------------------------- %%
%% -------------------------------------------------------------------------- %%
%% Script pentru punctarea unei teme.
%% -------------------------------------------------------------------------- %%
%% -------------------------------------------------------------------------- %%

%% -------------------------------------------------------------------------- %%
%% -------------------------------------------------------------------------- %%
%% grade_collapse/1
%% grade_collapse(-Score)
grade_collapse(0):-
    not(consult('collapse.pl')),
    format("Nu s-a putut încărca collapse.pl~n",[]),
    !.
grade_collapse(0):-
    not(current_predicate(collapse/3)),
    format("Nu există definiții pentru collapse/3~n",[]),
    !.
grade_collapse(Score):-
    consult('check_collapse.pl'),
    collapse_score(Score).
%% -------------------------------------------------------------------------- %%
%% -------------------------------------------------------------------------- %%
%% grade_algorithm/1
%% grade_algorithm(-Score)
grade_algorithm(0):-
    not(consult('my_player.pl')),
    format("Nu s-a putut încărca my_player.pl~n",[]),
    !.
grade_algorithm(0):-
    not(current_predicate(my_player/2)),
    format("Nu există definiții pentru my_player/2~n",[]),
    !.
grade_algorithm(Score):-
    consult('check_algorithm.pl'),
    algorithm_score(Score).
%% -------------------------------------------------------------------------- %%
%% -------------------------------------------------------------------------- %%
%% grade_homework/0
grade_homework:-
    grade_collapse(S1),
    grade_algorithm(S2),
    Score is S1 + S2,
    format("PUNCTAJ TOTAL: ~d~n",[Score]).
%% -------------------------------------------------------------------------- %%
%% -------------------------------------------------------------------------- %%
:-
    grade_homework.
:-
    halt.
