%% CHECK_COLLAPSE.PL
%% Tudor Berariu, 2013

:-consult('utils.pl').
:-consult('collapse.pl').

%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
%% Fișierul conține predicatele necesare pentru testarea predicatului
%% collapse/3 din collapse.pl.
%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%

%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
%% test_collapse/6
%% test_collapse(+Entanglements, +Measurement, +Vars, +AfterVars, +FreeVars,
%%      +Correct)
test_collapse(Name, Entanglements, Measurement, Vars, AfterVars,
        FreeVars, Correct):-
    (
        collapse(Entanglements, Measurement, Left),
        !,
        (
            equiv_same_list(Vars, AfterVars),
            !,
            (
                all(X, FreeVars, var(X)),
                !,
                (
                    equiv_diff(Correct, Left, []),
                    equiv_diff(Left, Correct, []),
                    length(Correct, L),
                    length(Left, L),
                    format("  [~w] collapse test PASSED~n",[Name]),
                    !
                ;
                    format("  [~w] collapse(~w, ~w, ..) FAILED. ~s : ~w ~~ ~w~n",
                            [Name, Entanglements, Measurement,
                                "incorrect left entanglements", Left, Correct]),
                    fail
                )
            ;
                format("  [~w] collapse(~w, ~w, ..) FAILED. ~s : ~w ~~ ~w~n",
                        [Name, Entanglements, Measurement,
                            "extra positions bound", Left, Correct]),
                fail
            )
        ;
            format("  [~w] collapse(~w, ~w, ..) FAILED. ~s : ~w ~~ ~w~n",
                    [Name, Entanglements, Measurement,
                        "positions left free", Vars, AfterVars]),
            fail
        )
    ;
        format('  [~w] collapse(~w, ~w, ..) FAILED. FALSE~w~n',
                [Name, Entanglements, Measurement]),
        fail
    ).
%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%


%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
collapse_test1:-
    test_collapse(1, [entanglement('X',9,P1,P1)],
            [9, P1],
            [P1, 'X', '0', 'X', '0', 'X', 'X', '0', '0'],
            ['X', 'X', '0', 'X', '0', 'X', 'X', '0', '0'],
            [],
            []).
%% ------------------------------------------------------------------------- %%
collapse_test2:-
    test_collapse(2, [entanglement('X',1,P1,P2), entanglement('0',4,P2,P3),
                entanglement('X',3,P3,P1), entanglement('0',2,P6,P7)],
            [1, P2],
            [P1, P2, P3, P4, P5, P6, P7, P8, P9],
            ['X', 'X', '0', P4, P5, P6, P7, P8, P9],
            [P4, P5, P6, P7, P8, P9],
            [entanglement('0',2,P6,P7)]).
%% ------------------------------------------------------------------------- %%
collapse_test3:-
    test_collapse(3, [entanglement('X',1,P1,P2), entanglement('0',4,P2,P3),
                entanglement('X',3,P3,P1), entanglement('0',2,P3,P7)],
            [1, P2],
            [P1, P2, P3, P4, P5, P6, P7, P8, P9],
            ['X', 'X', '0', P4, P5, P6, '0', P8, P9],
            [P4, P5, P6, P8, P9],
            []).
%% ------------------------------------------------------------------------- %%
collapse_test4:-
    test_collapse(4, [entanglement('X',1,P1,P2), entanglement('0',6,P2,P3),
                entanglement('X',3,P3,P1), entanglement('0',4,P3,P7),
                entanglement('X',5,P7,P9), entanglement('0',2,P9,P5)],
            [3, P3],
            [P1, P2, P3, P4, P5, P6, P7, P8, P9],
            ['X', '0', 'X', P4, '0', P6, '0', P8, 'X'],
            [P4, P6, P8],
            []).
%% ------------------------------------------------------------------------- %%
collapse_test5:-
    test_collapse(5, [entanglement('X',1,P1,P2), entanglement('0',2,P2,P3),
                entanglement('X',3,P3,P6), entanglement('0',4,P6,P7),
                entanglement('X',5,P7,P9), entanglement('0',6,P9,P5),
                entanglement('X',7,P5,P4), entanglement('0',8,P4,P1)],
            [6, P5],
            [P1, P2, P3, P4, P5, P6, P7, P8, P9],
            ['0', 'X', '0', 'X', '0', 'X', '0', P8, 'X'],
            [P8],
            []).
%% ------------------------------------------------------------------------- %%
collapse_test6:-
    test_collapse(6, [entanglement('X',1,P1,P2), entanglement('0',8,P4,P1),
                entanglement('X',5,P7,P9), entanglement('0',6,P9,P5),
                entanglement('X',3,P3,P6), entanglement('0',4,P6,P7),
                entanglement('X',7,P5,P4), entanglement('0',2,P2,P3)],
            [6, P5],
            [P1, P2, P3, P4, P5, P6, P7, P8, P9],
            ['0', 'X', '0', 'X', '0', 'X', '0', P8, 'X'],
            [P8],
            []).
%% ------------------------------------------------------------------------- %%
collapse_test7:-
    test_collapse(7, [entanglement('X',1,P1,P2), entanglement('0',8,P8,P1),
                entanglement('X',5,P7,P9), entanglement('0',6,P9,P5),
                entanglement('X',3,P3,P6), entanglement('0',4,P6,P7),
                entanglement('X',7,P5,P4), entanglement('0',2,P2,P8)],
            [2, P8],
            [P1, P2, P3, P4, P5, P6, P7, P8, P9],
            ['0', 'X', P3, P4, P5, P6, P7, '0', P9],
            [P3, P4, P5, P6, P7, P9],
            [entanglement('X',5,P7,P9), entanglement('0',6,P9,P5),
                entanglement('X',3,P3,P6), entanglement('0',4,P6,P7),
                entanglement('X',7,P5,P4)]).
%% ------------------------------------------------------------------------- %%
collapse_test8:-
    test_collapse(8, [entanglement('X',1,P1,P7), entanglement('X',5,P7,P9),
                entanglement('0',6,P2,P4), entanglement('X',3,P3,P9),
                entanglement('0',4,P1,P5), entanglement('X',7,P2,P4),
                entanglement('0',2,P5,P8)],
            [6, P4],
            [P1, P2, P3, P4, P5, P6, P7, P8, P9],
            [P1, 'X', P3, '0', P5, P6, P7, P8, P9],
            [P1, P3, P5, P6, P7, P8, P9],
            [entanglement('X',1,P1,P7), entanglement('X',5,P7,P9),
                entanglement('X',3,P3,P9), entanglement('0',4,P1,P5),
                entanglement('0',2,P5,P8)]).
%% ------------------------------------------------------------------------- %%
collapse_test9:-
    test_collapse(9, [entanglement('X',1,P1,P4), entanglement('0',2,P2,P5),
                entanglement('X',3,P3,P6), entanglement('0',4,P4,P7),
                entanglement('X',5,P5,P8), entanglement('0',6,P6,P9),
                entanglement('X',7,P7,P2), entanglement('0',8,P8,P3),
                entanglement('X',9,P9,P1)],
            [6, P6],
            [P1, P2, P3, P4, P5, P6, P7, P8, P9],
            ['X', '0', 'X', '0', 'X', '0', 'X', '0', 'X'],
            [],
            []).
%% ------------------------------------------------------------------------- %%
collapse_test10:-
    test_collapse(10, [entanglement('X',5,P1,P9),
                entanglement('0',4,P2,P5), entanglement('X',3,P5,P9),
                entanglement('0',2,P4,P6), entanglement('X',1,P1,P5)],
            [1, P5],
            [P1, P2, P3, P4, P5, P6, P7, P8, P9],
            ['X', '0', P3, P4, 'X', P6, P7, P8, 'X'],
            [P3, P4, P6, P7, P8],
            [entanglement('0',2,P4,P6)]).
%% ------------------------------------------------------------------------- %%
collapse_testcosmin:-
    test_collapse('cosmin', [entanglement('X',9,P3,P4),entanglement('0',8,P7,P1),
                entanglement('X',7,P3,P7),entanglement('0',6,P4,P7)],
            [9, P3],
            [ P1, '0',  P3,  P4, 'X', 'X',  P7, 'X', '0'],
            ['0', '0', 'X', '0', 'X', 'X', 'X', 'X', '0'],
            [],
            []).
%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%

%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
%% collapse_score/1
%% collapse_score(-Score)
collapse_score(3):-
    collapse_test1,
    collapse_test2,
    collapse_test3,
    collapse_test4,
    collapse_test5,
    collapse_test6,
    collapse_test7,
    collapse_test8,
    collapse_test9,
    collapse_test10,
    collapse_testcosmin,
    !.
collapse_score(0).
%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
