%% Tudor Berariu, 2013
%% quantum_tictactoe.pl

:-ensure_loaded('utils.pl').
:-ensure_loaded('collapse.pl').
:-ensure_loaded('check_validity.pl').
:-ensure_loaded('stdout_board.pl').

%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
%% 1. STAREA JOCULUI
%% ------------------------------------------------------------------------- %%
%% O STARE A JOCULUI va fi compusă din starea tablei de joc și mulțimea
%% perechilor de poziții legate.
%%
%% STAREA TABLEI DE JOC va fi reprezentată printr-o listă de 9 variabile:
%% [ Pos1, Pos2, Pos3 ,
%%   Pos4, Pos5, Pos6 ,
%%   Pos7, Pos8, Pos9 ]
%%  * Dacă o variabilă PosN este liberă, atunci poziția respectivă pe tablă
%%      este liberă.
%%  * Dacă o variabilă PosN este legată la constanta 'x', atunci în poziția
%%      respectivă pe tablă se află simbolul X.
%%  * Dacă o variabilă PosN este legată la constanta '0', atunci în poziția
%%      respectivă pe tablă se află simbolul 0.
%%
%% MULȚIMEA POZIȚIILOR LEGATE este reprezentată printr-o listă de structuri
%% entanglement(Symbol,Index,PosX,PosY), având semnificația că pozițiile
%% PosX și PosY sunt legate de simbolul Symbol (unde Symbol poate fi 'X' sau
%% '0'), iar Index este numărul mutării în care s-a plasat simbolul:
%% [ ..., entanglement(Symbol,Index,PosX,PosY), ... ]
%% În lista de perechi de poziții legate nu pot apărea decât variabile
%% nelegate.
%%
%% Convenții:
%%  * în general, în cod, lista cu pozițiile de pe tablă va avea numele Board
%%  * în general, în cod, lista cu pozițiile legate se va numi Entanglements
%%  * o variabilă care reprezintă o celulă a tablei va fi denumită cu
%%      Pos, Position, PosX, PosY, Pos1, Pos2, etc.
%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%

%% ------------------------------------------------------------------------- %%
%% 2. JUCĂTORII
%% ------------------------------------------------------------------------- %%
%% Pentru a implementa UN JUCĂTOR, trebuie scris un fapt
%% nume_jucător(move_predicate, measure_predicate) unde
%% move_predicate și collapse_predicate sunt două predicate cu
%% următoarele argumente:
%%
%% move_predicate(+Symbol, +Board, +Entanglements, -Move)
%% unde Symbol este simbolul jucătorului ce trebuie să mute, Board este
%% tabla în starea curentă, Entanglements lista perechilor de poziții
%% legate, iar Move este legată la satisfacerea predicatului la o listă cu
%% două variabile libere corespunzătoare unor poziții de pe tabla Board
%%
%% measure_predicate(+MySymbol, +Board, +Cycle, +OtherEntanglements,
%%         -[Index, Position])
%% unde MySymbol este simbolul jucătorului ce trebuie să facă măsurătoarea,
%% Board este tabla de joc în starea curentă, Cycle sunt acele legări care
%% formează un ciclu, OtherEntanglements lista celorlalte legări, iar
%% după satisfacerea scopului, Index va fi legată la indicele simbolului
%% măsurat, iar Position va fi una dintre variabilele corespunzătoare
%% pozițiilor în care se afla simbolul măsurat
%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%

%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
%% 3. STĂRI INIȚIALE ȘI FINALE
%% ------------------------------------------------------------------------- %%
%% init_board/1
%% init_board(-EmptyBoard)
%% init_board leagă variabila EmptyBoard la o listă cu 9 variabile libere
init_board([_, _, _, _, _, _, _, _, _]).
%% ------------------------------------------------------------------------- %%
%% symbol/1
%% symbol(?Symbol)
symbol('0').
symbol('X').
%% ------------------------------------------------------------------------- %%
%% symbol/1
%% symbol(?Symbol)
next('0', 'X').
next('X', '0').
%% ------------------------------------------------------------------------- %%
%% all_same/4
%% all_same(+Pos1, +Pos2, +Pos3, ?Player)
all_same(Pos1, Pos2, Pos3, Player):-
    symbol(Player),
    Pos1 == Player,
    Pos2 == Player,
    Pos3 == Player.
%% ------------------------------------------------------------------------- %%
%% three_symbols/2
%% three_symbols(+Board, +Player)
three_symbols([P1, P2, P3|_], Player):-
    all_same(P1, P2, P3, Player).
three_symbols([_, _, _|NextRow], Player):-
    three_symbols(NextRow, Player).
three_symbols([P1, _, _, P2, _, _, P3, _, _], Player):-
    all_same(P1, P2, P3, Player).
three_symbols([_, P1, _, _, P2, _, _, P3, _], Player):-
    all_same(P1, P2, P3, Player).
three_symbols([_, _, P1, _, _, P2, _, _, P3], Player):-
    all_same(P1, P2, P3, Player).
three_symbols([_, _, P1, _, _, P2, _, _, P3], Player):-
    all_same(P1, P2, P3, Player).
three_symbols([P1, _, _, _, P2, _, _, _, P3], Player):-
    all_same(P1, P2, P3, Player).
three_symbols([_, _, P1, _, P2, _, P3, _, _], Player):-
    all_same(P1, P2, P3, Player).
%% ------------------------------------------------------------------------- %%
%% draw/1
%% draw(+Board)
draw(Board):-
    three_symbols(Board, 'X'),
    three_symbols(Board, '0'),
    !.
draw(Board):-
    \+ (member(Pos, Board), var(Pos)),
    \+ three_symbols(Board, _).
%% ------------------------------------------------------------------------- %%
%% winner/2
%% winner(+Board, -Player)
winner(Board, Player):-
    next(Player1, Player2),
    !,
    findall(_, three_symbols(Board, Player1), R1),
    length(R1, S1),
    findall(_, three_symbols(Board, Player2), R2),
    length(R2, S2),
    (
        S1 > S2, Player = Player1, !
    ;
        S2 > S1, Player = Player2
    ).
%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%


%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
%% 4. GĂSIREA UNUI CICLU
%% ------------------------------------------------------------------------- %%
%% find_cycle/3
%% find_cycle(+Entanglements, -Cycle, -OtherEntanglements)
find_cycle(Entanglements, Cycle, OtherEntanglements):-
    [entanglement(_,_,Start,_)|_] = Entanglements,
    dfs_search([[Start,false,false,false]], Entanglements, Cycle),
    check_diff(Entanglements, Cycle, OtherEntanglements,
        entanglement(_,Index1,_,_), entanglement(_,Index2,_,_),
        Index1 == Index2),
    !.
find_cycle([_|Entanglements], Cycle, OtherEntanglements):-
    find_cycle(Entanglements, Cycle, OtherEntanglements).
%% ------------------------------------------------------------------------- %%
%% dfs_search/3
%% dfs_search(+Frontier, +Entanglements, -Cycle)
dfs_search([[X, Symbol, Index, [Parent|Details]]|_], _,
        [entanglement(Symbol,Index,X,Parent)|PathToX]):-
    is_antecedent(X, [Parent|Details], PathToX),
    !.
dfs_search([X|Frontier], Entanglements, Cycle):-
    get_children(X, Entanglements, Children),
    append(Children, Frontier, NewFrontier),
    dfs_search(NewFrontier, Entanglements, Cycle).
%% ------------------------------------------------------------------------- %%
%% get_children/3
%% get_children(+Node, +Entanglements, -Children)
get_children(Node, Entanglements, Children):-
    Node = [Variable, _, Index, _],
    filter(entanglement(_, Index1, Position1, Position2),
            Entanglements,
            (Index \== Index1, (Variable == Position1 ; Variable == Position2)),
            Arcs),
    map(create_node, Arcs, [Node], Children).
%% ------------------------------------------------------------------------- %%
%% create_node/3
%% create_node(+Entanglement, -Node, +Parent)
create_node(entanglement(Symbol,Index,Pos1,Pos2),
        [Child, Symbol, Index, Node], Node):-
    Node = [Parent|_],
    (
        Pos1 == Parent, !, Child = Pos2
    ;
        Pos2 == Parent, !, Child = Pos1
    ).
%% ------------------------------------------------------------------------- %%
%% is_antecedent/3
%% is_antecedent(+Node1, +Node2, -PathFrom2To1)
is_antecedent(Node1, [Node2|_], []):-
    Node1 == Node2,
    !.
is_antecedent(Node1, [Node2, Symbol2, Index2, [Parent|Details]],
        [entanglement(Symbol2, Index2, Node2, Parent)|Tail]):-
    is_antecedent(Node1, [Parent|Details], Tail).
%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%


%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
%% next_move/10
%% next_move(+Index, +Board, +Entanglements, +Player,
%%         +PlayerMovePredicate, +PlayerMeasurePredicate,
%%         +OpponentMovePredicate, +OpponentMeasurePredicate,
%%         -Winner, +Verbose)

next_move(_, Board, Entanglements, _, _, _, _, _, _, true):-
    display_board(Board,Entanglements),
    fail.

next_move(_, Board, _, _, _, _, _, _, '?', Verbose):-
    draw(Board),
    !,
    display_message(Verbose, "REMIZĂ").

next_move(_, Board, _, _, _, _, _, _, Player, Verbose):-
    winner(Board, Player),
    !,
    format(string(Message), "A CÂȘTIGAT ~w", [Player]),
    display_message(Verbose, Message).

next_move(Index, _, _, Symbol, _, _, _, _, _, true):-
    format(string(Message),'[~w] URMĂTORUL JUCĂTOR : ~w',[Index, Symbol]),
    display_message(true, Message),
    fail.

next_move(Index, Board, Entanglements, Player,
        PlayerMovePredicate, PlayerMeasurePredicate,
        OtherPlayerMovePredicate, OtherPlayerMeasurePredicate,
        Winner, Verbose):-
    find_cycle(Entanglements, Cycle, OtherEntanglements),
    !,
    display_cycle(Verbose, Cycle),
    repeat,
    MeasureGoal =.. [PlayerMeasurePredicate,
                Player, Board, Cycle, OtherEntanglements,
                [MIndex, MPosition]],
    call(MeasureGoal),
    (
        valid_measure(Cycle, [MIndex, MPosition]),
        !
    ;
        display_message(Verbose,"MĂSURĂTOAREA NU ESTE VALIDĂ!"), fail
    ),
    equiv_nth1(Nth, Board, MPosition),
    format(string(Message),"Jucătorul ~w a făcut măsurătoarea ~w = ~w",
            [Player, MIndex, Nth]),
    display_message(Verbose, Message),
    collapse(Entanglements, [MIndex, MPosition], LeftEntanglements),
    next_move(Index, Board, LeftEntanglements, Player,
            PlayerMovePredicate, PlayerMeasurePredicate,
            OtherPlayerMovePredicate, OtherPlayerMeasurePredicate,
            Winner, Verbose).

next_move(_, _, _, _, _, _, _, _, _, true):-
    format(string(Message), "Nu există cicluri", []),
    display_message(true, Message),
    fail.

next_move(Index, Board, Entanglements, Player,
        PlayerMovePredicate, PlayerMeasurePredicate,
        OtherPlayerMovePredicate, OtherPlayerMeasurePredicate,
        Winner, Verbose):-
    MoveGoal =.. [PlayerMovePredicate, Player, Board, Entanglements, Move],
    repeat,
    call(MoveGoal),
    (
        valid_move(Board, Move),
        !
    ;
        format(string(InvalidMove), "MUTAREA ~w NU ESTE VALIDĂ!", [Move]),
        display_message(Verbose, InvalidMove),
        fail
    ),
    Move = [Var1, Var2],
    equiv_nth1(Ind1, Board, Var1),
    equiv_nth1(Ind2, Board, Var2),
    format(string(Message), "Jucătorul ~w a mutat ~w", [Player, [Ind1, Ind2]]),
    display_message(Verbose, Message),
    Index1 is Index + 1,
    next(Player, OtherPlayer),
    next_move(Index1, Board,
        [entanglement(Player,Index,Var1,Var2)|Entanglements], OtherPlayer,
        OtherPlayerMovePredicate, OtherPlayerMeasurePredicate,
        PlayerMovePredicate, PlayerMeasurePredicate,
        Winner, Verbose).
%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%

%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
%% load_player/4
%% load_player(+Player, -MovePredicate, -MeasurePredicate, -CloseGoal)
load_player(Player, MovePredicate, MeasurePredicate, CloseGoal):-
    format(string(Path), "~w.pl", [Player]),
    ensure_loaded(Path),
    (
        current_predicate(Player/2),
        !,
        Goal =.. [Player, MovePredicate, MeasurePredicate],
        call(Goal),
        CloseGoal = true
    ;
        current_predicate(Player/4),
        Goal =.. [Player, MovePredicate, MeasurePredicate, Init, CloseGoal],
        call(Goal),
        call(Init)
    ),
    current_predicate(MovePredicate/4),
    current_predicate(MeasurePredicate/5),
    !.
load_player(Name, _, _):-
    format("Eroare la încărcarea jucătorului ~w.~n", [Name]),
    fail.

%% ------------------------------------------------------------------------- %%
%% close/1
%% close(CloseGoal)
%% se încearcă satisfacerea CloseGoal, dar se asigură că nu eșuează
player_close(Goal):-
    call(Goal),
    fail.
player_close(_).
%% ------------------------------------------------------------------------- %%
%% play/4
%% play(+Player1, +Player2, -Winner, +Verbose)
play(Player1, Player2, Winner, Verbose):-
    load_player(Player1, Player1MovePredicate, Player1MeasurePredicate, Close1),
    load_player(Player2, Player2MovePredicate, Player2MeasurePredicate, Close2),
    init_board(Board),
    next_move(1, Board, [], 'X',
        Player1MovePredicate, Player1MeasurePredicate,
        Player2MovePredicate, Player2MeasurePredicate,
        Winner, Verbose),
    player_close(Close1),
    player_close(Close2).
%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
