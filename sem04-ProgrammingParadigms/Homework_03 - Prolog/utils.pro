%% UTILS.PL
%% Tudor Berariu, 2013

%% -------------------------------------------------------------------------- %%
%% -------------------------------------------------------------------------- %%
%% Fișierul conține definițiile unor predicate foarte, foarte utile.
%% -------------------------------------------------------------------------- %%
%% -------------------------------------------------------------------------- %%


%% -------------------------------------------------------------------------- %%
%% -------------------------------------------------------------------------- %%
%% 1. Predicate care întorc rezultate aleator.
%% -------------------------------------------------------------------------- %%
%% random_int/2
%% random_int(+UpperBound,-RandomInteger)
%% random_int leagă variabila Value la o valoarea întreagă aleasă aleator din
%% intervalul [0 .. UpperBound)
random_int(UpperBound, Value):-
    get_time(Time),
    stamp_date_time(Time, Date, 'UTC'),
    date_time_value(second, Date, Second),
    Seed is floor(Second * 100000) mod 1000,
    set_random(seed(Seed)),
    random(Random),
    Value is floor(UpperBound * Random).

%% -------------------------------------------------------------------------- %%
%% random_element/2
%% random_element(+List, -Elem)
%% random_element
random_element(List, Elem):-
    length(List, Length),
    random_int(Length, Index),
    nth0(Index, List, Elem).
%% -------------------------------------------------------------------------- %%
%% -------------------------------------------------------------------------- %%


%% -------------------------------------------------------------------------- %%
%% -------------------------------------------------------------------------- %%
%% 2. Predicate ultra-șmechere.
%% -------------------------------------------------------------------------- %%
%% all/3
%% all(+Template, +List, +Goal)
all(_, [], _):-!.
all(Template, [Head | Tail], Goal):-
   \+ \+ (Template = Head, Goal),
   all(Template, Tail, Goal).
%% -------------------------------------------------------------------------- %%
%% filter/4
%% filter(+Template, +List, +Goal, -FilteredList)
%% Predicat super șmecher scris de mine, n-ai tu treabă.
filter(_, [], _, []):-!.
filter(Template, [Head | Tail], Goal, [Head | FilteredTail]):-
    \+ \+ (Template = Head, call(Goal)),
    !,
    filter(Template, Tail, Goal, FilteredTail).
filter(Template, [_ | Tail], Goal, FilteredTail):-
    filter(Template, Tail, Goal, FilteredTail).
%% -------------------------------------------------------------------------- %%
%% map/3
%% map(Predicate, List, Output)
%% Predicate primește 2 argumente: Head, Result
map(_, [], []):-!.
map(Predicate, [Head|Tail], [Result|Output]):-
    Goal =.. [Predicate, Head, Result],
    call(Goal),
    !,
    map(Predicate, Tail, Output).
%% -------------------------------------------------------------------------- %%
%% map/4
%% map(Predicate, List, OtherArgs, Output)
%% Predicate primește 2 argumente: Head, Result
map(_, [], _, []):-!.
map(Predicate, [Head|Tail], OtherArgs, [Result|Output]):-
    Goal =.. [Predicate, Head, Result | OtherArgs],
    call(Goal),
    !,
    map(Predicate, Tail, OtherArgs, Output).
%% -------------------------------------------------------------------------- %%
%% indices0/4
%% indices0(+List, -Indices, +Template, +Goal)
indices0(List, Indices, Template, Goal):-
    indices(0, List, Indices, Template, Goal).
%% indices1/4
%% indices1(+List, -Indices, +Template, +Goal)
indices1(List, Indices, Template, Goal):-
    indices(1, List, Indices, Template, Goal).
%% indices/5
%% indices(+Index, +List, -Indices, +Template, +Goal)
indices(_, [], [], _, _):-!.
indices(Index, [Head|Tail], [Index|Indices], Template, Goal):-
    \+ \+ (Template = Head, Goal),
    !,
    Index1 is Index + 1,
    indices(Index1, Tail, Indices, Template, Goal).
indices(Index, [_|Tail], Indices, Template, Goal):-
    Index1 is Index + 1,
    indices(Index1, Tail, Indices, Template, Goal).
%% -------------------------------------------------------------------------- %%
%% check_relation/5
%% check_relation(+X, +Y, +TemplateX, +TemplateY, +Goal)
%% check_relation verifică dacă scopul Goal poate fi satisfăcut când
%% TemplateX unifică cu X și TemplateY unifică cu Y
check_relation(X, Y, TemplateX, TemplateY, Goal):-
    \+ \+ call((X = TemplateX, Y = TemplateY, Goal)).
%% -------------------------------------------------------------------------- %%
%% check_member/3
%% check_member(+X, +List, +TemplateX, +TemplateY, +Goal)
%% check_member verifică dacă în List există un element pentru care
%% scopul check_relation(X, Y, TemplateX, TemplateY, Goal) poate fi
%% satisfăcut
check_member(X, [Head|_], TemplateX, TemplateY, Goal):-
    check_relation(X, Head, TemplateX, TemplateY, Goal),
    !.
check_member(X, [_|Tail], TemplateX, TemplateY, Goal):-
    check_member(X, Tail, TemplateX, TemplateY, Goal).
%% -------------------------------------------------------------------------- %%
%% check_nth0/6
%% check_nth0(?Index, +List, ?X, +TemplateX, +TemplateY, +Goal)
check_nth0(0, [Head|_], X, TemplateX, TemplateY, Goal):-
    check_relation(X, Head, TemplateX, TemplateY, Goal),
    !.
check_nth0(Index1, [_|Tail], X, TemplateX, TemplateElem, Goal):-
    check_nth0(Index, Tail, X, TemplateX, TemplateElem, Goal),
    Index1 is Index + 1.
%% -------------------------------------------------------------------------- %%
%% check_nth1/6
%% check_nth1(?Index, +List, ?X, +TemplateX, +TemplateY, +Goal)
check_nth1(Index1, List, X, TemplateX, TemplateY, Goal):-
    check_nth0(Index0, List, X, TemplateX, TemplateY, Goal),
    Index1 is Index0 + 1.
%% -------------------------------------------------------------------------- %%
%% check_union/6
%% check_union(+L1, +L2, -Union, +TemplateX, +TemplateY, +Goal)
check_union([], Union, Union, _, _, _):-
    !.
check_union([Head | Tail], L2, Union, TemplateX, TemplateY, Goal):-
    \+check_member(Head, L2, TemplateX, TemplateY, Goal),
    !,
    check_union(Tail, L2, Union, TemplateX, TemplateY, Goal).
check_union([Head | Tail], L2, [Head | Union], TemplateX, TemplateY, Goal):-
    check_union(Tail, L2, Union, TemplateX, TemplateY, Goal).
%% -------------------------------------------------------------------------- %%
%% check_union/6
%% check_union(+L1, +L2, -Diff, +TemplateX, +TemplateY, +Goal)
check_diff(L1, L2, Diff, TemplateX, TemplateY, Goal):-
    filter(Elem, L1,
            \+check_member(Elem, L2, TemplateX, TemplateY, Goal), Diff).
%% -------------------------------------------------------------------------- %%
%% check_same_list/5
%% check_same_list(L1, L2, Template1, Template2, Goal)
check_same_list([], [], _, _, _):-
    !.
check_same_list([Head1 | Tail1], [Head2 | Tail2], Template1, Template2, Goal):-
    check_relation(Head1, Head2, Template1, Template2, Goal),
    check_same_list(Tail1, Tail2, Template1, Template2, Goal).
%% -------------------------------------------------------------------------- %%
%% equiv_member/2
%% equiv_member(+Element, +List)
%% equiv_member verifică dacă un element se află într-o listă. Diferența față
%% de predicatul member/2 din Prolog este aceea că acesta verifică
%% echivalența și nu dacă reușește unificarea.
equiv_member(X, List):-
    check_member(X, List, TemplateX, TemplateElem, TemplateX == TemplateElem).
%% -------------------------------------------------------------------------- %%
%% equiv_nth0/3
%% equiv_nth0(?Index, +List, ?X)
equiv_nth0(Index, List, X):-
    check_nth0(Index, List, X, TemplateX, TemplateY, TemplateX==TemplateY).
%% -------------------------------------------------------------------------- %%
%% equiv_nth1/3
%% equiv_nth1(?Index, +List, ?X)
equiv_nth1(Index, List, X):-
    check_nth1(Index, List, X, TemplateX, TemplateY, TemplateX==TemplateY).
%% -------------------------------------------------------------------------- %%
%% equiv_union/3
%% equiv_union(+L1, +L2, -Union)
equiv_union(L1, L2, Union):-
    check_union(L1, L2, Union, X, Y, X==Y).
%% -------------------------------------------------------------------------- %%
%% equiv_diff/3
%% equiv_diff(+L1, +L2, -Diff)
equiv_diff(L1, L2, Diff):-
    check_diff(L1, L2, Diff, X, Y, X==Y).
%% -------------------------------------------------------------------------- %%
%% equiv_same_list/2
%% equiv_same_list(+L1, +L2)
equiv_same_list(L1, L2):-
    check_same_list(L1, L2, X, Y, X==Y).
%% -------------------------------------------------------------------------- %%
%% -------------------------------------------------------------------------- %%
