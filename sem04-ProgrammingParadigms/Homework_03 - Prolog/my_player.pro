%% MY_PLAYER.PL
%% Tudor Berariu, 2013

:-ensure_loaded('utils.pl').

%% verbose.

%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
%% Fișierul conține definițiile necesare unui jucător de QTTT care mută
%% aleator.
%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%


%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
%% my_move/4
%% my_move(+Symbol, +Board, +Entanglements, -Move)
my_move(_, Board, _, [PosX, PosY]):-
    filter(Position, Board, var(Position), FreePositions),
    (
        FreePositions = [PosX], % Dacă există o singură poziție liberă
        FreePositions = [PosY],
        !
    ;
        random_element(FreePositions, PosX),
        repeat, % Număr infinit de puncte de întoarcere
        random_element(FreePositions, PosY),
        PosX \== PosY, % Se generează PosY până este diferit de PosX
        ! % Renunță la punctele de întoarcere create de retry/0
    ).
%% ------------------------------------------------------------------------- %%
%% my_measure/5
%% my_measure(+MySymbol, +Board, +Cycle, +OtherEntanglements,
%%         -[Index, Position])
my_measure(_, _, Cycle, _, [Index, Pos]):-
    random_element(Cycle, entanglement(_, Index, Pos1, Pos2)),
    random_element([Pos1, Pos2], Pos).
%% ------------------------------------------------------------------------- %%
%% my_player/2
%% my_player(-MovePredicate, -MeasurePredicate)
my_player(my_move, my_measure).
%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
