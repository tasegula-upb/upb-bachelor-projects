#!/bin/bash

# RUN.SH
# Tudor Berariu, 2013

get_free_port()
{
    Port=35000
    while netstat -atwn | grep "^.*:${Port}.*:\*\s*LISTEN\s*$"
    do
	Port=$(( ${Port} + 1 ))
    done
    eval "$1='${Port}'"
}

PORT_DUMMY_1=
get_free_port PORT_DUMMY_1
eval "./qttt-minimax-agent $PORT_DUMMY_1 1 &"
PID_DUMMY_1=$!

sleep 1

PORT_DUMMY_2=
get_free_port PORT_DUMMY_2
eval "./qttt-minimax-agent $PORT_DUMMY_2 3 &"
PID_DUMMY_2=$!

rm -f ports.pl
echo "dummy1_port($PORT_DUMMY_1)." > ports.pl
echo "dummy2_port($PORT_DUMMY_2)." >> ports.pl

sleep 1

./tester.pl

rm -f ports.pl

kill $PID_DUMMY_1
kill $PID_DUMMY_2