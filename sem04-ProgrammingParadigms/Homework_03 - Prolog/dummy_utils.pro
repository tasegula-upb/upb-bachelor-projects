%% DUMMY_UTILS.PL
%% Tudor Berariu, 2013

%% position_to_string/2
position_to_string(X,'_'):-var(X),!.
position_to_string(Symbol, Symbol).

%% entanglements_to_string/3
entanglement_to_string(E, StrE, Board):-
    E = entanglement(Symbol, Index, Var1, Var2),
    equiv_nth1(I1, Board, Var1),
    equiv_nth1(I2, Board, Var2),
    format(string(StrE), "~w~d~d~d", [Symbol, Index, I1, I2]).

%% concat_all/2
concat_all(List, String):-
    concat_all(List, '', String).
%% concat_all/3
concat_all([], Acc, Acc):-!.
concat_all([S|Rest], Acc, Str):-
    string_concat(Acc, S, Next),
    concat_all(Rest, Next, Str).
