%% DUMMY_PLAYER_1.PL
%% Tudor Berariu, 2013

:-dynamic dummy1_socket/3.
:-ensure_loaded('utils.pl').
:-ensure_loaded('dummy_utils.pl').

%% dummy1_init_player/0
dummy1_init_player:-
    dummy1_port(Port),
    gethostname(Host),
    tcp_socket(Socket),
    tcp_connect(Socket, Host:Port),
    tcp_open_socket(Socket, In, Out),
    retractall(dummy1_socket(_, _, _)),
    assert(dummy1_socket(Socket, In, Out)).

%% dummy1_close_player/0
dummy1_close_player:-
    dummy1_socket(_, In, Out),
    format(Out, "STOP",[]),
    flush_output(Out),
    retractall(dummy1_socket(_, _, _)),
    close(In, [force(true)]),
    close(Out, [force(true)]).

%% dummy1_move/4
dummy1_move(Symbol, Board, Entanglements, Move):-
    map(position_to_string, Board, CharBoard),
    map(entanglement_to_string, Entanglements, [Board], StrEntanglements),
    concat_all(StrEntanglements, StrEnt),
    string_to_list(StrBoard, CharBoard),
    length(StrEntanglements,EntNo),
    dummy1_socket(_, In, Out),
    format(Out, "MOVE|~w|~s|~d|~s|END",[Symbol, StrBoard, EntNo, StrEnt]),
    flush_output(Out),
    read_line_to_codes(In, StrMove),
    format(atom(AtomMove),"~s",[StrMove]),
    atom_to_term(AtomMove, move(P1, P2),[]),
    nth1(P1, Board, Pos1),
    nth1(P2, Board, Pos2),
    Move = [Pos1, Pos2].

%% dummy1_measure
dummy1_measure(Symbol, Board, Cycle, Entanglements, Measure):-
    map(position_to_string, Board, CharBoard),
    append(Cycle, Entanglements, AllEntanglements),
    map(entanglement_to_string, AllEntanglements, [Board], StrEntanglements),
    concat_all(StrEntanglements, StrEnt),
    string_to_list(StrBoard, CharBoard),
    length(StrEntanglements,EntNo),
    dummy1_socket(_, In, Out),
    format(Out, "MEAS|~w|~s|~d|~s|END",[Symbol, StrBoard, EntNo, StrEnt]),
    flush_output(Out),
    read_line_to_codes(In, StrMove),
    format(atom(AtomMove),"~s",[StrMove]),
    atom_to_term(AtomMove, measure(P1, P2), []),
    nth1(P2, Board, Pos2),
    Measure = [P1, Pos2].

%% dummy_player_1
dummy_player_1(dummy1_move, dummy1_measure,
            dummy1_init_player, dummy1_close_player).
