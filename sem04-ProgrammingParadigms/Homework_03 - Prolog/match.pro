%% MATCH.PL
%% Tudor Berariu, 2013

:-use_module(library(time)).
:-consult('quantum_tictactoe.pl').

%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
%% Fișierul conține definițiile predicatelor necesare pentru confruntarea
%% jucătorilor.
%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%


%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
%% one_match/5
%% one_match(+Player1, +Player2, -Score1, -Score2, +Verbose)
one_match(Player1, Player2, Score1, Score2, Verbose):-
    play(Player1, Player2, Winner, Verbose),
    !,
    (
        Winner == '?', !, Score1 = 1, Score2 = 1
    ;
        Winner == 'X', Score1 = 1, Score2 = 0
    ;
        Winner == '0', Score1 = 0, Score2 = 1
    ).
%% ------------------------------------------------------------------------- %%
%% match/6
%% match(+N, +Player1, +Player2, -Final1, -Final2, +Verbose)
match(N, Player1, Player2, Final1, Final2, Verbose):-
    match(N, Player1, Player2, 0, 0, Final1, Final2, Verbose).
%% match/8
%% match(+N, +Player1, +Player2, +Score1, +Score2, -Final1, -Final2, +Verbose)
match(0, _, _, Score1, Score2, Score1, Score2, _):-!.
match(N, Player1, Player2, Before1, Before2, Final1, Final2, Verbose):-
    (
        N mod 2 =:= 0,
        !,
        one_match(Player1, Player2, Result1, Result2, Verbose)
    ;
        one_match(Player2, Player1, Result2, Result1, Verbose)
    ),
    Score1 is Before1 + Result1,
    Score2 is Before2 + Result2,
    (
        Verbose = true,
        !,
        format("~n~nSCOR CURENT : ~d - ~d~n~n",[Score1, Score2])
    ;
        Verbose = false
    ),
    N1 is N - 1,
    !,
    match(N1, Player1, Player2, Score1, Score2, Final1, Final2, Verbose).
%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
