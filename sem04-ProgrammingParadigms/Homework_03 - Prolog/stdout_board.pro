%% STDOUT_BOARD.PL
%% Tudor Berariu, 2013

:-consult('utils.pl').

%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
%% Fișierul conține definițiile necesare afișării stării curente a tablei.
%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%


%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
%% nth1_default/4
%% nth1_default(+Index, -Element, List, Default)
nth1_default(Index, Element, List, _):-
    nth1(Index, List, Element),
    !.
nth1_default(_, Default, _, Default).
%% ------------------------------------------------------------------------- %%
%% cell_row/3
%% cell_row(+Content, +Row, -String)
output('0', 1, '   0000   '):-!.
output('0', 2, '  00  00  '):-!.
output('0', 3, '   0000   '):-!.
output('X', 1, '  XX  XX  '):-!.
output('X', 2, '    XX    '):-!.
output('X', 3, '  XX  XX  '):-!.
output(CellSymbols, Row, CellRow):-
    RowIndex is (Row - 1) * 3,
    Index1 is RowIndex + 1,
    Index2 is RowIndex + 2,
    Index3 is RowIndex + 3,
    map(nth1_default, [Index1, Index2, Index3], [CellSymbols, '  '], Symbols),
    format(string(CellRow), ' ~w ~w ~w ', Symbols).
%% ------------------------------------------------------------------------- %%
%% separator/0
separator:-
    format('+----------+----------+----------+~n').
%% ------------------------------------------------------------------------- %%
%% separator/1
%% separator(-String)
symbol_name(entanglement(Symbol, Index, _, _), Name):-
    format(string(Name),'~w~w',[Symbol, Index]).
%% ------------------------------------------------------------------------- %%
%% get_symbols/3
%% get_symbols(+Position, -CellSymbols, +Entanglements)
get_symbols(Var, 'X', _):-Var == 'X', !.
get_symbols(Var, '0', _):-Var == '0', !.
get_symbols(Pos, CellSymbols, Entanglements):-
    filter(entanglement(_, _, Position1, Position2),
            Entanglements,
            (Position1 == Pos ; Position2 == Pos),
            FilteredEntanglements),
    map(symbol_name, FilteredEntanglements, CellSymbols).
%% ------------------------------------------------------------------------- %%
%% display_line/3
%% display_line(+Cells, +Row, +Separator)
display_line([], _):-!.
display_line([_, _, _|Cells], 4):-
    separator,
    display_line(Cells, 1).
display_line([Cell1, Cell2, Cell3|Cells], Row):-
    output(Cell1, Row, Cell1Row),
    output(Cell2, Row, Cell2Row),
    output(Cell3, Row, Cell3Row),
    format('|~w|~w|~w|~n', [Cell1Row, Cell2Row, Cell3Row]),
    Row1 is Row + 1,
    display_line([Cell1, Cell2, Cell3|Cells], Row1).
%% ------------------------------------------------------------------------- %%
%% display_board/2
%% display_board(+Board, +Entanglements)
display_board(Board, Entanglements):-
    map(get_symbols, Board, [Entanglements], CellsSymbols),
    display_line([_,_,_|CellsSymbols], 4),
    !.
%% ------------------------------------------------------------------------- %%
%% display_board/3
%% display_board(+Verbose, +Board, +Entanglements)
display_board(false, _, _):-!.
display_board(_, Board, Entanglements):-
    display_board(Board, Entanglements).
%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%


%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
%% symbol_string/3
%% symbol_string(+Symbol, +Index, -String)
symbol_string(Symbol, Index, String):-
    format(string(String), "~w~d", [Symbol, Index]).
%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%


%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
%% display_message/2
%% display_message(+Verbose, +Message)
display_message(false, _):-!.
display_message(_, Message):-format("~s~n", [Message]).
%% ------------------------------------------------------------------------- %%
%% display_cycle/2
%% display_cycle(+Verbose, +Cycle)
display_cycle(false, _):-!.
display_cycle(_, [Head|Tail]):-
    format("CICLU : "),
    format_cycle([Head|Tail], Head).
%% format_cycle/2
%% format_cycle(+Cycle, +First)
format_cycle([], entanglement(Symbol,Index,_,_)):-
    !,
    symbol_string(Symbol, Index, String),
    format("~s~n", [String]).
format_cycle([entanglement(Symbol,Index,_,_)|Next], First):-
    symbol_string(Symbol, Index, String),
    format("~s -- ", [String]),
    format_cycle(Next, First).
%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
