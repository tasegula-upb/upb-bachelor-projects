%% HUMAN_PLAYER.PL
%% Tudor Berariu, 2013

:-ensure_loaded('utils.pl').

%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
%% Fișierul conține definițiile necesare unui jucător de QTTT care primește
%% mutările și măsurătorile de la stdin.
%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%


%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
%% stdin_move/4
%% stdin_move(+Symbol, +Board, +Entanglements, -Move)
stdin_move(Symbol, Board, _, [VarX, VarY]):-
    indices1(Board, FreePositions, Template, var(Template)),
    (
        % Dacă există o singură poziție liberă
        FreePositions = [PosX],
        FreePositions = [PosY],
        !
    ;
        % altfel
	repeat,
	get_digit("Prima poziție", FreePositions, PosX),
	delete(FreePositions, PosX, FreePositions2),
        get_digit("A doua poziție", FreePositions2, PosY),
	format(string(Message),
	        "Mutarea ta este ~w în pozițiile (~d,~d).",
                [Symbol, PosX, PosY]),
	are_you_sure(Message),
        !
    ),
    nth1(PosX, Board, VarX),
    nth1(PosY, Board, VarY).
%% ------------------------------------------------------------------------- %%
%% stdin_measure/5
%% stdin_measure(+MySymbol, +Board, +Cycle, +OtherEntanglements,
%%         -[Index, Position])
stdin_measure(_, Board, Cycle, _, [Index, Position]):-
    map(index_of, Cycle, Indices),
    repeat,
    get_digit("Simbolul măsurat", Indices, Index),
    member(entanglement(Symbol, Index, Pos1, Pos2), Cycle),
    equiv_nth1(P1, Board, Pos1),
    equiv_nth1(P2, Board, Pos2),
    get_digit("Poziția finală a simbolului", [P1, P2], FinalCell),
    (
        FinalCell == P1, Position = Pos1
    ;
        FinalCell == P2, Position = Pos2
    ),
    format(string(Message), "~w~d în poziția ~d.", [Symbol, Index, FinalCell]),
    are_you_sure(Message),
    !.
%% ------------------------------------------------------------------------- %%
%% human_player/2
%% humam_player(-MovePredicate, -MeasurePredicate)
human_player(stdin_move, stdin_measure).
%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%


%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
%% get_digit/3
%% get_digit(+Message, +Digits, -Digit)
get_digit(Message, Digits, Digit):-
    repeat,
    format("~s (din ~w) : ",[Message, Digits]),
    get_char(C),
    get_char(_),
    char_code(C,Code),
    char_code('0',Zero),
    Digit is Code - Zero,
    member(Digit, Digits),
    !.
%% ------------------------------------------------------------------------- %%
%% are_you_sure/1
%% are_you_sure(+String)
are_you_sure(String):-
    repeat,
    format("~s Ești convins? (d/n) ", [String]),
    get_char(C),
    get_char(_),
    (
        C == 'd', !
    ;
        C == 'D', !
    ;
        C == 'n', !, fail
    ;
        C == 'N', !, fail
    ).
%% ------------------------------------------------------------------------- %%
%% index_of/2
%% index_of(+Entanglement, -Index)
index_of(entanglement(_,Index,_,_), Index).
%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
