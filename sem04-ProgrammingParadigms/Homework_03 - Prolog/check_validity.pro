%% CHECK_VALIDITY.PL
%% Tudor Berariu, 2013

:-ensure_loaded('utils.pl').

%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
%% Fișierul conține definițiile predicatelor ce verifică validitatea
%% mutărilor și măsurătorilor pentru o anumită stare a jocului.
%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%


%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
%% valid_move/2
%% valid_move(+Board, +[Pos1, Pos2])
%% valid_move verifică dacă perechea de poziții Pos1, Pos2 reprezintă o
%% mutare validă pe tabla curentă Board
valid_move(Board, [PosX, PosY]):-
    filter(Position, Board, var(Position), FreePositions),
    FreePositions == [PosX],
    FreePositions == [PosY],
    !.
valid_move(Board, [PosX, PosY]):-
    var(PosX),
    var(PosY),
    PosX \== PosY,
    equiv_member(PosX, Board), % PosX este dintre variabilele de pe tablă
    equiv_member(PosY, Board). % PosY este dintre variabilele de pe tablă
%% ------------------------------------------------------------------------- %%
%% valid_measure/2
%% valid_measure(+Cycle, +[Index, Position]).
%% valid_measure verifică dacă perechea [Index, Position] reprezintă o
%% măsurătoare validă a unui simbol din ciclul de legări Cycle
valid_measure(Cycle, [Index, Position]):-
    check_member(entanglement(_, Index, Position, _),
            Cycle, entanglement(_, Ia, Pos1, _),
            entanglement(_, Ib, Pos2a, Pos2b),
            (Ia == Ib, (Pos1 == Pos2a, ! ; Pos1 == Pos2b))).
%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%