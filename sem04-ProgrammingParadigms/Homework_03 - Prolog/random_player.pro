%% RANDOM_PLAYER.PL
%% Tudor Berariu, 2013

:-ensure_loaded('utils.pl').

%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
%% Fișierul conține definițiile necesare unui jucător de QTTT care mută
%% aleator.
%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%


%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
%% random_move/4
%% random_move(+Symbol, +Board, +Entanglements, -Move)
random_move(_, Board, _, [PosX, PosY]):-
    filter(Position, Board, var(Position), FreePositions),
    (
        FreePositions = [PosX], % Dacă există o singură poziție liberă
        FreePositions = [PosY],
        !
    ;
        random_element(FreePositions, PosX),
        repeat, % Număr infinit de puncte de întoarcere
        random_element(FreePositions, PosY),
        PosX \== PosY, % Se generează PosY până este diferit de PosX
        ! % Renunță la punctele de întoarcere create de retry/0
    ).
%% ------------------------------------------------------------------------- %%
%% random_measure/5
%% random_measure(+MySymbol, +Board, +Cycle, +OtherEntanglements,
%%         -[Index, Position])
random_measure(_, _, Cycle, _, [Index, Pos]):-
    random_element(Cycle, entanglement(_, Index, Pos1, Pos2)),
    random_element([Pos1, Pos2], Pos).
%% ------------------------------------------------------------------------- %%
%% random_player/2
%% random_player(-MovePredicate, -MeasurePredicate)
random_player(random_move, random_measure).
%% ------------------------------------------------------------------------- %%
%% ------------------------------------------------------------------------- %%
