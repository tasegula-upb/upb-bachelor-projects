Tudorica Constantin-Alexandru, 323CA
	Am creat un alt template numit new_world care mai contine si directia 
de miscare a bilei in acea stare.
	Directiile pot fi N, S, E, W, NE, NW, SE, SW si sunt definite prin cate
un fact fiecare.
	Fac match pe fiecare stare si pe firecare directie si vad daca pot
extinde starea in acea directie. Daca pot creez o noua stare. Cand ajung intr-o
stare de castig creez fact-ul win si apoi regulile de cleanup se ocupa sa curete
starile tinute minte.
	Am 2 functii cu care ma misc. Una care se misca peste un jucator (de pe
un spatiu gol sau alt jucator) numita jump si alta care se misca de pe un 
jucator pe spatiu liber, numita move.
	Sta foarte mult pe testul 28 si pe testul 33 pana cand gaseste o cale
spre destinatie.
	Timp de rulare: 9 minute pe un Celeron la 1.2Ghz single core
			4 minute pe un i7-2630m la 2.1Ghz