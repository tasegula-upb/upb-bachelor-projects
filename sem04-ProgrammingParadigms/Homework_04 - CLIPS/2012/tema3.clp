;(defrule moveUp
; 	(world 
;	 (limit ?h ?w)
;	 (ball ?x ?y)
; 	 (men $?men)
;	 (id ?id)
;	 (moves $?m)
;	)
;	=>
;		(printout t ?id crlf)
;)
(defrule cleanup
 	(declare (salience -100))
 	?p<-(world 
	 (limit ?h ?w)
	 (ball ?x ?y)
 	 (men $?men)
	 (id ?id)
	 (moves $?m)
	)

	=>
	(retract ?p)
)