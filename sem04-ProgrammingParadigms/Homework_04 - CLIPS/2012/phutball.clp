;(set-strategy breadth)
;La mine nu merge daca setam strategia breadth. Oricum nu cred ca improve-uie
;prea mult.

;Genesis

;Brave New World
(deftemplate new_world
	(multislot limit) ; max size (linii, coloane)
	(multislot ball) ; minge
	(multislot men) ; pozitiile una dupa alta, x y -
	(slot id) ; identificator pt lume
	(multislot moves) ; lista mutarilor, initial vida
	(slot dir)
)

;We are lost without directions
(deffacts directions
	(dir +1 0 S)
	(dir +1 -1 SW)
	(dir +1 +1 SE)
	(dir -1 0 N)
	(dir 0 -1 W)
	(dir 0 +1 E)
	(dir -1 -1 NW)
	(dir -1 +1 NE)

)

; And so it all began..
(defrule create_new_world
 	(declare (salience 100))
 	(world
	 (limit ?h ?w)
	 (ball ?x ?y)
 	 (men $?me)
	 (id ?id)
	 (moves $?m)
	)
	=>
	(assert
	(new_world
	 (limit ?h ?w)
	 (ball ?x ?y)
 	 (men $?me)
	 (id ?id)
	 (moves ?x ?y -)
	 (dir None)
	))
)

; We are the champions my friends...
(defrule win
	(declare (salience 1000)) ;Am cam exagerat cu salience dar e bine sa fie
	(new_world
		(limit ?h ?w)
		(ball ?x ?y)
		(moves $?m)
		(id ?id)
	)
	(test (= (- ?h 1) ?x))
	=>
	(assert (win (id ?id) (moves $?m)))
)

; Movement part

;The following rule jumps on a player
(defrule jump
	(dir ?dx ?dy ?dn)
	(new_world
	 (limit ?h ?w)
	 (ball ?x ?y)
 	 (men $?s ;Matchuim locatia omului
	      ?mx &:(= (+ ?x ?dx) ?mx) 
	      ?my &:(= (+ ?y ?dy) ?my) 
	      - 
	      $?e)
	 (id ?id)
	 (moves $?m)
	 (dir ?dir&None|?dn) ;Daca directia stari este None (adica e pe gol si poate lua orice directie sau este egala cu cea a driectiei
	)

	=>
	(assert
	(new_world
	 (limit ?h ?w)
	 (ball (+ ?x ?dx) (+ ?y ?dy))
	 (men $?s $?e)
	 (id ?id)
	 (moves $?m)
	 (dir ?dn)
	)
	)
)
;The following rule jumps from above a player to a empty space
(defrule move
	(dir ?dx ?dy ?dn)
	(new_world
	 (limit ?h ?w)
	 (ball ?x ?y)
 	 (men $?men)
	 (id ?id)
	 (moves $?m)
	 (dir ?dn) ;Directia trebuie sa fie aceeasi
	)

	; Verificam sa nu existe un alt om acolo
	; nu am gasit o metoda mai scurta de a face asta
	; Ar trebui sa se matchuie pe acelasi fapt avand in vedere ca matchui
	; cam totul. Daca s-ar matchui pe alt fact ar insemna ca are aceleasi
	; mutari, aceeasi pozitie a bilei si acelasi id ceea ce e imposibil.

	(not (new_world
	 (limit ?h ?w)
	 (ball ?x ?y)
 	 (men  $?s 
 	 	   ?mx&:(= (+ ?x ?dx) ?mx) 
 	 	   ?my&:(= (+ ?y ?dy) ?my) 
 	 	   - 
 	 	   $?e)
	 (id ?id)
	 (moves $?m)
	 (dir ?dn)
	))
	=>
	(assert
	(new_world
	 (limit ?h ?w)
	 (ball (+ ?x ?dx) (+ ?y ?dy))
	 (men $?men)
	 (id ?id)
	 (moves $?m (+ ?x ?dx) (+ ?y ?dy) - ) ;Adaugam mutarea
	 (dir None)
	)
	)
)


;Someone call the janitor

(defrule cleanup_world
 	(declare (salience 99)) ;L-am pus ca sa stearga world imediat dupa ce
 	?p<-(world )		;facem new_world
	=>
	(retract ?p)
)
(defrule cleanup_win			;Sterge dupa ce castigam, toate starile 
	(declare (salience 1001))	;de new_world aferente id-ului
	(win (id ?id))
	?p <- (new_world (id ?id))
	=>
	(retract ?p)
)
(defrule cleanup_new			;Sterge starile de new world (in caz ca
	(declare (salience -100))	;nu castigam)
	?p <-(new_world)
	=>
	(retract ?p)
)

;'Stop and ask for directions', I told him. But no, 'It's inter-dimensional', he 
;says. 'What can go wrong?'
(defrule cleanup_directions
	(declare (salience -100))
	?p <-(dir $?)
	=>
	(retract ?p)
)