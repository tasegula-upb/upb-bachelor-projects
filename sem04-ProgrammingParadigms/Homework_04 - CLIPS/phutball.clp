(deftemplate test_world
	(multislot direction) ; directia pe care ne vom misca
	(multislot limit) ; max size (orizontala, verticala)
	(multislot ball) ; minge
	(multislot men) ; pozitiile una dupa alta, x y -
	(slot id) ; identificator pt lume
	(multislot moves) ; lista mutarilor, initial vida
)

(deftemplate bonus_world
	(multislot direction) 
	(multislot limit) (multislot ball) (multislot men) (slot id) (multislot moves) 
	(multislot extra) ; retine noul jucator
	(multislot ballish) ; retine o noua minge; avem solutie atunci cand ballish = ball
)

; posibilele directii
(deffacts directions
	(go N 1 0)
	(go NE 1 1)
	(go E 0 1)
	(go SE -1 1)
	(go S -1 0)
	(go SW -1 -1)
	(go W 0 -1)
	(go NW 1 -1)
)
	
; creaza lumea de test si de bonus
(defrule create_world
	(declare (salience 101))
	?w <- (world 
		(limit ?a ?b)
		(ball ?x ?y)
		(men $?m)
		(id ?id)
		(moves)
	)
	=>
	(retract ?w)
	(assert 
		(test_world
			(direction NEVERLAND 0 0)
			(limit ?a ?b)
			(ball ?x ?y)
			(men $?m)
			(id ?id)
			(moves ?x ?y -)
		)
	)
	(assert 
		(bonus_world
			(direction NEVERLAND 0 0)
			(limit ?a ?b)
			(ball ?x ?y)
			(men $?m)
			(id ?id)
			(extra 0 0)
			(ballish 0 0)
		)
	)
)

; tratam doar cazul de test

; alegem o directie si vedem daca exista jucator acolo
; caz in care "sarim" pe el
(defrule watch_around
	(declare (salience 50))
	(go ?where ?dx ?dy)
	(test_world
		(direction NEVERLAND 0 0)
		(limit ?a ?b)
		(ball ?x ?y)
		(men 
			$?left 
			?mx &: (eq (+ ?x ?dx) ?mx) 
			?my &: (eq (+ ?y ?dy) ?my) - 
			$?right
		)
		(id ?id)
		(moves $?m)
	)
	=>
	(assert
		(test_world
			(direction ?where ?dx ?dy)
			(limit ?a ?b)
			(ball ?mx ?my)
			(men $?left $?right)
			(id ?id)
			(moves $?m)
		)
	)
)

; suntem pe un jucator si continuam sa sarim pe un altul
; in aceeasi directie
(defrule jump_on_player
	(declare (salience 60))
	?nw <- (test_world
		(direction 
			?where &: (neq ?where NEVERLAND)
			?dx ?dy
		)
		(limit ?a ?b)
		(ball ?x ?y)
		(men 
			$?left 
			?mx &: (eq (+ ?x ?dx) ?mx) 
			?my &: (eq (+ ?y ?dy) ?my) - 
			$?right
		)
		(id ?id)
		(moves $?m)
	)
	=>
	(retract ?nw)
	(assert 
		(test_world
			(direction ?where ?dx ?dy)
			(limit ?a ?b)
			(ball (+ ?x ?dx) (+ ?y ?dy))
			(men $?left $?right)
			(id ?id)
			(moves $?m)
		)
	)
)

; suntem pe un jucator, numai avem altii in directia de mers,
; astfel ca sarim pe spatiu liber si memoram locatia in moves
(defrule jump_from_player
	(declare (salience 70))
	(and
		(not (test_world
			(direction 
				?where &: (neq ?where NEVERLAND) 
				?dx ?dy
			)
			(limit ?a ?b)
			(ball ?x ?y)
			(men 
				$?left 
				?mx &: (eq (+ ?x ?dx) ?mx) 
				?my &: (eq (+ ?y ?dy) ?my) - 
				$?right
			)
			(id ?id)
			(moves $?m)
		))
		?nw <- (test_world
			(direction 
				?where &: (neq ?where NEVERLAND)
				?dx ?dy
			)
			(limit ?a ?b)
			(ball ?x ?y)
			(men $?left1 $?right1)
			(id ?id)
			(moves $?m)
		)
	)
	=>
	(retract ?nw)
	(assert
		(test_world
			(direction NEVERLAND 0 0)
			(limit ?a ?b)
			(ball (+ ?x ?dx) (+ ?y ?dy))
			(men $?left1 $?right1)
			(id ?id)
			(moves $?m (+ ?x ?dx) (+ ?y ?dy) -)
		)
	)
)

; verifica daca s-a ajuns la castig (mingea e pe ultimul rand)
(defrule did_win
	(declare (salience 99))
	?nw <- (test_world
		(direction NEVERLAND 0 0)
		(limit ?a ?b)
		(ball ?x &: (= ?x (- ?a 1))	?y)
		(men $?m)
		(id ?id)
		(moves $?n)
	)
	=>
	(retract ?nw)
	(assert (win (id ?id) (moves ?n)))
)

; daca s-a ajuns la castig, trebuie sa stergem faptele folosite
(defrule clean_winning_worlds
	(declare (salience 100))
	(win (id ?id) (moves $?n))
	?nw <- (test_world
			(direction $?)
			(limit $?)
			(ball $?)
			(men $?)
			(id ?id)
			(moves $?)
		)
	=>
	(retract ?nw)
)

; stergem si faptul de bonus deoarece numai avem nevoie de el
; (ajungandu-se la castig, nu e nevoie sa adaugam jucatori)
(defrule clean_winning_bonus
	(declare (salience 100))
;	(win (id ?id) (moves $?n))
	?bw <- (bonus_world
			(direction $?)
			(limit $?)
			(ball $?)
			(men $?)
			(id ?id)
			(moves $?)
			(extra $?)
			(ballish $?)
		)
	=>
	(retract ?bw)
)

; stergem si restul faptelor de tip "test_world"
(defrule clean_world
	(declare (salience 0))
	?nw <- (test_world
			(direction ?where ?dx ?dy)
			(limit $?)
			(ball $?)
			(men $?)
			(id ?)
			(moves $?)
		)
	=>
	(retract ?nw)
)

; stergem si faptele de directie
(defrule clean_go
	(declare (salience 0))
	?f <- (go $?)
	=>
	(retract ?f)
)
