module Regexp where

import Parser
import Data.Char (isLetter)
import Debug.Trace

-- definim tipul de date necesar
data RegExp x 
        = Operand x 
        | Letter x
        | Star [RegExp x] 
        | Plus [RegExp x] 
        | Ques [RegExp x]
        | Pipe [RegExp x] [RegExp x]
        | Group [RegExp x]
        deriving Show

{-
-- Construiesc cate un parser pentru fiecare caz simplu: 
-- cand un cuantificator este aplicat unei singure litere,
-- respectiv cand avem o litera fara cuantificator alaturat.
-}
letter = (spot isLetter) `transform` (\x -> Letter [x])
star = ((letter >*> token '*') `transform` f)
    where 
        f (x,_) = (Star [x])
plus = ((letter >*> token '+') `transform` f)
    where
        f (x,_) = (Plus [x])
ques = ((letter >*> token '?') `transform` f)
    where
        f (x,_) = (Ques [x])

{- 
-- Contruiesc parseri pentru cazurile cu paranteze (grupuri de litere si operatori)
-}
-- strange continutul dintre paranteze sub constructorul Group
group = (token '(' >*> parse >*> token ')') `transform` f
    where
        f (_, (x, _)) = Group x
        
-- parser pentru operatorii respectivi aplicati unei grupari cu paranteze	
starGroup  = (group >*> (token '*')) `transform` f
    where
        f (x,q) = Star [x]
plusGroup  = (group >*> (token '+')) `transform` f
    where
        f (x,q) = Plus [x]
quesGroup  = (group >*> (token '?')) `transform` f
    where
        f (x,q) = Ques [x]

-- prima constructie din sir
fstCons = letter `alt`
    star `alt`
    plus `alt`
    ques `alt`
    group `alt`
    starGroup `alt`
    plusGroup `alt`
    quesGroup `alt`
    none
    
parse = (maxList fstCons) 

{-
-- Grupeaza expresia obtinuta in functie de operatori.
-}
getGroup :: [RegExp x] -> [RegExp x]
getGroup [] = []
getGroup ((Letter x) : lefts) = Letter x : getGroup lefts
getGroup ((Group x) : lefts) = (Group (makeList x)) : getGroup lefts
getGroup ((Star x) : lefts) = (Star (makeList x)) : getGroup lefts
getGroup ((Plus x) : lefts) = (Plus (makeList x)) : getGroup lefts
getGroup ((Ques x) : lefts) = (Ques (makeList x)) : getGroup lefts

makeList :: [RegExp x] -> [RegExp x]
makeList [] = []
makeList list  
    | length group > 0 = 
        [Group group] ++ (makeList ((drop (length group) list)))
    | otherwise = []
        where
            group = getGroup list

{-
-- Primeste un caracter (litera) si in functie de operatorul alaturat
-- parcurge sirul pana cand numai are loc potrivire
-}
match2 :: RegExp [Char] -> Parser Char [Char]
match2 (Letter [x]) = spot (isToken x) `transform` f
    where
        isToken x e
            | x == e = True
            | otherwise = False
        f x = [x]
match2 (Star [x]) = (maxList (match2 x)) `transform` (foldl (++) [])
match2 (Plus [x]) = (maxNeList (match2 x)) `transform` (foldl (++) [])
match2 (Ques [x]) = (success []) `alt` (match2 x)
match2 (Group x) = match1 x

{-
-- Porneste de la inceputul sirului si incerca sa obtina o potrivire
-}
match1 :: [RegExp [Char]] -> Parser Char [Char]
match1 [] = success ""
match1 ((Group x):lefts) = ((match1 x) >*> (match1 lefts)) `transform` (uncurry (++))	
match1 (x:lefts) = ((match2 x) >*> (match1 lefts)) `transform` (uncurry (++))

{-
-- Obtine toate potrivirile pentru regex-ul primit 
-- (avand deja si reprezentarea sa in functie de operanzi)
-}
match :: [RegExp [Char]] -> [Char] -> [[Char]]
match regexp "" 
    | length p > 0 = [""]
    | otherwise = []
        where 
            p = match1 regexp ""
match regexp expr 
    | length parse == 0 = match regexp (tail expr)
    | length (fst p) == 0 = [""] ++ (match regexp (tail expr))
    | otherwise = [fst p] ++ (match regexp (snd p))
    where
        parse = match1 regexp expr
        p = last parse
        
matches expression input = if ('{' `elem` input) 
                            then [] 
                            else  match (parseAll expression) input
                                where
                                    parseAll expression = makeList (fst $ head $ parse expression) 
