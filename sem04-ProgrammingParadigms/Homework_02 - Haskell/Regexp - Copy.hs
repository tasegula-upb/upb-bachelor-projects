module Regexp where

import Parser
import Data.Char (isLetter)
import Debug.Trace

-- definim tipul de date necesar
data RegExp x 
		= Operand x 
		| Letter x
		| Star [RegExp x] 
		| Plus [RegExp x] 
		| Ques [RegExp x]
		| Pipe [RegExp x] [RegExp x]
		| Group [RegExp x]
		deriving Show

{-
-- Construiesc cate un parser pentru fiecare caz simplu: 
-- cand un cuantificator este aplicat unei singure litere,
-- respectiv cand avem o litera fara cuantificator alaturat.
-}
letter = (spot isLetter) `transform` (\x -> Letter [x])
star = ((letter >*> token '*') `transform` f)
	where 
		f (x,_) = (Star [x])
plus = ((letter >*> token '+') `transform` f)
	where
		f (x,_) = (Plus [x])
ques = ((letter >*> token '?') `transform` f)
	where
		f (x,_) = (Ques [x])

{- 
-- Contruiesc parseri pentru cazurile cu paranteze (grupuri de litere si operatori)
-}
-- strange continutul dintre paranteze sub constructorul Group
group = (token '(' >*> parse >*> token ')') `transform` f
	where
		f (_, (x, _)) = Group x
		
-- parser pentru operatorii respectivi aplicati unei grupari cu paranteze	
starGroup  = (group >*> (token '*')) `transform` f
	where
		f (x,q) = Star [x]
plusGroup  = (group >*> (token '+')) `transform` f
	where
		f (x,q) = Plus [x]
quesGroup  = (group >*> (token '?')) `transform` f
	where
		f (x,q) = Ques [x]

-- prima constructie din sir
fstCons = letter `alt`
	star `alt`
	plus `alt`
	ques `alt`
	group `alt`
	starGroup `alt`
	plusGroup `alt`
	quesGroup `alt`
	none
	
parse = (maxList fstCons) 

{-
-- Grupeaza expresia obtinuta in functie de operatori.
-}
getGroup :: [RegExp x] -> [RegExp x] -> [RegExp x]
getGroup [] acc = reverse acc
getGroup ((Letter x):letfs) acc = getGroup letfs ((Letter x):acc)
getGroup ((Group x):letfs) acc = getGroup letfs ((Group (makeList x)):acc)
getGroup ((Star x):letfs) acc = getGroup letfs ((Star (makeList x)):acc)
getGroup ((Plus x):letfs) acc = getGroup letfs ((Plus (makeList x)):acc)
getGroup ((Ques x):letfs) acc = getGroup letfs ((Ques (makeList x)):acc)

makeList :: [RegExp x] -> [RegExp x]
makeList [] = []
makeList list  
	| length group > 0 = 
		[Group group] ++ (makeList ((drop (length group) list)))
	| otherwise = []
		where
			group = getGroup list []

{-
-- Primeste un caracter (litera) si in functie de operatorul alaturat
-- parcurge sirul pana cand numai are loc potrivire
-}
m :: RegExp [Char] -> Parser Char [Char]
m (Letter [x]) = spot (isToken x) `transform` f
	where
		isToken x e
			| x == e = True
			| otherwise = False
		f x = [x]
m (Star [x]) = (maxList (m x)) `transform` (foldl (++) [])
m (Plus [x]) = (maxNeList (m x)) `transform` (foldl (++) [])
m (Ques [x]) = (success []) `alt` (m x)
m (Group x) = match0 x

{-
-- Porneste de la inceputul sirului si incerca sa obtina o potrivire
-}
match0 [] = success ""
match0 ((Group x):lefts) = ((match0 x) >*> (match0 lefts)) `transform` (uncurry (++))	
match0 (x:lefts)  = ((m x) >*> (match0 lefts)) `transform` (uncurry (++))

{-
-- Obtine toate potrivirile pentru regex-ul primit (avand deja si reprezentarea sa in functie de operanzi)
-}
match1 regex "" 
	| length p > 0 = [""]
	| otherwise = []
		where 
			p = match0 regex ""
match1 regex expr 
	| length parse == 0 = match1 regex (tail expr)
	| length (fst p) == 0 = [""] ++ (match1 regex (tail expr))
	| otherwise = [fst p] ++ (match1 regex (snd p))
	where
		parse = match0 regex expr
		p = last parse 

{-
--
-}
--match2 regex "" = [""]
--match2 regex expr
--	| length (fst p) == 0 = [""] ++ (match2 regex (tail expr))
--	| otherwise = [fst p] ++ (match2 regex (snd p))
--	where
--		parse = match0 regex expr
--		p = last parse 

{-
--
-}
match [Group [Ques x]] input = match1 [Group [Ques x]] input
match [Group [Star x]] input = match1 [Group [Star x]] input
match regex input = match1 regex input

matches expression input = if ('{' `elem` input) 
							then [] 
							else  match (parseAll expression) input
								where
									parseAll expression = makeList p
										where
											p = fst $ head $ parse expression
